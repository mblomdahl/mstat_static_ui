# -*- coding: utf-8 -*-
#
# Title:   Package 'mstat_static_ui'
# Author:  Mats Blomdahl
# Version: 2013-12-16

"""mstat_static_ui module

GAE module containing the static front-end UI.
"""
