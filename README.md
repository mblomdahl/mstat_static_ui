# _mstat\_static\_ui_ – batteries excluded

The _mstat\_static\_ui_ component wraps all static components of the UI in a GAE Python Module. For production use, the
module needs to be deployed into `/mstat_static_ui` and uploaded to App Engine.

## 1. Prerequisites

Software:

* [Google App Engine Python SDK 1.9.2](https://developers.google.com/appengine/downloads)
* [Python 2.7.6](http://python.org/download/)
* [Sencha Cmd 4.0](http://www.sencha.com/products/sencha-cmd/download)
* [Compass 0.12](http://compass-style.org/install/)
* [mstat 1.06](https://bitbucket.org/mblomdahl-ondemand/mstat)[^private]
* [mstat_front-end_api 1.06](https://bitbucket.org/mblomdahl/mstat_front-end_api)

[^private]: Currently *not* publicly accessible.


## 2. Configuration

Locate `mstat` dev root and run

    $ git clone git@bitbucket.org:mblomdahl/mstat_static_ui.git


### 2.1. Module YAML Config

Add a `mstat_static_ui.yaml` configuration to the `mstat` root and include the UI component's handlers/builtins config:

```
#!yaml
includes:
- mstat_static_ui/handlers.yaml
- mstat_static_ui/builtins.yaml
```

We'd also recommend excluding anything that doesn't add value to the user experience, e.g.

```
#!yaml
skip_files:

# Application Logic
- ^(.*/)?ecore/.*
- ^(.*/)?mstat_default/.*
- ^(.*/)?mstat_generic_handler/.*
- ^(.*/)?mstat_constants/.*
- ^(.*/)?mstat_front-end_api/.*
- ^(.*/)?mstat_worker/.*
- ^(.*/)?mstat_parser/.*
- ^(.*/)?mstat_tce_userspace_stats/.*
- ^(.*/)?mstat_model/.*

# Temp/Cache Files
- ^(.*/)?mstat_static_ui/workspace/build/testing/AdminInterface/\.sass\-cache/.*
- ^(.*/)?mstat_static_ui/workspace/build/production/AdminInterface/\.sass\-cache/.*
- ^(.*/)?mstat_static_ui/workspace/build/testing/SubscriberInterface/\.sass\-cache/.*
- ^(.*/)?mstat_static_ui/workspace/build/production/SubscriberInterface/\.sass\-cache/.*
- ^(.*/)?mstat_static_ui/workspace/build/temp/.*
- ^(.*/)?mstat_static_ui/workspace/ext/packages/.*
- ^(.*/)?mstat_static_ui/workspace/packages/ext\-default/build/\.sass\-cache/.*
- ^(.*/)?mstat_static_ui/architect/.*

# Misc Resources
- ^(.*/)?templates/.*
- ^(.*/)?samples/.*

# Configuration & Backup Files
- ^(.*/)?app\.yaml
- ^(.*/)?app\.yml
- ^(.*/)?index\.yaml
- ^(.*/)?index\.yml
- ^(.*/)?#.*#
- ^(.*/)?.*\.scss
- ^(.*/)?.*\.cfg
- ^(.*/)?.*\.xml
- ^(.*/)?.*\.mdg
- ^(.*/)?.*\.rb
- ^(.*/)?.*~
- ^(.*/)?.*\.py[co]
- ^(.*/)?.*/RCS/.*
- ^(.*/)?\..*
- ^(.*/)?.*\.bak$
```


### 2.2. Dispatch YAML Config

Add the following route to the `dispatch.yaml` configuration in the `mstat` root:

```
#!yaml
dispatch:
  # Route all invocations within the `mstat_static_ui` production path to the ``static-ui`` module.
- url: "*/ui/*"
  module: static-ui

  # Route all invocations within the `mstat_static_ui` development path to the ``static-ui`` module.
- url: "*/dev/*"
  module: static-ui
```


## 3. Components [draft]

_To be continued..._


### 3.1. The _Subscriber UI_ Front-End [draft]

_To be continued..._


#### 3.1.1. Development Environment [draft]

Root Directory: `/workspace/subscriber_ui_ext`

1. Run the SDK `dev_appserver.py` for target module(s):

        $ cd ~/my_dev_root/mstat
        $ dev_appserver.py dispatch.yaml mstat_static_ui.yaml mstat_front-end_api.yaml <yaml_file [yaml_file ...]>

2. Redirect web browser to: `//localhost:8080/dev/workspace/subscriber/`


#### 3.1.2. Build [draft]

Test build:

    $ cd ~/my_dev_root/mstat/mstat_static_ui/workspace
    $ ./build_st.sh

Test path: `//my_domain.td/dev/test/subscriber/`


Production build:

    $ cd ~/my_dev_root/mstat/mstat_static_ui/workspace
    $ ./build_sp.sh

Production path: `//my_domain.td/ui/subscriber/`


### 3.2. The _Admin UI_ Front-End [draft]

_To be continued..._


#### 3.2.1. Development Environment [draft]

Root Directory: `/workspace/admin_ui_ext`

1. Run the SDK `dev_appserver.py` for target module(s):

        $ cd ~/my_dev_root/mstat
        $ dev_appserver.py dispatch.yaml mstat_static_ui.yaml mstat_front-end_api.yaml <yaml_file [yaml_file ...]>

2. Redirect web browser to: `//localhost:8080/dev/workspace/admin/`


#### 3.2.2. Build [draft]

Test build:

    $ cd ~/my_dev_root/mstat/mstat_static_ui/workspace
    $ ./build_at.sh

Test path: `//my_domain.td/dev/test/admin/`


Production build:

    $ cd ~/my_dev_root/mstat/mstat_static_ui/workspace
    $ ./build_ap.sh

Production path: `//my_domain.td/ui/admin/`


### 3.3. Sencha _common_ module [draft]

_To be continued..._


#### 3.3.1. Development Environment [draft]

Root Directory: `/workspace/packages/common`

_To be continued..._


#### 3.3.2. Build [placeholder]

_To be continued..._


### 3.4. The Statistics Widget [draft]

_To be continued..._


#### 3.4.1. Development Environment [draft]

Root Directory: `/widget`

_To be continued..._


#### 3.4.2. Build [placeholder]

_To be continued..._


## 4. Deployment [draft]

The front-end UI is deployed just like any other App Engine module, e.g.

    $ appcfg.py --oauth2 update mstat_static_ui.yaml


## 5. Code Style [draft]

For project code style, refer to the
[Google JavaScript Style Guide](http://google-styleguide.googlecode.com/svn/trunk/javascriptguide.xml).

Comments are written in [JSDoc](http://usejsdoc.org/)-style, though we do our best to minimize use of tags explicitly
discouraged by the Google style guide.

Line-width: 120 ch.

Encoding: UTF-8

Line separator: Always use LF (\n), never CRLF

Indentation: Always use 4-space indents, never tabs (\t)


## 6. Known Issues [draft]

Many, but here's a few:

* The _Admin UI_ and _Subscriber UI_ SPAs are both bloated with functionality that could be refactored/generalized into
  the `common` module.
* User credentials are submitted in plain-text (though using TLS), should be hashed using SHA-256 on both the browser
  and the backend Ext Direct endpoint.
* _Admin UI_ grid components sometimes breaks when using the `GridView` column selector.
* The _Subscriber UI_ field spec should update dynamically on `selectionchange` events dispatched by the
  `SubscriptionsGridPanel` component.


## 7. Changelog

### Version 1.06

Changes:

* Implemented the `delete_subscription` Ext Direct endpoint within the _Admin UI_ front-end.
* Minor field spec update for the _Subscriber UI_.


### Version 1.05 [draft]

Changes:

* Disabled _News_ tabpanel in the _Subscriber UI_ SPA.
* Updated the _Subscriber UI_ Data API field specification.


### Version 1.04 [draft]

Changes:

* Added _System Performance_ tabpanel to the _Subscriber UI_ SPA.


## 8. Contact
For general inquiries, security issues and feature requests, contact lead developer 
[Mats Blomdahl](mailto:mats.blomdahl@gmail.com).

