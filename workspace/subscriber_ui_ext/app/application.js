Ext.Loader.setConfig({
    disableCaching: false,
    enabled: true
});

Ext.ns("Ext.app.REMOTING_API");

Ext.app.REMOTING_API = {
    "descriptor": "Ext.app.REMOTING_API",
    "url"       : "/user/api/ext_direct",
    "type"      : "remoting",
    "namespace": "Remote",
    "id": "UserCmd",
    "actions": {
        "UserCmd": [
            {"name": "about_me", "len": 1},
            {"name": "activate_safe_email_update", "len": 1},
            {"name": "activate_safe_password_update", "len": 1},
            {"name": "admin_login", "len": 1},
            {"name": "alt_query_subscription", "len": 1},
            {"name": "chart_data", "len": 1},
            {"name": "format_api_call", "len": 1},
            {"name": "grid_data", "len": 1},
            {"name": "daily_stats", "len": 1},
            {"name": "logout", "len": 1},
            {"name": "query_subscription", "len": 1},
            {"name": "register_safe_email_update", "len": 1},
            {"name": "register_safe_password_update", "len": 1},
            {"name": "send_email_notification", "len": 1},
            {"name": "submit_api_call", "len": 1},
            {"name": "subscriber_login", "len": 1},
            {"name": "verify_safe_password_update_key", "len": 1}
        ]
    }
};

Ext.require('Ext.data.proxy.Direct');
Ext.require('Ext.tab.Panel');
Ext.require('Ext.form.Label');
Ext.require('Ext.layout.container.Anchor');
Ext.require('Ext.grid.RowNumberer');
Ext.require('Ext.grid.column.Number');
Ext.require('Ext.grid.column.Date');
Ext.require('Ext.grid.column.Boolean');
Ext.require('Ext.form.Panel');
Ext.require('Ext.form.FieldSet');
Ext.require('Ext.form.RadioGroup');
Ext.require('Ext.form.field.Radio');
Ext.require('Ext.direct.*');
Ext.syncRequire('Ext.direct.RemotingProvider');
Ext.syncRequire('Ext.direct.Manager', function() {
    Ext.direct.Manager.addProvider(Ext.app.REMOTING_API);
});

Ext.define('SubscriberInterface.Application', {
    name: 'SubscriberInterface',

    extend: 'Ext.app.Application',

    globals: {

    },

    session: {
        subscriberAccountRecord: null,
        userAccountRecord: null
    },

    loggedIn: false,

    debug: false || window.location.pathname.search("/dev/workspace/") != -1,

    controllers: [
        'SubscriberInterface.controller.SubscriptionsPanelCtrl',

        'SubscriberInterface.controller.SubscriptionsSelectionContainerCtrl',
        'SubscriberInterface.controller.SubscriptionsGridPanelCtrl',
        'SubscriberInterface.controller.SubscriptionDetailsFormPanelCtrl',

        'SubscriberInterface.controller.SubscriptionsApiUsageDocsContainerCtrl',

        'SubscriberInterface.controller.SubscriptionsApiExplorerContainerCtrl',

        'SubscriberInterface.controller.SubscriptionsFieldDescContainerCtrl',

        'SubscriberInterface.controller.SubscriberSetPasswordWindowCtrl',
        'SubscriberInterface.controller.SubscriberLoginWindowCtrl',

        'SubscriberInterface.controller.ViewportCtrl'
    ],

    models: [
    ],

    stores: [
    ],

    views: [
    ],

    launch: function() {
        if (this.debug) {
            console.log('Application.launch');
        }

        if (!this.loggedIn) {
            if (window.location.hash.length == 25) {
                this.fireEvent('exSetPassword', window.location.hash.split('#')[1]);
            } else {
                this.fireEvent('exLogin');
            }
        }
    }

});
