/*
 * File: app/view/grid/SubscriptionsGridPanel.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('SubscriberInterface.view.grid.SubscriptionsGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.subscriptionsgridpanel',
    //exLoadInitialized: (function() { console.error('xtype: subscriptionsgridpanel / ' + (new Date()).valueOf());})(),

    requires: [
        'Common.view.panel.GridPanelRefreshTool'
    ],

    margin: '0 0 11 0',
    maxHeight: 220,
    minHeight: 150,
    minWidth: 420,
    frameHeader: false,
    columnLines: true,
    ui: 'light',
    style: 'border: 1px solid black',
    forceFit: true,
    store: 'SubscriberInterface.store.SubscriptionsStore',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'rownumberer',
                    hidden: true
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 110,
                    dataIndex: 'datastore_key_id',
                    text: 'datastore_key_id'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 135,
                    dataIndex: 'datastore_key_urlsafe',
                    text: 'datastore_key_urlsafe'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 160,
                    dataIndex: 'datastore_ancestor_key_id',
                    text: 'datastore_ancestor_key_id'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 185,
                    dataIndex: 'datastore_ancestor_key_urlsafe',
                    text: 'datastore_ancestor_key_urlsafe'
                },
                {
                    xtype: 'gridcolumn',
                    width: 110,
                    dataIndex: 'subscription_type',
                    text: 'subscription_type'
                },
                {
                    xtype: 'gridcolumn',
                    width: 110,
                    dataIndex: 'friendly_name',
                    text: 'subscription_title'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 80,
                    dataIndex: 'description',
                    text: 'description'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'content_restrictions',
                    text: 'content_restrictions'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    dataIndex: 'api_access_key',
                    text: 'api_access_key'
                },
                {
                    xtype: 'numbercolumn',
                    hidden: true,
                    width: 55,
                    dataIndex: 'quota',
                    text: 'quota',
                    format: '000'
                },
                {
                    xtype: 'numbercolumn',
                    hidden: true,
                    width: 115,
                    dataIndex: 'deleted',
                    text: 'update_frequency',
                    format: '000'
                },
                {
                    xtype: 'datecolumn',
                    width: 75,
                    dataIndex: 'start_date',
                    text: 'start_date',
                    format: 'Y-m-d'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 75,
                    dataIndex: 'deleted_datetime',
                    text: 'end_date',
                    format: 'Y-m-d'
                },
                {
                    xtype: 'datecolumn',
                    width: 125,
                    dataIndex: 'registered_datetime',
                    text: 'registered_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'suspended_datetime',
                    text: 'suspended_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'deleted_datetime',
                    text: 'deleted_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'booleancolumn',
                    hidden: true,
                    width: 80,
                    dataIndex: 'suspended',
                    text: 'suspended'
                },
                {
                    xtype: 'booleancolumn',
                    hidden: true,
                    width: 65,
                    dataIndex: 'deleted',
                    text: 'deleted'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 150,
                    dataIndex: 'sys_origin_user_account',
                    text: 'sys_origin_user_account'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 190,
                    dataIndex: 'sys_origin_organization_account',
                    text: 'sys_origin_organization_account'
                },
                {
                    xtype: 'numbercolumn',
                    hidden: true,
                    width: 140,
                    dataIndex: 'sys_changelog_version',
                    text: 'sys_changelog_version',
                    format: '000'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    dataIndex: 'sys_changelog',
                    text: 'sys_changelog'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_modified',
                    text: 'sys_modified',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_created',
                    text: 'sys_created',
                    format: 'Y-m-d H:i:s'
                }
            ]/*,
            tools: [
                {
                    type: 'help',
                    disabled: true,
                    callback: function() {
                        // show help here
                    }
                },
                {
                    xtype: 'gridpanelrefreshtool',
                    disabled: true,
                    hidden: true
                }
            ]*/
        });

        me.callParent(arguments);
    }

});
