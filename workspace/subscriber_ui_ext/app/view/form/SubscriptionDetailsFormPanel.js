/*
 * File: app/view/form/SubscriptionDetailsFormPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('SubscriberInterface.view.form.SubscriptionDetailsFormPanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.subscriptiondetailsformpanel',
    //exLoadInitialized: (function() { console.error('xtype: subscriptiondetailsformpanel / ' + (new Date()).valueOf());})(),

    requires: [
    ],

    disabled: false,
    id: 'subscriptionDetailsFormPanel',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'fieldset',
                    padding: '4 0 2 10',
                    title: 'Subscription Details',
                    items: [

                        {
                            xtype: 'displayfield',
                            anchor: '100%',
                            margin: '0 10 0 0',
                            fieldLabel: 'Subscription Title',
                            labelPad: 0,
                            labelWidth: 150,
                            name: 'friendly_name'
                        },
                        {
                            xtype: 'displayfield',
                            anchor: '100%',
                            margin: '0 10 0 0',
                            fieldLabel: 'Short Description',
                            labelPad: 0,
                            labelWidth: 150,
                            name: 'description'
                        },
                        {
                            xtype: 'displayfield',
                            anchor: '100%',
                            margin: '0 10 0 0',
                            fieldLabel: 'API Access Key',
                            labelPad: 0,
                            labelWidth: 150,
                            name: 'api_access_key'
                        },
                        {
                            xtype: 'displayfield',
                            anchor: '100%',
                            hidden: true,
                            margin: '0 10 0 0',
                            fieldLabel: 'API Quota Restrictions',
                            labelPad: 0,
                            labelWidth: 150,
                            name: 'quota'
                        },
                        {
                            xtype: 'displayfield',
                            anchor: '100%',
                            margin: '0 10 0 0',
                            fieldLabel: 'Data Update Frequency',
                            labelPad: 0,
                            labelWidth: 150,
                            name: 'update_frequency'
                        },
                        {
                            xtype: 'displayfield',
                            anchor: '100%',
                            margin: '0 10 0 0',
                            fieldLabel: 'Accessible Date Range',
                            labelPad: 0,
                            labelWidth: 150,
                            name: 'date_interval'
                        },
                        {
                            xtype: 'displayfield',
                            anchor: '100%',
                            margin: '0 10 10 0',
                            fieldLabel: 'Accessible Fields',
                            labelPad: 0,
                            labelWidth: 150,
                            name: 'fields'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
