/*
 * File: app/view/view/NewsRecordsDataView.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('SubscriberInterface.view.view.NewsRecordsDataView', {
    extend: 'Ext.view.View',
    alias: 'widget.newsrecordsdataview',

    store: 'SubscriberInterface.store.NewsStore',
    itemSelector: 'article.news-record-list-item',
    emptyText: 'News records unavailable',
    loadingText: 'Loading Updates...',

    initComponent: function() {
        var me = this;

        var listItemTpl = new Ext.XTemplate(
            '<tpl for=".">',
                '<article class="news-record-list-item">',
                    '<div>',
                        '<hgroup>',
                            '<h2>{title}</h2>',
                            '<h3>',
                                'Published: <time>{published_datetime:date("Y-m-d")}</time>',
                                '<span class="pipe-separator"></span>',
                                'Author: <a href="mailto:{author_email}">{author_full_name}</a>',
                            '</h3>',
                        '</hgroup>',
                        '<div class="news-record-list-item-summary">',
                            '{summary}',
                        '</div>',
                        '<aside class="news-record-list-item-body">',
                            '{body}',
                        '</aside>',
                    '</div>',
                '</article>',
            '</tpl>'
        );

        Ext.applyIf(me, {
            tpl: listItemTpl,
            loadingHeight: window.innerHeight // TODO(mats.blomdahl@gmail.com): Fix hack.
        });

        me.callParent(arguments);
    }

});
