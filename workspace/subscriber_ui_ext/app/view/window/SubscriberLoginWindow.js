/*
 * File: app/view/SubscriberLoginWindow.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('SubscriberInterface.view.window.SubscriberLoginWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.subscriberloginwindow',

    requires: [
        'Common.view.button.GenericActionButton',
        'Common.view.button.GenericCancelButton',
        'Common.view.button.GenericNextButton'
    ],
    //exLoadInitialized: (function() { console.error('xtype: subscriberloginwindow / ' + (new Date()).valueOf());})(),
    frame: true,
    height: 171,
    padding: 0,
    width: 380,
    layout: {
        align: 'stretch',
        type: 'vbox'
    },
    bodyPadding: 10,
    title: 'Enter Login Credentials',
    modal: true,

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    flex: 1,
                    id: 'subscriberLoginFormPanel',
                    padding: 0,
                    bodyPadding: '2 0 0 0',
                    title: '',
                    items: [
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            id: 'subscriberLoginUserEmailTextField',
                            margin: '0 0 10 0',
                            fieldLabel: 'Username (email)*',
                            labelWidth: 150,
                            name: 'user_email',
                            allowBlank: false,
                            enforceMaxLength: true,
                            maxLength: 50,
                            vtype: 'email'
                        },
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            id: 'subscriberLoginPasswordTextField',
                            margin: '0 0 10 0',
                            fieldLabel: 'Password*',
                            labelWidth: 150,
                            name: 'password',
                            inputType: 'password',
                            allowBlank: false,
                            enforceMaxLength: true,
                            maskRe: /\w/,
                            maxLength: 18,
                            regex: /^\w{6,18}$/,
                            regexText: 'Ogiltigt värde. Ett lösenord är minst 6 tecken långt och får ej innehålla skiljetecken.'
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'panel',
                    flex: 1,
                    dock: 'bottom',
                    frame: true,
                    margin: 0,
                    layout: {
                        align: 'stretch',
                        pack: 'end',
                        type: 'hbox'
                    },
                    bodyPadding: 0,
                    items: [
                        {
                            xtype: 'genericactionbutton',
                            height: 10,
                            margin: 10,
                            minWidth: 125,
                            text: 'Forgot Your Password?',
                            flex: 1
                        },
                        {
                            xtype: 'genericcancelbutton',
                            margin: '10 0 10 0'
                        },
                        {
                            xtype: 'genericnextbutton',
                            minWidth: 100,
                            text: 'Login',
                            cls: 'highlightms'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
