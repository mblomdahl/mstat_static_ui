/*
 * File: app/view/SubscriberSetPasswordWindow.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('SubscriberInterface.view.window.SubscriberSetPasswordWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.subscribersetpasswordwindow',

    requires: [
        'Common.view.button.GenericCancelButton',
        'Common.view.button.GenericNextButton'
    ],
    //exLoadInitialized: (function() { console.error('xtype: subscribersetpasswordwindow / ' + (new Date()).valueOf());})(),
    frame: true,
    height: 200,
    padding: 0,
    width: 380,
    layout: {
        align: 'stretch',
        type: 'vbox'
    },
    bodyPadding: 10,
    title: 'Submit Login Password',
    modal: true,

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    flex: 1,
                    id: 'subscriberSetPasswordFormPanel',
                    padding: 0,
                    bodyPadding: '2 0 0 0',
                    title: '',
                    items: [
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            disabled: true,
                            id: 'subscriberSetPasswordEmailTextField',
                            margin: '0 0 10 0',
                            fieldLabel: 'Username (email)*',
                            labelWidth: 150,
                            name: 'user_email',
                            allowBlank: false,
                            enforceMaxLength: true,
                            maxLength: 50,
                            vtype: 'email'
                        },
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            id: 'subscriberSetPasswordTextField',
                            margin: '0 0 10 0',
                            fieldLabel: 'Select Password*',
                            labelWidth: 150,
                            name: 'password',
                            inputType: 'password',
                            enforceMaxLength: true,
                            maskRe: /\w/,
                            maxLength: 18,
                            regex: /^\w{6,18}$/,
                            regexText: 'Ogiltigt värde. Ett lösenord är minst 6 tecken långt och får ej innehålla skiljetecken.'
                        },
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            id: 'subscriberSetPasswordCfrmTextField',
                            margin: '0 0 10 0',
                            fieldLabel: 'Confirm Password*',
                            labelWidth: 150,
                            name: 'password_cfrm',
                            inputType: 'password',
                            enforceMaxLength: true,
                            maskRe: /\w/,
                            maxLength: 18,
                            regex: /^\w{6,18}$/,
                            regexText: 'Ogiltigt värde. Ett lösenord är minst 6 tecken långt och får ej innehålla skiljetecken.'
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'panel',
                    flex: 1,
                    dock: 'bottom',
                    frame: true,
                    margin: 0,
                    layout: {
                        align: 'stretch',
                        pack: 'end',
                        type: 'hbox'
                    },
                    bodyPadding: 0,
                    items: [
                        {
                            xtype: 'genericcancelbutton',
                            margin: '10 0 10 0'
                        },
                        {
                            xtype: 'genericnextbutton',
                            minWidth: 100,
                            text: 'Submit Password'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
