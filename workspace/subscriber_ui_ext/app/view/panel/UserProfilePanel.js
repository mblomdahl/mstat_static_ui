/*
 * File: app/view/panel/UserProfilePanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('SubscriberInterface.view.panel.UserProfilePanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.userprofilepanel',
    //exLoadInitialized: (function() { console.error('xtype: userprofilepanel / ' + (new Date()).valueOf());})(),

    requires: [
        'Common.view.button.GenericToolbarLogoutButton',
        'Common.view.button.GenericToolbarLoginButton'
    ],

    autoScroll: true,
    layout: {
        type: 'anchor'
    },
    title: 'User Profile',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    items: [
                        '->',
                        {
                            xtype: 'generictoolbarloginbutton',
                            text: 'Login'
                        },
                        {
                            xtype: 'generictoolbarlogoutbutton',
                            text: 'Logout'
                        }

                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
