/*
 * File: app/view/panel/SubscriptionsPanel.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('SubscriberInterface.view.panel.SubscriptionsPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.subscriptionspanel',
    //exLoadInitialized: (function() { console.error('xtype: subscriptionspanel / ' + (new Date()).valueOf());})(),

    requires: [
        'SubscriberInterface.view.container.SubscriptionsSelectionContainer',
        'SubscriberInterface.view.container.SubscriptionsApiUsageDocsContainer',
        'SubscriberInterface.view.container.SubscriptionsFieldDescContainer',
        'SubscriberInterface.view.container.SubscriptionsApiExplorerContainer'
    ],

    autoScroll: true,
    layout: {
        type: 'anchor'
    },
    title: 'Available Subscriptions',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'subscriptionsselectioncontainer'
                },
                {
                    xtype: 'subscriptionsapiusagedocscontainer'
                },
                {
                    xtype: 'subscriptionsapiexplorercontainer'
                },
                {
                    xtype: 'subscriptionsfielddesccontainer'
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    items: [
                        {
                            xtype: 'button',
                            id: 'subscriptionsScrollToSelectionButton',
                            text: 'Subscriptions'
                        },
                        {
                            xtype: 'button',
                            id: 'subscriptionsScrollToApiUsageDocsButton',
                            text: 'Usage and Best Practices'
                        },
                        {
                            xtype: 'button',
                            id: 'subscriptionsScrollToApiExplorerButton',
                            text: 'API Explorer'
                        },
                        {
                            xtype: 'button',
                            id: 'subscriptionsScrollToFieldDescButton',
                            text: 'Schema/Fields'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
