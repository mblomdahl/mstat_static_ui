/*
 * File: app/view/panel/NewsPanel.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('SubscriberInterface.view.panel.NewsPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.newspanel',
    //exLoadInitialized: (function() { console.error('xtype: activitylogpanel / ' + (new Date()).valueOf());})(),

    requires: [
        'SubscriberInterface.view.view.NewsRecordsDataView'
    ],

    autoScroll: true,
    layout: {
        type: 'anchor'
    },
    title: 'News',


    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    margin: 20,
                    items: [
                        {
                            html: '<h1>News / Updates</h1>'
                        },
                        {
                            xtype: 'newsrecordsdataview'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
