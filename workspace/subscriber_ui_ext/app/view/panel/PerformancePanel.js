/*
 * File: app/view/panel/PerformancePanel.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('SubscriberInterface.view.panel.PerformancePanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.performancepanel',
    //exLoadInitialized: (function() { console.error('xtype: activitylogpanel / ' + (new Date()).valueOf());})(),

    requires: [
        'SubscriberInterface.view.container.PerformanceSystemInfoContainer',
        'SubscriberInterface.view.container.PerformanceChartsContainer'
    ],

    autoScroll: true,
    layout: {
        type: 'anchor'
    },
    title: 'System Performance',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'performancesysteminfocontainer'
                },
                {
                    xtype: 'performancechartscontainer'
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    items: [
                        {
                            xtype: 'button',
                            id: 'subscriptionsScrollToSelectionButton2',
                            text: 'System Detail'
                        },
                        {
                            xtype: 'button',
                            id: 'subscriptionsScrollToApiUsageDocsButton2',
                            text: 'Data Import Monitor'/*,
                            disabled: true*/
                        }/*,
                        {
                            xtype: 'button',
                            id: 'subscriptionsScrollToApiExplorerButton2',
                            text: 'Capitex Emails Parsed',
                            disabled: true
                        }*/
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
