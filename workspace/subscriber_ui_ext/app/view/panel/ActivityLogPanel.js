/*
 * File: app/view/panel/ActivityLogPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('SubscriberInterface.view.panel.ActivityLogPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.activitylogpanel',
    //exLoadInitialized: (function() { console.error('xtype: activitylogpanel / ' + (new Date()).valueOf());})(),

    requires: [
        'Common.view.button.GenericToolbarLogoutButton',
        'Common.view.button.GenericToolbarLoginButton'
    ],

    autoScroll: true,
    layout: {
        type: 'anchor'
    },
    title: 'Activity Log',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    items: [
                        '->',
                        {
                            xtype: 'generictoolbarloginbutton',
                            text: 'Login'
                        },
                        {
                            xtype: 'generictoolbarlogoutbutton',
                            text: 'Logout'
                        }

                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
