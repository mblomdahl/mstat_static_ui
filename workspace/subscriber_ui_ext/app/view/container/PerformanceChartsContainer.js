/*
 * File: app/view/container/PerformanceChartsContainer.js
 * Author: Mats Blomdahl
 * Version: 1.05
 */

Ext.define('SubscriberInterface.view.container.PerformanceChartsContainer', {
    extend: 'Ext.container.Container',
    alias: 'widget.performancechartscontainer',
    //exLoadInitialized: (function() { console.error('xtype: performancechartscontainer / ' + (new Date()).valueOf());})(),

    requires: [
        'SubscriberInterface.view.chart.RawDataRecordsCreatedLineChart',
        'SubscriberInterface.view.chart.TceRecordsCreatedLineChart',
        //'SubscriberInterface.view.chart.RawDataRecordsModifiedLineChart',
        'SubscriberInterface.view.chart.CapitexEmailsProcessedLineChart'
    ],

    id: 'performanceChartsContainer',
    disabled: false,
    margin: '20 20 40 20',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    html: '<h1>Data Import Monitor</h1>'/*,
                    disabled: true*/
                },
                /*{
                    html: [
                        '<p>Under den här rubriken avser vi publicera material som ska bidra med en fingervisning om hur vår system-lösning presterar med avseende på data-importen. Detta hoppas vi även ska bidra till kortare ledtider i anslutning till de oundvikliga driftstörningar som då och då inträffar.</p>',
                        '<p>Mäklarstatistik läser in rådata till ett halvdussin tabeller – en per ursprung/importväg – och exponerar därefter data gentemot dig som API-konsument baserat på',
                        '    <ol>',
                        '        <li>vilka regler som definierats för ditt/dina data-abonnemang och</li>',
                        '        <li>en intern rangordning mellan olika datakällor för dubbletthantering.</li>',
                        '    </ol>',
                        '</p>',
                        '<p>Bortsett från större omställningar (t.ex. adoption av en fundamentalt ny datakälla) så bör graferna nedan var någotsånär regelbundna på månads- och årsbasis. Se captions nedan för exempel på vilka särfall som kan betraktas som alarmerande för resp. utdrag.</p>',
                        '<p>Ifall du uppmärksammar att någon graferna nedan uppvisar ett oroväckande beteende så kan du skriva till <a href="mailto:mats@stormfors.se">mats@stormfors.se</a> för att försäkra dig om att vi har koll på ev. driftproblem.</p>'
                    ].join(' '),
                    margin: '0 0 20 0'
                },*/
                {
                    html: '<h2>Records Added</h2>'/*html: '<h2>Globally Unique Records Added by Preferred Source</h2>',
                    disabled: true*/
                },
                {
                    xtype: 'tcerecordscreatedlinechart',
                    margin: '0 0 40 0'  // margin: '0 0 20 0'
                },
                /*{
                    html: [
                        '<h3>Vad grafen illustrerar</h3>',
                        '<p>Grafen visar hur många globalt unika, d.v.s. fundamentalt nya, objekt vi får in varje dag <em><strong>efter</strong> dubbletthantering och tillgängliggörande i tabellen som Mäklarstatistiks data-API baseras på</em>. Enkelt uttryckt innehåller grafen de serier som du själv skulle kunna producera i MS Excel utifrån en mappning mellan <em>sys_raw_data_source</em>- och <em>sys_created</em>-fälten (fast mer lättillgängligt).</p>',
                        '<p>Exempel på alarmerande beteenden:',
                        '    <ul>',
                        '        <li><em>Records Ingested...</em>-grafens toppar och dalar för SF-LF saknar i motsvarighet för SF-LF ovan.</li>',
                        '        <li>Toppar för Capitex i <em>Records Ingested...</em>-grafen återspeglas inte ovan överhuvudtaget.</li>',
                        '        <li>Toppar för SFD i <em>Records Ingested...</em>-grafen avspeglas inte ovan.',
                        '    </ul>',
                        '</p>'
                    ].join(' '),
                    margin: '0 0 40 0'*//*,
                 disabled: true*//*
                },*/
                {
                    html: '<h2>Records Processed</h2>'/*,'<h2>New Records Ingested by Origin</h2>'
                    disabled: true*/
                },
                {
                    xtype: 'rawdatarecordscreatedlinechart',
                    margin: '0 0 40 0'  // margin: '0 0 20 0'
                }/*,
                {
                    html: [
                        '<h3>Vad grafen illustrerar</h3>',
                        '<p>Grafen illustrerar antal nya objekt som lästs in via resp. importväg baserat på fältet <em>objekt-ID</em>. Från var och en av källorna SF, LF och SFD bör vi observera att objekt inkommer i någon mån över en 3-dagarsperiod (undantaget större högtider, t.ex. jul/nyår). Från Capitex bör det droppa in några tusen objekt i slutet och början av varje månad.</p>',
                        '<p>Exempel på alarmerande beteenden:',
                        '    <ul>',
                        '        <li>Vi har en flatline från SFD, LF eller SF som sträcker sig mer än 3 dagar en vanlig vecka</li>',
                        '        <li>± 1 vecka från månadsskiftet inkommer mindre än 2 000 objekt från Capitex</li>',
                        '    </ul>',
                        '</p>'
                    ].join(' '),
                    margin: '0 0 40 0'*//*,
                    disabled: true*//*
                }*//*,
                {
                    html: '<h2>Records Updated (30 days)</h2>',
                    disabled: true
                },
                {
                    xtype: 'rawdatarecordsmodifiedlinechart',
                    margin: '0 0 40 0',
                    disabled: true
                },
                {
                    html: '<h1>Capitex Email Parser</h2>',
                    disabled: true
                },
                {
                    html: '<h2>Emails Processed</h2>',
                    disabled: true
                },
                {
                    xtype: 'capitexemailsprocessedlinechart',
                    margin: '0 0 40 0',
                    disabled: true
                },
                {
                    html: '<h2>Error Handling</h2>',
                    disabled: true
                },
                {
                    xtype: 'capitexemailsprocessedlinechart',
                    margin: '0 0 40 0',
                    disabled: true
                }*/
            ]
        });

        me.callParent(arguments);
    }

});
