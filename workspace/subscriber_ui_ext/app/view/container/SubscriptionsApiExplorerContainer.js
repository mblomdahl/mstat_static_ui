/*
 * File: app/view/container/SubscriptionsApiExplorerContainer.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('SubscriberInterface.view.container.SubscriptionsApiExplorerContainer', {
    extend: 'Ext.container.Container',
    alias: 'widget.subscriptionsapiexplorercontainer',
    //exLoadInitialized: (function() { console.error('xtype: subscriptionsapiexplorercontainer / ' + (new Date()).valueOf());})(),

    requires: [
        'Common.view.form.field.GenericNumberField',
        'Common.view.form.field.GenericTextField',
        'Common.view.form.field.GenericDateField',
        'Common.view.button.GenericActionButton'
    ],

    disabled: false,
    id: 'subscriptionsApiExplorerContainer',
    margin: '20 20 40 20',
    //style: 'background-color: rgba(100,100,100,0.75);',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    html: '<h1>Mäklarstatistik API Explorer</h1>'
                },
                {
                    html: '<h2>Request Parameters</h2>'
                },
                {
                    xtype: 'form',
                    id: 'subscriptionsApiExplorerFormPanel',
                    items: [
                        {
                            xtype: 'fieldset',
                            padding: '4 10 2 10',
                            title: 'Required Parameters',
                            items: [
                                {
                                    xtype: 'container',
                                    layout: {
                                        align: 'stretch',
                                        type: 'hbox'
                                    },
                                    items: [
                                        {
                                            xtype: 'genericnumberfield',
                                            width: 340,
                                            fieldLabel: '<i>apiVersion </i>',
                                            labelWidth: 150,
                                            name: 'api_version',
                                            allowBlank: false
                                        },
                                        {
                                            xtype: 'label',
                                            flex: 1,
                                            padding: '4 0 0 10',
                                            text: 'The API version; aimed at maintaining future compatibility. The only valid/deployed version is currently ´1´.'
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: {
                                        align: 'stretch',
                                        type: 'hbox'
                                    },
                                    items: [
                                        {
                                            xtype: 'generictextfield',
                                            margin: '0 0 10 0',
                                            width: 340,
                                            fieldLabel: '<i>apiAccessKey </i>',
                                            labelPad: 0,
                                            labelWidth: 150,
                                            name: 'api_access_key',
                                            //readOnly: true,
                                            allowBlank: false,
                                            emptyText: '1'
                                        },
                                        {
                                            xtype: 'label',
                                            flex: 1,
                                            padding: '4 0 0 10',
                                            text: 'Your individual API Access Key for the target subscription (24 digits, case-sensitive).'
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: {
                                        align: 'stretch',
                                        type: 'hbox'
                                    },
                                    items: [
                                        {
                                            xtype: 'genericdatefield',
                                            margin: '0 0 10 0',
                                            width: 340,
                                            fieldLabel: '<i>startDate</i> ',
                                            labelPad: 0,
                                            labelSeparator: ' ',
                                            labelWidth: 150,
                                            name: 'start_date',
                                            allowBlank: false,
                                            emptyText: 'YYYY-MM-DD'
                                        },
                                        {
                                            xtype: 'label',
                                            flex: 1,
                                            padding: '4 0 0 10',
                                            text: 'Lower contract date limit for API call (inclusive/open interval, ISO-format)'
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: {
                                        align: 'stretch',
                                        type: 'hbox'
                                    },
                                    items: [
                                        {
                                            xtype: 'genericdatefield',
                                            margin: '0 0 10 0',
                                            width: 340,
                                            fieldLabel: '<i>endDate </i>',
                                            labelPad: 0,
                                            labelSeparator: ' ',
                                            labelWidth: 150,
                                            name: 'end_date',
                                            //readOnly: true,
                                            allowBlank: false,
                                            emptyText: '(auto startDate + 1)'
                                        },
                                        {
                                            xtype: 'label',
                                            flex: 1,
                                            padding: '4 0 0 10',
                                            text: 'Upper contract date limit for API call (closed interval, ISO-format). E.g. to retrieve transactions signed 2013-02-23 you would submit a query for the interval 2012-02-23 <= contract_date < 2012-02-24.'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            margin: '1 0 0 0',
                            padding: '4 0 2 10',
                            title: 'Optional Parameters',
                            items: [
                                {
                                    xtype: 'radiogroup',
                                    anchor: '100%',
                                    margin: '0 0 10 0',
                                    fieldLabel: '<i>outputFormat </i>',
                                    labelPad: 0,
                                    labelSeparator: ' ',
                                    labelWidth: 135,
                                    columns: 1,
                                    vertical: true,
                                    items: [
                                        {
                                            xtype: 'radiofield',
                                            padding: '0 5 0 0',
                                            name: 'output_format',
                                            boxLabel: 'JSON',
                                            inputValue: 'json'
                                        },
                                        {
                                            xtype: 'radiofield',
                                            name: 'output_format',
                                            boxLabel: 'CSV (default, UTF-8, comma-separated)',
                                            checked: true,
                                            inputValue: 'csv'
                                        }
                                    ]
                                },
                                {
                                    xtype: 'radiogroup',
                                    anchor: '100%',
                                    margin: '0 0 10 0',
                                    fieldLabel: '<i>namingConvention </i>',
                                    labelPad: 0,
                                    labelSeparator: ' ',
                                    labelWidth: 135,
                                    columns: 1,
                                    vertical: true,
                                    items: [
                                        {
                                            xtype: 'radiofield',
                                            padding: '0 5 0 0',
                                            labelPad: 0,
                                            name: 'naming_convention',
                                            boxLabel: 'Default (e.g. <i>contract_date, transaction_id, contract_price, apartment_number, ...</i>)',
                                            checked: true,
                                            inputValue: 'default'
                                        },
                                        {
                                            xtype: 'radiofield',
                                            labelPad: 0,
                                            name: 'naming_convention',
                                            boxLabel: 'Legacy (e.g. <i>Kontraktsdatum, ObjektId, Pris, LghNr, ...</i>)',
                                            inputValue: 'legacy'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                align: 'stretch',
                                pack: 'end',
                                type: 'hbox'
                            },
                            items: [
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    id: 'subscriptionsApiExplorerGenerateSampleRequestButton',
                                    margin: '18 18 36 0',
                                    width: 175,
                                    text: 'Generate Sample Request'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    id: 'subscriptionsApiExplorerViewSampleResponseButton',
                                    margin: '18 18 36 0',
                                    width: 175,
                                    text: 'View Sample Data'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    id: 'subscriptionsApiExplorerDownloadDataFileButton',
                                    margin: '18 0 36 0',
                                    width: 175,
                                    text: 'Download Data File'
                                }
                            ]
                        }
                    ]
                },
                {
                    html: '<h2>Sample API Call</h2>'
                },
                {
                    xtype: 'container',
                    height: 300,
                    layout: {
                        align: 'stretch',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'textareafield',
                            flex: 1,
                            id: 'subscriptionsApiExplorerSampleRequestTextArea',
                            margin: '0 5 10 0',
                            fieldLabel: '<h3>HTTP Request</h3>',
                            labelAlign: 'top',
                            labelSeparator: null,
                            labelPad: 0,
                            labelWidth: 146
                        },
                        {
                            xtype: 'textareafield',
                            flex: 1,
                            id: 'subscriptionsApiExplorerSampleResponseTextArea',
                            margin: '0 0 10 4',
                            fieldLabel: '<h3>HTTP Response</h3>',
                            labelAlign: 'top',
                            labelPad: 0,
                            labelSeparator: null,
                            labelWidth: 146
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
