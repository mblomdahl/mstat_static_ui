/*
 * File: app/view/container/SubscriptionsSelectionContainer.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('SubscriberInterface.view.container.SubscriptionsSelectionContainer', {
    extend: 'Ext.container.Container',
    alias: 'widget.subscriptionsselectioncontainer',
    //exLoadInitialized: (function() { console.error('xtype: subscriptionsselectioncontainer / ' + (new Date()).valueOf());})(),

    requires: [
        'SubscriberInterface.view.grid.SubscriptionsGridPanel',
        'SubscriberInterface.view.form.SubscriptionDetailsFormPanel'
    ],

    id: 'subscriptionsSelectionContainer',
    disabled: true,
    margin: '20 20 40 20',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    html: '<h1>Available Subscriptions</h1>'
                },
                {
                    xtype: 'subscriptionsgridpanel'
                },
                {
                    xtype: 'subscriptiondetailsformpanel'
                }
            ]
        });

        me.callParent(arguments);
    }

});
