/*
 * File: app/view/container/SubscriptionsApiUsageDocsContainer.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('SubscriberInterface.view.container.SubscriptionsApiUsageDocsContainer', {
    extend: 'Ext.container.Container',
    alias: 'widget.subscriptionsapiusagedocscontainer',
    //exLoadInitialized: (function() { console.error('xtype: subscriptionsapiusagedocscontainer / ' + (new Date()).valueOf());})(),

    requires: [
    ],

    id: 'subscriptionsApiUsageDocsContainer',
    disabled: true,
    margin: '20 20 40 20',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    html: '<h1>API Usage and Best Practices</h1>'
                },
                {
                    html: [
                        '<p>Under development.</p>',
                        '<p>Until next release; feel free to contact Mats Blomdahl (<a href="mailto:mats@stormfors.se">mats@stormfors.se</a>, <a href="tel:+46730567567">+46 730 567 567</a>) with any and all integration issues.</p>'
                    ].join(' ')
                }
            ]
        });

        me.callParent(arguments);
    }

});
