/*
 * File: app/view/container/ViewportLoginLogoutContainer.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('SubscriberInterface.view.container.ViewportLoginLogoutContainer', {
    extend: 'Ext.container.Container',
    alias: 'widget.viewportloginlogoutcontainer',
    //exLoadInitialized: (function() { console.error('xtype: viewportloginlogoutcontainer / ' + (new Date()).valueOf());})(),

    requires: [
        'Common.view.button.GenericToolbarLogoutButton',
        'Common.view.button.GenericToolbarLoginButton'
    ],

    id: 'viewportLoginLogoutContainer',
    //constrain: true,
    floating: true,
    shadow: false,
    autoShow: true,
    width: 125,

    layout: {
        type: 'hbox',
        pack: 'end'
    },

    initComponent: function() {
        var me = this;

        function setCmpOffset(container, width) {
            width = width || Ext.getBody().getWidth();
            container.setPosition((width < 720 ? 720 : width) - 8 - container.width, 5);
        }

        Ext.applyIf(me, {
            //renderTo: x.el,
            listeners: {
                beforerender: {
                    fn: function(container, eOpts) {
                        setCmpOffset(container);
                        Ext.EventManager.onWindowResize(
                            function(width, height) {
                                setCmpOffset(container, width);
                            }, me, false
                        );
                    },
                    scope: me,
                    single: true
                }
            },
            items: [
                {
                    xtype: 'generictoolbarloginbutton',
                    disabled: true
                },
                {
                    xtype: 'generictoolbarlogoutbutton',
                    disabled: true
                }
            ]
        });

        me.callParent(arguments);
    }

});
