/*
 * File: app/view/container/PerformanceSystemInfoContainer.js
 * Author: Mats Blomdahl
 * Version: 1.06
 */

Ext.define('SubscriberInterface.view.container.PerformanceSystemInfoContainer', {
    extend: 'Ext.container.Container',
    alias: 'widget.performancesysteminfocontainer',
    //exLoadInitialized: (function() { console.error('xtype: performancesysteminfocontainer / ' + (new Date()).valueOf());})(),

    requires: [
    ],

    id: 'performanceSystemInfoContainer',
    disabled: false,
    margin: '20 20 40 20',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    html: '<h1>System Detail</h1>'
                },
                {

                    xtype: 'fieldset',
                    title: 'Details',
                    items: [
                        {
                            xtype: 'displayfield',
                            anchor: '100%',
                            fieldLabel: 'System Version',
                            labelWidth: 150,
                            name: 'sys_version',
                            value: '1.06'
                        },
                        {
                            xtype: 'displayfield',
                            anchor: '100%',
                            fieldLabel: 'Runtime',
                            labelWidth: 150,
                            name: 'sys_runtime',
                            value: 'python27 (GAE 1.9.2)'
                        },
                        {
                            xtype: 'displayfield',
                            anchor: '100%',
                            fieldLabel: 'Last Deployment',
                            labelWidth: 150,
                            name: 'last_sys_deployment',
                            value: '2014-04-28'
                        },
                        {
                            xtype: 'displayfield',
                            anchor: '100%',
                            fieldLabel: 'Upcoming Release',
                            labelWidth: 150,
                            name: 'next_sys_deployment',
                            value: 'Late May'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
