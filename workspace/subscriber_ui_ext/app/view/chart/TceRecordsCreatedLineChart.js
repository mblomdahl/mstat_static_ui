/*
 * File: app/view/chart/TceRecordsCreatedLineChart.js
 * Author: Mats Blomdahl
 * Version: 1.05
 */

Ext.define('SubscriberInterface.view.chart.TceRecordsCreatedLineChart', {
    extend: 'Ext.chart.Chart',
    alias: 'widget.tcerecordscreatedlinechart',
    //exLoadInitialized: (function() { console.error('xtype: tcerecordscreatedlinechart / ' + (new Date()).valueOf());})(),

    requires: [
        'Ext.chart.series.Area',
        'Ext.chart.axis.Numeric',
        'Ext.chart.axis.Category'
    ],

    id: 'tceRecordsCreatedLineChart',
    disabled: false,
    width: 680,
    height: 340,
    animate: true,
    store: 'SubscriberInterface.store.RawDataStatsStore',

    legend: {
        position: 'top',
        itemSpacing: 20,
        padding: 10
    },

    axes: [{
        type: 'numeric',
        fields: [
            'tce_mstat_sys_lf_created_entry_count',
            'tce_mstat_sys_sfd_created_entry_count',
            'tce_mstat_sys_sf_created_entry_count',
            'tce_mstat_sys_capitex_created_entry_count',
            'tce_mstat_sys_scb_created_entry_count'
        ],
        position: 'left',
        //title: 'New Transactions',
        /*label: {
         renderer: Ext.util.Format.numberRenderer('0,0')
         },*/
        grid: true,
        decimals: 0,
        minimum: 0
    }, {
        type: 'category',
        //title: 'Stats Date',
        position: 'bottom',
        fields: 'stats_datetime',
        label: {
            font: '10px Arial',
            rotate: {
                degrees: 330
            },
            renderer: function(v) {
                return Ext.Date.format(v, 'y-m-d');
            }
        }
    }],

    series: [{
        type: 'area',
        axis: 'left',
        xField: 'stats_datetime',
        yField: [
            'tce_mstat_sys_lf_created_entry_count',
            'tce_mstat_sys_sfd_created_entry_count',
            'tce_mstat_sys_sf_created_entry_count',
            'tce_mstat_sys_capitex_created_entry_count',
            'tce_mstat_sys_scb_created_entry_count'
        ],
        title: [
            'Source: LF API',  // \'sys_raw_data_source\' == \"LF\"',
            'Source: SFD API',  // \"SFD\"',
            'Source: SF API',  // '\"SF\"',
            'Source: Capitex Email',  // '\"Capitex\"',
            'Source: SCB'  // '\"SCB\"'
        ],
        highlight: true,
        tips: {
            width: 100,
            trackMouse: true,
            renderer: function(record, item) {
                var tipsDate = Ext.Date.format(record.get('stats_datetime'), 'Y-m-d');
                this.setTitle('' + record.get(item.storeField) + ' added<br> (' + tipsDate + ')');
            }
        },
        style: {
            lineWidth: 1,
            stroke: '#666',
            opacity: 0.86
        }/*,
        style: {
            fill: '#18428E',
            stroke: '#18428E',
            'stroke-width': 6
        },
        markerConfig: {
            //type: 'cross',
            size: 4,
            radius: 4,
            'stroke-width': 2
        }*/
    }]

});
