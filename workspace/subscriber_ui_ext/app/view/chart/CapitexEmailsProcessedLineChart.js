/*
 * File: app/view/chart/CapitexEmailsProcessedLineChart.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('SubscriberInterface.view.chart.CapitexEmailsProcessedLineChart', {
    extend: 'Ext.chart.Chart',
    alias: 'widget.capitexemailsprocessedlinechart',
    //exLoadInitialized: (function() { console.error('xtype: capitexemailsprocessedlinechart / ' + (new Date()).valueOf());})(),

    requires: [
        'Ext.chart.series.Line',
        'Ext.chart.axis.Numeric',
        'Ext.chart.axis.Time'
    ],

    //id: 'capitexEmailsProcessedLineChart',
    disabled: false,
    width: 680,
    height: 300,
    animate: true,
    store: 'SubscriberInterface.store.CapitexEmailStatsStore',

    legend: {
        position: 'top',
        itemSpacing: 20,
        padding: 10
    },

    axes: [
        {
            type: 'numeric',
            decimals: 0,
            //title: 'New Transactions',
            position: 'left',
            adjustMaximumByMajorUnit: true,
            adjustMinimumByMajorUnit: true,
            fields: [
                'parser_errors_thrown',
                'duplicates_discarded',
                'emails_processed'
            ],
            label: {
                renderer: Ext.util.Format.numberRenderer('0,0')
            },
            grid: true,
            minimum: 0
        },
        {
            type: 'time',
            //title: 'Stats Date',
            position: 'bottom',
            fields: [
                'stats_datetime'
            ],
            dateFormat: 'y-m-d',
            label: {
                font: '10px Arial',
                rotate: {
                    degrees: 315
                }
            }
        }
    ],

    series: [
        {
            type: 'line',
            highlight: {
                size: 7,
                radius: 7
            },
            axis: 'left',
            fill: true,
            xField: 'stats_datetime',
            yField: 'emails_processed',
            title: 'Emails Processed',
            tips: {
                layout: 'fit',
                width: 120,
                /*items: {
                    xtype: 'container',
                    layout: 'hbox',
                    items: [pieChart, grid]
                },*/
                trackMouse: false,
                renderer: function(record, item) {
                    var tipsDate = Ext.Date.format(record.get('stats_datetime'), 'Y-m-d');
                    this.setTitle('SFD: ' + record.get('emails_processed') + ' updated');
                }
            },
            /*style: {
                fill: '#18428E',
                stroke: '#18428E',
                'stroke-width': 6
            },*/
            markerConfig: {
                //type: 'cross',
                size: 4,
                radius: 4,
                'stroke-width': 2
            }
        },
        {
            type: 'line',
            highlight: {
                size: 7,
                radius: 7
            },
            axis: 'left',
            fill: true,
            xField: 'stats_datetime',
            yField: 'parser_errors_thrown',
            title: 'Parser Errors Thrown',
            tips: {
                layout: 'fit',
                width: 130,
                /*items: {
                 xtype: 'container',
                 layout: 'hbox',
                 items: [pieChart, grid]
                 },*/
                trackMouse: false,
                renderer: function(record, item) {
                    var tipsDate = Ext.Date.format(record.get('stats_datetime'), 'Y-m-d');
                    this.setTitle('Mspecs: ' + record.get('parser_errors_thrown') + ' updated');
                }
            },
            /*style: {
             fill: '#18428E',
             stroke: '#18428E',
             'stroke-width': 3
             },*/
            markerConfig: {
                //type: 'circle',
                size: 4,
                radius: 4,
                'stroke-width': 2
            }
        },
        {
            type: 'line',
            highlight: {
                size: 7,
                radius: 7
            },
            axis: 'left',
            fill: true,
            xField: 'stats_datetime',
            yField: 'duplicates_discarded',
            title: 'Duplicates Discarded',
            tips: {
                layout: 'fit',
                width: 140,
                /*items: {
                    xtype: 'container',
                    layout: 'hbox',
                    items: [pieChart, grid]
                },*/
                trackMouse: false,
                renderer: function(record, item) {
                    var tipsDate = Ext.Date.format(record.get('stats_datetime'), 'Y-m-d');
                    this.setTitle('Capitex: ' + record.get('duplicates_discarded') + ' updated');
                }
            },
            /*style: {
                fill: '#18428E',
                stroke: '#18428E',
                'stroke-width': 3
            },*/
            markerConfig: {
                //type: 'circle',
                size: 4,
                radius: 4,
                'stroke-width': 2
            }
        }
    ]
});
    /*initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    html: '<h1>Available Subscriptions</h1>'
                },
                {
                    xtype: 'subscriptionsgridpanel'
                },
                {
                    xtype: 'subscriptiondetailsformpanel'
                }
            ]
        });

        me.callParent(arguments);
    }

});
*/
