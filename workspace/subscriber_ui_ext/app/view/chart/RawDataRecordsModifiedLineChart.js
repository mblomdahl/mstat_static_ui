/*
 * File: app/view/chart/RawDataRecordsModifiedLineChart.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('SubscriberInterface.view.chart.RawDataRecordsModifiedLineChart', {
    extend: 'Ext.chart.Chart',
    alias: 'widget.rawdatarecordsmodifiedlinechart',
    //exLoadInitialized: (function() { console.error('xtype: rawdatarecordsmodifiedlinechart / ' + (new Date()).valueOf());})(),

    requires: [
        'Ext.chart.series.Line',
        'Ext.chart.axis.Numeric',
        'Ext.chart.axis.Time'
    ],

    //id: 'rawDataRecordsModifiedLineChart',
    disabled: false,
    width: 680,
    height: 300,
    animate: true,
    store: 'SubscriberInterface.store.RawDataStatsStore',

    legend: {
        position: 'top',
        itemSpacing: 20,
        padding: 10
    },

    axes: [
        {
            type: 'numeric',
            decimals: 0,
            //title: 'New Transactions',
            position: 'left',
            adjustMaximumByMajorUnit: true,
            adjustMinimumByMajorUnit: true,
            fields: [
                'sfd_modified',
                'capitex_modified',
                'lf_modified',
                'sf_modified'
            ],
            label: {
                renderer: Ext.util.Format.numberRenderer('0,0')
            },
            grid: true,
            minimum: 0
        },
        {
            type: 'time',
            //title: 'Stats Date',
            position: 'bottom',
            fields: [
                'stats_datetime'
            ],
            dateFormat: 'y-m-d',
            label: {
                font: '10px Arial',
                rotate: {
                    degrees: 315
                }
            }
        }
    ],

    series: [
        {
            type: 'line',
            highlight: {
                size: 7,
                radius: 7
            },
            axis: 'left',
            fill: true,
            xField: 'stats_datetime',
            yField: 'sfd_modified',
            title: 'Source: SFD API',
            tips: {
                layout: 'fit',
                width: 120,
                /*items: {
                    xtype: 'container',
                    layout: 'hbox',
                    items: [pieChart, grid]
                },*/
                trackMouse: false,
                renderer: function(record, item) {
                    var tipsDate = Ext.Date.format(record.get('stats_datetime'), 'Y-m-d');
                    this.setTitle('SFD: ' + record.get('sfd_modified') + ' updated');
                }
            },
            /*style: {
                fill: '#18428E',
                stroke: '#18428E',
                'stroke-width': 6
            },*/
            markerConfig: {
                //type: 'cross',
                size: 4,
                radius: 4,
                'stroke-width': 2
            }
        },
        {
            type: 'line',
            highlight: {
                size: 7,
                radius: 7
            },
            axis: 'left',
            fill: true,
            xField: 'stats_datetime',
            yField: 'lf_modified',
            title: 'Source: LF API',
            tips: {
                layout: 'fit',
                width: 130,
                /*items: {
                 xtype: 'container',
                 layout: 'hbox',
                 items: [pieChart, grid]
                 },*/
                trackMouse: false,
                renderer: function(record, item) {
                    var tipsDate = Ext.Date.format(record.get('stats_datetime'), 'Y-m-d');
                    this.setTitle('LF: ' + record.get('lf_modified') + ' updated');
                }
            },
            /*style: {
             fill: '#18428E',
             stroke: '#18428E',
             'stroke-width': 3
             },*/
            markerConfig: {
                //type: 'circle',
                size: 4,
                radius: 4,
                'stroke-width': 2
            }
        },
        {
            type: 'line',
            highlight: {
                size: 7,
                radius: 7
            },
            axis: 'left',
            fill: true,
            xField: 'stats_datetime',
            yField: 'sf_modified',
            title: 'Source: SF API',
            tips: {
                layout: 'fit',
                width: 130,
                /*items: {
                 xtype: 'container',
                 layout: 'hbox',
                 items: [pieChart, grid]
                 },*/
                trackMouse: false,
                renderer: function(record, item) {
                    var tipsDate = Ext.Date.format(record.get('stats_datetime'), 'Y-m-d');
                    this.setTitle('SF: ' + record.get('sf_modified') + ' updated');
                }
            },
            /*style: {
             fill: '#18428E',
             stroke: '#18428E',
             'stroke-width': 3
             },*/
            markerConfig: {
                //type: 'circle',
                size: 4,
                radius: 4,
                'stroke-width': 2
            }
        },
        {
            type: 'line',
            highlight: {
                size: 7,
                radius: 7
            },
            axis: 'left',
            fill: true,
            xField: 'stats_datetime',
            yField: 'capitex_modified',
            title: 'Source: Capitex Email',
            tips: {
                layout: 'fit',
                width: 140,
                /*items: {
                    xtype: 'container',
                    layout: 'hbox',
                    items: [pieChart, grid]
                },*/
                trackMouse: false,
                renderer: function(record, item) {
                    var tipsDate = Ext.Date.format(record.get('stats_datetime'), 'Y-m-d');
                    this.setTitle('Capitex: ' + record.get('capitex_modified') + ' updated');
                }
            },
            /*style: {
                fill: '#18428E',
                stroke: '#18428E',
                'stroke-width': 3
            },*/
            markerConfig: {
                //type: 'circle',
                size: 4,
                radius: 4,
                'stroke-width': 2
            }
        }
    ]
});
    /*initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    html: '<h1>Available Subscriptions</h1>'
                },
                {
                    xtype: 'subscriptionsgridpanel'
                },
                {
                    xtype: 'subscriptiondetailsformpanel'
                }
            ]
        });

        me.callParent(arguments);
    }

});
*/
