/*
 * File: app/view/Viewport.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('SubscriberInterface.view.Viewport', {
    extend: 'Ext.container.Viewport',
    //exLoadInitialized: (function() { console.error('xtype: viewport / ' + (new Date()).valueOf());})(),

    requires: [
        'SubscriberInterface.view.container.ViewportLoginLogoutContainer',
        'SubscriberInterface.view.panel.NewsPanel',
        'SubscriberInterface.view.panel.SubscriptionsPanel',
        'SubscriberInterface.view.panel.UserProfilePanel',
        'SubscriberInterface.view.panel.ActivityLogPanel',
        'SubscriberInterface.view.panel.PerformancePanel'
    ],

    layout: {
        type: 'fit'
    },

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'viewportloginlogoutcontainer'
                },
                {
                    xtype: 'tabpanel',
                    minWidth: 720,
                    activeTab: 1,
                    items: [
                        {
                            xtype: 'newspanel',
                            disabled: true
                        },
                        {
                            xtype: 'subscriptionspanel'
                        },
                        {
                            xtype: 'userprofilepanel',
                            disabled: true
                        },
                        {
                            xtype: 'activitylogpanel',
                            disabled: true
                        },
                        {
                            xtype: 'performancepanel'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    },

    renderTo: Ext.getBody()

});
