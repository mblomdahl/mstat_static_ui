/*
 * File: app/controller/SubscriptionsApiUsageDocsContainerCtrl.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('SubscriberInterface.controller.SubscriptionsApiUsageDocsContainerCtrl', {
    extend: 'Ext.app.Controller',
    alias: 'controller.subscriptionsapiusagedocscontainerctrl',
    //exLoadInitialized: (function() { console.error('xtype: subscriptionsfielddesccontainerctrl / ' + (new Date()).valueOf());})(),
    stores: [
    ],
    views: [
        'SubscriberInterface.view.container.SubscriptionsApiUsageDocsContainer'
    ],

    refs: [
        {
            ref: 'subscriptionsApiUsageDocsContainer',
            selector: 'subscriptionsapiusagedocscontainer',
            xtype: 'subscriptionsapiusagedocscontainer'
        }
    ],

    appState: {
    },

    init: function(application) {
        this.debug = false || application.debug;

        this.optDebug('initz: done');

        application.on({
            exLoginComplete: {
                fn: this.onExLoginComplete,
                scope: this
            },
            exLogout: {
                fn: this.onExLogout,
                scope: this
            }
        });
    },

    optDebug: function(label, data) {

        if (!this.debug) {
            if (arguments[arguments.length-1] !== 'override') {
                return;
            }
        }

        console.error(new Date() + label);
        if (arguments.length > 1) {
            for (var i = 1, j = arguments.length; i < j; i++) {
                if (arguments[i] === 'override') {
                    break;
                } else {
                    console.log(arguments[i]);
                }
            }
        }

    },

    onExLoginComplete: function(subscriberAccountRecord, userAccountRecord, eOpts) {
        this.optDebug('onExLoginComplete.arguments:', arguments);

        var apiUsageDocsContainer = this.getSubscriptionsApiUsageDocsContainer();
        apiUsageDocsContainer.setDisabled(false);
    },

    onExLogout: function(eOpts) {
        this.optDebug('onExLogout.arguments:', arguments);

        var apiUsageDocsContainer = this.getSubscriptionsApiUsageDocsContainer();
        apiUsageDocsContainer.setDisabled(true);
    }

});
