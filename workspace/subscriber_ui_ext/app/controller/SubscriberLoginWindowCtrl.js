/*
 * File: app/controller/SubscriberLoginWindowCtrl.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('SubscriberInterface.controller.SubscriberLoginWindowCtrl', {
    extend: 'Ext.app.Controller',
    alias: 'controller.subscriberloginwindowctrl',
    //exLoadInitialized: (function() { console.error('xtype: subscriberloginwindowctrl / ' + (new Date()).valueOf());})(),
    models: [
        'SubscriberInterface.model.UserAccountModel',
        'SubscriberInterface.model.SubscriberAccountModel'
    ],
    views: [
        'SubscriberInterface.view.window.SubscriberLoginWindow'
    ],

    refs: [
        {
            ref: 'subscriberLoginWindow',
            selector: 'subscriberloginwindow',
            xtype: 'subscriberloginwindow'
        },
        {
            ref: 'subscriberLoginFormPanel',
            selector: 'subscriberloginwindow form',
            xtype: 'form'
        },
        {
            ref: 'subscriberLoginUserEmailTextField',
            selector: '#subscriberLoginUserEmailTextField',
            xtype: 'textfield'
        },
        {
            ref: 'subscriberLoginPasswordTextField',
            selector: '#subscriberLoginPasswordTextField',
            xtype: 'textfield'
        },
        {
            ref: 'subscriberLoginResendButton',
            selector: 'subscriberloginwindow genericactionbutton',
            xtype: 'genericactionbutton'
        },
        {
            ref: 'subscriberLoginCancelButton',
            selector: 'subscriberloginwindow genericcancelbutton',
            xtype: 'genericcancelbutton'
        },
        {
            ref: 'subscriberLoginSubmitButton',
            selector: 'subscriberloginwindow genericnextbutton',
            xtype: 'genericnextbutton'
        }
    ],

    init: function(application) {

        this.debug = false || application.debug;

        this.control({
            '#subscriberLoginUserEmailTextField'             : {
                change    : this.onSubscriberLoginTextFieldChange,
                specialkey: this.onSubscriberLoginUserEmailTextFieldSpecialKey,
                blur      : this.onSubscriberLoginUserEmailTextFieldBlur
            },
            '#subscriberLoginPasswordTextField'             : {
                specialkey: this.onSubscriberLoginPasswordTextFieldSpecialKey,
                change    : this.onSubscriberLoginTextFieldChange
            },
            'subscriberloginwindow genericactionbutton'         : {
                click: this.onSubscriberLoginResendButtonClick
            },
            'subscriberloginwindow genericcancelbutton'          : {
                click: this.onSubscriberLoginCancelButtonClick
            },
            'subscriberloginwindow genericnextbutton'         : {
                click: this.onSubscriberLoginSubmitButtonClick
            },
            'subscriberloginwindow'         : {
                show: this.onSubscriberLoginWindowShow
            }
        });

        this.optDebug( 'initz: done' );

        application.on({
            exLogin: {
                fn: this.onExLogin,
                scope: this
            }
        });
    },

    optDebug: function(label, data) {

        if (!this.debug) {
            if (arguments[arguments.length-1] !== 'override') {
                return;
            }
        }

        console.error(new Date() + label);
        if (arguments.length > 1) {
            for (var i = 1, j = arguments.length; i < j; i++) {
                if (arguments[i] === 'override') {
                    break;
                } else {
                    console.log(arguments[i]);
                }
            }
        }

    },

    showWindow: function() {
        this.optDebug('showWindow.arguments: ', arguments);

        Ext.ClassManager.get('SubscriberInterface.view.window.SubscriberLoginWindow').create().show();
    },

    hideWindow: function(targetWindow) {
        this.optDebug('hideWindow.arguments: ', arguments);

        targetWindow = targetWindow || this.getSubscriberLoginWindow();

        targetWindow.close();

    },

    resizeWindowHeight: function(container, heightExtra, reposition) {
        this.optDebug('resizeWindowHeight.arguments: ', arguments);

        reposition = reposition || false;

        var parentWindow = container.up('window');
        if (!parentWindow.rendered) {
            this.optDebug('parentWindow.rendered:' + parentWindow.rendered);
            return;
        } else {
            this.optDebug('parentWindow:', parentWindow);
        }

        var activeContainerWrapper = container.up('container');
        if (!activeContainerWrapper.rendered) {
            this.optDebug('activeContainerWrapper.rendered:' + activeContainerWrapper.rendered);
            return;
        } else {
            this.optDebug('activeContainerWrapper:', activeContainerWrapper);
        }

        var wrapperHeightDelta = activeContainerWrapper.getHeight();
        this.optDebug('wrapperHeightDelta:', wrapperHeightDelta);

        var activeViewContent = activeContainerWrapper.query('#' + container.id + ' > container');
        this.optDebug('activeViewContent:', activeViewContent);

        for (var i = 0, j = activeViewContent.length; i < j; i++) {

            if (!activeViewContent[i].rendered) {
                this.optDebug('activeViewContent[i].rendered:' + activeViewContent[i].rendered);
                return;
            }

            wrapperHeightDelta -= activeViewContent[i].getHeight();
        }

        if (heightExtra) {
            wrapperHeightDelta -= heightExtra;
        }

        parentWindow.setHeight(parentWindow.getHeight() - wrapperHeightDelta);

        if (reposition) {

            return parentWindow.center();
            /*
            var windowPosition = parentWindow.getPosition();

            windowPosition[1] += wrapperHeightDelta / 2;
            if (windowPosition[1] < 0) {
            windowPosition[1] = 0;
            }

            parentWindow.setPosition(windowPosition[0], windowPosition[1]);
            */
        }
    },

    validateLoginForm: function() {
        this.optDebug('validateLoginForm.arguments: ', arguments);

        var loginForm = this.getSubscriberLoginFormPanel().getForm(),
            userEmailTextField = this.getSubscriberLoginUserEmailTextField(),
            passwordTextField = this.getSubscriberLoginPasswordTextField(),
            submitButton = this.getSubscriberLoginSubmitButton(),
            resendButton = this.getSubscriberLoginResendButton();

        if (userEmailTextField.isValid() && passwordTextField.isValid() && !passwordTextField.isDisabled()) {
            submitButton.setDisabled(false);
        } else {
            submitButton.setDisabled(true);

            if (userEmailTextField.isValid()) {
                resendButton.setDisabled(false);
                passwordTextField.setDisabled(false);
            } else {
                passwordTextField.setDisabled(true);
                resendButton.setDisabled(true);
            }
        }

    },

    onSubscriberLoginWindowShow: function(loginWindow) {
        var me = this;

        var passwordTextField = this.getSubscriberLoginPasswordTextField();

        passwordTextField.setDisabled(true);

        this.getSubscriberLoginSubmitButton().setDisabled(true);
        this.getSubscriberLoginResendButton().setDisabled(true);

        this.getSubscriberLoginUserEmailTextField().focus(false, 200);
    },

    onSubscriberLoginUserEmailTextFieldBlur: function(field) {
        this.optDebug('onSubscriberLoginUserEmailTextFieldBlur.arguments: ', arguments);

        var input = Ext.String.trim(typeof field === 'string' ? field : field.getValue()).toLowerCase();
        if (typeof field === 'string') {
            return input;
        }
        else {
            field.setValue(input);
        }

    },

    onSubscriberLoginTextFieldChange: function(textField, newValue, oldValue, eOpts) {
        this.optDebug('onSubscriberLoginTextFieldChange.arguments: ', arguments);

        this.validateLoginForm();

    },

    onSubscriberLoginUserEmailTextFieldSpecialKey: function(field, keyEvent, eOpts) {
        this.optDebug('onSubscriberLoginUserEmailTextFieldSpecialKey.arguments: ', arguments)

        if (keyEvent.getKey() === keyEvent.ENTER) {
            this.validateLoginForm();
            var submitButton = this.getSubscriberLoginSubmitButton();
            if (!submitButton.isDisabled()) {
                submitButton.fireEvent('click', submitButton);
            }
        }
    },

    onSubscriberLoginPasswordTextFieldSpecialKey: function(field, keyEvent, eOpts) {
        this.optDebug('onSubscriberLoginPasswordTextFieldSpecialKey.arguments: ', arguments)

        if (keyEvent.getKey() === keyEvent.ENTER) {
            this.validateLoginForm();
            var submitButton = this.getSubscriberLoginSubmitButton();
            if (!submitButton.isDisabled()) {
                submitButton.fireEvent('click', submitButton);
            }
        }
    },

    onSubscriberLoginSubmitButtonClick: function(button, clickEvent) {
        this.optDebug('onSubscriberLoginSubmitButtonClick.arguments: ', arguments);

        var loginForm = this.getSubscriberLoginFormPanel().getForm();

        if (loginForm.isValid()) {

            var me = this;
            var loginWindow = button.up('window');
            loginWindow.setLoading('Authenticating credentials...');
            button.setDisabled(false);

            function submitLoginCallback(buttonId, text, opt) {
                button.setDisabled(false);
                loginWindow.setLoading(false)
                return true;
            }

            var userAccountModel = Ext.ClassManager.get('SubscriberInterface.model.UserAccountModel');
            var subscriberAccountModel = Ext.ClassManager.get('SubscriberInterface.model.SubscriberAccountModel');

            Remote.UserCmd.subscriber_login(loginForm.getValues(), function(result, request, success) {
                me.optDebug('UserCmd.subscriber_login.result:', result);
                if (result) {
                    me.optDebug('UserCmd.subscriber_login.status_code:', result.status_code);
                }
                me.optDebug('UserCmd.subscriber_login.request:', request);
                me.optDebug('UserCmd.subscriber_login.success:', success);

                button.setDisabled(false);
                loginWindow.setLoading(false);

                if (success && !result.errors) {
                    me.application.fireEvent('exLoginSuccess', new subscriberAccountModel(result.subscriber_account), new userAccountModel(result.user_account));

                    submitLoginCallback();

                    loginWindow.close();
                } else {
                    if (result && result.errors) {
                        for (error in result.errors) {
                            Ext.Msg.show({
                                title: result.status_code + ': ' + error,
                                msg: result.errors[error],
                                buttons: Ext.Msg.OK,
                                icon: Ext.Msg.ERROR,
                                fn: submitLoginCallback
                            });
                            break;
                        }
                    } else {
                        submitLoginCallback();
                    }
                }

            });

        }

    },

    onSubscriberLoginCancelButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onSubscriberLoginCancelButtonClick.arguments: ', arguments);

        this.hideWindow(button.up('window'));

        this.application.fireEvent('exLoginCancel');
    },

    onSubscriberLoginResendButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onSubscriberLoginResendButtonClick.arguments: ', arguments);

        var userEmailTextField = this.getSubscriberLoginUserEmailTextField();

        if (userEmailTextField.validate()) {

            var me = this;
            var loginWindow = button.up('window');

            function passwordResetErrorCallback(buttonId, text, opt) {
                button.setDisabled(false);
                loginWindow.setLoading(false);
                return true;
            }

            function passwordResetSuccessCallback(buttonId, text, opt) {
                loginWindow.setLoading(false);
                return true;
            }

            loginWindow.setLoading('Submitting password reset...');
            button.setDisabled(true);

            Remote.UserCmd.register_safe_password_update({
                user_email: userEmailTextField.getValue()
            }, function(result, request, success) {
                me.optDebug('UserCmd.register_safe_password_update.result:', result);
                if (result) {
                    me.optDebug('UserCmd.register_safe_password_update.status_code:', result.status_code);
                }
                me.optDebug('UserCmd.register_safe_password_update.request:', request);
                me.optDebug('UserCmd.register_safe_password_update.success:', success);

                if (success && !result.errors) {

                    Remote.UserCmd.send_email_notification({
                        user_account: result.user_account_key_urlsafe,
                        template: 'safe_password_update_email'
                    }, function(result, request, success) {
                        me.optDebug('UserCmd.send_email_notification.result:', result);
                        if (result) {
                            me.optDebug('UserCmd.send_email_notification.status_code:', result.status_code);
                        }
                        me.optDebug('UserCmd.send_email_notification.request:', request);
                        me.optDebug('UserCmd.send_email_notification.success:', success);

                        if (success && !result.errors) {

                            Ext.Msg.show({
                                title: 'Password Reset Request',
                                msg: 'An email has been sent with password reset instructions for ' + userEmailTextField.getValue() + '.',
                                buttons: Ext.Msg.OK,
                                icon: Ext.Msg.INFO,
                                fn: passwordResetSuccessCallback
                            });

                        } else {
                            if (result && result.errors) {
                                for (error in result.errors) {

                                    Ext.Msg.show({
                                        title: result.status_code + ': ' + error,
                                        msg: result.errors[error],
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.Msg.ERROR,
                                        fn: passwordResetErrorCallback
                                    });
                                    break;
                                }
                            } else {
                                passwordResetErrorCallback();
                            }
                        }
                    });

                } else {
                    if (result && result.errors) {
                        for (error in result.errors) {

                            Ext.Msg.show({
                                title: result.status_code + ': ' + error,
                                msg: result.errors[error],
                                buttons: Ext.Msg.OK,
                                icon: Ext.Msg.ERROR,
                                fn: passwordResetErrorCallback
                            });
                            break;
                        }
                    } else {
                        passwordResetErrorCallback();
                    }
                }
            });

        }

    },

    onExLogin: function(eOpts) {
        this.optDebug('onExLogin.arguments: ', arguments);

        this.showWindow();

    }

});
