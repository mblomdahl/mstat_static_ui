/*
 * File: app/controller/SubscriptionsApiExplorerContainerCtrl.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('SubscriberInterface.controller.SubscriptionsApiExplorerContainerCtrl', {
    extend: 'Ext.app.Controller',
    alias: 'controller.subscriptionsapiexplorercontainerctrl',
    //exLoadInitialized: (function() { console.error('xtype: subscriptionsapiexplorercontainerctrl / ' + (new Date()).valueOf());})(),
    stores: [
        'SubscriberInterface.store.SubscriptionsStore'
    ],
    views: [
        'SubscriberInterface.view.container.SubscriptionsApiExplorerContainer'
    ],

    refs: [
        {
            ref: 'subscriptionsApiExplorerContainer',
            selector: 'subscriptionsapiexplorercontainer',
            xtype: 'subscriptionsapiexplorercontainer'
        },
        {
            ref: 'subscriptionsApiExplorerFormPanel',
            selector: 'subscriptionsapiexplorercontainer form',
            xtype: 'form'
        },
        {
            ref: 'subscriptionsApiExplorerGenerateSampleRequestButton',
            selector: '#subscriptionsApiExplorerGenerateSampleRequestButton',
            xtype: 'genericactionbutton'
        },
        {
            ref: 'subscriptionsApiExplorerViewSampleResponseButton',
            selector: '#subscriptionsApiExplorerViewSampleResponseButton',
            xtype: 'genericactionbutton'
        },
        {
            ref: 'subscriptionsApiExplorerDownloadDataFileButton',
            selector: '#subscriptionsApiExplorerDownloadDataFileButton',
            xtype: 'genericactionbutton'
        },
        {
            ref: 'subscriptionsApiExplorerSampleRequestTextArea',
            selector: '#subscriptionsApiExplorerSampleRequestTextArea',
            xtype: 'textareafield'
        },
        {
            ref: 'subscriptionsApiExplorerSampleResponseTextArea',
            selector: '#subscriptionsApiExplorerSampleResponseTextArea',
            xtype: 'textareafield'
        },
        {
            ref: 'subscriptionsApiExplorerStartDateField',
            selector: 'subscriptionsapiexplorercontainer genericdatefield[name="start_date"]',
            xtype: 'genericdatefield'
        },
        {
            ref: 'subscriptionsApiExplorerEndDateField',
            selector: 'subscriptionsapiexplorercontainer genericdatefield[name="end_date"]',
            xtype: 'genericdatefield'
        }
    ],

    appState: {
        currentSubscriptionSelected: null,
        apiCallConfig: null,
        apiCallPending: false,
        userApiAccessKey: null
    },

    init: function(application) {
        this.debug = false || application.debug;

        this.control({
            '#subscriptionsApiExplorerViewSampleResponseButton': {
                click: this.onViewSampleResponseButtonClick
            },
            '#subscriptionsApiExplorerGenerateSampleRequestButton': {
                click: this.onGenerateSampleRequestButtonClick
            },
            '#subscriptionsApiExplorerDownloadDataFileButton': {
                click: this.onDownloadDataFileButtonClick
            },
            '#subscriptionsApiExplorerFormPanel': {
                fieldvaliditychange: this.onFormPanelFieldChange
            },
            '#subscriptionsApiExplorerFormPanel genericdatefield, generictextfield, radiogroup': {
                change: this.onFormPanelFieldChange
            },
            '#subscriptionsApiExplorerFormPanel genericdatefield[name="start_date"]': {
                change: this.onStartDateFieldChange
            }
            /*,
            '#subscriptionsApiExplorerFormPanel': {
            fieldvaliditychange: this.onFormPanelFieldChange
            }*/
        });

        this.optDebug('initz: done');

        application.on({
            exLoginComplete: {
                fn: this.onExLoginComplete,
                scope: this
            },
            exSubscriptionSelect: {
                fn: this.onExSubscriptionSelect,
                scope: this
            },
            exSubscriptionApiCall: {
                fn: this.onExSubscriptionApiCall,
                scope: this
            },
            exSubscriptionApiCallComplete: {
                fn: this.onExSubscriptionApiCallComplete,
                scope: this
            },
            exSubscriptionDeselect: {
                fn: this.onExSubscriptionDeselect,
                scope: this
            },
            exLogout: {
                fn: this.onExLogout,
                scope: this
            }
        });
    },

    optDebug: function(label, data) {

        if (!this.debug) {
            if (arguments[arguments.length-1] !== 'override') {
                return;
            }
        }

        console.error(new Date() + label);
        if (arguments.length > 1) {
            for (var i = 1, j = arguments.length; i < j; i++) {
                if (arguments[i] === 'override') {
                    break;
                } else {
                    console.log(arguments[i]);
                }
            }
        }

    },

    updateFormPanel: function(subscription) {
        this.optDebug('updateFormPanel.arguments:', arguments);

        this.appState.currentSubscriptionSelected = subscription;
        this.appState.apiCallConfig = null;
        this.appState.apiCallPending = false;

        var apiExplorerContainer = this.getSubscriptionsApiExplorerContainer();
        var apiExplorerFormPanel = this.getSubscriptionsApiExplorerFormPanel();
        var apiExplorerForm = apiExplorerFormPanel.getForm();

        var subscriptionsStore = Ext.getStore('SubscriberInterface.store.SubscriptionsStore');
        var subscriptionRecord = subscriptionsStore.getById(subscription);

        var apiAccessKey = subscriptionRecord.get('api_access_key') + this.appState.userApiAccessKey;

        var apiCallConfig = {
            api_version: '1',
            api_access_key: apiAccessKey,
            start_date: '2013-08-13'
        };

        apiExplorerContainer.setDisabled(false);
        apiExplorerFormPanel.setLoading(false);
        apiExplorerForm.setValues(apiCallConfig);
    },

    resetFormPanel: function() {
        this.optDebug('resetFormPanel.arguments:', arguments);

        this.appState.currentSubscriptionSelected = null;
        this.appState.apiCallConfig = null;
        this.appState.apiCallPending = false;

        var apiExplorerContainer = this.getSubscriptionsApiExplorerContainer();
        var apiExplorerFormPanel = this.getSubscriptionsApiExplorerFormPanel();
        var apiExplorerForm = apiExplorerFormPanel.getForm();
        apiExplorerForm.reset();

        var sampleResponseTextArea = this.getSubscriptionsApiExplorerSampleResponseTextArea();
        sampleResponseTextArea.reset();

        var sampleRequestTextArea = this.getSubscriptionsApiExplorerSampleRequestTextArea();
        sampleRequestTextArea.reset();

        apiExplorerFormPanel.setLoading(false);
        apiExplorerContainer.setDisabled(true);
    },

    onFormPanelFieldChange: function(field, newValue, oldValue, eOpts) {
        this.optDebug('onFormPanelFieldChange.arguments:', arguments);

        var apiExplorerFormPanel = this.getSubscriptionsApiExplorerFormPanel();

        var generateSampleRequestButton = this.getSubscriptionsApiExplorerGenerateSampleRequestButton();
        var downloadDataFileButton = this.getSubscriptionsApiExplorerDownloadDataFileButton();
        var viewSampleResponseButton = this.getSubscriptionsApiExplorerViewSampleResponseButton();
        var sampleRequestTextArea = this.getSubscriptionsApiExplorerSampleRequestTextArea();

        generateSampleRequestButton.setDisabled(!apiExplorerFormPanel.getForm().isValid());

        viewSampleResponseButton.setDisabled(true);
        downloadDataFileButton.setDisabled(true);

        sampleRequestTextArea.reset();
    },

    onStartDateFieldChange: function(dateField, newValue, oldValue, eOpts) {
        this.optDebug('onStartDateFieldChange.arguments:', arguments);

        var endDateField = this.getSubscriptionsApiExplorerEndDateField();

        if (newValue) {
            endDateField.setValue(Ext.Date.add(newValue, Ext.Date.DAY, 1));
        } else {
            endDateField.reset();
        }
    },

    onGenerateSampleRequestButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onGenerateSampleRequestButtonClick.arguments:', arguments);

        var apiExplorerFormPanel = this.getSubscriptionsApiExplorerFormPanel();
        var apiCallConfig = apiExplorerFormPanel.getForm().getValues();

        var viewSampleResponseButton = this.getSubscriptionsApiExplorerViewSampleResponseButton();
        viewSampleResponseButton.setDisabled(true);

        var downloadDataFileButton = this.getSubscriptionsApiExplorerDownloadDataFileButton();
        downloadDataFileButton.setDisabled(true);

        var sampleResponseTextArea = this.getSubscriptionsApiExplorerSampleResponseTextArea();
        sampleResponseTextArea.reset();

        var sampleRequestTextArea = this.getSubscriptionsApiExplorerSampleRequestTextArea();
        sampleRequestTextArea.reset();

        button.setDisabled(true);
        apiExplorerFormPanel.setLoading('Generating Sample JSON Request Body ...');
        this.application.fireEvent('exSubscriptionApiCall');

        var me = this;
        Remote.UserCmd.format_api_call(apiCallConfig, function(result, request, success) {
            me.optDebug('UserCmd.format_api_call.result:', result);
            me.optDebug('UserCmd.format_api_call.request:', request);
            me.optDebug('UserCmd.format_api_call.success:', success);

            if (me.appState.apiCallPending) {
                if (success) {
                    sampleRequestTextArea.setValue(result);
                    viewSampleResponseButton.setDisabled(false);
                    me.appState.apiCallConfig = apiCallConfig;
                } else {
                    button.setDisabled(false);
                }
                me.application.fireEvent('exSubscriptionApiCallComplete');
                apiExplorerFormPanel.setLoading(false);
            } else {
                // API call aborted
                me.optDebug('appState.apiCallPending:', me.appState.apiCallPending);
            }
        });
    },

    onViewSampleResponseButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onViewSampleResponseButtonClick.arguments:', arguments);

        var apiCallConfig = this.appState.apiCallConfig;
        apiCallConfig.preview = true;

        var apiExplorerFormPanel = this.getSubscriptionsApiExplorerFormPanel();
        var sampleResponseTextArea = this.getSubscriptionsApiExplorerSampleResponseTextArea();

        /*var generateSampleRequestButton = this.getSubscriptionsApiExplorerGenerateSampleRequestButton();*/
        var downloadDataFileButton = this.getSubscriptionsApiExplorerDownloadDataFileButton();

        button.setDisabled(true);
        apiExplorerFormPanel.setLoading('Loading Sample Response ...');
        this.application.fireEvent('exSubscriptionApiCall');

        var me = this;
        Remote.UserCmd.submit_api_call(apiCallConfig, function(result, request, success) {
            me.optDebug('UserCmd.submit_api_call.result:', result);
            me.optDebug('UserCmd.submit_api_call.request:', request);
            me.optDebug('UserCmd.submit_api_call.success:', success);

            if (me.appState.apiCallPending) {
                sampleResponseTextArea.setValue(result);

                if (success) {
                    downloadDataFileButton.setDisabled(false);
                } else {
                    button.setDisabled(false);
                    /*generateSampleRequestButton.setDisabled(false);*/
                }
                me.application.fireEvent('exSubscriptionApiCallComplete');
                apiExplorerFormPanel.setLoading(false);
            } else {
                // API call aborted
                me.optDebug('appState.apiCallPending:', me.appState.apiCallPending);
            }
        });
    },

    onDownloadDataFileButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onDownloadDataFileButtonClick.arguments:', arguments);

        var apiExplorerFormPanel = this.getSubscriptionsApiExplorerFormPanel();
        var apiExplorerForm = apiExplorerFormPanel.getForm();
        var apiCallConfig = this.appState.apiCallConfig;
        //apiCallConfig.attachment = true;

        button.setDisabled(true);
        apiExplorerFormPanel.setLoading('Downloading Data File...');
        this.application.fireEvent('exSubscriptionApiCall');

        var me = this;
        apiExplorerForm.submit({
            clientValidation: false,
            params: apiCallConfig,
            method: 'POST',
            standardSubmit: true,
            timeout: 120,
            url: 'https://sys.maklarstatistik.se/user/api/query',
            baseParams: {
                'attachment': true
            },
            success: function(form, action) {
                if (me.appState.apiCallPending) {
                    Ext.Msg.alert('Success', action.result.msg);
                    me.application.fireEvent('exSubscriptionApiCallComplete');
                    apiExplorerFormPanel.setLoading(false);
                    //button.setDisabled(false);
                } else {
                    // API call aborted
                    me.optDebug('appState.apiCallPending:', me.appState.apiCallPending);
                }
            },
            failure: function(form, action) {
                if (me.appState.apiCallPending) {
                    switch (action.failureType) {
                        case Ext.form.action.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                        case Ext.form.action.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                        case Ext.form.action.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', action.result.msg);
                    }
                    me.application.fireEvent('exSubscriptionApiCallComplete');
                    apiExplorerFormPanel.setLoading(false);
                    button.setDisabled(false);
                } else {
                    // API call aborted
                    me.optDebug('appState.apiCallPending:', me.appState.apiCallPending);
                }
            }
        });

        /*window.setTimeout(function() {
            apiExplorerFormPanel.setLoading(false);
            button.setDisabled(false);
        }, 60e3);*/
    },

    onExLoginComplete: function(subscriberAccountRecord, userAccountRecord, eOpts) {
        this.optDebug('onExLoginComplete.arguments:', arguments);

        this.appState.userApiAccessKey = userAccountRecord.get('api_access_key');
    },

    onExSubscriptionSelect: function(subscription, eOpts) {
        this.optDebug('onExSubscriptionSelect.arguments:', arguments);

        this.updateFormPanel(subscription);
    },

    onExSubscriptionApiCall: function(eOpts) {
        this.optDebug('onExSubscriptionApiCall.arguments:', arguments);

        this.appState.apiCallPending = true;
    },

    onExSubscriptionApiCallComplete: function(eOpts) {
        this.optDebug('onExSubscriptionApiCallComplete.arguments:', arguments);

        this.appState.apiCallPending = false;
    },

    onExSubscriptionDeselect: function(eOpts) {
        this.optDebug('onExSubscriptionDeselect.arguments:', arguments);

        this.resetFormPanel();
    },

    onExLogout: function(eOpts) {
        this.optDebug('onExLogout.arguments:', arguments);

        this.resetFormPanel();

        this.appState.userApiAccessKey = null;
    }

});
