/*
 * File: app/controller/SubscriptionsGridPanelCtrl.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('SubscriberInterface.controller.SubscriptionsGridPanelCtrl', {
    extend: 'Ext.app.Controller',
    alias: 'controller.subscriptionsgridpanelctrl',
    //exLoadInitialized: (function() { console.error('xtype: subscriptionsgridpanelctrl / ' + (new Date()).valueOf());})(),
    stores: [
        'SubscriberInterface.store.SubscriptionsStore'
    ],
    views: [
        'SubscriberInterface.view.grid.SubscriptionsGridPanel'
    ],

    refs: [
        {
            ref: 'subscriptionsGridPanel',
            selector: 'subscriptionsgridpanel',
            xtype: 'subscriptionsgridpanel'
        }
    ],

    appState: {
        currentSubscriptionSelected: null
    },

    init: function(application) {
        this.debug = false || application.debug;

        this.control({
            'subscriptionsgridpanel': {
                selectionchange: this.onGridPanelSelectionChange,
                deselect: this.onGridPanelDeselect
            }
        });

        this.optDebug('initz: done');

        application.on({
            exLoginComplete: {
                fn: this.onExLoginComplete,
                scope: this
            },
            exSubscriptionsLoaded: {
                fn: this.onExSubscriptionsLoaded,
                scope: this
            },
            exSubscriptionSelect: {
                fn: this.onExSubscriptionSelect,
                scope: this
            },
            exSubscriptionDeselect: {
                fn: this.onExSubscriptionDeselect,
                scope: this
            },
            exLogout: {
                fn: this.onExLogout,
                scope: this
            }
        });
    },

    optDebug: function(label, data) {

        if (!this.debug) {
            if (arguments[arguments.length-1] !== 'override') {
                return;
            }
        }

        console.error(new Date() + label);
        if (arguments.length > 1) {
            for (var i = 1, j = arguments.length; i < j; i++) {
                if (arguments[i] === 'override') {
                    break;
                } else {
                    console.log(arguments[i]);
                }
            }
        }

    },

    loadSubscriptions: function(subscriber) {
        this.optDebug('loadSubscriptions.arguments:', arguments);

        var gridPanel = this.getSubscriptionsGridPanel();
        var store = gridPanel.getStore();

        if (!store.isLoading() && !store.getCount()) {
            store.load({
                filters: [
                    new Ext.util.Filter({
                        property: 'ancestor',
                        value   : subscriber
                    })
                ],
                callback: function(records, operation, success) {
                    this.application.fireEvent('exSubscriptionsLoaded');
                },
                scope: this
            });
        }
    },

    resetGridPanel: function() {
        this.optDebug('resetGridPanel.arguments:', arguments);

        var gridPanel = this.getSubscriptionsGridPanel();
        var selectionModel = gridPanel.getSelectionModel();

        selectionModel.deselectAll(true);
    },

    /*onManageSubscriptionsPanelBeforeActivate: function(tabPanel, eOpts) {
        this.optDebug('onManageSubscriptionsPanelBeforeActivate.arguments:', arguments);

        this.resetGridPanel();

    },

    onManageSubscriptionsPanelBeforeDeactivate: function(tabPanel, eOpts) {
        this.optDebug('onManageSubscriptionsPanelBeforeDeactivate.arguments:', arguments);

        this.resetGridPanel();
    },*/

    onGridPanelDeselect: function(gridPanel, record, index, eOpts) {
        this.optDebug('onGridPanelDeselect.arguments:', arguments);

        this.application.fireEvent('exSubscriptionDeselect');
    },

    onGridPanelSelectionChange: function(gridPanel, selected, eOpts) {
        this.optDebug('onGridPanelSelectionChange.arguments: ', arguments);

        if (selected.length) {
            var subscriptionRecord = selected[0];
            this.application.fireEvent('exSubscriptionSelect', subscriptionRecord.getId());
        }
    },

    onExLoginComplete: function(subscriberAccountRecord, userAccountRecord, eOpts) {
        this.optDebug('onExLoginComplete.arguments:', arguments);

        this.loadSubscriptions(subscriberAccountRecord.get('datastore_key_urlsafe'));
    },

    onExSubscriptionsLoaded: function(eOpts) {
        this.optDebug('onExSubscriptionsLoaded.arguments:', arguments);

        var gridPanel = this.getSubscriptionsGridPanel();

        if (gridPanel.store.getCount()) {
            gridPanel.selModel.doSingleSelect(gridPanel.store.getAt(0));
        }
    },

    onExSubscriptionSelect: function(subscription, eOpts) {
        this.optDebug('onExSubscriptionSelect.arguments:', arguments);

        this.appState.currentSubscriptionSelected = subscription;
    },

    onExSubscriptionDeselect: function(eOpts) {
        this.optDebug('onExSubscriptionDeselect.arguments:', arguments);

        this.appState.currentSubscriptionSelected = null;
    },

    onExLogout: function(eOpts) {
        this.optDebug('onExLogout.arguments:', arguments);

        this.appState.currentSubscriptionSelected = null;

        this.resetGridPanel();

        var gridPanel = this.getSubscriptionsGridPanel();
        var store = gridPanel.getStore();

        store.removeAll();
    }

});
