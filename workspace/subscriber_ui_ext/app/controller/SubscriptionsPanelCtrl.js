/*
 * File: app/controller/SubscriptionsPanelCtrl.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('SubscriberInterface.controller.SubscriptionsPanelCtrl', {
    extend: 'Ext.app.Controller',
    alias: 'controller.subscriptionspanelctrl',
    //exLoadInitialized: (function() { console.error('xtype: subscriptionspanelctrl / ' + (new Date()).valueOf());})(),
    stores: [
    ],
    views: [
        'SubscriberInterface.view.panel.SubscriptionsPanel'
    ],

    refs: [
        {
            ref: 'subscriptionsPanel',
            selector: 'subscriptionspanel',
            xtype: 'subscriptionspanel'
        },
        {
            ref: 'subscriptionsSelectionContainer',
            selector: 'subscriptionsselectioncontainer',
            xtype: 'subscriptionsselectioncontainer'
        },
        {
            ref: 'subscriptionsApiUsageDocsContainer',
            selector: 'subscriptionsapiusagedocscontainer',
            xtype: 'subscriptionsapiusagedocscontainer'
        },
        {
            ref: 'subscriptionsApiExplorerContainer',
            selector: 'subscriptionsapiexplorercontainer',
            xtype: 'subscriptionsapiexplorercontainer'
        },
        {
            ref: 'subscriptionsFieldDescContainer',
            selector: 'subscriptionsfielddesccontainer',
            xtype: 'subscriptionsfielddesccontainer'
        }
    ],

    appState: {

    },

    init: function(application) {
        this.debug = false || application.debug;

        this.control({
            'subscriptionspanel': {
                beforeactivate: this.onPanelBeforeActivate,
                beforedeactivate: this.onPanelBeforeDeactivate
            },
            '#subscriptionsScrollToSelectionButton': {
                click: this.onScrollToSelectionButtonClick
            },
            '#subscriptionsScrollToApiUsageDocsButton': {
                click: this.onScrollToApiUsageDocsButtonClick
            },
            '#subscriptionsScrollToApiExplorerButton': {
                click: this.onScrollToApiExplorerButtonClick
            },
            '#subscriptionsScrollToFieldDescButton': {
                click: this.onScrollToFieldDescButtonClick
            }
        });

        this.optDebug('initz: done');
    },

    optDebug: function(label, data) {

        if (!this.debug) {
            if (arguments[arguments.length-1] !== 'override') {
                return;
            }
        }

        console.error(new Date() + label);
        if (arguments.length > 1) {
            for (var i = 1, j = arguments.length; i < j; i++) {
                if (arguments[i] === 'override') {
                    break;
                } else {
                    console.log(arguments[i]);
                }
            }
        }

    },

    scrollPanel: function(targetCmp) {
        this.optDebug('scrollPanel.arguments:', arguments);

        var subscriptionPanel = this.getSubscriptionsPanel();

        subscriptionPanel.scrollBy(0, targetCmp.getY() - subscriptionPanel.getY() - 36, true);
    },

    onPanelBeforeActivate: function(tabPanel, eOpts) {
        this.optDebug('onPanelBeforeActivate.arguments:', arguments);

        //this.resetManageSubscriptionsGridPanel();

    },

    onPanelBeforeDeactivate: function(tabPanel, eOpts) {
        this.optDebug('onPanelBeforeDeactivate.arguments:', arguments);

        //this.resetManageSubscriptionsGridPanel();
    },

    onScrollToSelectionButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onScrollToSelectionButtonClick.arguments:', arguments);

        var targetContainer = this.getSubscriptionsSelectionContainer();

        this.scrollPanel(targetContainer);
    },

    onScrollToApiUsageDocsButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onScrollToApiUsageDocsButtonClick.arguments:', arguments);

        var targetContainer = this.getSubscriptionsApiUsageDocsContainer();

        this.scrollPanel(targetContainer);
    },

    onScrollToApiExplorerButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onScrollToApiExplorerButtonClick.arguments:', arguments);

        var targetContainer = this.getSubscriptionsApiExplorerContainer();

        this.scrollPanel(targetContainer);
    },

    onScrollToFieldDescButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onScrollToFieldDescButtonClick.arguments:', arguments);

        var targetContainer = this.getSubscriptionsFieldDescContainer();

        this.scrollPanel(targetContainer);
    }

});
