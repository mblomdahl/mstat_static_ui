/*
 * File: app/controller/SubscriptionsSelectionContainerCtrl.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('SubscriberInterface.controller.SubscriptionsSelectionContainerCtrl', {
    extend: 'Ext.app.Controller',
    alias: 'controller.subscriptionsselectioncontainerctrl',
    //exLoadInitialized: (function() { console.error('xtype: subscriptionsselectioncontainerctrl / ' + (new Date()).valueOf());})(),
    stores: [
    ],
    views: [
        'SubscriberInterface.view.container.SubscriptionsSelectionContainer'
    ],

    refs: [
        {
            ref: 'subscriptionsSelectionContainer',
            selector: 'subscriptionsselectioncontainer',
            xtype: 'subscriptionsselectioncontainer'
        }
    ],

    appState: {
    },

    init: function(application) {
        this.debug = false || application.debug;

        this.optDebug('initz: done');

        application.on({
            exLoginComplete: {
                fn: this.onExLoginComplete,
                scope: this
            },
            exLogout: {
                fn: this.onExLogout,
                scope: this
            }
        });
    },

    optDebug: function(label, data) {

        if (!this.debug) {
            if (arguments[arguments.length-1] !== 'override') {
                return;
            }
        }

        console.error(new Date() + label);
        if (arguments.length > 1) {
            for (var i = 1, j = arguments.length; i < j; i++) {
                if (arguments[i] === 'override') {
                    break;
                } else {
                    console.log(arguments[i]);
                }
            }
        }

    },

    onExLoginComplete: function(subscriberAccountRecord, userAccountRecord, eOpts) {
        this.optDebug('onExLoginComplete.arguments:', arguments);

        var selectionContainer = this.getSubscriptionsSelectionContainer();
        selectionContainer.setDisabled(false);
    },

    onExLogout: function(eOpts) {
        this.optDebug('onExLogout.arguments:', arguments);

        var selectionContainer = this.getSubscriptionsSelectionContainer();
        selectionContainer.setDisabled(true);
    }

});
