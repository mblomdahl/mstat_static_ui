/*
 * File: app/controller/SubscriptionDetailsFormPanelCtrl.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('SubscriberInterface.controller.SubscriptionDetailsFormPanelCtrl', {
    extend: 'Ext.app.Controller',
    alias: 'controller.subscriptiondetailsformpanelctrl',
    //exLoadInitialized: (function() { console.error('xtype: subscriptiondetailsformpanelctrl / ' + (new Date()).valueOf());})(),
    stores: [
        'SubscriberInterface.store.SubscriptionsStore'
    ],
    views: [
        'SubscriberInterface.view.form.SubscriptionDetailsFormPanel'
    ],

    refs: [
        {
            ref: 'subscriptionDetailsFormPanel',
            selector: 'subscriptiondetailsformpanel',
            xtype: 'subscriptiondetailsformpanel'
        }
    ],

    appState: {
        currentSubscriptionSelected: null,
        userApiAccessKey: null
    },

    init: function(application) {
        this.debug = false || application.debug;

        this.optDebug('initz: done');

        application.on({
            exLoginComplete: {
                fn: this.onExLoginComplete,
                scope: this
            },
            exSubscriptionSelect: {
                fn: this.onExSubscriptionSelect,
                scope: this
            },
            exSubscriptionDeselect: {
                fn: this.onExSubscriptionDeselect,
                scope: this
            },
            exLogout: {
                fn: this.onExLogout,
                scope: this
            }
        });
    },

    optDebug: function(label, data) {

        if (!this.debug) {
            if (arguments[arguments.length-1] !== 'override') {
                return;
            }
        }

        console.error(new Date() + label);
        if (arguments.length > 1) {
            for (var i = 1, j = arguments.length; i < j; i++) {
                if (arguments[i] === 'override') {
                    break;
                } else {
                    console.log(arguments[i]);
                }
            }
        }

    },

    updateFormPanel: function(subscription) {
        this.optDebug('updateFormPanel.arguments:', arguments);

        this.appState.currentSubscriptionSelected = subscription;

        var subscriptionRecord = Ext.getStore('SubscriberInterface.store.SubscriptionsStore').getById(subscription);

        var detailsFormPanel = this.getSubscriptionDetailsFormPanel();
        var detailsForm = detailsFormPanel.getForm();

        var apiAccessKey = subscriptionRecord.get('api_access_key') + this.appState.userApiAccessKey;
        var contentRestrictions = subscriptionRecord.get('content_restrictions');
        var dateInterval = contentRestrictions.start_date + ' – ';

        if (contentRestrictions.end_date) {
            dateInterval += contentRestrictions.end_date;
        } else {
            dateInterval += '<i>indefinite</i>';
        }

        var updateFrequency;
        switch (subscriptionRecord.get('update_frequency')) {
            case 'subscription_weekly_update_frequency': updateFrequency = 'Weekly'; break;
            case 'subscription_monthly_update_frequency': updateFrequency = 'Monthly'; break;
            default: updateFrequency = 'Daily'; break;
        }

        var subscriptionDetails = {
            friendly_name: subscriptionRecord.get('friendly_name'),
            description: subscriptionRecord.get('description'),
            api_access_key: apiAccessKey,
            quota: subscriptionRecord.get('quota') + ' full dataset downloads per update',
            fields: contentRestrictions.fields.join(', '),
            date_interval: dateInterval,
            update_frequency: updateFrequency
        };

        detailsFormPanel.setDisabled(false);
        detailsForm.setValues(subscriptionDetails);
    },

    resetFormPanel: function() {
        this.optDebug('resetFormPanel.arguments:', arguments);

        this.appState.currentSubscriptionSelected = null;

        var detailsFormPanel = this.getSubscriptionDetailsFormPanel();
        var detailsForm = detailsFormPanel.getForm();
        detailsForm.reset();
        detailsFormPanel.setDisabled(true);
    },

    onExLoginComplete: function(subscriberAccountRecord, userAccountRecord, eOpts) {
        this.optDebug('onExLoginComplete.arguments:', arguments);

        this.appState.userApiAccessKey = userAccountRecord.get('api_access_key');
    },

    onExSubscriptionSelect: function(subscription, eOpts) {
        this.optDebug('onExSubscriptionSelect.arguments:', arguments);

        this.updateFormPanel(subscription);
    },

    onExSubscriptionDeselect: function(eOpts) {
        this.optDebug('onExSubscriptionDeselect.arguments:', arguments);

        this.resetFormPanel();
    },

    onExLogout: function(eOpts) {
        this.optDebug('onExLogout.arguments:', arguments);

        this.resetFormPanel();

        this.appState.userApiAccessKey = null;
    }

});
