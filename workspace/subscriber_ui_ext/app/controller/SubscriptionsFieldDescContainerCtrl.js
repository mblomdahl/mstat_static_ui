/*
 * File: app/controller/SubscriptionsFieldDescContainerCtrl.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('SubscriberInterface.controller.SubscriptionsFieldDescContainerCtrl', {
    extend: 'Ext.app.Controller',
    alias: 'controller.subscriptionsfielddesccontainerctrl',
    //exLoadInitialized: (function() { console.error('xtype: subscriptionsfielddesccontainerctrl / ' + (new Date()).valueOf());})(),
    stores: [
    ],
    views: [
        'SubscriberInterface.view.container.SubscriptionsFieldDescContainer'
    ],

    refs: [
        {
            ref: 'subscriptionsFieldDescContainer',
            selector: 'subscriptionsfielddesccontainer',
            xtype: 'subscriptionsfielddesccontainer'
        }
    ],

    appState: {
        currentSubscriptionSelected: null
    },

    init: function(application) {
        this.debug = false || application.debug;

        this.optDebug('initz: done');

        application.on({
            exLoginComplete: {
                fn: this.onExLoginComplete,
                scope: this
            },
            exSubscriptionSelect: {
                fn: this.onExSubscriptionSelect,
                scope: this
            },
            exSubscriptionDeselect: {
                fn: this.onExSubscriptionDeselect,
                scope: this
            },
            exLogout: {
                fn: this.onExLogout,
                scope: this
            }
        });
    },

    optDebug: function(label, data) {

        if (!this.debug) {
            if (arguments[arguments.length-1] !== 'override') {
                return;
            }
        }

        console.error(new Date() + label);
        if (arguments.length > 1) {
            for (var i = 1, j = arguments.length; i < j; i++) {
                if (arguments[i] === 'override') {
                    break;
                } else {
                    console.log(arguments[i]);
                }
            }
        }

    },

    loadFieldDesc: function(subscription) {
        this.optDebug('loadFieldDesc.arguments:', arguments);

        this.appState.currentSubscriptionSelected = subscription;

        var subscriptionRecord = Ext.getStore('SubscriberInterface.store.SubscriptionsStore').getById(subscription);
    },

    resetFieldDesc: function() {
        this.optDebug('resetFieldDesc.arguments:', arguments);

        this.appState.currentSubscriptionSelected = null;

    },

    onExLoginComplete: function(subscriberAccountRecord, userAccountRecord, eOpts) {
        this.optDebug('onExLoginComplete.arguments:', arguments);

        var fieldDescContainer = this.getSubscriptionsFieldDescContainer();
        fieldDescContainer.setDisabled(false);
    },

    onExSubscriptionSelect: function(subscription, eOpts) {
        this.optDebug('onExSubscriptionSelect.arguments:', arguments);

        this.loadFieldDesc(subscription);
    },

    onExSubscriptionDeselect: function(eOpts) {
        this.optDebug('onExSubscriptionDeselect.arguments:', arguments);

        this.resetFieldDesc();
    },

    onExLogout: function(eOpts) {
        this.optDebug('onExLogout.arguments:', arguments);

        this.resetFieldDesc();

        var fieldDescContainer = this.getSubscriptionsFieldDescContainer();
        fieldDescContainer.setDisabled(true);
    }

});
