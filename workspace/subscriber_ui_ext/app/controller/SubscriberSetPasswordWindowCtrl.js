/*
 * File: app/controller/SubscriberSetPasswordWindowCtrl.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('SubscriberInterface.controller.SubscriberSetPasswordWindowCtrl', {
    extend: 'Ext.app.Controller',
    alias: 'controller.subscribersetpasswordwindowctrl',
    //exLoadInitialized: (function() { console.error('xtype: subscribersetpasswordwindowctrl / ' + (new Date()).valueOf());})(),
    models: [
        'SubscriberInterface.model.UserAccountModel',
        'SubscriberInterface.model.SubscriberAccountModel'
    ],
    views: [
        'SubscriberInterface.view.window.SubscriberSetPasswordWindow'
    ],

    refs: [
        {
            ref: 'subscriberSetPasswordWindow',
            selector: 'subscribersetpasswordwindow',
            xtype: 'subscribersetpasswordwindow'
        },
        {
            ref: 'subscriberSetPasswordFormPanel',
            selector: 'subscribersetpasswordwindow form',
            xtype: 'form'
        },
        {
            ref: 'subscriberSetPasswordUserEmailTextField',
            selector: 'subscribersetpasswordwindow textfield[name="user_email"]',
            xtype: 'textfield'
        },
        {
            ref: 'subscriberSetPasswordTextField',
            selector: '#subscriberSetPasswordTextField',
            xtype: 'textfield'
        },
        {
            ref: 'subscriberSetPasswordCfrmTextField',
            selector: '#subscriberSetPasswordCfrmTextField',
            xtype: 'textfield'
        },
        {
            ref: 'subscriberSetPasswordSubmitButton',
            selector: 'subscribersetpasswordwindow genericnextbutton',
            xtype: 'genericnextbutton'
        },
        {
            ref: 'subscriberSetPasswordCancelButton',
            selector: 'subscribersetpasswordwindow genericcancelbutton',
            xtype: 'genericcancelbutton'
        }
    ],

    init: function(application) {
        this.debug = false || application.debug;

        this.control({
            'subscribersetpasswordwindow textfield': {
                specialkey: this.onSubscriberSetPasswordTextFieldSpecialKey,
                change    : this.onSubscriberSetPasswordTextFieldChange
            },
            'subscribersetpasswordwindow genericcancelbutton': {
                click: this.onSubscriberSetPasswordCancelButtonClick
            },
            'subscribersetpasswordwindow genericnextbutton': {
                click: this.onSubscriberSetPasswordSubmitButtonClick
            },
            'subscribersetpasswordwindow': {
                show: this.onSubscriberSetPasswordWindowShow
            }
        });

        this.optDebug( 'initz: done' );

        this.pending_password_update_key = null;
        this.user_email = null;

        application.on({
            exSetPassword: {
                fn: this.onExSetPassword,
                scope: this
            }
        });
    },

    optDebug: function(label, data) {

        if (!this.debug) {
            if (arguments[arguments.length-1] !== 'override') {
                return;
            }
        }

        console.error(new Date() + label);
        if (arguments.length > 1) {
            for (var i = 1, j = arguments.length; i < j; i++) {
                if (arguments[i] === 'override') {
                    break;
                } else {
                    console.log(arguments[i]);
                }
            }
        }

    },

    vTypeSetup: function() {

    },

    showWindow: function() {
        this.optDebug('showWindow.arguments: ', arguments);

        Ext.ClassManager.get('SubscriberInterface.view.window.SubscriberSetPasswordWindow').create().show();
    },

    hideWindow: function(targetWindow) {
        this.optDebug('hideWindow.arguments: ', arguments);

        targetWindow = targetWindow || this.getSubscriberSetPasswordWindow();

        targetWindow.close();
    },

    resizeWindowHeight: function(container, heightExtra, reposition) {
        this.optDebug('resizeWindowHeight.arguments: ', arguments);

        reposition = reposition || false;

        var parentWindow = container.up('window');
        if (!parentWindow.rendered) {
            this.optDebug('parentWindow.rendered:' + parentWindow.rendered);
            return;
        } else {
            this.optDebug('parentWindow:', parentWindow);
        }

        var activeContainerWrapper = container.up('container');
        if (!activeContainerWrapper.rendered) {
            this.optDebug('activeContainerWrapper.rendered:' + activeContainerWrapper.rendered);
            return;
        } else {
            this.optDebug('activeContainerWrapper:', activeContainerWrapper);
        }

        var wrapperHeightDelta = activeContainerWrapper.getHeight();
        this.optDebug('wrapperHeightDelta:', wrapperHeightDelta);

        var activeViewContent = activeContainerWrapper.query('#' + container.id + ' > container');
        this.optDebug('activeViewContent:', activeViewContent);

        for (var i = 0, j = activeViewContent.length; i < j; i++) {

            if (!activeViewContent[i].rendered) {
                this.optDebug('activeViewContent[i].rendered:' + activeViewContent[i].rendered);
                return;
            }

            wrapperHeightDelta -= activeViewContent[i].getHeight();
        }

        if (heightExtra) {
            wrapperHeightDelta -= heightExtra;
        }

        parentWindow.setHeight(parentWindow.getHeight() - wrapperHeightDelta);

        if (reposition) {

            return parentWindow.center();
            /*
            var windowPosition = parentWindow.getPosition();

            windowPosition[1] += wrapperHeightDelta / 2;
            if (windowPosition[1] < 0) {
            windowPosition[1] = 0;
            }

            parentWindow.setPosition(windowPosition[0], windowPosition[1]);
            */
        }

    },

    validateSetPasswordForm: function() {
        this.optDebug('validateSetPasswordForm.arguments: ', arguments);

        this.optDebug('form:', this.getSubscriberSetPasswordFormPanel().getForm().getValues());

        var passwordForm = this.getSubscriberSetPasswordFormPanel().getForm();
        var passwordTextField = this.getSubscriberSetPasswordTextField();
        var passwordCfrmTextField = this.getSubscriberSetPasswordCfrmTextField();
        var submitButton = this.getSubscriberSetPasswordSubmitButton();

        if (passwordTextField.isValid() && (passwordCfrmTextField.getValue() == passwordTextField.getValue())) {
            submitButton.setDisabled(false);
        } else {
            submitButton.setDisabled(true);

            if (passwordTextField.isValid()) {
                passwordCfrmTextField.setDisabled(false);
            }
        }

    },

    onSubscriberSetPasswordWindowShow: function(setPasswordWindow) {
        this.optDebug('onSubscriberSetPasswordWindowShow.arguments: ', arguments);

        this.getSubscriberSetPasswordSubmitButton().setDisabled(true);
        this.getSubscriberSetPasswordCfrmTextField().setDisabled(true);
        this.getSubscriberSetPasswordUserEmailTextField().setValue(this.user_email);

        this.getSubscriberSetPasswordTextField().focus(false, 200);
    },

    onSubscriberSetPasswordTextFieldChange: function(textField, newValue, oldValue, eOpts) {
        this.optDebug('onSubscriberSetPasswordTextFieldChange.arguments: ', arguments);

        this.validateSetPasswordForm();


    },

    onSubscriberSetPasswordTextFieldSpecialKey: function(textField, keyEvent, eOpts) {
        this.optDebug('onSubscriberLoginPasswordTextFieldSpecialKey.arguments: ', arguments)

        if (keyEvent.getKey() === keyEvent.ENTER) {
            this.validateSetPasswordForm();
            var submitButton = this.getSubscriberSetPasswordSubmitButton();
            if (!submitButton.isDisabled()) {
                submitButton.fireEvent('click', submitButton);
            }
        }
    },

    onSubscriberSetPasswordSubmitButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onSubscriberSetPasswordSubmitButtonClick.arguments: ', arguments);

        var setPasswordForm = this.getSubscriberSetPasswordFormPanel().getForm();

        if (setPasswordForm.isValid()) {

            var me = this;
            var setPasswordWindow = button.up('window');
            setPasswordWindow.setLoading('Updating account credentials...');
            button.setDisabled(true);

            function setPasswordErrorCallback(buttonId, text, opt) {
                setPasswordWindow.setLoading(false);
                button.setDisabled(false);
            }

            function setPasswordSuccessCallback(buttonId, text, opt) {
                window.location.hash = '';
                window.location.reload();
                return true;
            }

            var userAccountModel = Ext.ClassManager.get('SubscriberInterface.model.UserAccountModel');
            var subscriberAccountModel = Ext.ClassManager.get('SubscriberInterface.model.SubscriberAccountModel');

            var password = this.getSubscriberSetPasswordTextField().getValue();
            var passwordUpdateKey = this.pending_password_update_key;

            Remote.UserCmd.activate_safe_password_update({
                'pending_password_update_key': passwordUpdateKey,
                'password': password
            }, function(result, request, success) {
                me.optDebug('UserCmd.activate_safe_password_update.result:', result);
                if (result) {
                    me.optDebug('UserCmd.activate_safe_password_update.status_code:', result.status_code);
                }
                me.optDebug('UserCmd.activate_safe_password_update.request:', request);
                me.optDebug('UserCmd.activate_safe_password_update.success:', success);
                if (success && !result.errors) {
                    Ext.Msg.show({
                        title: 'Account Credentials Updated',
                        msg: 'Your account credentials have been successfully updated. Press OK for redirection to the log in form.',
                        buttons: Ext.Msg.OK,
                        icon: Ext.Msg.INFO,
                        fn: setPasswordSuccessCallback
                    });
                } else {
                    if (result && result.errors) {
                        for (error in result.errors) {

                            Ext.Msg.show({
                                title: result.status_code + ': ' + error,
                                msg: result.errors[error],
                                buttons: Ext.Msg.OK,
                                icon: Ext.Msg.ERROR,
                                fn: setPasswordErrorCallback
                            });
                            break;
                        }
                    } else {
                        setPasswordErrorCallback();
                    }
                }

            });

        }

    },

    onSubscriberSetPasswordCancelButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onSubscriberSetPasswordCancelButtonClick.arguments: ', arguments);

        this.hideWindow(button.up('window'));

        this.application.fireEvent('exLogin');
    },

    onExSetPassword: function(passwordUpdateKey, eOpts) {
        this.optDebug('onSetPassword.arguments: ', arguments);

        var me = this;
        var userAccountModel = Ext.ClassManager.get('SubscriberInterface.model.UserAccountModel');
        var subscriberAccountModel = Ext.ClassManager.get('SubscriberInterface.model.SubscriberAccountModel');

        function setPasswordErrorCallback(buttonId, text, opt) {
            me.application.fireEvent('exLogin');
            return true;
        }

        function setPasswordSuccessCallback(buttonId, text, opt) {
            me.showWindow();
            return true;
        }

        Remote.UserCmd.verify_safe_password_update_key({
            pending_password_update_key: passwordUpdateKey
        }, function(result, request, success) {
            me.optDebug('UserCmd.verify_safe_password_update_key.result:', result);
            if (result) {
                me.optDebug('UserCmd.verify_safe_password_update_key.status_code:', result.status_code);
            }
            me.optDebug('UserCmd.verify_safe_password_update_key.request:', request);
            me.optDebug('UserCmd.verify_safe_password_update_key.success:', success);

            if (success && !result.errors) {
                //var userAccountRecord = new userAccountModel(result.user_account);
                //var subscriberAccountRecord = new subscriberAccountModel(result.subscriber_account);
                //me.application.fireEvent('exLoginSuccess', {
                //    userAccountRecord: userAccountRecord,
                //    subscriberAccountRecord: subscriberAccountRecord
                //});
                me.pending_password_update_key = result.user_account.sys_pending_password_update_key;
                me.user_email = result.user_account.user_email;
                setPasswordSuccessCallback();
            } else {
                /*if (result && result.errors) {
                for (error in result.errors) {

                Ext.Msg.show({
                title: result.status_code + ': ' + error,
                msg: result.errors[error],
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.ERROR,
                fn: setPasswordErrorCallback
                });
                break;
                }
                } else {*/
                setPasswordErrorCallback();
                //}
            }

        });

    }

});
