/*
 * File: app/controller/ViewportCtrl.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

// TODO: Move button enable/disable logic into component, isn't ctrl domain
Ext.define('SubscriberInterface.controller.ViewportCtrl', {
    extend: 'Ext.app.Controller',
    alias: 'controller.viewportctrl',
    //exLoadInitialized: (function() { console.error('xtype: viewportctrl / ' + (new Date()).valueOf());})(),
    stores: [
        'SubscriberInterface.store.RawDataStatsStore',
        'SubscriberInterface.store.NewsStore',
        'SubscriberInterface.store.CapitexEmailStatsStore',
        'SubscriberInterface.store.RawDataStatsStore'
    ],
    views: [
        'SubscriberInterface.view.Viewport'
    ],

    refs: [
        {
            ref: 'viewportLoginButton',
            selector: 'viewportloginlogoutcontainer generictoolbarloginbutton',
            xtype: 'generictoolbarloginbutton'
        },
        {
            ref: 'viewportLogoutButton',
            selector: 'viewportloginlogoutcontainer generictoolbarlogoutbutton',
            xtype: 'generictoolbarlogoutbutton'
        }
    ],

    init: function(application) {
        this.debug = false || application.debug;

        this.control({
            'viewportloginlogoutcontainer generictoolbarloginbutton': {
                click: this.onViewportLoginButtonClick
            },
            'viewportloginlogoutcontainer generictoolbarlogoutbutton': {
                click: this.onViewportLogoutButtonClick
            }
        });

        application.on({
            exLogin: {
                fn: this.onExLogin,
                scope: this
            },
            exLoginCancel: {
                fn: this.onExLoginCancel,
                scope: this
            },
            exLoginSuccess: {
                fn: this.onExLoginSuccess,
                scope: this
            },
            exLogout: {
                fn: this.onExLogout,
                scope: this
            }
        });
    },

    optDebug: function(label, data) {

        if (!this.debug) {
            if (arguments[arguments.length-1] !== 'override') {
                return;
            }
        }

        console.error(new Date() + label);
        if (arguments.length > 1) {
            for (var i = 1, j = arguments.length; i < j; i++) {
                if (arguments[i] === 'override') {
                    break;
                } else {
                    console.log(arguments[i]);
                }
            }
        }

    },

    doDisableLoginLogoutButtons: function(buttons) {
        this.optDebug('doDisableLoginLogoutButtons.arguments:', arguments);

        var loginButton = buttons && buttons.login || this.getViewportLoginButton();
        loginButton.setDisabled(true);

        var logoutButton = buttons && buttons.logout || this.getViewportLogoutButton();
        logoutButton.setDisabled(true);
    },

    doDisableLoginButton: function(buttons) {
        this.optDebug('doDisableLoginButton.arguments:', arguments);

        var loginButton = buttons && buttons.login || this.getViewportLoginButton();
        loginButton.setDisabled(true);

        var logoutButton = buttons && buttons.logout || this.getViewportLogoutButton();
        logoutButton.setDisabled(false);
    },

    doDisableLogoutButton: function(buttons) {
        this.optDebug('doDisableLogoutButton.arguments:', arguments);

        var loginButton = buttons && buttons.login || this.getViewportLoginButton();
        loginButton.setDisabled(false);

        var logoutButton = buttons && buttons.logout || this.getViewportLogoutButton();
        logoutButton.setDisabled(true);
    },

    doRemoveSession: function() {
        this.optDebug('doRemoveSession.arguments:', arguments);
        this.application.loggedIn = false;

        this.application.session = {
            subscriberAccountRecord: null,
            userAccountRecord: null
        };

        /* clear subscriptions list, clear stores */
    },

    doAddSession: function(userAccountRecord, subscriberAccountRecord) {
        this.optDebug('doAddSession.arguments:', arguments);

        this.application.loggedIn = true;

        this.application.session = {
            userAccountRecord: userAccountRecord,
            subscriberAccountRecord: subscriberAccountRecord
        };

        /* clear subscriptions list, clear stores */
    },

    onViewportLoginButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onViewportLoginButtonClick.arguments:', arguments);

        this.application.fireEvent('exLogin');
    },

    onViewportLogoutButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onViewportLogoutButtonClick.arguments:', arguments);

        this.application.fireEvent('exLogout');
    },

    onExLogin: function(eOpts) {
        this.optDebug('onExLogin.arguments:', arguments);

        this.doDisableLoginLogoutButtons();
    },

    onExLoginCancel: function(eOpts) {
        this.optDebug('onExLoginCancel.arguments:', arguments);

        this.doDisableLogoutButton();
        this.getViewportLoginButton().setDisabled(false);
    },

    onExLoginSuccess: function(subscriberAccountRecord, userAccountRecord, eOpts) {
        this.optDebug('onExLoginSuccess.arguments:', arguments);

        this.doDisableLoginButton();
        this.doAddSession(subscriberAccountRecord, userAccountRecord);

        this.application.fireEvent('exLoginComplete', subscriberAccountRecord, userAccountRecord);
    },

    onExLogout: function(eOpts) {
        this.optDebug('onExLogout.arguments:', arguments);

        this.doRemoveSession();
        this.doDisableLogoutButton();
    }

});
