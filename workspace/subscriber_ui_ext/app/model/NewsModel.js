/*
 * File: app/model/NewsModel.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('SubscriberInterface.model.NewsModel', {
    extend: 'Ext.data.Model',
    //exLoadInitialized: (function() { console.error('model: NewsModel / ' + (new Date()).valueOf());})(),

    idProperty: 'datastore_key_urlsafe',

    fields: [
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'published_datetime',
            type: 'date'
        },
        {
            name: 'author_full_name',
            type: 'string'
        },
        {
            name: 'author_email',
            type: 'string'
        },
        {
            name: 'title',
            type: 'string'
        },
        {
            name: 'summary',
            type: 'string'
        },
        {
            name: 'body',
            type: 'string'
        },
        {
            name: 'read',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'sys_origin_user_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_organization_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_changelog_version',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_changelog',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ]
});
