/*
 * File: app/model/SubscriptionModel.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('SubscriberInterface.model.SubscriptionModel', {
    extend: 'Ext.data.Model',
    //exLoadInitialized: (function() { console.error('model: Subscription / ' + (new Date()).valueOf());})(),
    idProperty: 'datastore_key_urlsafe',

    fields: [
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_ancestor_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_ancestor_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'subscription_type',
            type: 'string',
            useNull: true
        },
        {
            name: 'friendly_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'description',
            type: 'string',
            useNull: true
        },
        {
            name: 'content_restrictions',
            type: 'auto',
            useNull: true
        },
        {
            name: 'api_access_key',
            type: 'string',
            useNull: true
        },
        {
            name: 'quota',
            type: 'int',
            useNull: true
        },
        {
            dateFormat: 'Y-m-d',
            name: 'start_date',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-d',
            name: 'end_date',
            type: 'date',
            useNull: true
        },
        {
            name: 'update_frequency',
            type: 'string',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'registered_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'suspended_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'deleted_datetime',
            type: 'date',
            useNull: true
        },
        {
            name: 'suspended',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'deleted',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'sys_origin_user_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_organization_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_changelog_version',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_changelog',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ]
});
