/*
 * File: app/model/CapitexEmailStatsModel.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('SubscriberInterface.model.CapitexEmailStatsModel', {
    extend: 'Ext.data.Model',
    //exLoadInitialized: (function() { console.error('model: RawDataStatsModel / ' + (new Date()).valueOf());})(),

    idProperty: 'datastore_key_urlsafe',

    fields: [
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'stats_datetime',
            type: 'date'
        },
        {
            name: 'emails_processed',
            type: 'number'
        },
        {
            name: 'duplicates_discarded',
            type: 'number'
        },
        {
            name: 'parser_errors_thrown',
            type: 'number'
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ]
});
