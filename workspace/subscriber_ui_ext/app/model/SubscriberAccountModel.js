/*
 * File: app/model/SubscriberAccountModel.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('SubscriberInterface.model.SubscriberAccountModel', {
    extend: 'Ext.data.Model',
    //exLoadInitialized: (function() { console.error('model: SubscriberAccount / ' + (new Date()).valueOf());})(),
    uses: [
        'SubscriberInterface.model.UserAccountModel',
        'Common.model.ContactDetails',
        'Common.model.PostalAddress',
        'Common.model.WebAddress'
    ],

    idProperty: 'datastore_key_urlsafe',

    fields: [
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'organization_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'department_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'organization_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'contact_details',
            type: 'auto',
            useNull: true
        },
        {
            name: 'postal_addresses',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'activated_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'registered_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'suspended_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'deleted_datetime',
            type: 'date',
            useNull: true
        },
        {
            name: 'suspended',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'deleted',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'activated',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'billing_balance',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_origin_user_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_organization_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_changelog_version',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_changelog',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ],

    hasOne: [
        {
            associationKey: 'sys_origin_user_account',
            model: 'SubscriberInterface.model.UserAccountModel',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_origin_organization_account',
            model: 'SubscriberInterface.model.SubscriberAccountModel',
            primaryKey: 'datastore_key_id'
        }
    ],

    hasMany: [
        {
            associationKey: 'contact_details',
            model: 'Common.model.ContactDetails',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'postal_addresses',
            model: 'Common.model.PostalAddress',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'web_addresses',
            model: 'Common.model.WebAddress',
            primaryKey: 'datastore_key_id'
        }
    ]
});
