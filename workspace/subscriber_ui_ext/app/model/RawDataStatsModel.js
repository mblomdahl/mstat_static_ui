/*
 * File: app/model/RawDataStatsModel.js
 * Author: Mats Blomdahl
 * Version: 1.04
 */

Ext.define('SubscriberInterface.model.RawDataStatsModel', {
    extend: 'Ext.data.Model',
    //exLoadInitialized: (function() { console.error('model: RawDataStatsModel / ' + (new Date()).valueOf());})(),

    idProperty: 'datastore_key_id',

    fields: [
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'stats_datetime',
            type: 'date'
        },
        {
            name: 'sfd_created_entry_count',
            type: 'number'
        },
        {
            name: 'sfd_updated_entry_count',
            type: 'number'
        },
        {
            name: 'scb_created_entry_count',
            type: 'number'
        },
        {
            name: 'scb_updated_entry_count',
            type: 'number'
        },
        {
            name: 'capitex_created_entry_count',
            type: 'number'
        },
        {
            name: 'capitex_updated_entry_count',
            type: 'number'
        },
        {
            name: 'lf_created_entry_count',
            type: 'number'
        },
        {
            name: 'lf_updated_entry_count',
            type: 'number'
        },
        {
            name: 'sf_created_entry_count',
            type: 'number'
        },
        {
            name: 'sf_updated_entry_count',
            type: 'number'
        },
        {
            name: 'mspecs_created_entry_count',
            type: 'number'
        },
        {
            name: 'mspecs_updated_entry_count',
            type: 'number'
        },
        {
            name: 'tce_created_entry_count',
            type: 'number'
        },
        {
            name: 'tce_updated_entry_count',
            type: 'number'
        },
        {
            name: 'tce_mstat_sys_sfd_created_entry_count',
            type: 'number'
        },
        {
            name: 'tce_mstat_sys_lf_created_entry_count',
            type: 'number'
        },
        {
            name: 'tce_mstat_sys_sf_created_entry_count',
            type: 'number'
        },
        {
            name: 'tce_mstat_sys_scb_created_entry_count',
            type: 'number'
        },
        {
            name: 'tce_mstat_sys_capitex_created_entry_count',
            type: 'number'
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ]
});
