/*
 * File: app/store/NewsStore.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('SubscriberInterface.store.NewsStore', {
    extend: 'Ext.data.Store',
    //exLoadInitialized: (function() { console.error('storeId: news / ' + (new Date()).valueOf());})(),
    requires: [
        'SubscriberInterface.model.NewsModel'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'SubscriberInterface.model.NewsModel',
            storeId: 'news',
            data: [
                /*{
                    datastore_key_id: 'bla1',
                    datastore_key_urlsafe: 'blaha1',
                    published_datetime: '2013-10-17T17:20:01',
                    author_full_name: 'Author Name',
                    author_email: 'mats.blomdahl@gmail.com',
                    title: 'System- och drift-information via kundgränssnittet <strong>[ PREVIEW ]</strong>',
                    summary: [
                        '<p>Under senaste veckan har vi påbörjat arbete med att tillhandahålla en dashboard där du som kund kan följa vårt arbete med dataimporten från dag till dag.</p>',
                        '<p>Återvänd gärna hit under veckan som följer för att </p>'
                    ].join(' '),
                    body: [
                        '<p>Bla body bla body bla. Bla body bla body bla. Bla body bla body bla. Bla body bla body bla.</p>',
                        '<p>Bla body bla body bla. Bla body bla body bla. Bla body bla body bla. Bla body bla body bla. Bla body bla body bla. Bla body bla body bla.</p>',
                        '<p>Bla body bla body bla. Bla body bla body bla. Bla body bla body bla. Bla body bla body bla. Bla body bla body bla. Bla body bla body bla.</p>'
                    ].join(' '),
                    read: false
                },*/
                {
                    datastore_key_id: 'bla3',
                    datastore_key_urlsafe: 'blaha3',
                    published_datetime: '2013-12-13T17:05:03',
                    author_full_name: 'Mats Blomdahl',
                    author_email: 'mats.blomdahl@gmail.com',
                    title: 'Driftövervakning (pre-release)',
                    summary: [
                        '<p>',
                        'Inför att vår nya systemlösning lämnar beta-stadiet har vi driftsatt en första <em>alpha-',
                        'release</em> av driftövervakningssidan. ',
                        'Ambitionen för driftövervakningen är att ni som mikrodataabonnent ska kunna ta del av ',
                        'övergripande information hur vi, på dagsbasis, lyckas med vårt inläsnings- och aggregerings-',
                        'arbete.',
                        '</p>',
                        '<p>',
                        'Driftövervakningssidan återfinns under fliken <em>System Performance</em>. ',
                        'Med tiden kommer både antalet parametrar och graden av interaktivitet att öka.',
                        '</p>'
                    ].join(' '),
                    body: [
                        '<p>Bla body bla body bla. Bla body bla body bla. Bla body bla body bla. Bla body bla body ',
                        'bla. Bla body bla body bla. Bla body bla body bla.</p>',
                        '<p>Bla body bla body bla. Bla body bla body bla. Bla body bla body bla. Bla body bla body ',
                        'bla. Bla body bla body bla. Bla body bla body bla. Bla body bla body bla. Bla body bla body ',
                        'bla. Bla body bla body bla. Bla body bla body bla.</p>'
                    ].join(' '),
                    read: false
                },
                {
                    datastore_key_id: 'bla2',
                    datastore_key_urlsafe: 'blaha2',
                    published_datetime: '2013-10-01T11:10:20',
                    author_full_name: 'Mats Blomdahl',
                    author_email: 'mats.blomdahl@gmail.com',
                    title: 'Långvarig eftersläpning vid inläsning av Capitex-data',
                    summary: [
                        '<p>',
                        'Inläsningen av Capitex:s dataleverans via e-post är pausad sedan slutet av september månad. ',
                        'Detta är ett led i ett omfattande kvalitetssäkringsarbete kring Mäklarstatistiks mail-',
                        'hantering.',
                        '</p>',
                        '<p>',
                        'Arbetet beräknas vara helt klart månadsskiftet oktober-november. ',
                        'Under tiden hänvisar vi till ordinarie leverans via SCB.',
                        '</p>'
                    ].join(' '),
                    body: [
                        '<p>Bla body bla body bla. Bla body bla body bla. Bla body bla body bla. Bla body bla body ',
                        'bla. Bla body bla body bla. Bla body bla body bla.</p>',
                        '<p>Bla body bla body bla. Bla body bla body bla. Bla body bla body bla. Bla body bla body ',
                        'bla. Bla body bla body bla. Bla body bla body bla. Bla body bla body bla. Bla body bla body ',
                        'bla. Bla body bla body bla. Bla body bla body bla.</p>'
                    ].join(' '),
                    read: false
                },
                {
                    datastore_key_id: 'bla3',
                    datastore_key_urlsafe: 'blaha3',
                    published_datetime: '2013-10-01T08:50:11',
                    author_full_name: 'Mats Blomdahl',
                    author_email: 'mats.blomdahl@gmail.com',
                    title: 'Beta-testare',
                    summary: [
                        '<p>',
                        'Fr.o.m. den 1 oktober inleder Mäklarstatistik beta-testning med sina viktigaste objektdata-',
                        'kunder. Vi är mycket tacksamma för all input kring exponerad funktionalitet, transparens, ',
                        'driftsäkerhet och datakvalitet.',
                        '</p>',
                        '<p>',
                        'Du finner det mesta du kan behöva för att komma igång under fliken <em>Subscriptions</em>.',
                        '</p>',
                        '<p>',
                        'För tekniska frågor/synpunkter och support kring adoptionen av vår nya lösning för ',
                        'dataleverans – kontakta Mats Blomdahl på <a href="mailto:mats@stormfors.se">mats@stormfors.se',
                        '</a> eller <a href="tel:+46730567567">+46 730 567 567</a>.',
                        '</p>'
                    ].join(' '),
                    body: [
                        '<p>Bla body bla body bla. Bla body bla body bla. Bla body bla body bla. Bla body bla body ',
                        'bla. Bla body bla body bla. Bla body bla body bla.</p>',
                        '<p>Bla body bla body bla. Bla body bla body bla. Bla body bla body bla. Bla body bla body ',
                        'bla. Bla body bla body bla. Bla body bla body bla. Bla body bla body bla. Bla body bla body ',
                        'bla. Bla body bla body bla. Bla body bla body bla.</p>'
                    ].join(' '),
                    read: false
                }
            ]
        }, cfg)]);
    }
});
