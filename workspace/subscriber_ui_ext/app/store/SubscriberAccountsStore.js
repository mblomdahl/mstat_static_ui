/*
 * File: app/store/SubscriberAccountsStore.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('SubscriberInterface.store.SubscriberAccountsStore', {
    extend: 'Ext.data.Store',
    //exLoadInitialized: (function() { console.error('storeId: subscriberAccounts / ' + (new Date()).valueOf());})(),
    requires: [
        'SubscriberInterface.model.SubscriberAccountModel'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'SubscriberInterface.model.SubscriberAccountModel',
            storeId: 'subscriberAccounts',
            leadingBufferZone: 0,
            pageSize: 100,
            trailingBufferZone: 0,
            proxy: {
                type: 'direct',
                directionParam: 'order_direction',
                extraParams: {
                    kind: 'SubscriberAccount'
                },
                filterParam: 'filter_by',
                groupParam: 'group_by',
                limitParam: 'page_size',
                sortParam: 'order_by',
                startParam: 'offset',
                directFn: Remote.UserCmd.grid_data,
                reader: {
                    type: 'json',
                    idProperty: 'datastore_key_id',
                    root: 'data'
                },
                writer: {
                    type: 'json',
                    nameProperty: 'datastore_key_id',
                    allowSingle: false,
                    root: 'data'
                }
            }
        }, cfg)]);
    }
});
