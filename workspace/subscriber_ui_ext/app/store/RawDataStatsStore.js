/*
 * File: app/store/RawDataStatsStore.js
 * Author: Mats Blomdahl
 * Version: 1.04
 */

Ext.define('SubscriberInterface.store.RawDataStatsStore', {
    extend: 'Ext.data.Store',
    //exLoadInitialized: (function() { console.error('storeId: rawDataStats / ' + (new Date()).valueOf());})(),
    requires: [
        'SubscriberInterface.model.RawDataStatsModel'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'SubscriberInterface.model.RawDataStatsModel',
            storeId: 'rawDataStats',
            autoLoad: true,
            leadingBufferZone: 0,
            pageSize: 100,
            trailingBufferZone: 0,
            proxy: {
                type: 'direct',
                directionParam: 'order_direction',
                extraParams: {
                    kind: 'DailyStats'  // TODO(mats.blomdahl@gmail.com): Shame.
                },
                filterParam: 'filter_by',
                groupParam: 'group_by',
                limitParam: 'page_size',
                sortParam: 'order_by',
                startParam: 'offset',
                directFn: Remote.UserCmd.daily_stats,
                reader: {
                    type: 'json',
                    idProperty: 'datastore_key_id',
                    root: 'data'
                },
                writer: {
                    type: 'json',
                    nameProperty: 'datastore_key_id',
                    allowSingle: false,
                    root: 'data'
                }
            }
        }, cfg)]);
    }
});
