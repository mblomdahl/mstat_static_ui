/*
 * File: app/store/CapitexEmailStatsStore.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('SubscriberInterface.store.CapitexEmailStatsStore', {
    extend: 'Ext.data.Store',
    //exLoadInitialized: (function() { console.error('storeId: rawDataStats / ' + (new Date()).valueOf());})(),
    requires: [
        'SubscriberInterface.model.CapitexEmailStatsModel'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'SubscriberInterface.model.CapitexEmailStatsModel',
            storeId: 'capitexEmailStats',
            data: [
                {
                    datastore_key_id: 'bla12',
                    datastore_key_urlsafe: 'blaha11',
                    stats_datetime: '2013-10-08T00:00:00',
                    emails_processed: 131,
                    sfd_modified: 43,
                    capitex_created: 73,
                    duplicates_discarded: 15,
                    parser_errors_thrown: 0,
                    mspecs_modified: 0
                },
                {
                    datastore_key_id: 'bla22',
                    datastore_key_urlsafe: 'blaha21',
                    stats_datetime: '2013-10-09T00:00:00',
                    emails_processed: 92,
                    sfd_modified: 9,
                    capitex_created: 0,
                    duplicates_discarded: 0,
                    parser_errors_thrown: 0,
                    mspecs_modified: 0
                },
                {
                    datastore_key_id: 'bla32',
                    datastore_key_urlsafe: 'blaha31',
                    stats_datetime: '2013-10-10T00:00:00',
                    emails_processed: 59,
                    sfd_modified: 6,
                    capitex_created: 140,
                    duplicates_discarded: 36,
                    parser_errors_thrown: 0,
                    mspecs_modified: 0
                },
                {
                    datastore_key_id: 'bla42',
                    datastore_key_urlsafe: 'blaha41',
                    stats_datetime: '2013-10-11T00:00:00',
                    emails_processed: 145,
                    sfd_modified: 29,
                    capitex_created: 14,
                    duplicates_discarded: 3,
                    parser_errors_thrown: 0,
                    mspecs_modified: 0
                },
                {
                    datastore_key_id: 'bla12',
                    datastore_key_urlsafe: 'blaha113',
                    stats_datetime: '2013-10-12T00:00:00',
                    emails_processed: 131,
                    sfd_modified: 43,
                    capitex_created: 73,
                    duplicates_discarded: 15,
                    parser_errors_thrown: 0,
                    mspecs_modified: 0
                },
                {
                    datastore_key_id: 'bla22',
                    datastore_key_urlsafe: 'blaha213',
                    stats_datetime: '2013-10-13T00:00:00',
                    emails_processed: 92,
                    sfd_modified: 9,
                    capitex_created: 0,
                    duplicates_discarded: 0,
                    parser_errors_thrown: 0,
                    mspecs_modified: 0
                },
                {
                    datastore_key_id: 'bla32',
                    datastore_key_urlsafe: 'blaha3113',
                    stats_datetime: '2013-10-14T00:00:00',
                    emails_processed: 159,
                    sfd_modified: 61,
                    capitex_created: 140,
                    duplicates_discarded: 36,
                    parser_errors_thrown: 0,
                    mspecs_modified: 0
                },
                {
                    datastore_key_id: 'bla42',
                    datastore_key_urlsafe: 'blaha4131',
                    stats_datetime: '2013-10-15T00:00:00',
                    emails_processed: 145,
                    sfd_modified: 29,
                    capitex_created: 14,
                    duplicates_discarded: 3,
                    parser_errors_thrown: 0,
                    mspecs_modified: 0
                },
                {
                    datastore_key_id: 'bla12',
                    datastore_key_urlsafe: 'blaha1113',
                    stats_datetime: '2013-10-16T00:00:00',
                    emails_processed: 131,
                    sfd_modified: 43,
                    capitex_created: 73,
                    duplicates_discarded: 15,
                    parser_errors_thrown: 0,
                    mspecs_modified: 0
                },
                {
                    datastore_key_id: 'bla22',
                    datastore_key_urlsafe: 'blaha2113',
                    stats_datetime: '2013-10-17T00:00:00',
                    emails_processed: 92,
                    sfd_modified: 9,
                    capitex_created: 0,
                    duplicates_discarded: 0,
                    parser_errors_thrown: 0,
                    mspecs_modified: 0
                },
                {
                    datastore_key_id: 'bla321',
                    datastore_key_urlsafe: 'blaha311',
                    stats_datetime: '2013-10-18T00:00:00',
                    emails_processed: 159,
                    sfd_modified: 61,
                    capitex_created: 140,
                    duplicates_discarded: 36,
                    parser_errors_thrown: 0,
                    mspecs_modified: 0
                },
                {
                    datastore_key_id: 'bla421',
                    datastore_key_urlsafe: 'blaha411',
                    stats_datetime: '2013-10-19T00:00:00',
                    emails_processed: 14,
                    sfd_modified: 3,
                    capitex_created: 145,
                    duplicates_discarded: 29,
                    parser_errors_thrown: 0,
                    mspecs_modified: 0
                }
            ]
        }, cfg)]);
    }
});
