/*
 * File: app/model/JobConfiguration.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.model.JobConfiguration', {
    extend: 'Ext.data.Model',

    uses: [
        'AdminInterface.model.JobResource',
        'AdminInterface.model.JobLoadConfiguration',
        'AdminInterface.model.JobExtractConfiguration',
        'AdminInterface.model.JobCopyConfiguration',
        'AdminInterface.model.JobQueryConfiguration'
    ],

    fields: [
        {
            name: 'load',
            type: 'auto',
            useNull: true
        },
        {
            name: 'extract',
            type: 'auto',
            useNull: true
        },
        {
            name: 'copy',
            type: 'auto',
            useNull: true
        },
        {
            name: 'query',
            type: 'auto',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'AdminInterface.model.JobResource',
        primaryKey: 'datastore_key_id',
        foreignKey: 'configuration'
    },

    hasOne: [
        {
            associationKey: 'load',
            model: 'AdminInterface.model.JobLoadConfiguration'
        },
        {
            associationKey: 'extract',
            model: 'AdminInterface.model.JobExtractConfiguration',
            foreignKey: ''
        },
        {
            associationKey: 'copy',
            model: 'AdminInterface.model.JobCopyConfiguration'
        },
        {
            associationKey: 'query',
            model: 'AdminInterface.model.JobQueryConfiguration'
        }
    ]
});
