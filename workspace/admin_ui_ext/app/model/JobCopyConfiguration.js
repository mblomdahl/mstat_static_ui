/*
 * File: app/model/JobCopyConfiguration.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.model.JobCopyConfiguration', {
    extend: 'Ext.data.Model',

    uses: [
        'AdminInterface.model.JobConfiguration'
    ],

    fields: [
        {
            name: 'source_table',
            type: 'auto',
            useNull: true
        },
        {
            name: 'destination_table',
            type: 'auto',
            useNull: true
        },
        {
            name: 'create_disposition',
            type: 'string',
            useNull: true
        },
        {
            name: 'write_disposition',
            type: 'string',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'AdminInterface.model.JobConfiguration',
        foreignKey: 'copy'
    }
});
