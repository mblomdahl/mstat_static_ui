/*
 * File: app/model/DatasetResourceReference.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.model.DatasetResourceReference', {
    extend: 'Ext.data.Model',

    uses: [
        'AdminInterface.model.DatasetResource'
    ],

    idProperty: 'dataset_id',

    fields: [
        {
            name: 'project_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'dataset_id',
            type: 'string',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'AdminInterface.model.DatasetResource',
        primaryKey: 'datastore_key_id',
        foreignKey: 'dataset_reference'
    }
});
