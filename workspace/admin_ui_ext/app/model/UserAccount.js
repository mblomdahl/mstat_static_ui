/*
 * File: app/model/UserAccount.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.model.UserAccount', {
    extend: 'Ext.data.Model',

    stores: [

    ],
    exFieldsInitialized: false,
    idProperty: 'datastore_key_urlsafe',

    fields: [
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'parent_datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'parent_datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'ex_user_account',
            persist: false,
            type: 'string'
        },
        {
            name: 'user_email',
            type: 'string',
            useNull: true
        },
        {
            name: 'ex_full_name',
            persist: false,
            type: 'string'
        },
        {
            name: 'username',
            type: 'string',
            useNull: true
        },
        {
            name: 'user_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'google_user_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'google_user',
            type: 'auto',
            useNull: true
        },
        {
            name: 'password',
            type: 'string',
            useNull: true
        },
        {
            name: 'api_access_key',
            type: 'string',
            useNull: true
        },
        {
            name: 'contact_details',
            type: 'auto',
            useNull: true
        },
        {
            name: 'ex_billing_contact',
            persist: false,
            type: 'string'
        },
        {
            name: 'ex_technical_contact',
            persist: false,
            type: 'string'
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'registered_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'activated_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'suspended_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'deleted_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'password_changed_datetime',
            type: 'date',
            useNull: true
        },
        {
            name: 'suspended',
            type: 'boolean',
            useNull: true
        },
        {
            defaultValue: 'No',
            name: 'ex_suspended',
            persist: false,
            type: 'string'
        },
        {
            name: 'deleted',
            type: 'boolean',
            useNull: true
        },
        {
            defaultValue: 'No',
            name: 'ex_deleted',
            persist: false,
            type: 'string'
        },
        {
            name: 'activated',
            type: 'boolean',
            useNull: true
        },
        {
            defaultValue: 'No',
            name: 'ex_activated',
            persist: false,
            type: 'string'
        },
        {
            name: 'administrator',
            type: 'boolean',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'last_visit_datetime',
            type: 'date',
            useNull: true
        },
        {
            name: 'last_client_ip',
            type: 'string',
            useNull: true
        },
        {
            convert: function(v, rec) {
                /*console.log(v);
                console.log(rec);*/

                var translations = {
                    'subscriber_admin': 'Account Administrator',
                    'subscriber_user': 'Account User',
                    'system_su': 'System SU',
                    'system_admin': 'System Administrator'
                };

                if (translations[v]) {
                    rec.set('ex_sys_user_role', translations[v]);
                }

                return v;
            },
            name: 'sys_user_role',
            type: 'string',
            useNull: true
        },
        {
            name: 'ex_sys_user_role',
            persist: false,
            type: 'string'
        },
        {
            name: 'sys_session_id',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_pending_email_update_address',
            type: 'string',
            useNull: true
        },
        {
            name: 'sys_pending_email_update_key',
            type: 'string',
            useNull: true
        },
        {
            name: 'sys_pending_password_update_key',
            type: 'string',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_pending_password_update_expiration_datetime',
            type: 'date',
            useNull: true
        },
        {
            defaultValue: 'No',
            name: 'ex_sys_pending_password_update',
            persist: false,
            type: 'string'
        },
        {
            name: 'ex_sys_origin_user_account',
            persist: false,
            type: 'string'
        },
        {
            name: 'ex_sys_origin_organization_account',
            type: 'string'
        },
        {
            name: 'sys_changelog_version',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_changelog',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ],

    postProcessExFields: function(forceUpdate) {
        if (this.exFieldsInitialized && !forceUpdate) {
            return false;
        }

        if (this.raw.contact_details) {
            var contactDetailsStore = Ext.getStore('AdminInterface.store.ContactDetails');
            /*console.error('raw_contact_details:');
            console.error(this.raw.contact_details);*/

            var contactDetailsRecord = contactDetailsStore.getById(this.raw.contact_details.datastore_key_urlsafe);
            if (!contactDetailsRecord) {
                //console.error('if (!contactDetailsRecord) raw_contact_details:');
                contactDetailsRecord = contactDetailsStore.add(this.raw.contact_details)[0];
                contactDetailsRecord.postProcessExFields();
            } else {
                //console.error('else raw_contact_details:');
                contactDetailsRecord.set(this.raw.contact_details);
                contactDetailsRecord.postProcessExFields(true);
                //console.log(contactDetailsRecord.postProcessExFields(true));
            }

            /*console.log(contactDetailsRecord);

            console.error('UserAccount.postProcessExFields(' + forceUpdate + ')');*/

            // ex_full_name
            var exFullName = contactDetailsRecord.get('ex_full_name');
            this.set('ex_full_name', exFullName);

            // ex_user_account
            this.set('ex_user_account', [exFullName, ' (', this.get('user_email'), ')'].join(''));

            // ex_billing_contact
            this.set('ex_billing_contact', contactDetailsRecord.get('ex_billing_contact'));

            // ex_technical_contact
            this.set('ex_technical_contact', contactDetailsRecord.get('ex_technical_contact'));
        }

        // ex_suspended
        if (this.get('suspended')) {
            this.set('ex_suspended', Ext.Date.format(this.get('suspended_datetime'), 'Y-m-d H:i'));
        } else {
            this.set('ex_suspended', 'No');
        }

        // ex_deleted
        if (this.get('deleted')) {
            this.set('ex_deleted', Ext.Date.format(this.get('deleted_datetime'), 'Y-m-d H:i'));
        } else {
            this.set('ex_deleted', 'No');
        }

        // ex_activated
        if (this.get('activated')) {
            this.set('ex_activated', Ext.Date.format(this.get('activated_datetime'), 'Y-m-d H:i'));
        } else {
            this.set('ex_activated', 'No');
        }

        // ex_sys_pending_password_update
        var passwordUpdateExpiration = this.get('sys_pending_password_update_expiration_datetime');
        if (new Date() < passwordUpdateExpiration) {
            var exPendingPasswordUpdate = [
            'Yes (key: ',
            this.get('sys_pending_password_update_key'),
            ' / expires: ',
            Ext.Date.format(passwordUpdateExpiration, 'Y-m-d H:i'),
            ')'
            ].join('');

            this.set('ex_sys_pending_password_update', exPendingPasswordUpdate);
        } else {
            this.set('ex_sys_pending_password_update', "No");
        }

        if (false && this.raw.sys_origin_user_account) {
            // ex_sys_origin_user_account
            var sysOriginUserAccountRecord = this.getSysOriginUserAccount();
            if (sysOriginUserAccountRecord) {
                sysOriginUserAccountRecord.postProcessExFields();
                this.set('ex_sys_origin_user_account', sysOriginUserAccountRecord.get('ex_user_account'));
            }
        }

        if (false && this.raw.sys_origin_organization_account) {
            // ex_sys_origin_organization_account
            var sysOriginOrganizationAccountRecord = this.getSysOriginOrganizationAccount();
            if (sysOriginOrganizationAccountRecord) {
                sysOriginOrganizationAccountRecord.postProcessExFields();
                this.set('ex_sys_origin_organization_account', sysOriginOrganizationAccountRecord.get('ex_organization_account'));
            }
        }

        this.exFieldsInitialized = true;

        return true;

    },

    getFormatted: function() {

    }

});
