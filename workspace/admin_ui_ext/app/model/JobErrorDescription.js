/*
 * File: app/model/JobErrorDescription.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.model.JobErrorDescription', {
    extend: 'Ext.data.Model',

    uses: [
        'AdminInterface.model.JobStatus'
    ],

    fields: [
        {
            name: 'reason',
            type: 'string',
            useNull: true
        },
        {
            name: 'location',
            type: 'string',
            useNull: true
        },
        {
            name: 'debug_info',
            type: 'string',
            useNull: true
        },
        {
            name: 'message',
            type: 'string',
            useNull: true
        }
    ],

    belongsTo: [
        {
            model: 'AdminInterface.model.JobStatus',
            foreignKey: 'error_result'
        },
        {
            associationKey: '',
            model: 'AdminInterface.model.JobStatus',
            foreignKey: 'errors'
        }
    ]
});
