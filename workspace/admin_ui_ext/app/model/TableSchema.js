/*
 * File: app/model/TableSchema.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.model.TableSchema', {
    extend: 'Ext.data.Model',

    uses: [
        'AdminInterface.model.TableResource',
        'AdminInterface.model.TableSchemaItem'
    ],

    fields: [
        {
            name: 'fields',
            type: 'auto',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'AdminInterface.model.TableResource',
        primaryKey: 'datastore_key_id',
        foreignKey: 'schema'
    },

    hasMany: {
        associationKey: 'fields',
        model: 'AdminInterface.model.TableSchemaItem'
    }
});
