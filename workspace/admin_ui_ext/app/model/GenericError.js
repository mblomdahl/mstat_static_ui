/*
 * File: app/model/GenericError.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.model.GenericError', {
    extend: 'Ext.data.Model',

    uses: [
        'AdminInterface.model.OrganizationAccount',
        'AdminInterface.model.GenericErrorDescription'
    ],

    idProperty: 'datastore_key_id',

    fields: [
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'errors',
            type: 'auto',
            useNull: true
        },
        {
            name: 'code',
            type: 'int',
            useNull: true
        },
        {
            name: 'message',
            type: 'string',
            useNull: true
        },
        {
            name: 'sys_owners',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_writers',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_readers',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_user_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_organization_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_changelog_version',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_changelog',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ],

    hasMany: [
        {
            associationKey: 'sys_owners',
            model: 'AdminInterface.model.OrganizationAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_writers',
            model: 'AdminInterface.model.OrganizationAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_readers',
            model: 'AdminInterface.model.OrganizationAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'errors',
            model: 'AdminInterface.model.GenericErrorDescription',
            primaryKey: 'datastore_key_id'
        }
    ]
});
