/*
 * File: app/model/ContactDetails.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.model.ContactDetails', {
    extend: 'Ext.data.Model',

    exFieldsInitialized: false,
    stores: [

    ],
    idProperty: 'datastore_key_urlsafe',

    fields: [
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'parent_datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'parent_datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            convert: function(v, rec) {

                /*console.log('labels:');
                console.log(v);
                console.log(rec);*/

                if (v.length) {
                    if (v.indexOf("billing") !== -1) {
                        rec.set('ex_billing_contact', 'Yes');
                    }

                    if (v.indexOf("tech") !== -1) {
                        rec.set('ex_technical_contact', 'Yes');
                    }
                }

                return v;
            },
            name: 'labels',
            type: 'auto',
            useNull: true
        },
        {
            defaultValue: 'No',
            name: 'ex_billing_contact',
            persist: false,
            type: 'string'
        },
        {
            defaultValue: 'No',
            name: 'ex_technical_contact',
            persist: false,
            type: 'string'
        },
        {
            name: 'personal_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'first_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'last_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'ex_full_name',
            type: 'string'
        },
        {
            name: 'job_title',
            type: 'string',
            useNull: true
        },
        {
            name: 'job_organization_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'job_department_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'job_organization_id',
            type: 'string',
            useNull: true
        },
        {
            convert: function(v, rec) {

                /*console.log('emailAddresses:');
                console.log(v);
                console.log(rec);*/

                var emailAddresses = [];

                Ext.Array.each(v, function(addressEntry, index, dataset) {
                    emailAddresses.push(addressEntry.email_address + ' (' + addressEntry.label + ')');
                });

                rec.set('ex_email_addresses', emailAddresses.join(', '));

                return v;
            },
            name: 'email_addresses',
            type: 'auto',
            useNull: true
        },
        {
            name: 'ex_email_addresses',
            persist: false,
            type: 'string'
        },
        {
            convert: function(v, rec) {

                /*console.log('phoneNumbers:');
                console.log(v);
                console.log(rec);*/

                var phoneNumbers = [];

                Ext.Array.each(v, function(phoneNumberEntry, index, dataset) {
                    phoneNumbers.push(phoneNumberEntry.phone_number + ' (' + phoneNumberEntry.label + ')');
                });

                rec.set('ex_phone_numbers', phoneNumbers.join(', '));

                return v;
            },
            name: 'phone_numbers',
            type: 'auto',
            useNull: true
        },
        {
            name: 'ex_phone_numbers',
            persist: false,
            type: 'string'
        },
        {
            convert: function(v, rec) {

                /*console.log('postalAddresses:');
                console.log(v);
                console.log(rec);*/

                var phoneNumbers = [];

                Ext.Array.each(v, function(phoneNumberEntry, index, dataset) {
                    phoneNumbers.push(phoneNumberEntry.phone_number + ' (' + phoneNumberEntry.label + ')');
                });

                rec.set('ex_phone_numbers', phoneNumbers.join(', '));

                return v;
            },
            name: 'postal_addresses',
            type: 'auto',
            useNull: true
        },
        {
            name: 'ex_postal_addresses',
            persist: false,
            type: 'string'
        },
        {
            name: 'ex_web_addresses',
            persist: false,
            type: 'string'
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'deleted_datetime',
            type: 'date',
            useNull: true
        },
        {
            name: 'deleted',
            type: 'boolean',
            useNull: true
        },
        {
            defaultValue: 'No',
            name: 'ex_deleted',
            persist: false,
            type: 'string'
        },
        {
            name: 'sys_origin_user_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'ex_sys_origin_user_account',
            persist: false,
            type: 'string'
        },
        {
            name: 'sys_origin_organization_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'ex_sys_origin_organization_account',
            persist: false,
            type: 'string'
        },
        {
            name: 'sys_changelog_version',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_changelog',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ],

    getFormatted: function() {

    },

    postProcessExFields: function(forceUpdate) {
        if (this.exFieldsInitialized && !forceUpdate) {
            return false;
        }

        // ex_postal_addresses
        // TODO(mats.blomdahl@gmail.com): Implementation.

        // ex_web_addresses
        // TODO(mats.blomdahl@gmail.com): Implementation.

        // ex_sys_origin_organization_account
        // TODO(mats.blomdahl@gmail.com): Implementation.

        // ex_sys_origin_user_account
        // TODO(mats.blomdahl@gmail.com): Implementation.

        // ex_deleted
        if (this.get('deleted')) {
            this.set('ex_deleted', Ext.Date.format(this.get('deleted_datetime'), 'Y-m-d H:i:s'));
        }

        // ex_full_name
        this.set('ex_full_name', this.get('first_name') + ' ' + this.get('last_name'));

        this.exFieldsInitialized = true;

        return true;
    }

});
