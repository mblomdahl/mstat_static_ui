/*
 * File: app/model/JobLoadConfiguration.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.model.JobLoadConfiguration', {
    extend: 'Ext.data.Model',

    uses: [
        'AdminInterface.model.JobConfiguration'
    ],

    fields: [
        {
            name: 'source_uris',
            type: 'auto',
            useNull: true
        },
        {
            name: 'schema',
            type: 'auto',
            useNull: true
        },
        {
            name: 'destination_table',
            type: 'auto',
            useNull: true
        },
        {
            name: 'create_disposition',
            type: 'string',
            useNull: true
        },
        {
            name: 'write_disposition',
            type: 'string',
            useNull: true
        },
        {
            name: 'field_delimiter',
            type: 'string',
            useNull: true
        },
        {
            name: 'skip_leading_rows',
            type: 'int',
            useNull: true
        },
        {
            name: 'encoding',
            type: 'string',
            useNull: true
        },
        {
            name: 'quote',
            type: 'string',
            useNull: true
        },
        {
            name: 'allow_quoted_newlines',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'source_format',
            type: 'string',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'AdminInterface.model.JobConfiguration',
        foreignKey: 'load'
    }
});
