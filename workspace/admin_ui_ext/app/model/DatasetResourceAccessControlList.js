/*
 * File: app/model/DatasetResourceAccessControlList.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.model.DatasetResourceAccessControlList', {
    extend: 'Ext.data.Model',

    uses: [
        'AdminInterface.model.DatasetResource',
        'AdminInterface.model.DatasetResourceAccessControlListItem'
    ],

    fields: [
        {
            name: 'access',
            type: 'string',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'AdminInterface.model.DatasetResource',
        primaryKey: 'datastore_key_id',
        foreignKey: 'access'
    },

    hasMany: {
        associationKey: 'access',
        model: 'AdminInterface.model.DatasetResourceAccessControlListItem'
    }
});
