/*
 * File: app/model/JobStatus.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.model.JobStatus', {
    extend: 'Ext.data.Model',

    uses: [
        'AdminInterface.model.JobResource',
        'AdminInterface.model.JobErrorDescription'
    ],

    fields: [
        {
            name: 'state',
            type: 'string',
            useNull: true
        },
        {
            name: 'error_result',
            type: 'auto',
            useNull: true
        },
        {
            name: 'errors',
            type: 'auto',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'AdminInterface.model.JobResource',
        primaryKey: 'datastore_key_id',
        foreignKey: 'status'
    },

    hasOne: {
        associationKey: 'error_result',
        model: 'AdminInterface.model.JobErrorDescription'
    },

    hasMany: {
        associationKey: 'errors',
        model: 'AdminInterface.model.JobErrorDescription'
    }
});
