/*
 * File: app/model/TableResourceReference.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.model.TableResourceReference', {
    extend: 'Ext.data.Model',

    uses: [
        'AdminInterface.model.TableResource'
    ],

    idProperty: 'table_id',

    fields: [
        {
            name: 'project_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'dataset_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'table_id',
            type: 'string',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'AdminInterface.model.TableResource',
        primaryKey: 'datastore_key_id',
        foreignKey: 'table_reference'
    }
});
