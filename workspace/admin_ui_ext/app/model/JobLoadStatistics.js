/*
 * File: app/model/JobLoadStatistics.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.model.JobLoadStatistics', {
    extend: 'Ext.data.Model',

    uses: [
        'AdminInterface.model.JobStatistics'
    ],

    fields: [
        {
            name: 'input_files',
            type: 'int',
            useNull: true
        },
        {
            name: 'input_file_bytes',
            type: 'int',
            useNull: true
        },
        {
            name: 'output_rows',
            type: 'int',
            useNull: true
        },
        {
            name: 'output_bytes',
            type: 'int',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'AdminInterface.model.JobStatistics',
        foreignKey: 'load'
    }
});
