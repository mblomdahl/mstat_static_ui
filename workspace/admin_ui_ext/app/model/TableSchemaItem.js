/*
 * File: app/model/TableSchemaItem.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.model.TableSchemaItem', {
    extend: 'Ext.data.Model',

    uses: [
        'AdminInterface.model.TableSchema'
    ],

    fields: [
        {
            name: 'name',
            type: 'string',
            useNull: true
        },
        {
            name: 'type',
            type: 'string',
            useNull: true
        },
        {
            name: 'mode',
            type: 'string',
            useNull: true
        },
        {
            name: 'fields',
            type: 'auto',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'AdminInterface.model.TableSchema',
        foreignKey: 'fields'
    }
});
