/*
 * File: app/model/OrganizationAccount.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.model.OrganizationAccount', {
    extend: 'Ext.data.Model',

    exFieldsInitialized: false,
    stores: [

    ],
    idProperty: 'datastore_key_urlsafe',

    fields: [
        {
            name: 'ex_organization_kind',
            persist: false,
            type: 'string'
        },
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'organization_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'department_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'organization_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'ex_organization_account',
            persist: false,
            type: 'string'
        },
        {
            name: 'contact_details',
            type: 'auto',
            useNull: true
        },
        {
            name: 'ex_contact_details',
            persist: false,
            type: 'string'
        },
        {
            convert: function(v, rec) {

                /*console.log('postalAddresses:');
                console.log(v);
                console.log(rec);*/

                if (v && v.length) {
                    var postalAddresses = [];

                    Ext.Array.each(v, function(addressEntry, index, dataset) {
                        postalAddresses.push(addressEntry.postal_address + ' (' + addressEntry.label + ')');
                    });

                    rec.set('ex_postal_addresses', postalAddresses.join(', '));
                }

                return v;
            },
            name: 'postal_addresses',
            type: 'auto',
            useNull: true
        },
        {
            name: 'ex_postal_addresses',
            persist: false,
            type: 'string'
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'activated_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'registered_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'suspended_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'deleted_datetime',
            type: 'date',
            useNull: true
        },
        {
            name: 'suspended',
            type: 'boolean',
            useNull: true
        },
        {
            defaultValue: 'No',
            name: 'ex_suspended',
            persist: false,
            type: 'string'
        },
        {
            name: 'deleted',
            type: 'boolean',
            useNull: true
        },
        {
            defaultValue: 'No',
            name: 'ex_deleted',
            persist: false,
            type: 'string'
        },
        {
            name: 'activated',
            type: 'boolean',
            useNull: true
        },
        {
            defaultValue: 'No',
            name: 'ex_activated',
            persist: false,
            type: 'string'
        },
        {
            name: 'billing_balance',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_origin_user_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'ex_sys_origin_user_account',
            persist: false,
            type: 'string'
        },
        {
            name: 'sys_origin_organization_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'ex_sys_origin_organization_account',
            persist: false,
            type: 'string'
        },
        {
            name: 'sys_changelog_version',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_changelog',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ],

    postProcessExFields: function(forceUpdate) {
        if (this.exFieldsInitialized && !forceUpdate) {
            return false;
        }

        // ex_user_account
        this.set('ex_organization_account', this.getFormatted());

        // ex_suspended
        if (this.get('suspended')) {
            this.set('ex_suspended', Ext.Date.format(this.get('suspended_datetime'), 'Y-m-d H:i'));
        } else {
            this.set('ex_suspended', "No");
        }

        // ex_deleted
        if (this.get('deleted')) {
            this.set('ex_deleted', Ext.Date.format(this.get('deleted_datetime'), 'Y-m-d H:i'));
        } else {
            this.set('ex_deleted', "No");
        }

        // ex_activated
        if (this.get('activated')) {
            this.set('ex_activated', Ext.Date.format(this.get('activated_datetime'), 'Y-m-d H:i'));
        } else {
            this.set('ex_activated', "No");
        }

        if (false && this.raw.sys_origin_user_account) {
            // ex_sys_origin_user_account
            var sysOriginUserAccountRecord = this.getSysOriginUserAccount();
            if (sysOriginUserAccountRecord) {
                sysOriginUserAccountRecord.postProcessExFields();
                this.set('ex_sys_origin_user_account', sysOriginUserAccountRecord.get('ex_user_account'));
            }
        }

        if (false && this.raw.sys_origin_organization_account) {
            // ex_sys_origin_organization_account
            var sysOriginOrganizationAccountRecord = this.getSysOriginOrganizationAccount();
            if (sysOriginOrganizationAccountRecord) {
                sysOriginOrganizationAccountRecord.postProcessExFields();
                this.set('ex_sys_origin_organization_account', sysOriginOrganizationAccountRecord.get('ex_organization_account'));
            }
        }

        this.exFieldsInitialized = true;

        return true;
    },

    setExOrganizationKindSubscriberAccount: function() {
        this.set('ex_organization_kind', 'SubscriberAccount');
    },

    setExOrganizationKindSystemAdminAccount: function() {
        this.set('ex_organization_kind', 'SystemAdminAccount');
    },

    getFormatted: function(properties) {
        properties = properties || {};

        var organizationAccount = properties.organization_name || this.get('organization_name');

        var departmentName = properties.organization_name || this.get('department_name');
        var organizationId = properties.organization_name || this.get('organization_id');

        organizationAccount += departmentName ? ', ' + departmentName : '';

        organizationAccount += organizationId ? ' (' + organizationId + ')' : '';

        return organizationAccount;
    }

});
