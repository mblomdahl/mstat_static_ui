/*
 * File: app/model/Subscription.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.model.Subscription', {
    extend: 'Ext.data.Model',

    exFieldsInitialized: false,
    stores: [

    ],
    idProperty: 'datastore_key_urlsafe',

    fields: [
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'parent_datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'parent_datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            convert: function(v, rec) {

                var translation = {
                    'data_api_subscription': 'Mäklarstatistik Data API',
                    'widget_service_subscription': 'Statistics Widget Service'
                };

                if (translation[v]) {
                    rec.set('ex_subscription_type', translation[v]);
                }

                return v;
            },
            name: 'subscription_type',
            type: 'string',
            useNull: true
        },
        {
            name: 'ex_subscription_type',
            persist: false,
            type: 'string'
        },
        {
            name: 'friendly_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'description',
            type: 'string',
            useNull: true
        },
        {
            convert: function(v, rec) {

                if (v && v.fields) {
                    rec.set('ex_content_restriction_field_names', v.fields.join(', '));

                    var lowerAccessibleDateLim = v.start_date;
                    var upperAccessibleDateLim = v.end_date || '<i>indefinite</i>'; // Ext.Date.parse(v.end_date, "Y-m-d")
                    rec.set('ex_content_restriction_date_interval', lowerAccessibleDateLim + ' – ' + upperAccessibleDateLim);

                } else {
                    rec.set('ex_content_restrictions', null);
                }

                return v;
            },
            name: 'content_restrictions',
            type: 'auto',
            useNull: true
        },
        {
            name: 'ex_content_restriction_field_names',
            persist: false,
            type: 'string'
        },
        {
            name: 'ex_content_restriction_date_interval',
            persist: false,
            type: 'string'
        },
        {
            name: 'api_access_key',
            type: 'string',
            useNull: true
        },
        {
            name: 'quota',
            type: 'int',
            useNull: true
        },
        {
            dateFormat: 'Y-m-d',
            name: 'start_date',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-d',
            name: 'end_date',
            type: 'auto',
            useNull: true
        },
        {
            convert: function(v, rec) {

                var translation = {
                    'subscription_daily_update_frequency': 'Daily',
                    'subscription_weekly_update_frequency': 'Weekly',
                    'subscription_monthly_update_frequency': 'Monthly'
                };

                if (translation[v]) {
                    rec.set('ex_update_frequency', translation[v]);
                }

                return v;
            },
            name: 'update_frequency',
            type: 'string',
            useNull: true
        },
        {
            name: 'ex_update_frequency',
            persist: false,
            type: 'string'
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'registered_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'suspended_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'deleted_datetime',
            type: 'date',
            useNull: true
        },
        {
            name: 'suspended',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'ex_suspended',
            persist: false,
            type: 'string'
        },
        {
            name: 'deleted',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'ex_deleted',
            persist: false,
            type: 'string'
        },
        {
            name: 'sys_origin_user_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'ex_sys_origin_user_account',
            persist: false,
            type: 'string'
        },
        {
            name: 'sys_origin_organization_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'ex_sys_origin_organization_account',
            persist: false,
            type: 'string'
        },
        {
            name: 'sys_changelog_version',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_changelog',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ],

    postProcessExFields: function(forceUpdate) {
        if (this.exFieldsInitialized && !forceUpdate) {
            return false;
        }

        // ex_suspended
        if (this.get('suspended')) {
            this.set('ex_suspended', Ext.Date.format(this.get('suspended_datetime'), 'Y-m-d H:i'));
        } else {
            this.set('ex_suspended', 'No');
        }

        // ex_deleted
        if (this.get('deleted')) {
            this.set('ex_deleted', Ext.Date.format(this.get('deleted_datetime'), 'Y-m-d H:i'));
        } else {
            this.set('ex_deleted', 'No');
        }

        if (false && this.raw.sys_origin_user_account) {
            // ex_sys_origin_user_account
            var sysOriginUserAccountRecord = this.getSysOriginUserAccount();
            if (sysOriginUserAccountRecord) {
                sysOriginUserAccountRecord.postProcessExFields();
                this.set('ex_sys_origin_user_account', sysOriginUserAccountRecord.get('ex_user_account'));
            }
        }

        if (false && this.raw.sys_origin_organization_account) {
            // ex_sys_origin_organization_account
            var sysOriginOrganizationAccountRecord = this.getSysOriginOrganizationAccount();
            if (sysOriginOrganizationAccountRecord) {
                sysOriginOrganizationAccountRecord.postProcessExFields();
                this.set('ex_sys_origin_organization_account', sysOriginOrganizationAccountRecord.get('ex_organization_account'));
            }
        }

        this.exFieldsInitialized = true;

        return true;
    },

    getFormatted: function() {

    }

});
