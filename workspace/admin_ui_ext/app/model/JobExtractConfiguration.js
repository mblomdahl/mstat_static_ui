/*
 * File: app/model/JobExtractConfiguration.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.model.JobExtractConfiguration', {
    extend: 'Ext.data.Model',

    uses: [
        'AdminInterface.model.JobConfiguration'
    ],

    fields: [
        {
            name: 'source_table',
            type: 'auto',
            useNull: true
        },
        {
            name: 'destination_uris',
            type: 'auto',
            useNull: true
        },
        {
            name: 'field_delimiter',
            type: 'string',
            useNull: true
        },
        {
            name: 'print_header',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'destination_format',
            type: 'string',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'AdminInterface.model.JobConfiguration',
        foreignKey: 'extract'
    }
});
