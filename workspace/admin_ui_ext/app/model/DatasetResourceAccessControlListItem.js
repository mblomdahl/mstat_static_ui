/*
 * File: app/model/DatasetResourceAccessControlListItem.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.model.DatasetResourceAccessControlListItem', {
    extend: 'Ext.data.Model',

    uses: [
        'AdminInterface.model.DatasetResourceAccessControlList'
    ],

    fields: [
        {
            name: 'role',
            type: 'string',
            useNull: true
        },
        {
            name: 'user_by_email',
            type: 'string',
            useNull: true
        },
        {
            name: 'group_by_email',
            type: 'string',
            useNull: true
        },
        {
            name: 'domain',
            type: 'string',
            useNull: true
        },
        {
            name: 'special_group',
            type: 'string',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'AdminInterface.model.DatasetResourceAccessControlList',
        primaryKey: 'datastore_key_id',
        foreignKey: 'access'
    }
});
