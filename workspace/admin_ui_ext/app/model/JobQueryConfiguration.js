/*
 * File: app/model/JobQueryConfiguration.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.model.JobQueryConfiguration', {
    extend: 'Ext.data.Model',

    uses: [
        'AdminInterface.model.JobConfiguration'
    ],

    fields: [
        {
            name: 'query',
            type: 'string',
            useNull: true
        },
        {
            name: 'destination_table',
            type: 'auto',
            useNull: true
        },
        {
            name: 'create_disposition',
            type: 'string',
            useNull: true
        },
        {
            name: 'write_disposition',
            type: 'string',
            useNull: true
        },
        {
            name: 'default_dataset',
            type: 'auto',
            useNull: true
        },
        {
            name: 'priority',
            type: 'string',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'AdminInterface.model.JobConfiguration',
        foreignKey: 'query'
    }
});
