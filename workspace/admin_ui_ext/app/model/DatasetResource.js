/*
 * File: app/model/DatasetResource.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.model.DatasetResource', {
    extend: 'Ext.data.Model',

    uses: [
        'AdminInterface.model.DatasetResourceReference',
        'AdminInterface.model.DatasetResourceAccessControlList',
        'AdminInterface.model.OrganizationAccount'
    ],

    idProperty: 'datastore_key_id',

    fields: [
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'kind',
            type: 'string',
            useNull: true
        },
        {
            name: 'etag',
            type: 'string',
            useNull: true
        },
        {
            name: 'id',
            type: 'string',
            useNull: true
        },
        {
            name: 'self_link',
            type: 'string',
            useNull: true
        },
        {
            name: 'access',
            type: 'auto',
            useNull: true
        },
        {
            name: 'dataset_reference',
            type: 'auto',
            useNull: true
        },
        {
            name: 'friendly_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'description',
            type: 'string',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'last_modified_time',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'creation_time',
            type: 'date',
            useNull: true
        },
        {
            name: 'sys_owners',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_writers',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_readers',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_user_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_organization_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_changelog_version',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_changelog',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ],

    hasOne: [
        {
            associationKey: 'dataset_reference',
            model: 'AdminInterface.model.DatasetResourceReference',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'access',
            model: 'AdminInterface.model.DatasetResourceAccessControlList'
        }
    ],

    hasMany: [
        {
            associationKey: 'sys_owners',
            model: 'AdminInterface.model.OrganizationAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_writers',
            model: 'AdminInterface.model.OrganizationAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_readers',
            model: 'AdminInterface.model.OrganizationAccount',
            primaryKey: 'datastore_key_id'
        }
    ]
});
