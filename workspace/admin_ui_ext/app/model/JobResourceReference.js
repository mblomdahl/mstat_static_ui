/*
 * File: app/model/JobResourceReference.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.model.JobResourceReference', {
    extend: 'Ext.data.Model',

    uses: [
        'AdminInterface.model.JobResource'
    ],

    idProperty: 'job_id',

    fields: [
        {
            name: 'project_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'job_id',
            type: 'string',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'AdminInterface.model.JobResource',
        primaryKey: 'datastore_key_id',
        foreignKey: 'job_reference'
    }
});
