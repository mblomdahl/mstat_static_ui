/*
 * File: app/model/JobStatistics.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.model.JobStatistics', {
    extend: 'Ext.data.Model',

    uses: [
        'AdminInterface.model.JobResource',
        'AdminInterface.model.JobLoadStatistics',
        'AdminInterface.model.JobQueryStatistics'
    ],

    fields: [
        {
            name: 'query',
            type: 'auto',
            useNull: true
        },
        {
            name: 'load',
            type: 'auto',
            useNull: true
        },
        {
            name: 'total_bytes_processed',
            type: 'int',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'start_time',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'end_time',
            type: 'date',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'AdminInterface.model.JobResource',
        primaryKey: 'datastore_key_id',
        foreignKey: 'statistics'
    },

    hasOne: [
        {
            associationKey: 'load',
            model: 'AdminInterface.model.JobLoadStatistics'
        },
        {
            associationKey: 'query',
            model: 'AdminInterface.model.JobQueryStatistics'
        }
    ]
});
