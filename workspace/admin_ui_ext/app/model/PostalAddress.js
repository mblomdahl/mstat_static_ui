/*
 * File: app/model/PostalAddress.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.model.PostalAddress', {
    extend: 'Ext.data.Model',

    exFieldsInitialized: false,
    stores: [

    ],
    idProperty: 'datastore_key_urlsafe',

    fields: [
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'parent_datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'parent_datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'labels',
            type: 'auto',
            useNull: true
        },
        {
            name: 'recipient_lines',
            type: 'auto',
            useNull: true
        },
        {
            name: 'box_address',
            type: 'string',
            useNull: true
        },
        {
            name: 'route_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'street_number',
            type: 'string',
            useNull: true
        },
        {
            name: 'street_entrance',
            type: 'string',
            useNull: true
        },
        {
            name: 'apartment_number',
            type: 'string',
            useNull: true
        },
        {
            name: 'apartment_floor',
            type: 'float',
            useNull: true
        },
        {
            name: 'postal_code',
            type: 'string',
            useNull: true
        },
        {
            name: 'postal_town',
            type: 'string',
            useNull: true
        },
        {
            name: 'country',
            type: 'string',
            useNull: true
        },
        {
            name: 'formatted_address',
            type: 'string',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'deleted_datetime',
            type: 'date',
            useNull: true
        },
        {
            name: 'deleted',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'sys_origin_user_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_organization_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_changelog_version',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_changelog',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ],

    postProcessExFields: function(forceUpdate) {
        if (this.exFieldsInitialized && !forceUpdate) {
            return false;
        }

        this.exFieldsInitialized = true;

        return true;
    },

    getFormatted: function(properties) {

    }

});
