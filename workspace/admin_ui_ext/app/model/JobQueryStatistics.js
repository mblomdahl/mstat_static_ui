/*
 * File: app/model/JobQueryStatistics.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.model.JobQueryStatistics', {
    extend: 'Ext.data.Model',

    uses: [
        'AdminInterface.model.JobStatistics'
    ],

    fields: [
        {
            name: 'total_bytes_processed',
            type: 'int',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'AdminInterface.model.JobStatistics',
        foreignKey: 'query'
    }
});
