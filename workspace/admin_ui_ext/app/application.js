Ext.Loader.setConfig({
    disableCaching: false,
    enabled: true
});

Ext.ns('Ext.app.REMOTING_API');

Ext.app.REMOTING_API = {
    "descriptor": "Ext.app.REMOTING_API",
    "url": "/admin/api/ext_direct",
    "type": "remoting",
    "namespace": "Remote",
    "id": "AdminCmd",
    "actions": {
        "AdminCmd": [
            {'name': "about_me", "len": 1},
            {'name': "activate_safe_email_update", "len": 1},
            {'name': "activate_safe_password_update", "len": 1},
            {'name': "add_or_edit_subscriber_account", "len": 1},
            {'name': "add_or_edit_subscription", "len": 1},
            {'name': "add_or_edit_user_account", "len": 1},
            {'name': "admin_login", "len": 1},
            {'name': "chart_data", "len": 1},
            {'name': "delete_subscriber_account", "len": 1},
            {'name': "delete_subscription", "len": 1},
            {'name': "delete_user_account", "len": 1},
            {'name': "grid_data", "len": 1},
            {'name': "logout", "len": 1},
            {'name': "memcache", "len": 1},
            {'name': "register_safe_email_update", "len": 1},
            {'name': "register_safe_password_update", "len": 1},
            {'name': "send_email_notification", "len": 1},
            {'name': "subscriber_login", "len": 1},
            {'name': "suspend_subscriber_account", "len": 1},
            {'name': "suspend_subscription", "len": 1},
            {'name': "suspend_user_account", "len": 1},
            {'name': "unsuspend_subscriber_account", "len": 1},
            {'name': "unsuspend_subscription", "len": 1},
            {'name': "unsuspend_user_account", "len": 1},
            {'name': "verify_safe_password_update_key", "len": 1}
        ]
    }
};

Ext.require('Ext.data.proxy.Direct');
Ext.require('Ext.grid.RowNumberer');
Ext.require('Ext.form.Label');
Ext.require('Ext.form.Panel');
Ext.require('Ext.form.FieldSet');
Ext.require('Ext.form.RadioGroup');
Ext.require('Ext.form.field.Radio');
Ext.require('Ext.slider.Single');
Ext.require('Ext.layout.container.Column');
Ext.require('Ext.layout.container.Table');
Ext.require('Ext.tab.Panel');
Ext.require('Ext.direct.*');
Ext.syncRequire('Ext.direct.RemotingProvider');
Ext.syncRequire('Ext.direct.Manager', function() {
    Ext.direct.Manager.addProvider(Ext.app.REMOTING_API);
});

Ext.define('AdminInterface.Application', {
    name: 'AdminInterface',

    extend: 'Ext.app.Application',

    globals: {

    },

    session: {
        systemAdminAccountRecord: null,
        userAccountRecord: null
    },

    loggedIn: false,

    debug: false || window.location.pathname.search("/dev/workspace/") != -1,

    views: [
        /*'AddSubscriberWindow',
        'RouteNameTextField',
        'BoxAddressTextField',
        'SubscriberAccountCombobox',
        'StreetEntranceTextField',
        'StreetNumberTextField',
        'OrganizationNameTextField',
        'PostalAddressContainer',
        'ContactDetailsContainer',
        'PersonNameTextField',
        'EmailAddressTextField',
        'PhoneNumberTextField',
        'WebAddressTextField',
        'SubscriberAccountsGridPanel',
        'SystemAdminAccountsGridPanel',
        'UserAccountsGridPanel',
        'ContactDetailsGridPanel',
        'PostalAddressesGridPanel',
        'WebAddressesGridPanel',
        'DatasetResourcesGridPanel',
        'TableResourcesGridPanel',
        'JobResourcesGridPanel',
        'GenericErrorsGridPanel',
        'GridPanelRefreshTool',
        'ManageSubscribersTabPanel',
        'GenericActionButton',
        'AddUserWindow',
        'AddSubscriptionWindow',
        'GenericDateField',
        'SubscriptionsGridPanel',
        'FieldInclusionCheckbox',
        'ManageSubscriptionsTabPanel',
        'SystemPerformanceTabPanel',
        'RegistrationNumberTextField',
        'ViewUserWindow',
        'GenericDisplayField',
        'DatastoreKeyIdColumn',
        'DatastoreKeyUrlsafeColumn',
        'OrganizationNameColumn',
        'DepartmentNameColumn',
        'OrganizationIdColumn',
        'ExOrganizationKind',
        'ActivatedDatetimeColumn',
        'PostalAddressesColumn',
        'ContactDetailsColumn',
        'RegisteredDatetimeColumn',
        'SuspendedDatetimeColumn',
        'DeletedDatetimeColumn',
        'SuspendedColumn',
        'DeletedColumn',
        'ActivatedColumn',
        'BillingBalanceColumn',
        'ExUserAccountColumn',
        'OriginOrganizationAccountColumn',
        'SysChangelogVersion',
        'SysChangelog',
        'SysModifiedColumn',
        'SysCreatedColumn',
        'ExOrganizationAccountColumn',
        'ExPostalAddressesColumn',
        'ExSuspendedColumn',
        'ExDeletedColumn',
        'ExActivatedColumn',
        'ExSysOriginUserAccountColumn',
        'ExSysOriginOrganizationAccountColumn',
        'LastVisitDatetimeColumn',
        'AdministratorColumn',
        'LastClientIpColumn',
        'SysUserRoleColumn',
        'SysSessionIdColumn',
        'SysPendingEmailUpdateAddressColumn',
        'SysPendingEmailUpdateKeyColumn',
        'SysPendingPasswordUpdateKeyColumn',
        'SysPendingPasswordUpdateExpirationDatetimeColumn',
        'ParentDatastoreKeyIdColumn',
        'ParentDatastoreKeyUrlsafeColumn',
        'UserEmailColumn',
        'UserIdColumn',
        'GoogleUserIdColumn',
        'GoogleUserColumn',
        'PasswordColumn',
        'ApiAccessKeyColumn',
        'PasswordChangedDatetimeColumn',
        'ExFullNameColumn',
        'ExBillingContactColumn',
        'ExTechnicalContactColumn',
        'ExSysUserRoleColumn',
        'ExSysPendingPasswordUpdateColumn',
        'SubscriptionTypeColumn',
        'FriendlyNameColumn',
        'DescriptionColumn',
        'ContentRestrictionsColumn',
        'QuotaColumn',
        'ExUpdateFrequencyColumn',
        'StartDateColumn',
        'EndDateColumn',
        'ViewSubscriberWindow',
        'ViewSubscriptionWindow',
        'EditSubscriberWindow'*/
    ],

    models: [
        /*'OrganizationAccount',
        'UserAccount',
        'ContactDetails',
        'PostalAddress',
        'WebAddress',
        'DatasetResource',
        'DatasetResourceReference',
        'DatasetResourceAccessControlListItem',
        'DatasetResourceAccessControlList',
        'GenericError',
        'GenericErrorDescription',
        'JobResource',
        'JobResourceReference',
        'JobStatus',
        'JobStatistics',
        'JobConfiguration',
        'JobLoadConfiguration',
        'JobExtractConfiguration',
        'JobCopyConfiguration',
        'JobQueryConfiguration',
        'JobErrorDescription',
        'JobLoadStatistics',
        'JobQueryStatistics',
        'TableDataResource',
        'TableResource',
        'TableResourceReference',
        'TableSchema',
        'TableSchemaItem',
        'Subscription'*/
    ],

    controllers: [
        'ManageSubscribersCtrl',
        'ContactDetailsCtrl',
        'PostalAddressCtrl',
        'AdminInterfaceCtrl',
        'AddSubscriberCtrl',
        'AddUserCtrl',
        'AddSubscriptionCtrl',
        'OrganizationDetailsCtrl',
        'ViewUserCtrl',
        'ViewSubscriberCtrl',
        'ViewSubscriptionCtrl',
        'EditSubscriberCtrl'
    ],

    stores: [
        /*'SubscriberAccounts',
        'UserAccounts',
        'ContactDetails',
        'PostalAddresses',
        'WebAddresses',
        'DatasetResources',
        'TableResources',
        'JobResources',
        'GenericErrors',
        'TableDataResources',
        'MyArrayStore',
        'ApartmentFloors',
        'SystemAdminAccounts',
        'EmptySet',
        'Subscriptions'*/
    ],

    launch: function() {
        /*
         Ext.override(Ext.view.AbstractView,{
         onUnbindStore: function(store) {
         var mask = this.loadMask;
         if (mask && mask.isComponent) { // if (!store && mask) {
         mask.bindStore(null);
         }
         }
         });
         */

        //this.fireEvent
        // failed. see http://www.sencha.com/forum/archive/index.php/t-158566.html?s=d787b7aa2c859249504ec4880b1cd392 for in-depth discussion

        if (this.debug) {
            userAccountsStore = Ext.getStore('UserAccounts');
            systemAdminAccountsStore = Ext.getStore('SystemAdminAccounts');
            subscriptionsStore = Ext.getStore('Subscriptions');
            contactDetailsStore = Ext.getStore('ContactDetails');
            postalAddressesStore = Ext.getStore('PostalAddresses');
            subscriberAccountsStore = Ext.getStore('SubscriberAccounts');
            webAddressesStore = Ext.getStore('WebAddresses');
        }

        if (!this.loggedIn) {
            this.fireEvent('exLogin');
        }

        if (this.debug) {
            app = this;
        }
    },

    optDebug: function(label, data) {

        if (!this.debug) {
            if (arguments[arguments.length - 1] !== 'override') {
                return;
            }
        }

        console.error(new Date() + label);
        if (arguments.length > 1) {
            for (var i = 1, j = arguments.length; i < j; i++) {
                if (arguments[i] === 'override') {
                    break;
                } else {
                    console.log(arguments[i]);
                }
            }
        }

    }

});
