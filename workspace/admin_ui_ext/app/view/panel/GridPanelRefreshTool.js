/*
 * File: app/view/panel/GridPanelRefreshTool.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.panel.GridPanelRefreshTool', {
    extend: 'Ext.panel.Tool',
    alias: 'widget.gridpanelrefreshtool',

    type: 'refresh',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            listeners: {
                click: {
                    fn: me.onToolClick,
                    scope: me
                }
            }
        });

        me.callParent(arguments);
    },

    onToolClick: function(tool, e, eOpts) {


        //console.log('yah, refresh \'em');

        var gridPanel = tool.up('gridpanel');

        //console.log(gridPanel);

        var store = gridPanel.getStore();

        //console.log('pre-(re)load store:');
        //console.log(store);

        if (store.getCount()) {
            store.reload();
        } else {
            store.prefetch({limit: 100});
            store.load();
        }

        //store.reload();

        //console.log('post-(re)load store:');
        //console.log(store);

        if (!window.mstatDebug) {
            window.mstatDebug = {};
        }

        window.mstatDebug.refreshGridPanel = gridPanel;

    }

});
