/*
 * File: app/view/panel/InteractiveDataExplorerTabPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.panel.InteractiveDataExplorerTabPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.interactivedataexplorertabpanel',

    requires: [
        'AdminInterface.view.grid.SubscriberAccountsGridPanel',
        'AdminInterface.view.grid.SystemAdminAccountsGridPanel',
        'AdminInterface.view.grid.UserAccountsGridPanel',
        'AdminInterface.view.grid.ContactDetailsGridPanel',
        'AdminInterface.view.grid.PostalAddressesGridPanel',
        'AdminInterface.view.grid.WebAddressesGridPanel',
        'AdminInterface.view.grid.DatasetResourcesGridPanel',
        'AdminInterface.view.grid.TableResourcesGridPanel',
        'AdminInterface.view.grid.JobResourcesGridPanel',
        'AdminInterface.view.grid.GenericErrorsGridPanel'
    ],

    layout: {
        align: 'stretch',
        type: 'vbox'
    },
    title: 'Data Explorer',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'panel',
                    items: [
                        {
                            xtype: 'button',
                            margin: '20 0 0 20',
                            padding: '',
                            width: 187,
                            text: 'TransactionCompositeEntries',
                            listeners: {
                                click: {
                                    fn: me.onButtonClick,
                                    scope: me
                                }
                            }
                        },
                        {
                            xtype: 'button',
                            margin: '20 0 0 20',
                            width: 141,
                            text: 'SfdRawDataEntries',
                            listeners: {
                                click: {
                                    fn: me.onButtonClick1,
                                    scope: me
                                }
                            }
                        },
                        {
                            xtype: 'button',
                            margin: '20 0 0 20',
                            width: 126,
                            text: 'ScbRawDataEntries',
                            listeners: {
                                click: {
                                    fn: me.onButtonClick2,
                                    scope: me
                                }
                            }
                        },
                        {
                            xtype: 'button',
                            margin: '20 0 0 20',
                            width: 156,
                            text: 'CapitexRawDataEntries',
                            listeners: {
                                click: {
                                    fn: me.onButtonClick3,
                                    scope: me
                                }
                            }
                        },
                        {
                            xtype: 'form',
                            padding: '20 0 20 20',
                            items: [
                                {
                                    xtype: 'combobox',
                                    fieldLabel: 'Table',
                                    name: 'kind',
                                    displayField: 'name',
                                    forceSelection: true,
                                    queryMode: 'local',
                                    store: 'MyArrayStore',
                                    typeAhead: true,
                                    typeAheadDelay: 50,
                                    valueField: 'value'
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Start date',
                                    name: 'start_date',
                                    allowBlank: false,
                                    emptyText: 'YYYY-MM-DD'
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'End date',
                                    name: 'end_date',
                                    allowBlank: false,
                                    emptyText: 'YYYY-MM-DD'
                                },
                                {
                                    xtype: 'button',
                                    margin: '20 0 0 20',
                                    width: 110,
                                    text: 'Download',
                                    listeners: {
                                        click: {
                                            fn: me.onButtonClick4,
                                            scope: me
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'tabpanel',
                    flex: 1,
                    activeTab: 0,
                    items: [
                        {
                            xtype: 'panel',
                            layout: {
                                type: 'card'
                            },
                            title: 'SubscriberAccounts',
                            items: [
                                {
                                    xtype: 'subscriberaccountsgridpanel'
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            layout: {
                                type: 'card'
                            },
                            title: 'SystemAdminAccounts',
                            items: [
                                {
                                    xtype: 'systemadminaccountsgridpanel'
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            layout: {
                                type: 'card'
                            },
                            title: 'UserAccounts',
                            items: [
                                {
                                    xtype: 'useraccountsgridpanel'
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            layout: {
                                type: 'card'
                            },
                            title: 'ContactDetails',
                            items: [
                                {
                                    xtype: 'contactdetailsgridpanel'
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            layout: {
                                type: 'card'
                            },
                            title: 'PostalAddresses',
                            items: [
                                {
                                    xtype: 'postaladdressesgridpanel'
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            layout: {
                                type: 'card'
                            },
                            title: 'WebAddresses',
                            items: [
                                {
                                    xtype: 'webaddressesgridpanel'
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            layout: {
                                type: 'card'
                            },
                            title: 'DatasetResources',
                            items: [
                                {
                                    xtype: 'datasetresourcesgridpanel'
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            layout: {
                                type: 'card'
                            },
                            title: 'TableResources',
                            items: [
                                {
                                    xtype: 'tableresourcesgridpanel'
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            layout: {
                                type: 'card'
                            },
                            title: 'JobResources',
                            items: [
                                {
                                    xtype: 'jobresourcesgridpanel'
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            layout: {
                                type: 'card'
                            },
                            title: 'GenericErrors',
                            items: [
                                {
                                    xtype: 'genericerrorsgridpanel'
                                }
                            ]
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    },

    onButtonClick: function(button, e, eOpts) {
        window.location.href = 'http://maklarstatistik.appspot.com/playground/transaction_composite_entries/app.html';
    },

    onButtonClick1: function(button, e, eOpts) {

        window.location.href = 'http://maklarstatistik.appspot.com/playground/sfd_raw_data_entries/app.html';
    },

    onButtonClick2: function(button, e, eOpts) {

        window.location.href = 'http://maklarstatistik.appspot.com/playground/scb_raw_data_entries/app.html';
    },

    onButtonClick3: function(button, e, eOpts) {

        window.location.href = 'http://maklarstatistik.appspot.com/playground/capitex_raw_data_entries/app.html';
    },

    onButtonClick4: function(button, e, eOpts) {

        values = button.up('form').getValues();

        window.location.href = 'http://hps.maklarstatistik.appspot.com/backend/hps/process_transaction_data_to_csv?kind='+values.kind+'&start_date='+values.start_date+'&end_date='+values.end_date+'';
    }

});
