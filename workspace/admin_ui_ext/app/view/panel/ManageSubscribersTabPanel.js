/*
 * File: app/view/panel/ManageSubscribersTabPanel.js
 * Author: Mats Blomdahl
 * Version: 1.06
 */

Ext.define('AdminInterface.view.panel.ManageSubscribersTabPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.managesubscriberstabpanel',

    requires: [
        'AdminInterface.view.grid.SubscriberAccountsGridPanel',
        'AdminInterface.view.grid.UserAccountsGridPanel',
        'AdminInterface.view.grid.SubscriptionsGridPanel',
        'AdminInterface.view.button.GenericActionButton'
    ],

    layout: {
        align: 'stretch',
        type: 'vbox'
    },
    title: 'Manage Subscribers',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    flex: 2,
                    id: 'manageSubscribersSelectSubscriberContainer',
                    minHeight: 220,
                    layout: {
                        align: 'stretch',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'subscriberaccountsgridpanel',
                            title: 'Accounts',
                            flex: 1,
                            margins: '10 0 10 10'
                        },
                        {
                            xtype: 'container',
                            margins: '10 0 0 0',
                            layout: {
                                align: 'stretch',
                                type: 'vbox'
                            },
                            items: [
                                {
                                    xtype: 'genericactionbutton',
                                    height: 30,
                                    id: 'manageSubscribersAddSubscriberButton',
                                    text: 'Add Account'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    height: 30,
                                    id: 'manageSubscribersEditSubscriberButton',
                                    text: 'Edit Account'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    height: 30,
                                    id: 'manageSubscribersSuspendSubscriberButton',
                                    text: 'Suspend Account'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    height: 30,
                                    id: 'manageSubscribersUnsuspendSubscriberButton',
                                    text: '&nbsp;Un-suspend Account'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    height: 30,
                                    id: 'manageSubscribersDeleteSubscriberButton',
                                    text: 'Delete Account'
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'container',
                    flex: 1,
                    id: 'manageSubscribersSelectUserContainer',
                    minHeight: 220,
                    layout: {
                        align: 'stretch',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'useraccountsgridpanel',
                            title: 'Users',
                            flex: 1,
                            margins: '10 0 10 10'
                        },
                        {
                            xtype: 'container',
                            margins: '10 0 0 0',
                            layout: {
                                align: 'stretch',
                                type: 'vbox'
                            },
                            items: [
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    height: 30,
                                    id: 'manageSubscribersAddUserButton',
                                    text: 'Add User'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    height: 30,
                                    id: 'manageSubscribersEditUserButton',
                                    text: 'Edit User'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    height: 30,
                                    id: 'manageSubscribersSuspendUserButton',
                                    text: 'Suspend User'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    height: 30,
                                    id: 'manageSubscribersUnsuspendUserButton',
                                    text: 'Un-suspend User'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    height: 30,
                                    id: 'manageSubscribersDeleteUserButton',
                                    text: 'Delete User'
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'container',
                    flex: 1,
                    id: 'manageSubscribersSelectSubscriptionContainer',
                    minHeight: 220,
                    layout: {
                        align: 'stretch',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'subscriptionsgridpanel',
                            flex: 1,
                            margins: '10 0 10 10'
                        },
                        {
                            xtype: 'container',
                            margins: '10 0 0 0',
                            layout: {
                                align: 'stretch',
                                type: 'vbox'
                            },
                            items: [
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    height: 30,
                                    id: 'manageSubscribersAddSubscriptionButton',
                                    text: 'Add Subscription'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    height: 30,
                                    id: 'manageSubscribersEditSubscriptionButton',
                                    text: 'Edit Subscription'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    height: 30,
                                    id: 'manageSubscribersSuspendSubscriptionButton',
                                    text: 'Suspend Subscription'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    height: 30,
                                    id: 'manageSubscribersUnsuspendSubscriptionButton',
                                    text: 'Un-suspend Subscription'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    height: 30,
                                    id: 'manageSubscribersDeleteSubscriptionButton',
                                    text: 'Delete Subscription'
                                }
                            ]
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
