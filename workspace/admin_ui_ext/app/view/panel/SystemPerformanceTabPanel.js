/*
 * File: app/view/panel/SystemPerformanceTabPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.panel.SystemPerformanceTabPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.systemperformancetabpanel',

    title: 'System Performance',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
