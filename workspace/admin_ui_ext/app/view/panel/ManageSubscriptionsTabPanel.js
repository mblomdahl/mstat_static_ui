/*
 * File: app/view/panel/ManageSubscriptionsTabPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.panel.ManageSubscriptionsTabPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.managesubscriptionstabpanel',

    layout: {
        align: 'stretch',
        type: 'vbox'
    },
    title: 'Manage Subscriptions',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
