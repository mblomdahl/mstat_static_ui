/*
 * File: app/view/button/GenericActionButton.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.button.GenericActionButton', {
    extend: 'Ext.button.Button',
    alias: 'widget.genericactionbutton',

    margin: '0 10 10 10',
    minWidth: 175,
    arrowAlign: 'bottom',
    iconAlign: 'bottom',
    text: 'Action',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
