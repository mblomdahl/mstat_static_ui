/*
 * File: app/view/button/GenericBackButton.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.button.GenericBackButton', {
    extend: 'Ext.button.Button',
    alias: 'widget.genericbackbutton',

    margin: 10,
    minWidth: 75,
    text: 'Back',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
