/*
 * File: app/view/button/GenericCancelButton.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.button.GenericCancelButton', {
    extend: 'Ext.button.Button',
    alias: 'widget.genericcancelbutton',

    margin: 10,
    minWidth: 75,
    text: 'Cancel',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
