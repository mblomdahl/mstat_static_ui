/*
 * File: app/view/button/GenericNextButton.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.button.GenericNextButton', {
    extend: 'Ext.button.Button',
    alias: 'widget.genericnextbutton',

    margin: 10,
    minWidth: 125,
    text: 'Next',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
