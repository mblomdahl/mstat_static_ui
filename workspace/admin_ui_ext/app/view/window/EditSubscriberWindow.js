/*
 * File: app/view/window/EditSubscriberWindow.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.window.EditSubscriberWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.editsubscriberwindow',

    requires: [
        'AdminInterface.view.form.field.OrganizationNameTextField',
        'AdminInterface.view.form.field.RegistrationNumberTextField',
        'AdminInterface.view.button.GenericCancelButton',
        'AdminInterface.view.button.GenericBackButton',
        'AdminInterface.view.button.GenericNextButton'
    ],

    height: 718,
    id: 'editSubscriberWindow',
    width: 560,
    layout: {
        align: 'stretch',
        type: 'vbox'
    },
    bodyBorder: false,
    bodyPadding: '10 10 0 10',
    title: 'Edit Subscriber Account',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    flex: 1,
                    id: 'editSubscriberStepContainer',
                    margin: 0,
                    layout: {
                        type: 'card'
                    },
                    items: [
                        {
                            xtype: 'container',
                            id: 'editSubscriberStep1Container',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: 0,
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            html: '<h3>Step 1: Supply Account Company Name and Address Details</h3>',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Company Name and Address ( * marks required fields)',
                                    items: [
                                        {
                                            xtype: 'form',
                                            id: 'editSubscriberCompanyNameAndRegistrationNumberFormPanel',
                                            margin: 0,
                                            layout: {
                                                align: 'stretch',
                                                type: 'hbox'
                                            },
                                            bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                            items: [
                                                {
                                                    xtype: 'organizationnametextfield',
                                                    fieldLabel: 'Company Name*',
                                                    labelAlign: 'top',
                                                    name: 'organization_name',
                                                    allowBlank: false,
                                                    flex: 2,
                                                    margins: '0 5 10 0'
                                                },
                                                {
                                                    xtype: 'organizationnametextfield',
                                                    fieldLabel: 'Company Department',
                                                    labelAlign: 'top',
                                                    name: 'department_name',
                                                    flex: 2,
                                                    margins: '0 5 10 5'
                                                },
                                                {
                                                    xtype: 'registrationnumbertextfield',
                                                    fieldLabel: 'Organization ID',
                                                    labelAlign: 'top',
                                                    name: 'organization_id',
                                                    flex: 1.3,
                                                    margins: '0 0 10 5'
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'panel',
                    dock: 'bottom',
                    frame: true,
                    margin: 0,
                    layout: {
                        align: 'stretch',
                        pack: 'end',
                        type: 'hbox'
                    },
                    bodyPadding: 0,
                    items: [
                        {
                            xtype: 'genericcancelbutton'
                        },
                        {
                            xtype: 'genericbackbutton'
                        },
                        {
                            xtype: 'genericnextbutton',
                            text: 'Submit Changes'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
