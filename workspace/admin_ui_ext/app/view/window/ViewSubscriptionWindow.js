/*
 * File: app/view/window/ViewSubscriptionWindow.js
 * Author: Mats Blomdahl
 * Version: 1.06
 */

Ext.define('AdminInterface.view.window.ViewSubscriptionWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.viewsubscriptionwindow',

    requires: [
        'AdminInterface.view.form.field.GenericDisplayField',
        'AdminInterface.view.button.GenericCancelButton',
        'AdminInterface.view.button.GenericActionButton'
    ],

    height: 400,
    id: 'viewSubscriptionWindow',
    padding: '',
    width: 560,
    layout: {
        align: 'stretch',
        type: 'vbox'
    },
    bodyBorder: false,
    bodyPadding: '10 10 0 10',
    title: 'Subscription Details',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    flex: 1,
                    id: 'viewSubscriptionStepContainer',
                    margin: 0,
                    layout: {
                        type: 'card'
                    },
                    items: [
                        {
                            xtype: 'container',
                            id: 'viewSubscriptionStep1Container',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: '0 0 10 0',
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            html: '<h3>Subscription Details for </h3>',
                                            margin: '',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Parent Subscriber Account',
                                    items: [
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'viewSubscriptionParentSubscriberDisplayField',
                                            margin: 0,
                                            fieldLabel: 'Parent Subscriber',
                                            labelWidth: 200
                                        }
                                    ]
                                },
                                {
                                    xtype: 'form',
                                    id: 'viewSubscriptionDetailsFormPanel',
                                    layout: {
                                        type: 'auto'
                                    },
                                    items: [
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 7 10',
                                            title: 'Subscription Details',
                                            items: [
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'datastore_key_id'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'parent_datastore_key_id'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'ex_subscription_type'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'description'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'registered_datetime'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'api_access_key'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'ex_suspended'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'ex_deleted'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'start_date'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'end_date'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'ex_sys_origin_user_account'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'ex_sys_origin_organization_account'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'sys_changelog_version'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'sys_changelog'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'sys_modified'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'sys_created'
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 7 10',
                                            title: 'Access Restrictions Configuration',
                                            items: [
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'ex_update_frequency'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    fieldLabel: 'content_restriction_date_interval',
                                                    name: 'ex_content_restriction_date_interval'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    fieldLabel: '<u>content_restriction_field_names</u>',
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    name: 'ex_content_restriction_field_names',
                                                    anchor: '100%'
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'panel',
                    dock: 'bottom',
                    frame: true,
                    margin: 0,
                    layout: {
                        align: 'stretch',
                        pack: 'end',
                        type: 'hbox'
                    },
                    bodyPadding: 0,
                    items: [
                        {
                            xtype: 'genericcancelbutton',
                            text: 'Close'
                        },
                        {
                            xtype: 'genericactionbutton',
                            margin: 10,
                            minWidth: 75,
                            text: 'Edit Subscription'
                        },
                        {
                            xtype: 'genericactionbutton',
                            hidden: true,
                            margin: 10,
                            minWidth: 75,
                            text: 'Suspend Subscription'
                        },
                        {
                            xtype: 'genericactionbutton',
                            hidden: true,
                            margin: 10,
                            minWidth: 75,
                            text: 'Delete Subscription'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
