/*
 * File: app/view/window/AddSubscriptionWindow.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('AdminInterface.view.window.AddSubscriptionWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.addsubscriptionwindow',

    requires: [
        'AdminInterface.view.container.FieldCheckerButtonsContainer',
        'AdminInterface.view.form.field.SubscriberAccountCombobox',
        'AdminInterface.view.form.field.GenericTextField',
        'AdminInterface.view.form.field.GenericDateField',
        'AdminInterface.view.form.field.FieldInclusionCheckbox',
        'AdminInterface.view.button.GenericCancelButton',
        'AdminInterface.view.button.GenericBackButton',
        'AdminInterface.view.button.GenericNextButton'
    ],

    height: 685,
    id: 'addSubscriptionWindow',
    width: 560,
    layout: {
        align: 'stretch',
        type: 'vbox'
    },
    bodyBorder: false,
    bodyPadding: '10 10 0 10',
    title: 'Add Subscription',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    flex: 1,
                    id: 'addSubscriptionStepContainer',
                    margin: 0,
                    layout: {
                        type: 'card'
                    },
                    items: [
                        {
                            xtype: 'container',
                            id: 'addSubscriptionStep1Container',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: 0,
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            html: '<h3>Step 1: Configure Parent Account, Subscription Type and Access Restrictions</h3>',
                                            margin: '',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'form',
                                    id: 'addSubscriptionStep1FormPanel',
                                    margin: 0,
                                    bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                    items: [
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            title: 'Target/"Parent" Account (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'subscriberaccountcombobox',
                                                    fieldLabel: 'Parent Subscriber*',
                                                    allowBlank: false,
                                                    anchor: '100%'
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            title: 'Enter Descriptive Details (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'generictextfield',
                                                    margin: '0 0 10 0',
                                                    fieldLabel: 'Title / Friendly Name*',
                                                    name: 'friendly_name',
                                                    allowBlank: false,
                                                    enforceMaxLength: true,
                                                    maxLength: 250,
                                                    anchor: '100%'
                                                },
                                                {
                                                    xtype: 'generictextfield',
                                                    margin: '0 0 10 0',
                                                    fieldLabel: 'Short Description',
                                                    name: 'description',
                                                    anchor: '100%'
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            title: 'Specify Subscription Type (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'radiogroup',
                                                    margin: '0 0 10 0',
                                                    fieldLabel: 'Subscription Type*',
                                                    labelWidth: 150,
                                                    allowBlank: false,
                                                    items: [
                                                        {
                                                            xtype: 'radiofield',
                                                            name: 'subscription_type',
                                                            boxLabel: 'Mäklarstatistik Data API',
                                                            checked: true,
                                                            inputValue: 'data_api_subscription'
                                                        },
                                                        {
                                                            xtype: 'radiofield',
                                                            disabled: true,
                                                            name: 'subscription_type',
                                                            boxLabel: 'Statistics Widget Service',
                                                            inputValue: 'widget_service_subscription'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'container',
                                                    layout: {
                                                        align: 'stretch',
                                                        type: 'hbox'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'genericdatefield',
                                                            fieldLabel: 'Subscription Period Start Date*',
                                                            labelAlign: 'top',
                                                            labelSeparator: ' ',
                                                            name: 'subscription_start_date',
                                                            pickerAlign: 'tr-br?',
                                                            altFormats: 'Y-m-d|Ymd|y-m-d|ymd',
                                                            flex: 1,
                                                            margins: '0 5 10 0'
                                                        },
                                                        {
                                                            xtype: 'genericdatefield',
                                                            fieldLabel: 'Period End Date (indefinite if omitted)',
                                                            labelAlign: 'top',
                                                            labelSeparator: ' ',
                                                            name: 'subscription_end_date',
                                                            pickerAlign: 'tr-br?',
                                                            altFormats: 'Y-m-d|Ymd|y-m-d|ymd',
                                                            showToday: false,
                                                            flex: 1,
                                                            margins: '0 0 10 5'
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            layout: {
                                                align: 'stretch',
                                                type: 'vbox'
                                            },
                                            title: 'Specify Access Restrictions (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'container',
                                                    layout: {
                                                        align: 'stretch',
                                                        type: 'hbox'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'genericdatefield',
                                                            fieldLabel: 'Data Access Start Date*',
                                                            labelAlign: 'top',
                                                            labelSeparator: ' ',
                                                            name: 'start_date',
                                                            pickerAlign: 'tr-br?',
                                                            altFormats: 'Y-m-d|Ymd|y-m-d|ymd',
                                                            flex: 1,
                                                            margins: '0 5 5 0'
                                                        },
                                                        {
                                                            xtype: 'genericdatefield',
                                                            fieldLabel: 'Data Access End Date',
                                                            labelAlign: 'top',
                                                            labelSeparator: ' ',
                                                            name: 'end_date',
                                                            pickerAlign: 'tr-br?',
                                                            altFormats: 'Y-m-d|Ymd|y-m-d|ymd',
                                                            showToday: false,
                                                            flex: 1,
                                                            margins: '0 0 5 5'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'label',
                                                    flex: 1,
                                                    margins: '0 0 10 0',
                                                    html: '<i><u>Note</u>: The "Data Access Date Interval" parameters refers to the date limits on the subscriber\'s queries, thus controlling what amount of historic data is made accessible.</i>'
                                                },
                                                {
                                                    xtype: 'container',
                                                    layout: {
                                                        type: 'hbox'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'container',
                                                            flex: 1,
                                                            margins: '0 5 0 0',
                                                            hidden: true,
                                                            layout: {
                                                                align: 'stretch',
                                                                type: 'hbox'
                                                            },
                                                            items: [
                                                                {
                                                                    xtype: 'slider',
                                                                    flex: 1,
                                                                    margins: '0 5 10 0',
                                                                    fieldLabel: 'Dataset Download Quota / Update*',
                                                                    labelAlign: 'top',
                                                                    labelSeparator: ' ',
                                                                    labelWidth: 125,
                                                                    name: 'quota',
                                                                    value: 9,
                                                                    increment: 2,
                                                                    keyIncrement: 2,
                                                                    maxValue: 13,
                                                                    minValue: 1
                                                                },
                                                                {
                                                                    xtype: 'numberfield',
                                                                    margins: '19 0 10 5',
                                                                    id: 'addSubscriptionFormPanelQuotaAuxNumberField',
                                                                    width: 26,
                                                                    fieldLabel: 'Label',
                                                                    hideLabel: true,
                                                                    labelAlign: 'top',
                                                                    labelWidth: 25,
                                                                    value: 9,
                                                                    readOnly: true,
                                                                    decimalPrecision: 0,
                                                                    step: 2
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            xtype: 'radiogroup',
                                                            flex: 1,
                                                            id: 'addSubscriptionStep1UpdateFrequencyRadioGroup',
                                                            margin: '2 0 5 0',
                                                            fieldLabel: 'Source Data Update Frequency*',
                                                            labelPad: 0,
                                                            labelWidth: 150,
                                                            allowBlank: false,
                                                            columns: 1,
                                                            items: [
                                                                {
                                                                    xtype: 'radiofield',
                                                                    name: 'update_frequency',
                                                                    boxLabel: 'Daily',
                                                                    inputValue: 'subscription_daily_update_frequency'
                                                                },
                                                                {
                                                                    xtype: 'radiofield',
                                                                    name: 'update_frequency',
                                                                    boxLabel: 'Weekly',
                                                                    inputValue: 'subscription_weekly_update_frequency'
                                                                },
                                                                {
                                                                    xtype: 'radiofield',
                                                                    name: 'update_frequency',
                                                                    boxLabel: 'Monthly',
                                                                    checked: true,
                                                                    inputValue: 'subscription_monthly_update_frequency'
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'label',
                                                    flex: 1,
                                                    margins: '0 0 10 0',
                                                    hidden: true,
                                                    html: '<i><u>Note</u>: The "Data Access Date Interval" parameters refers to the date limits applied to the subscriber\'s Data API queries. And, especially, it controls which range of historic transaction data is made accessible to the subscriber.</i>'
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            id: 'addSubscriptionStep2aContainer',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: 0,
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            html: '<h3>Step 2a: Select Subset of Fields to Include</h3>',
                                            margin: '',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'form',
                                    id: 'addSubscriptionStep2aFormPanel',
                                    margin: 0,
                                    frameHeader: false,
                                    items: [
                                        {
                                            xtype: 'fieldset',
                                            padding: '0 10 2 10',
                                            layout: {
                                                type: 'column'
                                            },
                                            title: 'Specify Field Access Restrictions (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'fieldcheckerbuttonscontainer',
                                                    id: 'addSubscriptionStep2aFieldCheckerContainer'
                                                },
                                                {
                                                    xtype: 'checkboxgroup',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionBasicTransactionCheckboxGroup',
                                                    margin: '-5 -10 7 0',
                                                    fieldLabel: '<u><b>Include</b>: Basic Transaction Details</u>',
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    labelWidth: 150,
                                                    columns: 2,
                                                    vertical: true,
                                                    items: [
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'transaction_id<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "ObjektId"</i>',
                                                            inputValue: 'transaction_id'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'real_estate_ad_publicized<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Annonsdatum"</i>',
                                                            inputValue: 'real_estate_ad_publicized'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'contract_date<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Kontraktsdatum"</i>',
                                                            inputValue: 'contract_date'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'contract_price<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Pris"</i>',
                                                            inputValue: 'contract_price'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'date_of_possession<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Tilltradesdatum"</i>',
                                                            inputValue: 'date_of_possession'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'data_transfer_date<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Overforingsdatum"</i>',
                                                            inputValue: 'data_transfer_date'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'mstat_restricted<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "KansligAffar"</i>',
                                                            inputValue: 'mstat_restricted'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'real_estate_ad_asking_price<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Startpris"</i>',
                                                            inputValue: 'real_estate_ad_asking_price'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'real_estate_designation<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Fastbet"</i>',
                                                            inputValue: 'real_estate_designation'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'real_estate_taxation_code<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Taxkod"</i>',
                                                            inputValue: 'real_estate_taxation_code'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'real_estate_agent<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Maklare"</i>',
                                                            inputValue: 'real_estate_agent'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'real_estate_company<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Foretag"</i>',
                                                            inputValue: 'real_estate_company'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'checkboxgroup',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionGeoPtCheckboxGroup',
                                                    margin: '0 -10 10 0',
                                                    fieldLabel: '<u><b>Include</b>: Coordinates / Geolocation</u>',
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    labelWidth: 150,
                                                    columns: 2,
                                                    items: [
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            disabled: true,
                                                            height: 24,
                                                            boxLabel: 'location_wgs84_geopt',
                                                            inputValue: 'location_wgs84_geopt'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 24,
                                                            boxLabel: 'location_rt90_geopt',
                                                            inputValue: 'location_rt90_geopt'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'location_rt90_geopt_east<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "East"</i>',
                                                            inputValue: 'location_rt90_geopt_east'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'location_rt90_geopt_north<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "North"</i>',
                                                            inputValue: 'location_rt90_geopt_north'
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            id: 'addSubscriptionStep2bContainer',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: 0,
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            html: '<h3>Step 2b: Select Subset of Fields to Include</h3>',
                                            margin: '',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'form',
                                    id: 'addSubscriptionStep2bFormPanel',
                                    margin: 0,
                                    frameHeader: false,
                                    items: [
                                        {
                                            xtype: 'fieldset',
                                            padding: '0 10 2 10',
                                            layout: {
                                                type: 'column'
                                            },
                                            title: 'Specify Field Access Restrictions (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'fieldcheckerbuttonscontainer',
                                                    id: 'addSubscriptionStep2bFieldCheckerContainer'
                                                },
                                                {
                                                    xtype: 'checkboxgroup',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionGeolocationCheckboxgroup',
                                                    margin: '-5 -10 0 0',
                                                    layout: {
                                                        columns: 4,
                                                        type: 'table'
                                                    },
                                                    fieldLabel: '<u><b>Include</b>: Admin. Location Fields</u>',
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    labelWidth: 150,
                                                    vertical: true,
                                                    items: [
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            disabled: true,
                                                            height: 24,
                                                            width: 113,
                                                            boxLabel: 'locality',
                                                            inputValue: 'locality'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            disabled: true,
                                                            height: 24,
                                                            width: 122,
                                                            boxLabel: 'sublocality',
                                                            inputValue: 'sublocality'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 24,
                                                            width: 116,
                                                            boxLabel: 'county',
                                                            inputValue: 'county'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 24,
                                                            boxLabel: 'county_lkf',
                                                            inputValue: 'county_lkf'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            width: 113,
                                                            boxLabel: 'municipality<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                                                            inputValue: 'municipality'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            width: 122,
                                                            boxLabel: 'municipality_lkf<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                                                            inputValue: 'municipality_lkf'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            width: 116,
                                                            boxLabel: 'congregation<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                                                            inputValue: 'congregation'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'congregation_lkf<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "LKF"</i>',
                                                            inputValue: 'congregation_lkf'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'checkboxgroup',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionAddressCheckboxGroup',
                                                    margin: '0 -10 5 0',
                                                    fieldLabel: '<u><b>Include</b>: Address Fields</u>',
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    labelWidth: 150,
                                                    columns: 3,
                                                    items: [
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            padding: '0 0 0 0',
                                                            width: 180,
                                                            boxLabel: 'formatted_address<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Gatuadress"</i>',
                                                            inputValue: 'formatted_address'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            width: 160,
                                                            boxLabel: 'route_name<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                                                            inputValue: 'route_name'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'street_number<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                                                            inputValue: 'street_number'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            width: 180,
                                                            boxLabel: 'street_entrance<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                                                            inputValue: 'street_entrance'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            width: 160,
                                                            boxLabel: 'apartment_number<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "LghNr"</i>',
                                                            inputValue: 'apartment_number'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'metric_position<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                                                            inputValue: 'metric_position'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            width: 180,
                                                            boxLabel: 'postal_code<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                                                            inputValue: 'postal_code'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 24,
                                                            width: 160,
                                                            boxLabel: 'postal_town',
                                                            inputValue: 'postal_town'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'checkboxgroup',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionRateableValueCheckboxGroup',
                                                    margin: '0 -10 5 0',
                                                    layout: {
                                                        columns: 2,
                                                        type: 'table'
                                                    },
                                                    fieldLabel: '<u><b>Include</b>: Rateable Value and Key Ratios</u>',
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    labelWidth: 150,
                                                    vertical: true,
                                                    items: [
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            width: 240,
                                                            boxLabel: 'baseline_year_of_assessment<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Taxar"</i>',
                                                            inputValue: 'baseline_year_of_assessment'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'baseline_rateable_value<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Taxeringsvarde"</i>',
                                                            inputValue: 'baseline_rateable_value'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            width: 240,
                                                            boxLabel: 'year_of_assessment<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                                                            inputValue: 'year_of_assessment'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'rateable_value<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "SCB_Taxeringsvarde"</i>',
                                                            inputValue: 'rateable_value'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            width: 240,
                                                            boxLabel: 'price_by_rateable_value<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "SCB_KB"</i>',
                                                            inputValue: 'price_by_rateable_value'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 0 0 0',
                                                            boxLabel: 'price_per_sq_meter<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "KBoy"</i>',
                                                            inputValue: 'price_per_sq_meter'
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            id: 'addSubscriptionStep2cContainer',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: 0,
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            html: '<h3>Step 2c: Select Subset of Fields to Include</h3>',
                                            margin: '',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'form',
                                    id: 'addSubscriptionStep2cFormPanel',
                                    margin: 0,
                                    items: [
                                        {
                                            xtype: 'fieldset',
                                            padding: '0 10 2 10',
                                            layout: {
                                                type: 'column'
                                            },
                                            title: 'Specify Field Access Restrictions (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'fieldcheckerbuttonscontainer',
                                                    id: 'addSubscriptionStep2cFieldCheckerContainer'
                                                },
                                                {
                                                    xtype: 'checkboxgroup',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionMiscPropertiesCheckboxGroup',
                                                    margin: '-5 -10 5 0',
                                                    fieldLabel: '<u><b>Include</b>: Admin. Classifications and Misc. Estate Properties</u>',
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    labelWidth: 125,
                                                    columns: 2,
                                                    items: [
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'type_of_housing<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Boendeform"</i>',
                                                            inputValue: 'type_of_housing'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'housing_category<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Typ"</i>',
                                                            inputValue: 'housing_category'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'housing_tenure<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Upplatelseform"</i>',
                                                            inputValue: 'housing_tenure'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'apartment_floor<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Vaning"</i>',
                                                            inputValue: 'apartment_floor'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'building_storeys<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Vaningar"</i>',
                                                            inputValue: 'building_storeys'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'living_area<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Boyta"</i>',
                                                            inputValue: 'living_area'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'plot_area<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Tomtarea"</i>',
                                                            inputValue: 'plot_area'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'number_of_rooms<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Rum"</i>',
                                                            inputValue: 'number_of_rooms'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'elevator<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Hiss"</i>',
                                                            inputValue: 'elevator'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'balcony<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Balkong"</i>',
                                                            inputValue: 'balcony'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'new_production<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "NyProd"</i>',
                                                            inputValue: 'new_production'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'build_year<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "ByggAr"</i>',
                                                            inputValue: 'build_year'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            disabled: true,
                                                            height: 24,
                                                            boxLabel: 'build_year_lower',
                                                            inputValue: 'build_year_lower'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            disabled: true,
                                                            height: 24,
                                                            boxLabel: 'build_year_upper',
                                                            inputValue: 'build_year_upper'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'energy_performance<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Energiprestanda"</i>',
                                                            inputValue: 'energy_performance'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'energy_rating<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Energiklass"</i>',
                                                            inputValue: 'energy_rating'
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            id: 'addSubscriptionStep2dContainer',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: 0,
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            html: '<h3>Step 2d: Select Subset of Fields to Include</h3>',
                                            margin: '',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'form',
                                    id: 'addSubscriptionStep2dFormPanel',
                                    margin: 0,
                                    items: [
                                        {
                                            xtype: 'fieldset',
                                            padding: '0 10 2 10',
                                            layout: {
                                                type: 'column'
                                            },
                                            title: 'Specify Field Access Restrictions (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'fieldcheckerbuttonscontainer',
                                                    id: 'addSubscriptionStep2dFieldCheckerContainer'
                                                },
                                                {
                                                    xtype: 'checkboxgroup',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionEstateFeesCheckboxGroup',
                                                    margin: '-5 -10 5 0',
                                                    layout: {
                                                        columns: 2,
                                                        type: 'table'
                                                    },
                                                    fieldLabel: '<u><b>Include</b>: Real Estate Fees</u>',
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    labelWidth: 150,
                                                    items: [
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            width: 233,
                                                            boxLabel: 'monthly_fee<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Manavg"</i>',
                                                            inputValue: 'monthly_fee'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'annual_fee<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                                                            inputValue: 'annual_fee'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            disabled: true,
                                                            height: 24,
                                                            width: 233,
                                                            shadow: false,
                                                            boxLabel: 'operating_costs',
                                                            inputValue: 'operating_costs'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 24,
                                                            boxLabel: 'heating_included',
                                                            inputValue: 'heating_included'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'checkboxgroup',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionHousingCooperativeCheckboxGroup',
                                                    margin: '0 -10 5 0',
                                                    layout: {
                                                        columns: 3,
                                                        type: 'table'
                                                    },
                                                    fieldLabel: '<u><b>Include</b>: Housing Cooperative</u>',
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    labelWidth: 150,
                                                    columns: 2,
                                                    vertical: true,
                                                    items: [
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            width: 233,
                                                            boxLabel: 'housing_cooperative_name<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "BRF"</i>',
                                                            inputValue: 'housing_cooperative_name'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'housing_cooperative_registration_number<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "OrgNr"</i>',
                                                            inputValue: 'housing_cooperative_registration_number',
                                                            colspan: 2
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            width: 233,
                                                            shadow: false,
                                                            boxLabel: 'housing_cooperative_share<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Andelstal"</i>',
                                                            inputValue: 'housing_cooperative_share'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'checkboxgroup',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionSystemPropertiesCheckboxGroup',
                                                    margin: '0 -10 10 0',
                                                    layout: {
                                                        columns: 2,
                                                        type: 'table'
                                                    },
                                                    fieldLabel: '<u><b>Include</b>: Raw Data, Invalidation Codes and System Metadata</u>',
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    labelWidth: 150,
                                                    items: [
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            padding: '0 10 0 0',
                                                            width: 233,
                                                            boxLabel: 'raw_type_of_housing<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "raw_Boendeform"</i>',
                                                            inputValue: 'raw_type_of_housing'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'raw_housing_tenure<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "raw_Upplatelseform"</i>',
                                                            inputValue: 'raw_housing_tenure'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            padding: '0 10 0 0',
                                                            boxLabel: 'raw_real_estate_company_name<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "raw_Firmanamn"</i>',
                                                            inputValue: 'raw_real_estate_company_name'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'raw_real_estate_company_postal_town<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "raw_FirmaPostort"</i>',
                                                            inputValue: 'raw_real_estate_company_postal_town'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            padding: '0 10 0 0',
                                                            boxLabel: 'raw_real_estate_company_franchisor<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "raw_DBSpacename"</i>',
                                                            inputValue: 'raw_real_estate_company_franchisor'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            padding: '0 10 0 0',
                                                            boxLabel: 'sys_raw_data_source<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Kalla"</i>',
                                                            inputValue: 'sys_raw_data_source'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 48,
                                                            boxLabel: 'sys_scb_invalidation_code<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "SCBBortfall"</i>',
                                                            inputValue: 'sys_scb_invalidation_code'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            disabled: true,
                                                            height: 48,
                                                            boxLabel: 'sys_invalidation_codes',
                                                            inputValue: 'sys_invalidation_codes'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            disabled: true,
                                                            height: 24,
                                                            padding: '0 10 0 0',
                                                            boxLabel: 'sys_changelog_version',
                                                            inputValue: 'sys_changelog_version'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            disabled: true,
                                                            height: 24,
                                                            boxLabel: 'sys_changelog',
                                                            inputValue: 'sys_changelog'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 24,
                                                            padding: '0 10 0 0',
                                                            boxLabel: 'sys_modified',
                                                            inputValue: 'sys_modified'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            height: 24,
                                                            boxLabel: 'sys_created',
                                                            inputValue: 'sys_created'
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            id: 'addSubscriptionStep3Container',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: '0 0 10 0',
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            html: '<h3>Step 3: Review and Confirm Subscription Configuration</h3>',
                                            margin: '',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Target/"Parent" Account for the Subscription',
                                    items: [
                                        {
                                            xtype: 'displayfield',
                                            id: 'addSubscriptionConfirmParentSubscriberDisplayField',
                                            margin: '0 0 5 0',
                                            fieldLabel: 'Target Subscriber',
                                            labelWidth: 200
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Subscription Details',
                                    items: [
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addSubscriptionConfirmFriendlyNameDisplayField',
                                            margin: 0,
                                            fieldLabel: 'Title / Friendly Name',
                                            labelWidth: 200
                                        },
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addSubscriptionConfirmDescriptionDisplayField',
                                            margin: 0,
                                            fieldLabel: 'Description',
                                            labelWidth: 200
                                        },
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addSubscriptionConfirmSubscriptionTypeDisplayField',
                                            margin: 0,
                                            fieldLabel: 'Subscription Type',
                                            labelWidth: 200
                                        },
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addSubscriptionConfirmSubscriptionDateIntervalDisplayField',
                                            margin: '0 0 5 0',
                                            fieldLabel: 'Service Availability Limits',
                                            labelWidth: 200
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Access Restrictions Configuration',
                                    items: [
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addSubscriptionConfirmDateIntervalDisplayField',
                                            margin: 0,
                                            fieldLabel: 'Accessible Date Range',
                                            labelWidth: 200
                                        },
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            hidden: true,
                                            id: 'addSubscriptionConfirmQuotaDisplayField',
                                            margin: '0 0 10 0',
                                            fieldLabel: 'Quota Restrictions',
                                            labelWidth: 200
                                        },
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addSubscriptionConfirmUpdateFrequencyDisplayField',
                                            margin: '0 0 10 0',
                                            fieldLabel: 'Data Update Frequency',
                                            labelWidth: 200
                                        },
                                        {
                                            xtype: 'fieldset',
                                            padding: '0 0 0 10',
                                            title: 'Fields Included',
                                            maxHeight: 195,
                                            overflowY: 'auto',
                                            items: [
                                                {
                                                    xtype: 'displayfield',
                                                    anchor: '100%',
                                                    id: 'addSubscriptionConfirmFieldInclusionDisplayField',
                                                    margin: '4 12 7 0',
                                                    fieldLabel: '<u>Fields Included</u>',
                                                    hideEmptyLabel: false,
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    labelWidth: 200
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'panel',
                    dock: 'bottom',
                    frame: true,
                    margin: 0,
                    layout: {
                        align: 'stretch',
                        pack: 'end',
                        type: 'hbox'
                    },
                    bodyPadding: 0,
                    items: [
                        {
                            xtype: 'genericcancelbutton'
                        },
                        {
                            xtype: 'genericbackbutton'
                        },
                        {
                            xtype: 'genericnextbutton'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
