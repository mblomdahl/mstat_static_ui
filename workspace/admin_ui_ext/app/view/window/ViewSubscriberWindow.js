/*
 * File: app/view/window/ViewSubscriberWindow.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.window.ViewSubscriberWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.viewsubscriberwindow',

    requires: [
        'AdminInterface.view.form.field.GenericDisplayField',
        'AdminInterface.view.button.GenericCancelButton',
        'AdminInterface.view.button.GenericActionButton'
    ],

    height: 400,
    id: 'viewSubscriberWindow',
    padding: '',
    width: 560,
    layout: {
        align: 'stretch',
        type: 'vbox'
    },
    bodyBorder: false,
    bodyPadding: '10 10 0 10',
    title: 'Subscriber Details',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    flex: 1,
                    id: 'viewSubscriberStepContainer',
                    margin: 0,
                    layout: {
                        type: 'card'
                    },
                    items: [
                        {
                            xtype: 'container',
                            id: 'viewSubscriberStep1Container',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: '0 0 10 0',
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            html: '<h3>Subscriber Details for </h3>',
                                            margin: '',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 7 10',
                                    title: 'Account Details/Settings',
                                    items: [
                                        {
                                            xtype: 'form',
                                            id: 'viewSubscriberDetailsFormPanel',
                                            layout: {
                                                type: 'auto'
                                            },
                                            items: [
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'datastore_key_id'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'parent_datastore_key_id'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'organization_name'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'department_name'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'organization_id'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'contact_details'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'ex_contact_details'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'postal_addresses'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'ex_postal_addresses'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'registered_datetime'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'ex_activated'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'ex_suspended'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'ex_deleted'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'ex_billing_contact'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'ex_technical_contact'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'billing_balance'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'ex_sys_origin_user_account'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'ex_sys_origin_organization_account'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'sys_changelog_version'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'sys_changelog'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'sys_modified'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'sys_created'
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'panel',
                    dock: 'bottom',
                    frame: true,
                    margin: 0,
                    layout: {
                        align: 'stretch',
                        pack: 'end',
                        type: 'hbox'
                    },
                    bodyPadding: 0,
                    items: [
                        {
                            xtype: 'genericcancelbutton',
                            text: 'Close'
                        },
                        {
                            xtype: 'genericactionbutton',
                            margin: 10,
                            minWidth: 75,
                            text: 'Edit Account'
                        },
                        {
                            xtype: 'genericactionbutton',
                            hidden: true,
                            margin: 10,
                            minWidth: 75,
                            text: 'Suspend Account'
                        },
                        {
                            xtype: 'genericactionbutton',
                            hidden: true,
                            margin: 10,
                            minWidth: 75,
                            text: 'Delete Account'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
