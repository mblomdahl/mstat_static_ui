/*
 * File: app/view/window/ViewUserWindow.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.window.ViewUserWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.viewuserwindow',

    requires: [
        'AdminInterface.view.form.field.GenericDisplayField',
        'AdminInterface.view.button.GenericCancelButton',
        'AdminInterface.view.button.GenericActionButton'
    ],

    height: 400,
    id: 'viewUserWindow',
    padding: '',
    width: 560,
    layout: {
        align: 'stretch',
        type: 'vbox'
    },
    bodyBorder: false,
    bodyPadding: '10 10 0 10',
    title: 'User Details',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    flex: 1,
                    id: 'viewUserStepContainer',
                    margin: 0,
                    layout: {
                        type: 'card'
                    },
                    items: [
                        {
                            xtype: 'container',
                            id: 'viewUserStep1Container',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: '0 0 10 0',
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            html: '<h3>User Details for </h3>',
                                            margin: '',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Parent Subscriber Account',
                                    items: [
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'viewUserParentSubscriberDisplayField',
                                            margin: 0,
                                            fieldLabel: 'Parent Subscriber',
                                            labelWidth: 200
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 7 10',
                                    title: 'User Details/Settings',
                                    items: [
                                        {
                                            xtype: 'form',
                                            id: 'viewUserDetailsFormPanel',
                                            layout: {
                                                type: 'auto'
                                            },
                                            items: [
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'datastore_key_id'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'parent_datastore_key_id'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'user_email'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'ex_full_name'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'google_user_id'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'contact_details'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'api_access_key'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'registered_datetime'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'ex_activated'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'password_changed_datetime'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'ex_suspended'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'ex_deleted'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'ex_billing_contact'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'ex_technical_contact'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'last_visit_datetime'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'ex_sys_user_role'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'sys_session_id'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    fieldLabel: 'ex_sys_pending_passwd_update',
                                                    name: 'ex_sys_pending_password_update'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'ex_sys_origin_user_account'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'ex_sys_origin_organization_account'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'sys_changelog_version'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    hidden: true,
                                                    name: 'sys_changelog'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'sys_modified'
                                                },
                                                {
                                                    xtype: 'genericdisplayfield',
                                                    name: 'sys_created'
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'panel',
                    dock: 'bottom',
                    frame: true,
                    margin: 0,
                    layout: {
                        align: 'stretch',
                        pack: 'end',
                        type: 'hbox'
                    },
                    bodyPadding: 0,
                    items: [
                        {
                            xtype: 'genericcancelbutton',
                            text: 'Close'
                        },
                        {
                            xtype: 'genericactionbutton',
                            margin: 10,
                            minWidth: 75,
                            text: 'Edit User'
                        },
                        {
                            xtype: 'genericactionbutton',
                            hidden: true,
                            margin: 10,
                            minWidth: 75,
                            text: 'Suspend User'
                        },
                        {
                            xtype: 'genericactionbutton',
                            hidden: true,
                            margin: 10,
                            minWidth: 75,
                            text: 'Delete User'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
