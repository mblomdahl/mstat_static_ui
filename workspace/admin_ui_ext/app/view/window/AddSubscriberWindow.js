/*
 * File: app/view/window/AddSubscriberWindow.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.window.AddSubscriberWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.addsubscriberwindow',

    requires: [
        'AdminInterface.view.container.PostalAddressContainer',
        'AdminInterface.view.container.ContactDetailsContainer',
        'AdminInterface.view.form.field.OrganizationNameTextField',
        'AdminInterface.view.form.field.RegistrationNumberTextField',
        'AdminInterface.view.form.field.GenericCheckbox',
        'AdminInterface.view.form.field.WebAddressTextField',
        'AdminInterface.view.button.GenericCancelButton',
        'AdminInterface.view.button.GenericBackButton',
        'AdminInterface.view.button.GenericNextButton'
    ],

    height: 718,
    id: 'addSubscriberWindow',
    width: 560,
    layout: {
        align: 'stretch',
        type: 'vbox'
    },
    bodyBorder: false,
    bodyPadding: '10 10 0 10',
    title: 'Add Subscriber Account',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    flex: 1,
                    id: 'addSubscriberStepContainer',
                    margin: 0,
                    layout: {
                        type: 'card'
                    },
                    items: [
                        {
                            xtype: 'container',
                            id: 'addSubscriberStep1Container',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: 0,
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            html: '<h3>Step 1: Supply Account Company Name and Address Details</h3>',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Company Name and Address ( * marks required fields)',
                                    items: [
                                        {
                                            xtype: 'form',
                                            id: 'addSubscriberCompanyNameAndRegistrationNumberFormPanel',
                                            margin: 0,
                                            layout: {
                                                align: 'stretch',
                                                type: 'hbox'
                                            },
                                            bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                            items: [
                                                {
                                                    xtype: 'organizationnametextfield',
                                                    fieldLabel: 'Company Name*',
                                                    labelAlign: 'top',
                                                    name: 'organization_name',
                                                    allowBlank: false,
                                                    flex: 2,
                                                    margins: '0 5 10 0'
                                                },
                                                {
                                                    xtype: 'organizationnametextfield',
                                                    fieldLabel: 'Company Department',
                                                    labelAlign: 'top',
                                                    name: 'department_name',
                                                    flex: 2,
                                                    margins: '0 5 10 5'
                                                },
                                                {
                                                    xtype: 'registrationnumbertextfield',
                                                    fieldLabel: 'Organization ID',
                                                    labelAlign: 'top',
                                                    name: 'organization_id',
                                                    flex: 1.3,
                                                    margins: '0 0 10 5'
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'fieldset',
                                            margin: '0 0 10 0',
                                            padding: '4 10 2 10',
                                            title: 'Billing Address ( * marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'form',
                                                    id: 'addSubscriberBillingAddressFormPanel',
                                                    margin: 0,
                                                    layout: {
                                                        align: 'stretch',
                                                        type: 'hbox'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'postaladdresscontainer',
                                                            margin: '0 0 5 0',
                                                            flex: 1
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'fieldset',
                                            margin: '0 0 10 0',
                                            padding: '4 10 2 10',
                                            title: 'Company Office Address ( * marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'container',
                                                    margin: 0,
                                                    layout: {
                                                        type: 'anchor'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'genericcheckbox',
                                                            margin: '0 0 5 0',
                                                            name: 'office_address_as_billing_address',
                                                            boxLabel: 'Same as Billing Address',
                                                            anchor: '100%'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'form',
                                                    id: 'addSubscriberOfficeAddressFormPanel',
                                                    margin: 0,
                                                    items: [
                                                        {
                                                            xtype: 'postaladdresscontainer'
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            id: 'addSubscriberStep2Container',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: 0,
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            html: '<h3>Step 2: Supply Contact Details and Create Account Admin User</h3>',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Contact Details',
                                    items: [
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            title: 'Billing Contact (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'form',
                                                    id: 'addSubscriberBillingContactFormPanel',
                                                    margin: '0 0 0 0',
                                                    items: [
                                                        {
                                                            xtype: 'contactdetailscontainer'
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            title: 'Technical Contact (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'container',
                                                    layout: {
                                                        type: 'anchor'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'genericcheckbox',
                                                            margin: 0,
                                                            name: 'technical_contact_as_billing_contact',
                                                            boxLabel: 'Same as Billing Contact',
                                                            anchor: '100%'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'form',
                                                    id: 'addSubscriberTechnicalContactFormPanel',
                                                    margin: 0,
                                                    items: [
                                                        {
                                                            xtype: 'contactdetailscontainer'
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            title: 'Admin User (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'container',
                                                    layout: {
                                                        type: 'anchor'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'genericcheckbox',
                                                            margin: '0 0 5 0',
                                                            name: 'admin_user_as_technical_contact',
                                                            boxLabel: 'Same as Technical Contact',
                                                            anchor: '100%'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'form',
                                                    id: 'addSubscriberAdminUserFormPanel',
                                                    margin: 0,
                                                    items: [
                                                        {
                                                            xtype: 'contactdetailscontainer'
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'webaddresstextfield',
                                            id: 'addSubscriberCompanyWebPageTextField',
                                            fieldLabel: 'Company Web Page',
                                            anchor: '100%'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            id: 'addSubscriberStep3Container',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: '0 0 10 0',
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            html: '<h3>Step 3: Review and Confirm Account Details</h3>',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 7 10',
                                    title: 'Company and Address Details',
                                    items: [
                                        {
                                            xtype: 'form',
                                            id: 'addSubscriberConfirmCompanyNameAndRegistrationNumberFormPanel',
                                            margin: 0,
                                            layout: {
                                                type: 'auto'
                                            },
                                            items: [
                                                {
                                                    xtype: 'displayfield',
                                                    margin: 0,
                                                    fieldLabel: 'Company Name',
                                                    labelWidth: 150,
                                                    name: 'organization_name'
                                                },
                                                {
                                                    xtype: 'displayfield',
                                                    margin: 0,
                                                    fieldLabel: 'Department Name',
                                                    labelWidth: 150,
                                                    name: 'department_name'
                                                },
                                                {
                                                    xtype: 'displayfield',
                                                    margin: 0,
                                                    fieldLabel: 'Organization ID',
                                                    labelWidth: 150,
                                                    name: 'organization_id'
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addSubscriberConfirmBillingAddressDisplayField',
                                            margin: 0,
                                            fieldLabel: 'Billing Address',
                                            labelWidth: 150
                                        },
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addSubscriberConfirmOfficeAddressDisplayField',
                                            margin: 0,
                                            fieldLabel: 'Office Address',
                                            labelWidth: 150
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 7 10',
                                    title: 'Contact Details',
                                    items: [
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addSubscriberConfirmBillingContactDisplayField',
                                            margin: 0,
                                            fieldLabel: 'Billing Contact',
                                            labelWidth: 150
                                        },
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addSubscriberConfirmTechnicalContactDisplayField',
                                            margin: 0,
                                            fieldLabel: 'Technical Contact',
                                            labelWidth: 150
                                        },
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addSubscriberConfirmCompanyWebPageDisplayField',
                                            margin: 0,
                                            fieldLabel: 'Company Web Page',
                                            labelWidth: 150
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Initial Admin User',
                                    items: [
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addSubscriberConfirmAdminUserDisplayField',
                                            margin: '0 0 5 0',
                                            fieldLabel: 'Admin User',
                                            labelWidth: 150
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'panel',
                    dock: 'bottom',
                    frame: true,
                    margin: 0,
                    layout: {
                        align: 'stretch',
                        pack: 'end',
                        type: 'hbox'
                    },
                    bodyPadding: 0,
                    items: [
                        {
                            xtype: 'genericcancelbutton'
                        },
                        {
                            xtype: 'genericbackbutton'
                        },
                        {
                            xtype: 'genericnextbutton'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
