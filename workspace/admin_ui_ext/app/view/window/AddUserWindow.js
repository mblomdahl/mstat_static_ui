/*
 * File: app/view/window/AddUserWindow.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.window.AddUserWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.adduserwindow',

    requires: [
        'AdminInterface.view.form.field.SubscriberAccountCombobox',
        'AdminInterface.view.container.ContactDetailsContainer',
        'AdminInterface.view.button.GenericCancelButton',
        'AdminInterface.view.button.GenericBackButton',
        'AdminInterface.view.button.GenericNextButton'
    ],

    height: 400,
    id: 'addUserWindow',
    padding: '',
    width: 560,
    layout: {
        align: 'stretch',
        type: 'vbox'
    },
    bodyBorder: false,
    bodyPadding: '10 10 0 10',
    title: 'Add New User',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    flex: 1,
                    id: 'addUserStepContainer',
                    margin: 0,
                    layout: {
                        type: 'card'
                    },
                    items: [
                        {
                            xtype: 'container',
                            id: 'addUserStep1Container',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: '0 0 10 0',
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            html: '<h3>Step 1: Select Account and Submit User Details</h3>',
                                            margin: '',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'form',
                                    id: 'addUserFormPanel',
                                    margin: 0,
                                    items: [
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            title: 'Target/"Parent" Account (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'subscriberaccountcombobox',
                                                    fieldLabel: 'Parent Subscriber*',
                                                    allowBlank: false,
                                                    anchor: '100%'
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            title: 'User Contact Details and Account Privileges (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'radiogroup',
                                                    fieldLabel: 'User Privileges*',
                                                    labelWidth: 150,
                                                    allowBlank: false,
                                                    items: [
                                                        {
                                                            xtype: 'radiofield',
                                                            margin: '0 0 5 0',
                                                            name: 'administrator',
                                                            boxLabel: 'Account Administrator',
                                                            inputValue: 'true'
                                                        },
                                                        {
                                                            xtype: 'radiofield',
                                                            margin: '0 0 5 0',
                                                            name: 'administrator',
                                                            boxLabel: 'Account User',
                                                            checked: true,
                                                            inputValue: 'false'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'contactdetailscontainer'
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            id: 'addUserStep2Container',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: '0 0 10 0',
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            html: '<h3>Step 2: Review and Confirm User Configuration</h3>',
                                            margin: '',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Target/"Parent" Account',
                                    items: [
                                        {
                                            xtype: 'form',
                                            id: 'addUserConfirmParentSubscriberFormPanel',
                                            margin: '0 0 5 0',
                                            layout: {
                                                type: 'auto'
                                            },
                                            items: [
                                                {
                                                    xtype: 'displayfield',
                                                    margin: 0,
                                                    fieldLabel: 'Company Name',
                                                    labelWidth: 150,
                                                    name: 'organization_name'
                                                },
                                                {
                                                    xtype: 'displayfield',
                                                    margin: 0,
                                                    fieldLabel: 'Department Name',
                                                    labelWidth: 150,
                                                    name: 'department_name'
                                                },
                                                {
                                                    xtype: 'displayfield',
                                                    margin: 0,
                                                    fieldLabel: 'Registration Number',
                                                    labelWidth: 150,
                                                    name: 'organization_id'
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 7 10',
                                    title: 'Contact Details and Privileges',
                                    items: [
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addUserConfirmAccountPrivilegesDisplayField',
                                            margin: 0,
                                            fieldLabel: 'User Privileges',
                                            labelWidth: 150
                                        },
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addUserConfirmContactDetailsDisplayField',
                                            margin: 0,
                                            fieldLabel: 'User Contact Details',
                                            labelWidth: 150
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'panel',
                    dock: 'bottom',
                    frame: true,
                    margin: 0,
                    layout: {
                        align: 'stretch',
                        pack: 'end',
                        type: 'hbox'
                    },
                    bodyPadding: 0,
                    items: [
                        {
                            xtype: 'genericcancelbutton'
                        },
                        {
                            xtype: 'genericbackbutton'
                        },
                        {
                            xtype: 'genericnextbutton'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
