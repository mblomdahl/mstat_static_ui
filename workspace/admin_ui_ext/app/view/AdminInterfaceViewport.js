/*
 * File: app/view/AdminInterfaceViewport.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.AdminInterfaceViewport', {
    extend: 'Ext.container.Viewport',

    requires: [
        'AdminInterface.view.panel.ManageSubscribersTabPanel'
    ],

    layout: {
        align: 'stretch',
        type: 'vbox'
    },

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'tabpanel',
                    flex: 1,
                    activeTab: 0,
                    items: [
                        {
                            xtype: 'managesubscriberstabpanel',
                            autoScroll: true
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
