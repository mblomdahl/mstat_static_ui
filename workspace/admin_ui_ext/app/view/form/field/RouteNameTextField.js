/*
 * File: app/view/form/field/RouteNameTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.form.field.RouteNameTextField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.routenametextfield',

    preserveDisabled: false,
    fieldLabel: 'Route Name*',
    labelAlign: 'top',
    labelSeparator: ' ',
    name: 'route_name',
    allowBlank: false,
    enforceMaxLength: true,
    maxLength: 30,
    minLength: 3,
    vtype: 'routename',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
