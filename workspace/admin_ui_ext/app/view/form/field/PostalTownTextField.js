/*
 * File: app/view/form/field/PostalTownTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.form.field.PostalTownTextField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.postaltowntextfield',

    preserveDisabled: false,
    fieldLabel: 'Postal Town*',
    labelAlign: 'top',
    labelSeparator: ' ',
    name: 'postal_town',
    allowBlank: false,
    enforceMaxLength: true,
    maxLength: 30,
    minLength: 2,
    vtype: 'postaltown',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
