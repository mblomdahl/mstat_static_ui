/*
 * File: app/view/form/field/WebAddressTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.form.field.WebAddressTextField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.webaddresstextfield',

    preserveDisabled: false,
    margin: '0 0 10 0',
    fieldLabel: 'Web Address',
    labelWidth: 150,
    inputType: 'url',
    emptyText: 'http://',
    vtype: 'url',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            stripCharsRe: /[ åäö\\]/i
        });

        me.callParent(arguments);
    }

});
