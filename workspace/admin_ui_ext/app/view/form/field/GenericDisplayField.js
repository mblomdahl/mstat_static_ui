/*
 * File: app/view/form/field/GenericDisplayField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.form.field.GenericDisplayField', {
    extend: 'Ext.form.field.Display',
    alias: 'widget.genericdisplayfield',

    requires: [
        'AdminInterface.view.form.field.override.GenericDisplayField'
    ],

    margin: 0,
    fieldLabel: 'Generic Display Field',
    labelWidth: 200,
    tabIndex: -1,

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            listeners: {
                beforerender: {
                    fn: me.onDisplayFieldBeforeRender,
                    scope: me
                }
            }
        });

        me.callParent(arguments);
    },

    onDisplayFieldBeforeRender: function(component, eOpts) {
        if (component.getFieldLabel() == 'Generic Display Field') {
            if (component.name) {

                component.setFieldLabel(component.name);
            }
        }

    },

    renderer: function(value, displayField) {
        if (value instanceof Date) {
            if ((value.getHours() + value.getMinutes() + value.getSeconds()) === 0) {
                return Ext.Date.format(value, 'Y-m-d');
            } else {
                return Ext.Date.format(value, 'Y-m-d H:i');
            }
        }

        if (value === true) {
            return 'Yes';
        }

        if (value === false) {
            return 'No';
        }

        /*if (value === '') {
        if (!displayField.isHidden()) {
        displayField.setVisible(false);
        }
        } else {
        if (displayField.isHidden()) {
        displayField.setVisible(true);
        }
        }*/

        return '' + value;

    }

});
