/*
 * File: app/view/form/field/GenericTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.form.field.GenericTextField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.generictextfield',

    preserveDisabled: false,
    fieldLabel: 'Generic Text',
    labelSeparator: ' ',
    labelWidth: 150,

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
