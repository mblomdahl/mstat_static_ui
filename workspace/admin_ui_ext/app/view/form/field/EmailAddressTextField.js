/*
 * File: app/view/form/field/EmailAddressTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.form.field.EmailAddressTextField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.emailaddresstextfield',

    preserveDisabled: false,
    fieldLabel: 'Email Address',
    labelSeparator: ' ',
    labelWidth: 150,
    inputType: 'email',
    allowOnlyWhitespace: false,
    enforceMaxLength: true,
    maxLength: 50,
    minLength: 7,
    vtype: 'email',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
