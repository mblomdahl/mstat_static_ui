/*
 * File: app/view/form/field/BoxAddressTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.form.field.BoxAddressTextField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.boxaddresstextfield',

    preserveDisabled: false,
    margin: '0 0 5 0',
    fieldLabel: 'Box Address (<em>Note: Do not prefix input with "Box "</em>)*',
    labelAlign: 'top',
    labelSeparator: ' ',
    name: 'box_address',
    allowBlank: false,
    enforceMaxLength: true,
    maxLength: 5,
    minLength: 1,
    vtype: 'boxaddress',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
