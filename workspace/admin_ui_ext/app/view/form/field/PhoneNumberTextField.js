/*
 * File: app/view/form/field/PhoneNumberTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.form.field.PhoneNumberTextField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.phonenumbertextfield',

    preserveDisabled: false,
    fieldLabel: 'Phone Number',
    labelSeparator: ' ',
    labelWidth: 150,
    inputType: 'tel',
    enforceMaxLength: true,
    maxLength: 18,
    minLength: 8,
    vtype: 'phonenumber',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
