/*
 * File: app/view/form/field/PostalCodeTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.form.field.PostalCodeTextField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.postalcodetextfield',

    preserveDisabled: false,
    fieldLabel: 'Postal Code*',
    labelAlign: 'top',
    labelSeparator: ' ',
    name: 'postal_code',
    allowBlank: false,
    enforceMaxLength: true,
    maxLength: 6,
    minLength: 5,
    vtype: 'postalcode',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
