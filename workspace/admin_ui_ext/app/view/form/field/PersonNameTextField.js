/*
 * File: app/view/form/field/PersonNameTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.form.field.PersonNameTextField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.personnametextfield',

    preserveDisabled: false,
    fieldLabel: 'Person Name*',
    labelSeparator: ' ',
    labelWidth: 150,
    allowBlank: false,
    enforceMaxLength: true,
    maxLength: 30,
    minLength: 2,
    vtype: 'personname',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
