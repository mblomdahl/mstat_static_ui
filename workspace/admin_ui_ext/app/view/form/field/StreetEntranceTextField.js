/*
 * File: app/view/form/field/StreetEntranceTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.form.field.StreetEntranceTextField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.streetentrancetextfield',

    preserveDisabled: false,
    fieldLabel: 'Entrance',
    labelAlign: 'top',
    labelSeparator: ' ',
    name: 'street_entrance',
    enforceMaxLength: true,
    maxLength: 1,
    vtype: 'streetentrance',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
