/*
 * File: app/view/form/field/GenericCheckbox.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.form.field.GenericCheckbox', {
    extend: 'Ext.form.field.Checkbox',
    alias: 'widget.genericcheckbox',

    margin: '0 0 10 0',
    fieldLabel: '',
    boxLabel: 'Generic Checkbox',
    inputValue: 'true',
    uncheckedValue: 'false',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
