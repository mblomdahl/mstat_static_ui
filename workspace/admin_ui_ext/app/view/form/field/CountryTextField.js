/*
 * File: app/view/form/field/CountryTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.form.field.CountryTextField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.countrytextfield',

    preserveDisabled: false,
    fieldLabel: 'Country*',
    labelAlign: 'top',
    labelSeparator: ' ',
    name: 'country',
    value: 'Sweden',
    readOnly: true,
    allowBlank: false,
    vtype: 'country',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
