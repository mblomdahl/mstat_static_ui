Ext.define('AdminInterface.view.form.field.override.GenericDisplayField', {
    override: 'AdminInterface.view.form.field.GenericDisplayField',

    valueToRaw: function(value) {
        return Ext.identityFn(value);
    },

    setValue: function(value) {
        var me = this;
        if (value === null) {
            return me;
        }
        me.setRawValue(me.valueToRaw(value));
        return me.mixins.field.setValue.call(me, value);
    }
});
