/*
 * File: app/view/form/field/ApartmentFloorCombobox.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.form.field.ApartmentFloorCombobox', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.apartmentfloorcombobox',

    preserveDisabled: false,
    fieldLabel: 'Office Floor',
    labelAlign: 'top',
    labelSeparator: ' ',
    name: 'apartment_floor',
    displayField: 'name',
    forceSelection: true,
    queryMode: 'local',
    store: 'AdminInterface.store.ApartmentFloors',
    typeAhead: true,
    typeAheadDelay: 50,
    valueField: 'value',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
