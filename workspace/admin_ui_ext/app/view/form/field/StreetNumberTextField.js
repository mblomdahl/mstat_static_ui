/*
 * File: app/view/form/field/StreetNumberTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.form.field.StreetNumberTextField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.streetnumbertextfield',

    preserveDisabled: false,
    fieldLabel: 'Street Number*',
    labelAlign: 'top',
    labelSeparator: ' ',
    name: 'street_number',
    allowBlank: false,
    enforceMaxLength: true,
    maxLength: 7,
    minLength: 1,
    vtype: 'streetnumber',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
