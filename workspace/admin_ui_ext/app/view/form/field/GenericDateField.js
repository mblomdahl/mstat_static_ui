/*
 * File: app/view/form/field/GenericDateField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.form.field.GenericDateField', {
    extend: 'Ext.form.field.Date',
    alias: 'widget.genericdatefield',

    fieldLabel: 'Generic Date',
    labelWidth: 150,
    altFormats: 'Y-m-d',
    format: 'Y-m-d',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
