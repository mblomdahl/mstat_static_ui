/*
 * File: app/view/form/field/OrganizationNameTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.form.field.OrganizationNameTextField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.organizationnametextfield',

    preserveDisabled: false,
    fieldLabel: 'Organization Name',
    labelSeparator: ' ',
    labelWidth: 150,
    enforceMaxLength: true,
    maxLength: 50,
    vtype: 'organizationname',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
