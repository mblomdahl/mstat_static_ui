/*
 * File: app/view/form/field/SubscriberAccountCombobox.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.form.field.SubscriberAccountCombobox', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.subscriberaccountcombobox',

    margin: '0 0 10 0',
    fieldLabel: 'Subscriber Account',
    labelWidth: 150,
    name: 'subscriber',
    displayField: 'organization_name',
    forceSelection: true,
    queryMode: 'local',
    store: 'AdminInterface.store.SubscriberAccounts',
    typeAhead: true,
    typeAheadDelay: 50,
    valueField: 'datastore_key_urlsafe',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
