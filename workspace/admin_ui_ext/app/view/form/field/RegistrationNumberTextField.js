/*
 * File: app/view/form/field/RegistrationNumberTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.form.field.RegistrationNumberTextField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.registrationnumbertextfield',

    preserveDisabled: false,
    fieldLabel: 'Registration Number',
    labelSeparator: ' ',
    labelWidth: 150,
    emptyText: 'NNNNNN-NNNN',
    enforceMaxLength: true,
    maxLength: 11,
    minLength: 10,
    vtype: 'registrationnumber',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
