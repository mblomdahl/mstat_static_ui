/*
 * File: app/view/grid/SubscriberAccountsGridPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.SubscriberAccountsGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.subscriberaccountsgridpanel',

    requires: [
        'AdminInterface.view.grid.column.DatastoreKeyIdColumn',
        'AdminInterface.view.grid.column.DatastoreKeyUrlsafeColumn',
        'AdminInterface.view.grid.column.OrganizationNameColumn',
        'AdminInterface.view.grid.column.DepartmentNameColumn',
        'AdminInterface.view.grid.column.OrganizationIdColumn',
        'AdminInterface.view.grid.column.ExOrganizationAccountColumn',
        'AdminInterface.view.grid.column.ExOrganizationKindColumn',
        'AdminInterface.view.grid.column.ContactDetailsColumn',
        'AdminInterface.view.grid.column.PostalAddressesColumn',
        'AdminInterface.view.grid.column.ExPostalAddressesColumn',
        'AdminInterface.view.grid.column.RegisteredDatetimeColumn',
        'AdminInterface.view.grid.column.ActivatedDatetimeColumn',
        'AdminInterface.view.grid.column.ActivatedColumn',
        'AdminInterface.view.grid.column.ExActivatedColumn',
        'AdminInterface.view.grid.column.SuspendedDatetimeColumn',
        'AdminInterface.view.grid.column.SuspendedColumn',
        'AdminInterface.view.grid.column.ExSuspendedColumn',
        'AdminInterface.view.grid.column.DeletedDatetimeColumn',
        'AdminInterface.view.grid.column.DeletedColumn',
        'AdminInterface.view.grid.column.ExDeletedColumn',
        'AdminInterface.view.grid.column.BillingBalanceColumn',
        'AdminInterface.view.grid.column.OriginUserAccountColumn',
        'AdminInterface.view.grid.column.ExSysOriginUserAccountColumn',
        'AdminInterface.view.grid.column.OriginOrganizationAccountColumn',
        'AdminInterface.view.grid.column.ExSysOriginOrganizationAccountColumn',
        'AdminInterface.view.grid.column.SysChangelogVersionColumn',
        'AdminInterface.view.grid.column.SysChangelogColumn',
        'AdminInterface.view.grid.column.SysModifiedColumn',
        'AdminInterface.view.grid.column.SysCreatedColumn',
        'AdminInterface.view.panel.GridPanelRefreshTool'
    ],

    disabled: true,
    title: 'SubscriberAccounts',
    columnLines: true,
    style: 'border: 1px solid black',
    forceFit: true,
    store: 'AdminInterface.store.SubscriberAccounts',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'rownumberer',
                    hidden: true
                },
                {
                    xtype: 'datastorekeyidcolumn',
                    maxWidth: 70,
                    minWidth: 64,
                    width: 64,
                    resizable: false,
                    text: 'key_id'
                },
                {
                    xtype: 'datastorekeyurlsafecolumn'
                },
                {
                    xtype: 'organizationnamecolumn'
                },
                {
                    xtype: 'departmentnamecolumn'
                },
                {
                    xtype: 'organizationidcolumn'
                },
                {
                    xtype: 'exorganizationaccountcolumn',
                    width: 400
                },
                {
                    xtype: 'exorganizationkindcolumn'
                },
                {
                    xtype: 'contactdetailscolumn'
                },
                {
                    xtype: 'postaladdressescolumn'
                },
                {
                    xtype: 'expostaladdressescolumn'
                },
                {
                    xtype: 'registereddatetimecolumn',
                    maxWidth: 135,
                    minWidth: 115,
                    width: 125,
                    resizable: false,
                    text: 'ex_registered'
                },
                {
                    xtype: 'activateddatetimecolumn'
                },
                {
                    xtype: 'activatedcolumn'
                },
                {
                    xtype: 'exactivatedcolumn',
                    maxWidth: 135,
                    minWidth: 115,
                    width: 125,
                    resizable: false
                },
                {
                    xtype: 'suspendeddatetimecolumn'
                },
                {
                    xtype: 'suspendedcolumn'
                },
                {
                    xtype: 'exsuspendedcolumn',
                    maxWidth: 135,
                    minWidth: 115,
                    width: 125,
                    resizable: false
                },
                {
                    xtype: 'deleteddatetimecolumn'
                },
                {
                    xtype: 'deletedcolumn'
                },
                {
                    xtype: 'exdeletedcolumn'
                },
                {
                    xtype: 'billingbalancecolumn'
                },
                {
                    xtype: 'originuseraccountcolumn'
                },
                {
                    xtype: 'exsysoriginuseraccountcolumn'
                },
                {
                    xtype: 'originorganizationaccountcolumn'
                },
                {
                    xtype: 'exsysoriginorganizationaccountcolumn'
                },
                {
                    xtype: 'syschangelogversioncolumn'
                },
                {
                    xtype: 'syschangelogcolumn'
                },
                {
                    xtype: 'sysmodifiedcolumn',
                    maxWidth: 135,
                    minWidth: 115,
                    width: 125,
                    resizable: false
                },
                {
                    xtype: 'syscreatedcolumn',
                    maxWidth: 135,
                    minWidth: 115,
                    width: 125,
                    resizable: false
                }
            ],
            tools: [
                {
                    xtype: 'gridpanelrefreshtool',
                    disabled: true
                }
            ]
        });

        me.callParent(arguments);
    }

});
