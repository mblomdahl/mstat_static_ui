/*
 * File: app/view/grid/DatasetResourcesGridPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.DatasetResourcesGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.datasetresourcesgridpanel',

    requires: [
        'AdminInterface.view.panel.GridPanelRefreshTool'
    ],

    title: 'DatasetResources',
    columnLines: true,
    store: 'DatasetResources',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'rownumberer'
                },
                {
                    xtype: 'gridcolumn',
                    width: 110,
                    dataIndex: 'datastore_key_id',
                    text: 'datastore_key_id'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 135,
                    dataIndex: 'datastore_key_urlsafe',
                    text: 'datastore_key_urlsafe'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'kind',
                    text: 'kind'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'etag',
                    text: 'etag'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'id',
                    text: 'id'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'self_link',
                    text: 'self_link'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'access',
                    text: 'access'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'dataset_reference',
                    text: 'dataset_reference'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'friendly_name',
                    text: 'friendly_name'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'description',
                    text: 'description'
                },
                {
                    xtype: 'datecolumn',
                    width: 125,
                    dataIndex: 'last_modified_time',
                    text: 'last_modified_time',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    width: 125,
                    dataIndex: 'creation_time',
                    text: 'creation_time',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_owners',
                    text: 'sys_owners'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_writers',
                    text: 'sys_writers'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_readers',
                    text: 'sys_readers'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_origin_user_account',
                    text: 'sys_origin_user_account'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_origin_organization_account',
                    text: 'sys_origin_organization_account'
                },
                {
                    xtype: 'numbercolumn',
                    hidden: true,
                    width: 140,
                    dataIndex: 'sys_changelog_version',
                    text: 'sys_changelog_version',
                    format: '000'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    dataIndex: 'sys_changelog',
                    text: 'sys_changelog'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_modified',
                    text: 'sys_modified',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_created',
                    text: 'sys_created',
                    format: 'Y-m-d H:i:s'
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top'
                }
            ],
            tools: [
                {
                    xtype: 'gridpanelrefreshtool'
                }
            ]
        });

        me.callParent(arguments);
    }

});
