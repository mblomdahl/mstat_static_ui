/*
 * File: app/view/grid/UserAccountsGridPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.UserAccountsGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.useraccountsgridpanel',

    requires: [
        'AdminInterface.view.grid.column.DatastoreKeyIdColumn',
        'AdminInterface.view.grid.column.DatastoreKeyUrlsafeColumn',
        'AdminInterface.view.grid.column.ParentDatastoreKeyIdColumn',
        'AdminInterface.view.grid.column.ParentDatastoreKeyUrlsafeColumn',
        'AdminInterface.view.grid.column.ExFullNameColumn',
        'AdminInterface.view.grid.column.UserEmailColumn',
        'AdminInterface.view.grid.column.ExUserAccountColumn',
        'AdminInterface.view.grid.column.ExBillingContactColumn',
        'AdminInterface.view.grid.column.ExTechnicalContactColumn',
        'AdminInterface.view.grid.column.UserIdColumn',
        'AdminInterface.view.grid.column.GoogleUserIdColumn',
        'AdminInterface.view.grid.column.GoogleUserColumn',
        'AdminInterface.view.grid.column.ContactDetailsColumn',
        'AdminInterface.view.grid.column.PasswordColumn',
        'AdminInterface.view.grid.column.ApiAccessKeyColumn',
        'AdminInterface.view.grid.column.RegisteredDatetimeColumn',
        'AdminInterface.view.grid.column.ActivatedDatetimeColumn',
        'AdminInterface.view.grid.column.ActivatedColumn',
        'AdminInterface.view.grid.column.ExActivatedColumn',
        'AdminInterface.view.grid.column.SuspendedDatetimeColumn',
        'AdminInterface.view.grid.column.SuspendedColumn',
        'AdminInterface.view.grid.column.ExSuspendedColumn',
        'AdminInterface.view.grid.column.DeletedDatetimeColumn',
        'AdminInterface.view.grid.column.DeletedColumn',
        'AdminInterface.view.grid.column.ExDeletedColumn',
        'AdminInterface.view.grid.column.PasswordChangedDatetimeColumn',
        'AdminInterface.view.grid.column.AdministratorColumn',
        'AdminInterface.view.grid.column.LastVisitDatetimeColumn',
        'AdminInterface.view.grid.column.LastClientIpColumn',
        'AdminInterface.view.grid.column.SysUserRoleColumn',
        'AdminInterface.view.grid.column.ExSysUserRoleColumn',
        'AdminInterface.view.grid.column.SysSessionIdColumn',
        'AdminInterface.view.grid.column.SysPendingEmailUpdateAddressColumn',
        'AdminInterface.view.grid.column.SysPendingEmailUpdateKeyColumn',
        'AdminInterface.view.grid.column.SysPendingPasswordUpdateKeyColumn',
        'AdminInterface.view.grid.column.SysPendingPasswordUpdateExpirationDatetimeColumn',
        'AdminInterface.view.grid.column.ExSysPendingPasswordUpdateColumn',
        'AdminInterface.view.grid.column.OriginUserAccountColumn',
        'AdminInterface.view.grid.column.ExSysOriginUserAccountColumn',
        'AdminInterface.view.grid.column.OriginOrganizationAccountColumn',
        'AdminInterface.view.grid.column.ExSysOriginOrganizationAccountColumn',
        'AdminInterface.view.grid.column.SysChangelogVersionColumn',
        'AdminInterface.view.grid.column.SysChangelogColumn',
        'AdminInterface.view.grid.column.SysModifiedColumn',
        'AdminInterface.view.grid.column.SysCreatedColumn',
        'AdminInterface.view.panel.GridPanelRefreshTool'
    ],

    disabled: true,
    title: 'UserAccounts',
    columnLines: true,
    style: 'border: 1px solid black',
    forceFit: true,
    store: 'AdminInterface.store.UserAccounts',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'rownumberer',
                    hidden: true
                },
                {
                    xtype: 'datastorekeyidcolumn',
                    maxWidth: 70,
                    minWidth: 64,
                    width: 64,
                    resizable: false,
                    text: 'key_id'
                },
                {
                    xtype: 'datastorekeyurlsafecolumn'
                },
                {
                    xtype: 'parentdatastorekeyidcolumn'
                },
                {
                    xtype: 'parentdatastorekeyurlsafecolumn'
                },
                {
                    xtype: 'exfullnamecolumn',
                    width: 175
                },
                {
                    xtype: 'useremailcolumn',
                    width: 235
                },
                {
                    xtype: 'exuseraccountcolumn',
                    width: 350
                },
                {
                    xtype: 'exbillingcontactcolumn'
                },
                {
                    xtype: 'extechnicalcontactcolumn'
                },
                {
                    xtype: 'useridcolumn'
                },
                {
                    xtype: 'googleuseridcolumn'
                },
                {
                    xtype: 'googleusercolumn'
                },
                {
                    xtype: 'contactdetailscolumn'
                },
                {
                    xtype: 'passwordcolumn'
                },
                {
                    xtype: 'apiaccesskeycolumn'
                },
                {
                    xtype: 'registereddatetimecolumn',
                    hidden: true,
                    maxWidth: 135,
                    minWidth: 115,
                    width: 125,
                    resizable: false,
                    text: 'ex_registered'
                },
                {
                    xtype: 'activateddatetimecolumn'
                },
                {
                    xtype: 'activatedcolumn'
                },
                {
                    xtype: 'exactivatedcolumn',
                    maxWidth: 135,
                    minWidth: 115,
                    width: 125,
                    resizable: false
                },
                {
                    xtype: 'suspendeddatetimecolumn'
                },
                {
                    xtype: 'suspendedcolumn'
                },
                {
                    xtype: 'exsuspendedcolumn',
                    maxWidth: 135,
                    minWidth: 115,
                    width: 125,
                    resizable: false
                },
                {
                    xtype: 'deleteddatetimecolumn'
                },
                {
                    xtype: 'deletedcolumn'
                },
                {
                    xtype: 'exdeletedcolumn'
                },
                {
                    xtype: 'passwordchangeddatetimecolumn'
                },
                {
                    xtype: 'administratorcolumn',
                    maxWidth: 64,
                    minWidth: 64,
                    width: 64,
                    resizable: false,
                    text: 'admin'
                },
                {
                    xtype: 'lastvisitdatetimecolumn',
                    maxWidth: 135,
                    minWidth: 115,
                    width: 125,
                    resizable: false,
                    text: 'ex_last_visit'
                },
                {
                    xtype: 'lastclientipcolumn'
                },
                {
                    xtype: 'sysuserrolecolumn'
                },
                {
                    xtype: 'exsysuserrolecolumn'
                },
                {
                    xtype: 'syssessionidcolumn'
                },
                {
                    xtype: 'syspendingemailupdateaddresscolumn'
                },
                {
                    xtype: 'syspendingemailupdatekeycolumn'
                },
                {
                    xtype: 'syspendingpasswordupdatekeycolumn'
                },
                {
                    xtype: 'syspendingpasswordupdateexpirationdatetimecolumn'
                },
                {
                    xtype: 'exsyspendingpasswordupdatecolumn'
                },
                {
                    xtype: 'originuseraccountcolumn'
                },
                {
                    xtype: 'exsysoriginuseraccountcolumn'
                },
                {
                    xtype: 'originorganizationaccountcolumn'
                },
                {
                    xtype: 'exsysoriginorganizationaccountcolumn'
                },
                {
                    xtype: 'syschangelogversioncolumn'
                },
                {
                    xtype: 'syschangelogcolumn'
                },
                {
                    xtype: 'sysmodifiedcolumn',
                    maxWidth: 135,
                    minWidth: 115,
                    width: 125,
                    resizable: false
                },
                {
                    xtype: 'syscreatedcolumn',
                    maxWidth: 135,
                    minWidth: 115,
                    width: 125,
                    resizable: false
                }
            ],
            tools: [
                {
                    xtype: 'gridpanelrefreshtool',
                    disabled: true
                }
            ]
        });

        me.callParent(arguments);
    }

});
