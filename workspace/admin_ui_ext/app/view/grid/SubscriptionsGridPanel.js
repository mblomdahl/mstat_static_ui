/*
 * File: app/view/grid/SubscriptionsGridPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.SubscriptionsGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.subscriptionsgridpanel',

    requires: [
        'AdminInterface.view.grid.column.DatastoreKeyIdColumn',
        'AdminInterface.view.grid.column.DatastoreKeyUrlsafeColumn',
        'AdminInterface.view.grid.column.ParentDatastoreKeyIdColumn',
        'AdminInterface.view.grid.column.ParentDatastoreKeyUrlsafeColumn',
        'AdminInterface.view.grid.column.SubscriptionTypeColumn',
        'AdminInterface.view.grid.column.FriendlyNameColumn',
        'AdminInterface.view.grid.column.DescriptionColumn',
        'AdminInterface.view.grid.column.ContentRestrictionsColumn',
        'AdminInterface.view.grid.column.ApiAccessKeyColumn',
        'AdminInterface.view.grid.column.QuotaColumn',
        'AdminInterface.view.grid.column.UpdateFrequencyColumn',
        'AdminInterface.view.grid.column.ExUpdateFrequencyColumn',
        'AdminInterface.view.grid.column.StartDateColumn',
        'AdminInterface.view.grid.column.EndDateColumn',
        'AdminInterface.view.grid.column.RegisteredDatetimeColumn',
        'AdminInterface.view.grid.column.SuspendedDatetimeColumn',
        'AdminInterface.view.grid.column.DeletedDatetimeColumn',
        'AdminInterface.view.grid.column.SuspendedColumn',
        'AdminInterface.view.grid.column.ExSuspendedColumn',
        'AdminInterface.view.grid.column.DeletedColumn',
        'AdminInterface.view.grid.column.ExDeletedColumn',
        'AdminInterface.view.grid.column.OriginUserAccountColumn',
        'AdminInterface.view.grid.column.ExSysOriginUserAccountColumn',
        'AdminInterface.view.grid.column.OriginOrganizationAccountColumn',
        'AdminInterface.view.grid.column.ExSysOriginOrganizationAccountColumn',
        'AdminInterface.view.grid.column.SysChangelogVersionColumn',
        'AdminInterface.view.grid.column.SysChangelogColumn',
        'AdminInterface.view.grid.column.SysModifiedColumn',
        'AdminInterface.view.grid.column.SysCreatedColumn',
        'AdminInterface.view.panel.GridPanelRefreshTool'
    ],

    disabled: true,
    title: 'Subscriptions',
    columnLines: true,
    style: 'border: 1px solid black',
    forceFit: true,
    store: 'AdminInterface.store.Subscriptions',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'rownumberer',
                    hidden: true
                },
                {
                    xtype: 'datastorekeyidcolumn',
                    maxWidth: 70,
                    minWidth: 64,
                    width: 64,
                    resizable: false,
                    text: 'key_id'
                },
                {
                    xtype: 'datastorekeyurlsafecolumn'
                },
                {
                    xtype: 'parentdatastorekeyidcolumn'
                },
                {
                    xtype: 'parentdatastorekeyurlsafecolumn'
                },
                {
                    xtype: 'subscriptiontypecolumn'
                },
                {
                    xtype: 'friendlynamecolumn',
                    width: 225
                },
                {
                    xtype: 'descriptioncolumn',
                    width: 300
                },
                {
                    xtype: 'contentrestrictionscolumn'
                },
                {
                    xtype: 'apiaccesskeycolumn'
                },
                {
                    xtype: 'quotacolumn'
                },
                {
                    xtype: 'updatefrequencycolumn'
                },
                {
                    xtype: 'exupdatefrequencycolumn',
                    maxWidth: 105,
                    minWidth: 95,
                    width: 100,
                    resizable: false,
                    text: 'update_freq.'
                },
                {
                    xtype: 'startdatecolumn',
                    maxWidth: 95,
                    minWidth: 85,
                    width: 90,
                    resizable: false
                },
                {
                    xtype: 'enddatecolumn',
                    maxWidth: 90,
                    minWidth: 80,
                    width: 85,
                    resizable: false
                },
                {
                    xtype: 'registereddatetimecolumn',
                    maxWidth: 135,
                    minWidth: 115,
                    width: 125,
                    resizable: false,
                    text: 'ex_registered'
                },
                {
                    xtype: 'suspendeddatetimecolumn'
                },
                {
                    xtype: 'deleteddatetimecolumn'
                },
                {
                    xtype: 'suspendedcolumn'
                },
                {
                    xtype: 'exsuspendedcolumn',
                    maxWidth: 135,
                    minWidth: 115,
                    width: 125,
                    resizable: false
                },
                {
                    xtype: 'deletedcolumn'
                },
                {
                    xtype: 'exdeletedcolumn',
                    maxWidth: 135,
                    minWidth: 115,
                    width: 125,
                    resizable: false
                },
                {
                    xtype: 'originuseraccountcolumn'
                },
                {
                    xtype: 'exsysoriginuseraccountcolumn'
                },
                {
                    xtype: 'originorganizationaccountcolumn'
                },
                {
                    xtype: 'exsysoriginorganizationaccountcolumn'
                },
                {
                    xtype: 'syschangelogversioncolumn'
                },
                {
                    xtype: 'syschangelogcolumn'
                },
                {
                    xtype: 'sysmodifiedcolumn',
                    maxWidth: 135,
                    minWidth: 115,
                    width: 125,
                    resizable: false
                },
                {
                    xtype: 'syscreatedcolumn',
                    maxWidth: 135,
                    minWidth: 115,
                    width: 125,
                    resizable: false
                }
            ],
            tools: [
                {
                    xtype: 'gridpanelrefreshtool',
                    disabled: true
                }
            ]
        });

        me.callParent(arguments);
    }

});
