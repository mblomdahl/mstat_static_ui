/*
 * File: app/view/grid/SystemAdminAccountsGridPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.SystemAdminAccountsGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.systemadminaccountsgridpanel',

    requires: [
        'AdminInterface.view.panel.GridPanelRefreshTool'
    ],

    disabled: true,
    title: 'SystemAdminAccounts',
    columnLines: true,
    store: 'SystemAdminAccounts',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'rownumberer'
                },
                {
                    xtype: 'gridcolumn',
                    width: 142,
                    dataIndex: 'datastore_key_id',
                    text: 'datastore_key_id'
                },
                {
                    xtype: 'gridcolumn',
                    width: 170,
                    dataIndex: 'datastore_key_urlsafe',
                    text: 'datastore_key_urlsafe'
                },
                {
                    xtype: 'gridcolumn',
                    width: 137,
                    dataIndex: 'organization_name',
                    text: 'company_name'
                },
                {
                    xtype: 'gridcolumn',
                    width: 147,
                    dataIndex: 'department_name',
                    text: 'department_name'
                },
                {
                    xtype: 'gridcolumn',
                    width: 165,
                    dataIndex: 'organization_id',
                    text: 'registration_number'
                },
                {
                    xtype: 'gridcolumn',
                    width: 132,
                    dataIndex: 'ex_contact_details',
                    text: 'contact_details'
                },
                {
                    xtype: 'gridcolumn',
                    width: 146,
                    dataIndex: 'postal_addresses',
                    text: 'postal_addresses'
                },
                {
                    xtype: 'datecolumn',
                    width: 153,
                    dataIndex: 'activated_datetime',
                    text: 'activated_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    width: 158,
                    dataIndex: 'registered_datetime',
                    text: 'registered_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    width: 166,
                    dataIndex: 'suspended_datetime',
                    text: 'suspended_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    width: 141,
                    dataIndex: 'deleted_datetime',
                    text: 'deleted_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'booleancolumn',
                    width: 103,
                    dataIndex: 'suspended',
                    text: 'suspended'
                },
                {
                    xtype: 'booleancolumn',
                    width: 84,
                    dataIndex: 'deleted',
                    text: 'deleted'
                },
                {
                    xtype: 'booleancolumn',
                    width: 93,
                    dataIndex: 'activated',
                    text: 'activated'
                },
                {
                    xtype: 'gridcolumn',
                    width: 194,
                    dataIndex: 'sys_origin_user_account',
                    text: 'sys_origin_user_account'
                },
                {
                    xtype: 'gridcolumn',
                    width: 242,
                    dataIndex: 'sys_origin_organization_account',
                    text: 'sys_origin_organization_account'
                },
                {
                    xtype: 'numbercolumn',
                    width: 182,
                    dataIndex: 'sys_changelog_version',
                    text: 'sys_changelog_version',
                    format: '000'
                },
                {
                    xtype: 'gridcolumn',
                    width: 128,
                    dataIndex: 'sys_changelog',
                    text: 'sys_changelog'
                },
                {
                    xtype: 'datecolumn',
                    width: 125,
                    dataIndex: 'sys_modified',
                    text: 'sys_modified',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    width: 125,
                    dataIndex: 'sys_created',
                    text: 'sys_created',
                    format: 'Y-m-d H:i:s'
                }
            ],
            tools: [
                {
                    xtype: 'gridpanelrefreshtool'
                }
            ]
        });

        me.callParent(arguments);
    }

});
