/*
 * File: app/view/grid/column/ExSysPendingPasswordUpdateColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.ExSysPendingPasswordUpdateColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.exsyspendingpasswordupdatecolumn',

    hidden: true,
    dataIndex: 'ex_sys_pending_password_update',
    text: 'ex_sys_pending_password_update',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
