/*
 * File: app/view/grid/column/SysSessionIdColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.SysSessionIdColumn', {
    extend: 'Ext.grid.column.Number',
    alias: 'widget.syssessionidcolumn',

    hidden: true,
    dataIndex: 'sys_session_id',
    text: 'sys_session_id',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
