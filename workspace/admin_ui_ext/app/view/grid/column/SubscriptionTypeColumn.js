/*
 * File: app/view/grid/column/SubscriptionTypeColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.SubscriptionTypeColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.subscriptiontypecolumn',

    hidden: true,
    dataIndex: 'subscription_type',
    text: 'subscription_type',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
