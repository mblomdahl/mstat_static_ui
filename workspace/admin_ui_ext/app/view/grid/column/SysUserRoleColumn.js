/*
 * File: app/view/grid/column/SysUserRoleColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.SysUserRoleColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.sysuserrolecolumn',

    hidden: true,
    dataIndex: 'sys_user_role',
    text: 'sys_user_role',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
