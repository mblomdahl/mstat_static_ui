/*
 * File: app/view/grid/column/ExSysUserRoleColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.ExSysUserRoleColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.exsysuserrolecolumn',

    hidden: true,
    dataIndex: 'ex_sys_user_role',
    text: 'ex_sys_user_role',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
