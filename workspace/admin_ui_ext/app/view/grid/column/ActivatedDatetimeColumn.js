/*
 * File: app/view/grid/column/ActivatedDatetimeColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.ActivatedDatetimeColumn', {
    extend: 'Ext.grid.column.Date',
    alias: 'widget.activateddatetimecolumn',

    hidden: true,
    dataIndex: 'activated_datetime',
    text: 'activated_datetime',
    format: 'Y-m-d H:i',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
