/*
 * File: app/view/grid/column/PasswordChangedDatetimeColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.PasswordChangedDatetimeColumn', {
    extend: 'Ext.grid.column.Date',
    alias: 'widget.passwordchangeddatetimecolumn',

    hidden: true,
    dataIndex: 'password_changed_datetime',
    text: 'password_changed_datetime',
    format: 'Y-m-d H:i',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
