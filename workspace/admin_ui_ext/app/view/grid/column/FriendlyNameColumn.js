/*
 * File: app/view/grid/column/FriendlyNameColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.FriendlyNameColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.friendlynamecolumn',

    dataIndex: 'friendly_name',
    text: 'friendly_name',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
