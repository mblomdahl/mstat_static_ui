/*
 * File: app/view/grid/column/ExActivatedColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.ExActivatedColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.exactivatedcolumn',

    dataIndex: 'ex_activated',
    text: 'ex_activated',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
