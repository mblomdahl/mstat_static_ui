/*
 * File: app/view/grid/column/UserEmailColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.UserEmailColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.useremailcolumn',

    dataIndex: 'user_email',
    text: 'user_email',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
