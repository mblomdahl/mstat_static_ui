/*
 * File: app/view/grid/column/SysChangelogVersionColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.SysChangelogVersionColumn', {
    extend: 'Ext.grid.column.Number',
    alias: 'widget.syschangelogversioncolumn',

    hidden: true,
    dataIndex: 'sys_changelog_version',
    text: 'sys_changelog_version',
    format: '000',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
