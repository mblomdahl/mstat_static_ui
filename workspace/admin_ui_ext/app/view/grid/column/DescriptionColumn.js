/*
 * File: app/view/grid/column/DescriptionColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.DescriptionColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.descriptioncolumn',

    hidden: true,
    dataIndex: 'description',
    text: 'description',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
