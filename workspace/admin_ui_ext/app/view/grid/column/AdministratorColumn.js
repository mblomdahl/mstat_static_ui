/*
 * File: app/view/grid/column/AdministratorColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.AdministratorColumn', {
    extend: 'Ext.grid.column.Boolean',
    alias: 'widget.administratorcolumn',

    hidden: true,
    dataIndex: 'administrator',
    text: 'administrator',
    falseText: 'No',
    trueText: 'Yes',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
