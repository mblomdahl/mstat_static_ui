/*
 * File: app/view/grid/column/OriginOrganizationAccountColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.OriginOrganizationAccountColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.originorganizationaccountcolumn',

    hidden: true,
    dataIndex: 'sys_origin_organization_account',
    text: 'sys_origin_organization_account',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
