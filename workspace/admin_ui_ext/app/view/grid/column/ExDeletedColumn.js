/*
 * File: app/view/grid/column/ExDeletedColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.ExDeletedColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.exdeletedcolumn',

    hidden: true,
    dataIndex: 'ex_deleted',
    text: 'ex_deleted',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
