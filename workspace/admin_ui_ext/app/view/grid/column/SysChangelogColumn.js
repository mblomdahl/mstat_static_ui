/*
 * File: app/view/grid/column/SysChangelogColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.SysChangelogColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.syschangelogcolumn',

    hidden: true,
    dataIndex: 'sys_changelog',
    text: 'sys_changelog',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
