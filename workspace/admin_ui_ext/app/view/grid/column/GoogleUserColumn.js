/*
 * File: app/view/grid/column/GoogleUserColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.GoogleUserColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.googleusercolumn',

    hidden: true,
    dataIndex: 'google_user',
    text: 'google_user',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
