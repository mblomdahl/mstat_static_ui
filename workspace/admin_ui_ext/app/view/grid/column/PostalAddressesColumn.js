/*
 * File: app/view/grid/column/PostalAddressesColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.PostalAddressesColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.postaladdressescolumn',

    hidden: true,
    dataIndex: 'postal_addresses',
    text: 'postal_addresses',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
