/*
 * File: app/view/grid/column/RegisteredDatetimeColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.RegisteredDatetimeColumn', {
    extend: 'Ext.grid.column.Date',
    alias: 'widget.registereddatetimecolumn',

    dataIndex: 'registered_datetime',
    text: 'registered_datetime',
    format: 'Y-m-d H:i',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
