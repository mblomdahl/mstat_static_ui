/*
 * File: app/view/grid/column/ExTechnicalContactColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.ExTechnicalContactColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.extechnicalcontactcolumn',

    hidden: true,
    dataIndex: 'ex_technical_contact',
    text: 'ex_technical_contact',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
