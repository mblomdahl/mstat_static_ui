/*
 * File: app/view/grid/column/ExSuspendedColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.ExSuspendedColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.exsuspendedcolumn',

    dataIndex: 'ex_suspended',
    text: 'ex_suspended',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
