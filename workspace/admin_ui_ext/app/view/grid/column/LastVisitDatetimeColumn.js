/*
 * File: app/view/grid/column/LastVisitDatetimeColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.LastVisitDatetimeColumn', {
    extend: 'Ext.grid.column.Date',
    alias: 'widget.lastvisitdatetimecolumn',

    dataIndex: 'last_visit_datetime',
    text: 'last_visit_datetime',
    format: 'Y-m-d H:i',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
