/*
 * File: app/view/grid/column/OriginUserAccountColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.OriginUserAccountColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.originuseraccountcolumn',

    hidden: true,
    dataIndex: 'sys_origin_user_account',
    text: 'sys_origin_user_account',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
