/*
 * File: app/view/grid/column/SysCreatedColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.SysCreatedColumn', {
    extend: 'Ext.grid.column.Date',
    alias: 'widget.syscreatedcolumn',

    hidden: true,
    dataIndex: 'sys_created',
    text: 'sys_created',
    format: 'Y-m-d H:i',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
