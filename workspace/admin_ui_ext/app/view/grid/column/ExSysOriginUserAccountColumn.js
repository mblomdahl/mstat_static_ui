/*
 * File: app/view/grid/column/ExSysOriginUserAccountColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.ExSysOriginUserAccountColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.exsysoriginuseraccountcolumn',

    hidden: true,
    dataIndex: 'ex_sys_origin_user_account',
    text: 'ex_sys_origin_user_account',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
