/*
 * File: app/view/grid/column/ApiAccessKeyColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.ApiAccessKeyColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.apiaccesskeycolumn',

    hidden: true,
    dataIndex: 'api_access_key',
    text: 'api_access_key',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
