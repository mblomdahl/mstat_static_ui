/*
 * File: app/view/grid/column/ActivatedColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.ActivatedColumn', {
    extend: 'Ext.grid.column.Boolean',
    alias: 'widget.activatedcolumn',

    hidden: true,
    dataIndex: 'activated',
    text: 'activated',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
