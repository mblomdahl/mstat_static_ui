/*
 * File: app/view/grid/column/LastClientIpColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.LastClientIpColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.lastclientipcolumn',

    hidden: true,
    dataIndex: 'last_client_ip',
    text: 'last_client_ip',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
