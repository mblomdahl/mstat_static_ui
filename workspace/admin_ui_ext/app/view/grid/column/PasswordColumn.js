/*
 * File: app/view/grid/column/PasswordColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.PasswordColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.passwordcolumn',

    hidden: true,
    dataIndex: 'password',
    text: 'password',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
