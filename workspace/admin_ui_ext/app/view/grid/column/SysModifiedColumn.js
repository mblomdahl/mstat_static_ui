/*
 * File: app/view/grid/column/SysModifiedColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.SysModifiedColumn', {
    extend: 'Ext.grid.column.Date',
    alias: 'widget.sysmodifiedcolumn',

    dataIndex: 'sys_modified',
    text: 'sys_modified',
    format: 'Y-m-d H:i',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
