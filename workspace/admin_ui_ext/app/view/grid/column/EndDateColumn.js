/*
 * File: app/view/grid/column/EndDateColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.EndDateColumn', {
    extend: 'Ext.grid.column.Date',
    alias: 'widget.enddatecolumn',

    dataIndex: 'end_date',
    text: 'end_date',
    format: 'Y-m-d',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
