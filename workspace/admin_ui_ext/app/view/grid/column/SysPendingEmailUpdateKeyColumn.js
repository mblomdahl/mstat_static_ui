/*
 * File: app/view/grid/column/SysPendingEmailUpdateKeyColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.SysPendingEmailUpdateKeyColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.syspendingemailupdatekeycolumn',

    hidden: true,
    dataIndex: 'sys_pending_email_update_key',
    text: 'sys_pending_email_update_key',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
