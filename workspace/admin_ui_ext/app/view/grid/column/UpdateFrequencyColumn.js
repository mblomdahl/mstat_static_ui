/*
 * File: app/view/grid/column/UpdateFrequencyColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.UpdateFrequencyColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.updatefrequencycolumn',

    hidden: true,
    dataIndex: 'update_frequency',
    text: 'update_frequency',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
