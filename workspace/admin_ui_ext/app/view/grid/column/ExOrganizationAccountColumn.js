/*
 * File: app/view/grid/column/ExOrganizationAccountColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.ExOrganizationAccountColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.exorganizationaccountcolumn',

    dataIndex: 'ex_organization_account',
    text: 'ex_organization_account',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
