/*
 * File: app/view/grid/column/ExOrganizationKindColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.ExOrganizationKindColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.exorganizationkindcolumn',

    hidden: true,
    dataIndex: 'ex_organization_kind',
    text: 'ex_organization_kind',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
