/*
 * File: app/view/grid/column/BillingBalanceColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.BillingBalanceColumn', {
    extend: 'Ext.grid.column.Number',
    alias: 'widget.billingbalancecolumn',

    hidden: true,
    dataIndex: 'billing_balance',
    text: 'billing_balance',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
