/*
 * File: app/view/grid/column/GoogleUserIdColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.GoogleUserIdColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.googleuseridcolumn',

    hidden: true,
    dataIndex: 'google_user_id',
    text: 'google_user_id',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
