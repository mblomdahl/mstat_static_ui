/*
 * File: app/view/grid/column/ExPostalAddressesColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.ExPostalAddressesColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.expostaladdressescolumn',

    hidden: true,
    dataIndex: 'ex_postal_addresses',
    text: 'ex_postal_addresses',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
