/*
 * File: app/view/grid/column/SysPendingPasswordUpdateKeyColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.SysPendingPasswordUpdateKeyColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.syspendingpasswordupdatekeycolumn',

    hidden: true,
    dataIndex: 'sys_pending_password_update_key',
    text: 'sys_pending_password_update_key',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
