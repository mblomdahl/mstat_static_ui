/*
 * File: app/view/grid/column/ParentDatastoreKeyIdColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.ParentDatastoreKeyIdColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.parentdatastorekeyidcolumn',

    hidden: true,
    dataIndex: 'parent_datastore_key_id',
    text: 'parent_datastore_key_id',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
