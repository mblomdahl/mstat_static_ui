/*
 * File: app/view/grid/column/ParentDatastoreKeyUrlsafeColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.ParentDatastoreKeyUrlsafeColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.parentdatastorekeyurlsafecolumn',

    hidden: true,
    dataIndex: 'parent_datastore_key_urlsafe',
    text: 'parent_datastore_key_urlsafe',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
