/*
 * File: app/view/grid/column/SysPendingEmailUpdateAddressColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.SysPendingEmailUpdateAddressColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.syspendingemailupdateaddresscolumn',

    hidden: true,
    dataIndex: 'sys_pending_email_update_address',
    text: 'sys_pending_email_update_address',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
