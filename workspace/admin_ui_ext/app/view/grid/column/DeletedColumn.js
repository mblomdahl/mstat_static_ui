/*
 * File: app/view/grid/column/DeletedColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.DeletedColumn', {
    extend: 'Ext.grid.column.Boolean',
    alias: 'widget.deletedcolumn',

    hidden: true,
    dataIndex: 'deleted',
    text: 'deleted',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
