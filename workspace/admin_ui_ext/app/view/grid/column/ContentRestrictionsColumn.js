/*
 * File: app/view/grid/column/ContentRestrictionsColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.ContentRestrictionsColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.contentrestrictionscolumn',

    hidden: true,
    dataIndex: 'content_restrictions',
    text: 'content_restrictions',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
