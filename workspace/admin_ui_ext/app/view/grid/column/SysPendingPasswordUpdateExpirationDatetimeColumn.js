/*
 * File: app/view/grid/column/SysPendingPasswordUpdateExpirationDatetimeColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.SysPendingPasswordUpdateExpirationDatetimeColumn', {
    extend: 'Ext.grid.column.Date',
    alias: 'widget.syspendingpasswordupdateexpirationdatetimecolumn',

    hidden: true,
    dataIndex: 'sys_pending_password_update_expiration_datetime',
    text: 'sys_pending_password_update_expiration_datetime',
    format: 'Y-m-d H:i',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
