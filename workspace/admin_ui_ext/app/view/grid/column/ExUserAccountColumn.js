/*
 * File: app/view/grid/column/ExUserAccountColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.ExUserAccountColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.exuseraccountcolumn',

    hidden: true,
    dataIndex: 'ex_user_account',
    text: 'ex_user_account',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
