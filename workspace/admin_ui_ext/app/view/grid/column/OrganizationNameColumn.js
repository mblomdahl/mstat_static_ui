/*
 * File: app/view/grid/column/OrganizationNameColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.OrganizationNameColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.organizationnamecolumn',

    hidden: true,
    dataIndex: 'organization_name',
    text: 'company_name',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
