/*
 * File: app/view/grid/column/ExFullNameColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.ExFullNameColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.exfullnamecolumn',

    dataIndex: 'ex_full_name',
    text: 'ex_full_name',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
