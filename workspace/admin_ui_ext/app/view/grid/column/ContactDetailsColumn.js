/*
 * File: app/view/grid/column/ContactDetailsColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.ContactDetailsColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.contactdetailscolumn',

    hidden: true,
    dataIndex: 'contact_details',
    text: 'contact_details',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
