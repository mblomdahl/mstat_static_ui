/*
 * File: app/view/grid/column/QuotaColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.QuotaColumn', {
    extend: 'Ext.grid.column.Number',
    alias: 'widget.quotacolumn',

    hidden: true,
    dataIndex: 'quota',
    text: 'quota',
    format: '000',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
