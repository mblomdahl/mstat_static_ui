/*
 * File: app/view/grid/column/DeletedDatetimeColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.DeletedDatetimeColumn', {
    extend: 'Ext.grid.column.Date',
    alias: 'widget.deleteddatetimecolumn',

    hidden: true,
    dataIndex: 'deleted_datetime',
    text: 'deleted_datetime',
    format: 'Y-m-d H:i',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
