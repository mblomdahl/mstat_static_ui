/*
 * File: app/view/grid/column/ExBillingContactColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.ExBillingContactColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.exbillingcontactcolumn',

    hidden: true,
    dataIndex: 'ex_billing_contact',
    text: 'ex_billing_contact',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
