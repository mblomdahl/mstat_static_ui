/*
 * File: app/view/grid/column/DatastoreKeyIdColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.DatastoreKeyIdColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.datastorekeyidcolumn',

    dataIndex: 'datastore_key_id',
    text: 'datastore_key_id',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
