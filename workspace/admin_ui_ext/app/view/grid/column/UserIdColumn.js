/*
 * File: app/view/grid/column/UserIdColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.UserIdColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.useridcolumn',

    hidden: true,
    dataIndex: 'user_id',
    text: 'user_id',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
