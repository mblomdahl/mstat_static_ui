/*
 * File: app/view/grid/column/SuspendedColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.SuspendedColumn', {
    extend: 'Ext.grid.column.Boolean',
    alias: 'widget.suspendedcolumn',

    hidden: true,
    dataIndex: 'suspended',
    text: 'suspended',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
