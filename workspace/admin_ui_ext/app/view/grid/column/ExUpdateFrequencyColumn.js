/*
 * File: app/view/grid/column/ExUpdateFrequencyColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.ExUpdateFrequencyColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.exupdatefrequencycolumn',

    dataIndex: 'ex_update_frequency',
    text: 'ex_update_frequency',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
