/*
 * File: app/view/grid/column/DatastoreKeyUrlsafeColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.DatastoreKeyUrlsafeColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.datastorekeyurlsafecolumn',

    hidden: true,
    dataIndex: 'datastore_key_urlsafe',
    text: 'datastore_key_urlsafe',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
