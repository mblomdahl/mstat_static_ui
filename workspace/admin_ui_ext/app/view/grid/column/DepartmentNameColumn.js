/*
 * File: app/view/grid/column/DepartmentNameColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.DepartmentNameColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.departmentnamecolumn',

    hidden: true,
    dataIndex: 'department_name',
    text: 'department_name',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
