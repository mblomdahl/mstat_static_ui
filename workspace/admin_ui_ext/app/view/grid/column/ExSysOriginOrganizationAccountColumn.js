/*
 * File: app/view/grid/column/ExSysOriginOrganizationAccountColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.ExSysOriginOrganizationAccountColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.exsysoriginorganizationaccountcolumn',

    hidden: true,
    dataIndex: 'ex_sys_origin_organization_account',
    text: 'ex_sys_origin_organization_account',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
