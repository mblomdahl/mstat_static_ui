/*
 * File: app/view/grid/column/SuspendedDatetimeColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.SuspendedDatetimeColumn', {
    extend: 'Ext.grid.column.Date',
    alias: 'widget.suspendeddatetimecolumn',

    hidden: true,
    dataIndex: 'suspended_datetime',
    text: 'suspended_datetime',
    format: 'Y-m-d H:i',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
