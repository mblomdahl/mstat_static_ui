/*
 * File: app/view/grid/column/OrganizationIdColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.OrganizationIdColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.organizationidcolumn',

    hidden: true,
    dataIndex: 'organization_id',
    text: 'registration_number',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
