/*
 * File: app/view/grid/column/StartDateColumn.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.grid.column.StartDateColumn', {
    extend: 'Ext.grid.column.Date',
    alias: 'widget.startdatecolumn',

    dataIndex: 'start_date',
    text: 'start_date',
    format: 'Y-m-d',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
