/*
 * File: app/view/Viewport.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.Viewport', {
    extend: 'AdminInterface.view.AdminInterfaceViewport',
    renderTo: Ext.getBody()
});
