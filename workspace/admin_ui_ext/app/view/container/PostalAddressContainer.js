/*
 * File: app/view/container/PostalAddressContainer.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.container.PostalAddressContainer', {
    extend: 'Ext.container.Container',
    alias: 'widget.postaladdresscontainer',

    requires: [
        'AdminInterface.view.container.PostalAddressTypeContainer',
        'AdminInterface.view.form.field.GenericTextField',
        'AdminInterface.view.form.field.PostalCodeTextField',
        'AdminInterface.view.form.field.PostalTownTextField',
        'AdminInterface.view.form.field.CountryTextField'
    ],

    exFieldsDisabled: false,
    margin: '0 0 0 0',
    layout: {
        type: 'anchor'
    },

    initComponent: function() {
        var me = this;

        me.addEvents(
            'exFieldsEnable',
            'exFieldsDisable'
        );

        Ext.applyIf(me, {
            linkedContainers: {

            },
            items: [
                {
                    xtype: 'container',
                    layout: {
                        type: 'anchor'
                    },
                    items: [
                        {
                            xtype: 'generictextfield',
                            margin: '0 0 5 0',
                            fieldLabel: 'Recipient Line 1*',
                            name: 'recipient_lines_1',
                            allowBlank: false,
                            anchor: '100%'
                        },
                        {
                            xtype: 'generictextfield',
                            margin: '0 0 5 0',
                            fieldLabel: 'Recipient Line 2',
                            name: 'recipient_lines_2',
                            anchor: '100%'
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'anchor'
                    },
                    items: [
                        {
                            xtype: 'radiogroup',
                            preserveDisabled: false,
                            margin: 0,
                            fieldLabel: 'Address Type',
                            labelWidth: 150,
                            items: [
                                {
                                    xtype: 'radiofield',
                                    preserveDisabled: false,
                                    margin: '0 0 5 0',
                                    name: 'address_type',
                                    boxLabel: 'Street Address',
                                    checked: true,
                                    inputValue: 'street_address'
                                },
                                {
                                    xtype: 'radiofield',
                                    preserveDisabled: false,
                                    margin: '0 0 5 0',
                                    name: 'address_type',
                                    boxLabel: 'Box Address',
                                    inputValue: 'box_address'
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'postaladdresstypecontainer'
                },
                {
                    xtype: 'container',
                    margin: '0 0 0 0',
                    layout: {
                        align: 'stretch',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'postalcodetextfield',
                            flex: 0.75,
                            margins: '0 5 5 0'
                        },
                        {
                            xtype: 'postaltowntextfield',
                            flex: 2.5,
                            margins: '0 5 5 5'
                        },
                        {
                            xtype: 'countrytextfield',
                            flex: 1,
                            margins: '0 0 5 5'
                        }
                    ]
                }
            ],
            listeners: {
                exFieldsDisable: {
                    fn: me.onContainerExFieldsDisable,
                    scope: me
                },
                exFieldsEnable: {
                    fn: me.onContainerExFieldsEnable,
                    scope: me
                }
            }
        });

        me.callParent(arguments);
    },

    onContainerExFieldsDisable: function(eventOptions) {
        //console.error('fieldsDisable event');
        //console.log(arguments);

        //var targetAddressFieldsQuery = this.controller.addressFieldXTypes.join(', ');

        if (!this.exFieldsDisabled) {
            this.exFieldsDisabled = true;
            var containerFields = this.query('field, checkboxgroup');

            for (var i = containerFields.length; i--; ) {
                containerFields[i].setDisabled(true);
                containerFields[i].preserveDisabled = true;
            }
        }
    },

    onContainerExFieldsEnable: function(eventOptions) {
        //console.error('fieldsEnable event');
        //console.log(arguments);

        //var targetAddressFieldsQuery = this.controller.addressFieldXTypes.join(', ');

        var excludeXtypes;
        if (this.down('radiogroup').getValue().address_type == "box_address") {
            // Don't enable hidden inputs
            excludeXtypes = [
            'routenametextfield',
            'streetnumbertextfield',
            'streetentrancetextfield',
            'apartmentfloorcombobox'
            ];

        } else {
            // This

            excludeXtypes = [
            'boxaddresstextfield'
            ];

        }

        if (this.exFieldsDisabled) {
            this.exFieldsDisabled = false;
            var containerFields = this.query('field, checkboxgroup');

            for (var i = containerFields.length; i--; ) {
                if (excludeXtypes.indexOf(containerFields[i].xtype) == -1) {
                    containerFields[i].setDisabled(false);
                }
                containerFields[i].preserveDisabled = false;
            }
        }

    },

    getLinkedContainers: function(handle) {

        if (handle) {
            return this.linkedContainers[handle];
        }

        var linkedContainers = [];

        for (var property in this.linkedContainers) {
            linkedContainers.push(this.linkedContainers[property]);
        }

        if (!linkedContainers.length) {
            return null;
        } else {
            return linkedContainers;
        }

    },

    bindLinkedContainer: function(handle, container) {
        container = arguments[arguments.length - 1];

        if (arguments.length > 1) {
            this.linkedContainers[handle] = container;
        } else {
            this.linkedContainers[container.id] = container;
        }

        return container;
    },

    releaseLinkedContainer: function(handle) {

        if (handle) {
            var container = this.linkedContainers[handle];
            delete this.linkedContainers[handle];
            return container;
        } else {
            var linkedContainers = this.getLinkedContainers();
            this.linkedContainers = {};
            return linkedContainers;
        }

    },

    getValues: function() {

        var fields = this.query('field');
        var valueMap = {};

        for (var i = fields.length; i--; ) {
            valueMap[fields[i].name] = fields[i].getValue();
        }

        return valueMap;
    },

    getFormatted: function(fieldValues) {

        fieldValues = fieldValues || this.getValues();

        //console.log('fieldValues:');
        //console.log(fieldValues);

        var formattedAddress = [fieldValues.recipient_lines_1];

        fieldValues.recipient_lines_2 ? formattedAddress.push(fieldValues.recipient_lines_2) : null;

        if (fieldValues.address_type === 'box_address') {
            formattedAddress.push('Box ' + fieldValues.box_address);
        } else {
            var streetAddress = [fieldValues.route_name, fieldValues.street_number, fieldValues.street_entrance].join(' ').trim();

            if (fieldValues.apartment_floor) {
                streetAddress += ', ' + fieldValues.apartment_floor;
            }

            formattedAddress.push(streetAddress);
        }

        if (fieldValues.postal_code) {
            formattedAddress.push(fieldValues.postal_code.substr(0,3) + ' ' + fieldValues.postal_code.substr(3) + '&nbsp;&nbsp;' + fieldValues.postal_town);
        }

        return formattedAddress.join('<br>\n');

    }

});
