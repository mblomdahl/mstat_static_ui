/*
 * File: app/view/container/PostalAddressTypeContainer.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.container.PostalAddressTypeContainer', {
    extend: 'Ext.container.Container',
    alias: 'widget.postaladdresstypecontainer',

    requires: [
        'AdminInterface.view.form.field.RouteNameTextField',
        'AdminInterface.view.form.field.StreetNumberTextField',
        'AdminInterface.view.form.field.StreetEntranceTextField',
        'AdminInterface.view.form.field.ApartmentFloorCombobox',
        'AdminInterface.view.form.field.BoxAddressTextField'
    ],

    layout: {
        type: 'card'
    },

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    layout: {
                        align: 'stretch',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'routenametextfield',
                            fieldLabel: 'Street Name*',
                            flex: 2.5,
                            margins: '0 5 5 0'
                        },
                        {
                            xtype: 'streetnumbertextfield',
                            flex: 1.2,
                            margins: '0 5 5 5'
                        },
                        {
                            xtype: 'streetentrancetextfield',
                            flex: 1,
                            margins: '0 5 5 5'
                        },
                        {
                            xtype: 'apartmentfloorcombobox',
                            flex: 1,
                            margins: '0 0 5 5'
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'anchor'
                    },
                    items: [
                        {
                            xtype: 'boxaddresstextfield',
                            disabled: true,
                            anchor: '65%'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
