/*
 * File: app/view/container/ContactDetailsContainer.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.view.container.ContactDetailsContainer', {
    extend: 'Ext.container.Container',
    alias: 'widget.contactdetailscontainer',

    requires: [
        'AdminInterface.view.form.field.PersonNameTextField',
        'AdminInterface.view.form.field.GenericTextField',
        'AdminInterface.view.form.field.EmailAddressTextField',
        'AdminInterface.view.form.field.PhoneNumberTextField'
    ],

    exFieldsDisabled: false,
    margin: 0,

    initComponent: function() {
        var me = this;

        me.addEvents(
            'exFieldsDisable',
            'exFieldsEnable'
        );

        Ext.applyIf(me, {
            linkedContainers: [

            ],
            items: [
                {
                    xtype: 'container',
                    layout: {
                        align: 'stretch',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'personnametextfield',
                            fieldLabel: 'First Name*',
                            labelAlign: 'top',
                            name: 'first_name',
                            flex: 2,
                            margins: '0 5 10 0'
                        },
                        {
                            xtype: 'personnametextfield',
                            fieldLabel: 'Last Name*',
                            labelAlign: 'top',
                            name: 'last_name',
                            flex: 2,
                            margins: '0 5 10 5'
                        },
                        {
                            xtype: 'generictextfield',
                            fieldLabel: 'Job Title',
                            labelAlign: 'top',
                            name: 'job_title',
                            flex: 1.5,
                            margins: '0 0 10 5'
                        }
                    ]
                },
                {
                    xtype: 'container',
                    margin: '0 0 0 0',
                    layout: {
                        align: 'stretch',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'emailaddresstextfield',
                            fieldLabel: 'Email Address*',
                            labelAlign: 'top',
                            name: 'email_addresses_work',
                            allowBlank: false,
                            flex: 3,
                            margins: '0 5 10 0'
                        },
                        {
                            xtype: 'phonenumbertextfield',
                            fieldLabel: 'Primary/Office Phone',
                            labelAlign: 'top',
                            name: 'phone_numbers_office',
                            flex: 1.8,
                            margins: '0 5 10 5'
                        },
                        {
                            xtype: 'phonenumbertextfield',
                            fieldLabel: 'Mobile Phone',
                            labelAlign: 'top',
                            name: 'phone_numbers_mobile',
                            flex: 1.5,
                            margins: '0 0 10 5'
                        }
                    ]
                }
            ],
            listeners: {
                exFieldsDisable: {
                    fn: me.onContainerExFieldsDisable,
                    scope: me
                },
                exFieldsEnable: {
                    fn: me.onContainerExFieldsEnable,
                    scope: me
                }
            }
        });

        me.callParent(arguments);
    },

    onContainerExFieldsDisable: function(eventOptions) {
        //console.error('fieldsDisable.arguments');
        //console.log(arguments);

        if (!this.exFieldsDisabled) {
            this.exFieldsDisabled = true;
            var containerFields = this.query('field');

            for (var i = containerFields.length; i--; ) {
                containerFields[i].setDisabled(true);
                containerFields[i].preserveDisabled = true;
            }
        }

    },

    onContainerExFieldsEnable: function(eventOptions) {
        //console.error('fieldsEnable.arguments:');
        //console.log(arguments);

        if (this.exFieldsDisabled) {
            this.exFieldsDisabled = false;
            var containerFields = this.query('field');

            for (var i = containerFields.length; i--; ) {
                containerFields[i].setDisabled(false);
                containerFields[i].preserveDisabled = false;
            }
        }

    },

    getLinkedContainers: function(handle) {

        if (handle) {
            return this.linkedContainers[handle];
        }

        var linkedContainers = [];

        for (var property in this.linkedContainers) {
            linkedContainers.push(this.linkedContainers[property]);
        }

        if (!linkedContainers.length) {
            return null;
        } else {
            return linkedContainers;
        }

    },

    bindLinkedContainer: function(handle, container) {
        container = arguments[arguments.length - 1];

        if (arguments.length > 1) {
            this.linkedContainers[handle] = container;
        } else {
            this.linkedContainers[container.id] = container;
        }

        return container;
    },

    releaseLinkedContainer: function(handle) {

        if (handle) {
            var container = this.linkedContainers[handle];
            delete this.linkedContainers[handle];
            return container;
        } else {
            var linkedContainers = this.getLinkedContainers();
            this.linkedContainers = {};
            return linkedContainers;
        }

    },

    getValues: function() {

        var fields = this.query('field');
        var valueMap = {};

        for (var i = fields.length; i--; ) {
            valueMap[fields[i].name] = fields[i].getValue();
        }

        return valueMap;
    },

    getFormatted: function(fieldValues) {

        fieldValues = fieldValues || this.getValues();

        //console.log('fieldValues:');
        //console.log(fieldValues);

        var nameLine = fieldValues.first_name + ' ' + fieldValues.last_name;

        nameLine = fieldValues.job_title ? [nameLine, fieldValues.job_title].join(', ') : nameLine;

        var formattedDetails = [nameLine];

        formattedDetails.push('Email Address: ' + fieldValues.email_addresses_work + ' (work)');

        if (fieldValues.phone_numbers_office || fieldValues.phone_numbers_mobile) {
            var mobilePhone = fieldValues.phone_numbers_mobile ? fieldValues.phone_numbers_mobile + ' (mobile)' : null;
            var officePhone = fieldValues.phone_numbers_office ? fieldValues.phone_numbers_office + ' (primary/office)' : null;

            if (officePhone && mobilePhone) {
                var phoneNumbersLine = 'Phone Numbers: ' + [officePhone, mobilePhone].join(', ');
            } else {
                var phoneNumbersLine = 'Phone Number: ' + (officePhone || mobilePhone);
            }

            formattedDetails.push(phoneNumbersLine);
        }

        return formattedDetails.join('<br>\n');
    }

});
