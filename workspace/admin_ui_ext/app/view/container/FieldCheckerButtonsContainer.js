/*
 * File: app/view/container/FieldCheckerButtonsContainer.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('AdminInterface.view.container.FieldCheckerButtonsContainer', {
    extend: 'Ext.container.Container',
    alias: 'widget.fieldcheckerbuttonscontainer',

    columnWidth: 1,
    layout: {
        pack: 'end',
        type: 'hbox'
    },

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    layout: {
                        align: 'stretch',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'container',
                            layout: {
                                align: 'stretch',
                                pack: 'end',
                                type: 'vbox'
                            },
                            items: [
                                {
                                    xtype: 'label',
                                    margins: '0 0 2 0',
                                    text: 'Check fields:'
                                },
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox'
                                    },
                                    items: [
                                        {
                                            xtype: 'button',
                                            margins: '0 5 0 0',
                                            text: 'All'
                                        },
                                        {
                                            xtype: 'button',
                                            margins: '0 5 5 5',
                                            text: 'Standard'
                                        },
                                        {
                                            xtype: 'button',
                                            margins: '0 0 5 5',
                                            text: 'None'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }
});
