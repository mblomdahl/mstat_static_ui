/*
 * File: app/controller/AddSubscriptionCtrl.js
 * Author: Mats Blomdahl
 * Version: 1.05
 */

Ext.define('AdminInterface.controller.AddSubscriptionCtrl', {
    extend: 'Ext.app.Controller',
    alias: 'controller.addsubscriptionctrl',

    models: [
        'Subscription',
        'OrganizationAccount'
    ],
    stores: [
        'AdminInterface.store.SubscriberAccounts',
        'AdminInterface.store.UserAccounts',
        'AdminInterface.store.Subscriptions'
    ],
    views: [
        'AdminInterface.view.window.AddSubscriptionWindow'
    ],

    refs: [
        {
            ref: 'addSubscriptionWindow',
            selector: '#addSubscriptionWindow',
            xtype: 'addsubscriptionwindow'
        },
        {
            ref: 'addSubscriptionStepContainer',
            selector: '#addSubscriptionStepContainer',
            xtype: 'container'
        },
        {
            ref: 'addSubscriptionStep1Container',
            selector: '#addSubscriptionStep1Container',
            xtype: 'container'
        },
        {
            ref: 'addSubscriptionStep2aContainer',
            selector: '#addSubscriptionStep2aContainer',
            xtype: 'container'
        },
        {
            ref: 'addSubscriptionStep2bContainer',
            selector: '#addSubscriptionStep2bContainer',
            xtype: 'container'
        },
        {
            ref: 'addSubscriptionStep2cContainer',
            selector: '#addSubscriptionStep2cContainer',
            xtype: 'container'
        },
        {
            ref: 'addSubscriptionStep2dContainer',
            selector: '#addSubscriptionStep2dContainer',
            xtype: 'container'
        },
        {
            ref: 'addSubscriptionStep3Container',
            selector: '#addSubscriptionStep3Container',
            xtype: 'container'
        },
        {
            ref: 'addSubscriptionStep1FormPanel',
            selector: '#addSubscriptionStep1FormPanel',
            xtype: 'form'
        },
        {
            ref: 'addSubscriptionStep2aFormPanel',
            selector: '#addSubscriptionStep2aFormPanel',
            xtype: 'form'
        },
        {
            ref: 'addSubscriptionStep2bFormPanel',
            selector: '#addSubscriptionStep2bFormPanel',
            xtype: 'form'
        },
        {
            ref: 'addSubscriptionStep2cFormPanel',
            selector: '#addSubscriptionStep2cFormPanel',
            xtype: 'form'
        },
        {
            ref: 'addSubscriptionStep2dFormPanel',
            selector: '#addSubscriptionStep2dFormPanel',
            xtype: 'form'
        },
        {
            ref: 'addSubscriptionStep1SubscriberCombobox',
            selector: 'addsubscriptionwindow subscriberaccountcombobox',
            xtype: 'subscriberaccountcombobox'
        },
        {
            ref: 'addSubscriptionStep1QuotaSliderField',
            selector: '#addSubscriptionStep1FormPanel sliderfield',
            xtype: 'slider'
        },
        {
            ref: 'addSubscriptionStep1QuotaAuxNumberField',
            selector: '#addSubscriptionFormPanelQuotaAuxNumberField',
            xtype: 'numberfield'
        },
        {
            ref: 'addSubscriptionConfirmParentSubscriberDisplayField',
            selector: '#addSubscriptionConfirmParentSubscriberDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addSubscriptionConfirmFriendlyNameDisplayField',
            selector: '#addSubscriptionConfirmFriendlyNameDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addSubscriptionConfirmDescriptionDisplayField',
            selector: '#addSubscriptionConfirmDescriptionDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addSubscriptionConfirmSubscriptionDateIntervalDisplayField',
            selector: '#addSubscriptionConfirmSubscriptionDateIntervalDisplayField',
            xtype: 'form'
        },
        {
            ref: 'addSubscriptionConfirmSubscriptionTypeDisplayField',
            selector: '#addSubscriptionConfirmSubscriptionTypeDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addSubscriptionConfirmSubscriptionDateIntervalDisplayField',
            selector: '#addSubscriptionConfirmSubscriptionDateIntervalDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addSubscriptionConfirmDateIntervalDisplayField',
            selector: '#addSubscriptionConfirmDateIntervalDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addSubscriptionConfirmQuotaDisplayField',
            selector: '#addSubscriptionConfirmQuotaDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addSubscriptionConfirmUpdateFrequencyDisplayField',
            selector: '#addSubscriptionConfirmUpdateFrequencyDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addSubscriptionConfirmFieldInclusionDisplayField',
            selector: '#addSubscriptionConfirmFieldInclusionDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addSubscriptionCancelButton',
            selector: 'addsubscriptionwindow genericcancelbutton',
            xtype: 'genericcancelbutton'
        },
        {
            ref: 'addSubscriptionBackButton',
            selector: 'addsubscriptionwindow genericbackbutton',
            xtype: 'genericbackbutton'
        },
        {
            ref: 'addSubscriptionNextButton',
            selector: 'addsubscriptionwindow genericnextbutton',
            xtype: 'genericnextbutton'
        }
    ],

    /**
     * # Stub.
     *
     * @param application
     */
    init: function(application) {
        this.optDebug = application.optDebug;
        this.debug = false || application.debug;
        this.optDebug('AddSubscriptionCtrl.arguments', arguments);

        this.control({
            '#addSubscriptionWindow': {
                hide: this.onAddSubscriptionWindowHide,
                show: this.onAddSubscriptionWindowShow
            },
            '#addSubscriptionWindow field:focusable': {
                specialkey: this.onAddSubscriptionWindowFieldSpecialKey
            },
            '#addSubscriptionStep1Container': {
                beforeactivate: this.onAddSubscriptionStep1ContainerBeforeActivate,
                beforeshow: this.onAddSubscriptionStep1ContainerActivate,
                activate: this.onAddSubscriptionStep1ContainerActivate
            },
            '#addSubscriptionStepContainer > container[id^="addSubscriptionStep2"]': {
                afterrender: this.onAddSubscriptionStep2ComponentsAfterRender,
                beforeactivate: this.onAddSubscriptionStep2ContainerBeforeActivate,
                activate: this.onAddSubscriptionStep2ContainerActivate
            },
            '#addSubscriptionStepContainer > container[id^="addSubscriptionStep2"] field': {
                afterrender: this.onAddSubscriptionStep2ComponentsAfterRender
            },
            '#addSubscriptionStep3Container': {
                beforeactivate: this.onAddSubscriptionStep3ContainerBeforeActivate,
                activate: this.onAddSubscriptionStep3ContainerActivate,
                beforedeactivate: this.onAddSubscriptionStep3ContainerBeforeDeactivate
            },
            '#addSubscriptionStep1FormPanel': {
                fieldvaliditychange: this.onAddSubscriptionStep1FormPanelFieldValidityChange
            },
            '#addSubscriptionStep1FormPanel sliderfield': {
                change: this.onAddSubscriptionStep1QuotaSliderFieldChange
            },
            '#addSubscriptionStep1UpdateFrequencyRadioGroup': {
                change: this.onAddSubscriptionStep1UpdateFrequencyRadioGroupChange
            },
            '#addSubscriptionStepContainer fieldinclusioncheckbox': {
                change: this.onAddSubscriptionStep2FieldInclusionCheckboxChange
            },
            'container[id^="addSubscriptionStep2"][id$="FieldCheckerContainer"] button': {
                click: this.onAddSubscriptionStep2FieldCheckerButtonClick
            },
            'addsubscriptionwindow genericcancelbutton': {
                click: this.onAddSubscriptionCancelButtonClick
            },
            'addsubscriptionwindow genericbackbutton': {
                click: this.onAddSubscriptionBackButtonClick
            },
            'addsubscriptionwindow genericnextbutton': {
                click: this.onAddSubscriptionNextButtonClick
            }
        });

        this.activeStep = 0;

        this.stepConfig = [{
            // step 1
            nextCallback: function(button, clickEvent, eOpts) {
                this.optDebug('step1NextCallback.arguments', arguments);
                this.validateStep1();
            }
        }, {
            // step 2a
            nextCallback: function(button, clickEvent, eOpts) {
                this.optDebug('step2aNextCallback.arguments', arguments);
                this.validateStep2a();
            }
        }, {
            // step 2b
            nextCallback: function(button, clickEvent, eOpts) {
                this.optDebug('step2bNextCallback.arguments', arguments);
                this.validateStep2b();
            }
        }, {
            // step 2c
            nextCallback: function(button, clickEvent, eOpts) {
                this.optDebug('step2cNextCallback.arguments', arguments);
                this.validateStep2c();
            }
        }, {
            // step 2d
            nextCallback: function(button, clickEvent, eOpts) {
                this.optDebug('step2dNextCallback.arguments', arguments);
                this.validateStep2d();
            }
        }, {
            // step 3
            nextCallback: function(button, clickEvent, eOpts) {
                this.optDebug('step3NextCallback.arguments', arguments);
                if (this.editSubscription) {
                    this.submitEditSubscription();
                } else {
                    this.submitAddSubscription();
                }
            }
        }];

        this.disabledFields = [
            'locality',
            'sublocality',
            'location_wgs84_geopt',
            'build_year_lower',
            'build_year_upper',
            'operating_costs',
            'sys_changelog',
            'sys_changelog_version',
            'sys_invalidation_codes'
        ];

        this.standardFields = [
            'transaction_id',
            'contract_price',
            'contract_date',
            'date_of_possession',
            'data_transfer_date',
            'mstat_restricted',
            'real_estate_designation',
            'real_estate_taxation_code',
            'municipality',
            'municipality_lkf',
            'county',
            'county_lkf',
            'congregation',
            'congregation_lkf',
            'monthly_fee',
            'location_rt90_geopt',
            'apartment_number',
            'formatted_address',
            'housing_cooperative_registration_number',
            'housing_cooperative_name',
            'housing_cooperative_share',
            'heating_included',
            'type_of_housing',
            'housing_category',
            'housing_tenure',
            'apartment_floor',
            'building_storeys',
            'energy_rating',
            'energy_performance',
            'living_area',
            'plot_area',
            'number_of_rooms',
            'elevator',
            'balcony',
            'new_production',
            'build_year',
            'baseline_year_of_assessment',
            'baseline_rateable_value',
            'sys_scb_invalidation_code',
            'sys_raw_data_source'
        ];


        application.on({
            exDialogOpen: {
                fn: this.onExDialogOpen,
                scope: this
            },
            exDialogClose: {
                fn: this.onExDialogClose,
                scope: this
            }
        });
    },

    /**
     * # Stub.
     *
     * @param parentSubscriber
     * @param subscription
     */
    showWindow: function(parentSubscriber, subscription) {
        this.optDebug('showWindow.arguments: ', arguments);

        this.resetFields();

        Ext.ClassManager.get('AdminInterface.view.window.AddSubscriptionWindow').create();

        var addSubscriptionWindow = this.getAddSubscriptionWindow();

        if (subscription) {
            var subscriptionsStore = Ext.getStore('AdminInterface.store.Subscriptions');
            this.editSubscription = subscriptionsStore.getById(subscription);
            this.addSubscriptionStep1Fields = {
                subscriber: parentSubscriber,
                start_date: this.editSubscription.get('content_restrictions').start_date,
                end_date: this.editSubscription.get('content_restrictions').end_date,
                subscription_start_date: this.editSubscription.get('start_date'),
                subscription_end_date: this.editSubscription.get('end_date'),
                friendly_name: this.editSubscription.get('friendly_name'),
                description: this.editSubscription.get('description'),
                update_frequency: this.editSubscription.get('update_frequency'),
                subscription_type: this.editSubscription.get('subscription_type')
            };
            addSubscriptionWindow.setTitle('Edit Subscription');

        } else {

            if (parentSubscriber) {
                this.optDebug('parentSubscriber:', parentSubscriber);

                var defaultStartDate = new Date();

                defaultStartDate.setFullYear(defaultStartDate.getFullYear()-1);
                defaultStartDate.setDate(1);

                this.addSubscriptionStep1Fields = {
                    subscriber: parentSubscriber,
                    subscription_start_date: new Date(),
                    start_date: defaultStartDate
                };
            }

            addSubscriptionWindow.setTitle('Add New Subscription');
            //this.getAddUserFormPanelAncestorCombobox().setValue(parentSubscriber);
        }

        this.setActiveStep(0);

        addSubscriptionWindow.show();

        target = this;
    },

    /**
     * # Stub.
     *
     * @param dialogWindow
     */
    hideWindow: function(dialogWindow) {
        this.optDebug('hideWindow.arguments: ', arguments);

        dialogWindow = dialogWindow || this.getAddSubscriptionWindow();

        dialogWindow.close();
    },

    /**
     * # Stub.
     *
     * @param container
     * @param heightExtra
     * @param reposition
     * @returns {center|*|center|center|center|Component}
     */
    resizeWindowHeight: function(container, heightExtra, reposition) {
        this.optDebug('resizeWindowHeight.arguments: ', arguments);

        reposition = reposition || false;

        var parentWindow = container.up('window');
        if (!parentWindow.rendered) {
            this.optDebug('parentWindow.rendered:' + parentWindow.rendered);
            return;
        } else {
            this.optDebug('parentWindow:', parentWindow);
        }

        var activeContainerWrapper = container.up('container');
        if (!activeContainerWrapper.rendered) {
            this.optDebug('activeContainerWrapper.rendered:' + activeContainerWrapper.rendered);
            return;
        } else {
            this.optDebug('activeContainerWrapper:', activeContainerWrapper);
        }

        var wrapperHeightDelta = activeContainerWrapper.getHeight();
        this.optDebug('wrapperHeightDelta:', wrapperHeightDelta);

        var activeViewContent = activeContainerWrapper.query('#' + container.id + ' > container');
        this.optDebug('activeViewContent:', activeViewContent);

        for (var i = 0, j = activeViewContent.length; i < j; i++) {

            if (!activeViewContent[i].rendered) {
                this.optDebug('activeViewContent[i].rendered:' + activeViewContent[i].rendered);
                return;
            }

            wrapperHeightDelta -= activeViewContent[i].getHeight();
        }

        if (heightExtra) {
            wrapperHeightDelta -= heightExtra;
        }

        parentWindow.setHeight(parentWindow.getHeight() - wrapperHeightDelta);

        if (reposition) {

            return parentWindow.center();
            /*
            var windowPosition = parentWindow.getPosition();

            windowPosition[1] += wrapperHeightDelta / 2;
            if (windowPosition[1] < 0) {
            windowPosition[1] = 0;
            }

            parentWindow.setPosition(windowPosition[0], windowPosition[1]);
            */
        }

    },

    /**
     * # Stub.
     *
     * @param stepIndex
     */
    setActiveStep: function(stepIndex) {
        // Card layout of step view wrapper
        var stepContainerLayout = this.getAddSubscriptionStepContainer().getLayout();

        // Save wrapper active step state
        this.activeStep = stepIndex;

        stepContainerLayout.setActiveItem(stepIndex);
    },

    /**
     * # Stub.
     */
    resetFields: function() {
        this.optDebug('resetFields.arguments', arguments);

        this.editSubscription = null;

        // Reset step 1 saved state
        this.addSubscriptionStep1Fields = null;

        // Reset step 2 saved state
        this.addSubscriptionStep2LastCheckboxFocused = null;
        this.addSubscriptionStep2IgnoreCheckboxChange = false;

        // Reset step 2a saved state
        this.addSubscriptionStep2aFields = null;

        // Reset step 2b saved state
        this.addSubscriptionStep2bFields = null;

        // Reset step 2c saved state
        this.addSubscriptionStep2cFields = null;

        // Reset step 2d saved state
        this.addSubscriptionStep2dFields = null;

    },

    /**
     * # Stub.
     *
     * @param container
     * @param fields
     */
    checkFieldInclusionCheckboxes: function(container, fields) {
        this.optDebug('checkFieldInclusionCheckboxes.arguments: ', arguments);

        // Enabled descendant field inclusion checkboxes
        var fieldInclusionCheckboxes = container.query('fieldinclusioncheckbox[disabled=false]');

        // Suspend handling of FieldInclusionCheckbox component change events
        this.addSubscriptionStep2IgnoreCheckboxChange = true;

        for (var i = fieldInclusionCheckboxes.length; i--; ) {
            // Check/uncheck standard field
            fieldInclusionCheckboxes[i].setValue(fields.indexOf(fieldInclusionCheckboxes[i].inputValue) !== -1);
        }

        // Resume handling of checkbox change events
        this.addSubscriptionStep2IgnoreCheckboxChange = false;
    },

    /**
     * # Stub.
     *
     * @param container
     */
    checkAllFieldInclusionCheckboxes: function(container) {
        this.optDebug('checkAllFieldInclusionCheckboxes.arguments: ', arguments);

        // Enabled descendant field inclusion checkboxes
        var fieldInclusionCheckboxes = container.query('fieldinclusioncheckbox[value=false][disabled=false]');

        // Suspend handling of FieldInclusionCheckbox component change events
        this.addSubscriptionStep2IgnoreCheckboxChange = true;

        // Set all checkboxes to `checked`
        for (var i = fieldInclusionCheckboxes.length; i--; ) {
            fieldInclusionCheckboxes[i].setValue(true);
        }

        // Resume handling of checkbox change events
        this.addSubscriptionStep2IgnoreCheckboxChange = false;
    },

    /**
     * # Stub.
     *
     * @param container
     */
    checkStandardFieldInclusionCheckboxes: function(container) {
        this.optDebug('checkStandardFieldInclusionCheckboxes.arguments: ', arguments);

        // Check collection of all default/standard fields
        this.checkFieldInclusionCheckboxes(container, this.standardFields);


    },

    /**
     * # Stub.
     *
     * @param container
     */
    uncheckAllFieldInclusionCheckboxes: function(container) {
        this.optDebug('uncheckAllFieldInclusionCheckboxes.arguments: ', arguments);

        // Suspend handling of FieldInclusionCheckbox component change events
        this.addSubscriptionStep2IgnoreCheckboxChange = true;

        container.down('form').getForm().reset();

        // Resume handling of checkbox change events
        this.addSubscriptionStep2IgnoreCheckboxChange = false;
    },

    /**
     * # Stub.
     *
     * @returns {*}
     */
    validateStep1: function() {
        this.optDebug('validateStep1.arguments: ', arguments);

        // Combobox for selecting subscriber entry
        var addSubscriptionSubscriberCombobox = this.getAddSubscriptionStep1SubscriberCombobox();

        // Form panel for subscription settings such as friendly name, description and accessible date range
        var addSubscriptionFormPanel = this.getAddSubscriptionStep1FormPanel();

        // Query for subscription settings form fields to enable/disabl
        var addSubscriptionFormPanelFields = addSubscriptionFormPanel.query([
        'radiogroup',
        'genericdatefield',
        'slider',
        'displayfield'
        ].join(', '));

        // Check if ancestor selection is valid
        var validateFormPanel = addSubscriptionSubscriberCombobox.isValid();
        this.optDebug('addSubscriptionSubscriberCombobox.isValid(): ', validateFormPanel);

        // Disable subscription input fields for settings
        for (var i = addSubscriptionFormPanelFields.length; i--; ) {
            addSubscriptionFormPanelFields[i].setDisabled(!validateFormPanel);
        }

        if (validateFormPanel) {
            // Validate subscription settings form panel
            if (!addSubscriptionFormPanel.getForm().hasInvalidField()) {
                // Save state for step 1 view
                this.addSubscriptionStep1Fields = addSubscriptionFormPanel.getForm().getValues();
                this.addSubscriptionStep1Fields.subscriber = addSubscriptionSubscriberCombobox.getValue();

                // Update `next` button to reflect successful validation
                return this.getAddSubscriptionNextButton().setDisabled(false);
            }
        }

        // Update `next` button to reflect validation failure
        this.getAddSubscriptionNextButton().setDisabled(true);


    },

    /**
     * # Stub.
     *
     */
    validateStep2a: function() {
        this.optDebug('validateStep2a.arguments: ', arguments);

        // Form panel containing field inclusion checkboxes
        var addSubscriptionFormPanel = this.getAddSubscriptionStep2aFormPanel();

        // Check for checked FieldInclusionCheckbox components in step 2a view
        var checkedFieldInclusionCheckbox = !!addSubscriptionFormPanel.query('fieldinclusioncheckbox[value=true][disabled=false]').length;

        this.optDebug('checkedFieldInclusionCheckbox: ', checkedFieldInclusionCheckbox);
        if (checkedFieldInclusionCheckbox) {
            // Save state for step 2a view
            this.addSubscriptionStep2aFields = addSubscriptionFormPanel.getForm().getValues();
        }

        // Update `next` button to reflect validation outcome
        this.getAddSubscriptionNextButton().setDisabled(!checkedFieldInclusionCheckbox);

    },

    /**
     * # Stub.
     *
     */
    validateStep2b: function() {
        this.optDebug('validateStep2b.arguments: ', arguments);

        // Step 2b view basic form containing field inclusion checkboxes
        var addSubscriptionStep2bForm = this.getAddSubscriptionStep2bFormPanel().getForm();

        // Save state for step 2b view
        this.addSubscriptionStep2bFields = addSubscriptionStep2bForm.getValues();

    },

    /**
     * # Stub.
     *
     */
    validateStep2c: function() {
        this.optDebug('validateStep2c.arguments: ', arguments);

        // Step 2c view basic form containing field inclusion checkboxes
        var addSubscriptionStep2cForm = this.getAddSubscriptionStep2cFormPanel().getForm();

        // Save state for step 2c view
        this.addSubscriptionStep2cFields = addSubscriptionStep2cForm.getValues();

    },

    /**
     * # Stub.
     *
     */
    validateStep2d: function() {
        this.optDebug('validateStep2d.arguments: ', arguments);

        // Step 2d view basic form containing field inclusion checkboxes
        var addSubscriptionStep2dForm = this.getAddSubscriptionStep2dFormPanel().getForm();

        // Save state for step 2c view
        this.addSubscriptionStep2dFields = addSubscriptionStep2dForm.getValues();

    },

    /**
     * # Stub.
     *
     * @param parentSubscriber
     * @returns {Object|*}
     */
    getFormattedParentSubscriber: function(parentSubscriber) {
        this.optDebug('getFormattedParentSubscriber.arguments: ', arguments);

        var subscriberAccountsStore = Ext.getStore('AdminInterface.store.SubscriberAccounts');
        var parentSubscriberRecord = subscriberAccountsStore.getById(parentSubscriber);

        return parentSubscriberRecord.get('ex_organization_account');
    },

    /**
     * # Stub.
     *
     * @param parentSubscriber
     * @param callback
     */
    submitDemoSubscription: function(parentSubscriber, callback) {
        this.optDebug('submitDemoSubscription.arguments: ', arguments);

        var demoFields = [
            'transaction_id',
            'real_estate_ad_publicized',
            'real_estate_ad_asking_price',
            'contract_date',
            'contract_price',
            'date_of_possession',
            'data_transfer_date',
            'mstat_restricted',
            'real_estate_designation',
            'real_estate_taxation_code',
            'location_rt90_geopt',
            'location_rt90_geopt_east',
            'location_rt90_geopt_north',
            'county',
            'county_lkf',
            'municipality',
            'municipality_lkf',
            'congregation',
            'congregation_lkf',
            'formatted_address',
            'apartment_number',
            'route_name',
            'street_number',
            'metric_position',
            'street_entrance',
            'postal_code',
            'postal_town',
            'baseline_year_of_assessment',
            'baseline_rateable_value',
            'year_of_assessment',
            'rateable_value',
            'price_by_rateable_value',
            'price_per_sq_meter',
            'type_of_housing',
            'housing_category',
            'housing_tenure',
            'apartment_floor',
            'building_storeys',
            'living_area',
            'plot_area',
            'number_of_rooms',
            'elevator',
            'balcony',
            'new_production',
            'energy_performance',
            'energy_rating',
            'build_year',
            'monthly_fee',
            'annual_fee',
            'heating_included',
            'housing_cooperative_name',
            'housing_cooperative_registration_number',
            'housing_cooperative_share',
            'raw_type_of_housing',
            'raw_housing_tenure',
            'sys_raw_data_source',
            'sys_scb_invalidation_code',
            'sys_modified',
            'sys_created'
        ];
        var dateToday = Ext.Date.format(new Date(), 'Y-m-d');

        var apiEnvelope = {
            ancestor: parentSubscriber,
            quota: 1,
            subscription_type: 'data_api_subscription',
            friendly_name: "Demo-abonnemang",
            description: "Alla fält + daglig uppdatering, 2013-09-24–2013-09-26. API-access fr.o.m. " + dateToday + ".",
            start_date: dateToday,
            end_date: undefined,
            update_frequency: 'subscription_daily_update_frequency',
            content_restrictions: {
                datastore_kind: 'TransactionCompositeEntry',
                fields: demoFields,
                start_date: "2013-09-24",
                end_date: "2013-09-26"
            }
        };

        var me = this;
        var subscriptionsStore = Ext.getStore('AdminInterface.store.Subscriptions');

        Remote.AdminCmd.add_or_edit_subscription(apiEnvelope, function(result, request, success) {
            me.optDebug('AdminCmd.ADD_or_edit_subscription.result', result);
            me.optDebug('AdminCmd.ADD_or_edit_subscription.request', request);
            me.optDebug('AdminCmd.ADD_or_edit_subscription.success', success);

            if (success) {
                subscriptionsStore.add(result);
            }
            callback(success);
        });
    },

    /**
     * # Stub.
     *
     */
    submitAddSubscription: function() {
        this.optDebug('submitAddSubscription.arguments: ', arguments);

        var me = this;
        var addSubscriptionWindow = this.getAddSubscriptionWindow();
        var loadMask = addSubscriptionWindow.setLoading('Adding Subscription ...');
        var subscriptionsStore = Ext.getStore('AdminInterface.store.Subscriptions');

        Remote.AdminCmd.add_or_edit_subscription(this.getApiInput(), function(result, request, success) {
            me.optDebug('AdminCmd.ADD_or_edit_subscription.result', result);
            me.optDebug('AdminCmd.ADD_or_edit_subscription.request', request);
            me.optDebug('AdminCmd.ADD_or_edit_subscription.success', success);

            addSubscriptionWindow.setLoading(false);

            if (success) {
                addSubscriptionWindow.close();
                subscriptionsStore.add(result);
            }
        });

    },

    /**
     * # Stub.
     *
     */
    submitEditSubscription: function() {
        this.optDebug('submitEditSubscription.arguments: ', arguments);

        var me = this;
        var addSubscriptionWindow = this.getAddSubscriptionWindow();
        var loadMask = addSubscriptionWindow.setLoading('Submitting Changes ...');
        var subscriptionsStore = Ext.getStore('AdminInterface.store.Subscriptions');

        Remote.AdminCmd.add_or_edit_subscription(this.getApiInput(), function(result, request, success) {
            me.optDebug('AdminCmd.add_or_EDIT_subscription.result', result);
            me.optDebug('AdminCmd.add_or_EDIT_subscription.request', request);
            me.optDebug('AdminCmd.add_or_EDIT_subscription.success', success);

            addSubscriptionWindow.setLoading(false);

            if (success) {
                //me.editSubscription.beginEdit();
                me.editSubscription.raw = result;
                me.editSubscription.set(result);
                me.editSubscription.postProcessExFields(true);
                me.editSubscription.commit()
                //me.editSubscription.endEdit();
                addSubscriptionWindow.close();
            }
        });

    },

    /**
     * # Stub.
     *
     *     {
     *         ancestor: String,
     *         quota: String,
     *         subscription_type: String,
     *         friendly_name: String,
     *         description: String,
     *         start_date: String,
     *         end_date: (String|Undefined),
     *         update_frequency: String,
     *         content_restrictions: {
     *             datastore_kind: String,
     *             fields: String[],
     *             start_date: String,
     *             end_date: (String|Undefined)
     *         }
     *     }
     *
     * @returns {Object} API data
     */
    getApiInput: function() {
        this.optDebug('getApiInput.arguments: ', arguments);

        var step1Fields = this.addSubscriptionStep1Fields;
        var fieldInclusion = this.addSubscriptionStep2aFields.field_inclusion;

        if (this.addSubscriptionStep2bFields.field_inclusion) {
            fieldInclusion = fieldInclusion.concat(this.addSubscriptionStep2bFields.field_inclusion);
        }

        if (this.addSubscriptionStep2cFields.field_inclusion) {
            fieldInclusion = fieldInclusion.concat(this.addSubscriptionStep2cFields.field_inclusion);
        }

        if (this.addSubscriptionStep2dFields.field_inclusion) {
            fieldInclusion = fieldInclusion.concat(this.addSubscriptionStep2dFields.field_inclusion);
        }

        var apiEnvelope = {
            ancestor: step1Fields.subscriber,
            quota: step1Fields.quota,
            subscription_type: step1Fields.subscription_type,
            friendly_name: step1Fields.friendly_name,
            description: step1Fields.description,
            start_date: step1Fields.subscription_start_date,
            end_date: step1Fields.subscription_end_date || undefined,
            update_frequency: step1Fields.update_frequency,
            content_restrictions: {
                datastore_kind: 'TransactionCompositeEntry',
                fields: fieldInclusion,
                start_date: step1Fields.start_date,
                end_date: step1Fields.end_date || undefined
            }
        };

        if (this.editSubscription) {
            apiEnvelope.subscription = this.editSubscription.get('datastore_key_urlsafe');
        }

        return apiEnvelope;
    },

    /**
     * # Stub.
     *
     * @param dialogWindow
     */
    onAddSubscriptionWindowShow: function(dialogWindow) {
        this.optDebug('onAddSubscriptionWindowShow.arguments: ', arguments);

        //return true;
    },

    /**
     * # Stub.
     *
     * @param window
     */
    onAddSubscriptionWindowHide: function(window) {
        this.optDebug('onAddSubscriptionWindowHide.arguments: ', arguments);

        //this.resetFields();


        //this.onSetActivePage(0);
    },

    /**
     * # Stub.
     *
     * @param field
     * @param keyEvent
     * @param eOpts
     * @returns {fireEvent|*|Boolean}
     */
    onAddSubscriptionWindowFieldSpecialKey: function(field, keyEvent, eOpts) {
        this.optDebug('onAddSubscriptionWindowFieldSpecialKey.arguments:', arguments);

        if (keyEvent.getKey() !== keyEvent.ENTER) {
            return;
        } else if (!(field.isXType('fieldinclusioncheckbox') || field.isXType('generictextfield') || field.isXType('genericdatefield'))) {
            return;
        }

        var nextButton = this.getAddSubscriptionNextButton();

        if (!nextButton.isDisabled()) {
            return nextButton.fireEvent('click');
        }
    },

    /**
     * # Stub.
     *
     * @param container
     * @param eOpts
     */
    onAddSubscriptionStep1ContainerBeforeActivate: function(container, eOpts) {
        this.optDebug('onAddSubscriptionStep1ContainerBeforeActivate.arguments: ', arguments);

        var addSubscriptionForm = this.getAddSubscriptionStep1FormPanel().getForm();

        if (this.addSubscriptionStep1Fields) {
            addSubscriptionForm.setValues(this.addSubscriptionStep1Fields);
        } else {
            addSubscriptionForm.reset();
        }

        var addSubscriptionSubscriberCombobox = this.getAddSubscriptionStep1SubscriberCombobox();
        addSubscriptionSubscriberCombobox.setDisabled(!!(this.editSubscription));

        this.getAddSubscriptionNextButton().setText("Next");

        // Proceed to standard validation
        this.validateStep1();

    },

    /**
     * # Stub.
     *
     * @param container
     * @param eOpts
     */
    onAddSubscriptionStep1ContainerActivate: function(container, eOpts) {
        this.optDebug('onAddSubscriptionStep1ContainerActivate.arguments:', arguments);

        // Focus first input field in the step 1 view
        container.down('generictextfield[name="friendly_name"]').focus(true, 200);

        // Resize window to content
        this.resizeWindowHeight(container, 0, true);

    },

    /**
     * # Stub.
     *
     * @param fieldAncestor
     * @param labelable
     * @param isValid
     * @param eOpts
     */
    onAddSubscriptionStep1FormPanelFieldValidityChange: function(fieldAncestor, labelable, isValid, eOpts) {
        this.optDebug('onAddSubscriptionStep1FormPanelFieldValidityChange.arguments: ', arguments);

        // Proceed to standard validation
        this.validateStep1();
    },

    /**
     * # Stub.
     *
     * @param radioGroup
     * @param newValue
     * @param oldValue
     * @param eOpts
     */
    onAddSubscriptionStep1UpdateFrequencyRadioGroupChange: function(radioGroup, newValue, oldValue, eOpts) {
        this.optDebug('onAddSubscriptionStep1UpdateFrequencyRadioGroupChange.arguments: ', arguments);

        switch(newValue.update_frequency) {
            case 'subscription_daily_update_frequency': newValue = 1; break;
            case 'subscription_weekly_update_frequency': newValue = 3; break;
            case 'subscription_monthly_update_frequency': newValue = 9; break;
        }

        this.getAddSubscriptionStep1QuotaSliderField().setValue(newValue);

    },

    /**
     * # Stub.
     *
     * @param slider
     * @param newValue
     * @param thumb
     * @param eOpts
     */
    onAddSubscriptionStep1QuotaSliderFieldChange: function(slider, newValue, thumb, eOpts) {
        this.optDebug('onAddSubscriptionStep1QuotaSliderFieldChange.arguments: ', arguments);

        this.getAddSubscriptionStep1QuotaAuxNumberField().setValue(newValue);
    },

    /**
     * # Stub.
     *
     * @param component
     * @param eOpts
     */
    onAddSubscriptionStep2ComponentsAfterRender: function(component, eOpts) {
        this.optDebug('onAddSubscriptionStep2ComponentsAfterRender.arguments: ', arguments);

        // Step 2 view wrapper container
        var parentContainer = component.up('#addSubscriptionStepContainer > container') || component;

        new Ext.util.KeyMap({
            target: component.getEl(),
            binding: [{
                key: "a",
                fn: function() {
                    // Check all enabled descendant FieldInclusionCheckbox components
                    this.checkAllFieldInclusionCheckboxes(parentContainer);

                    // Ensures the appropriate validateStep2x method is triggered
                    this.addSubscriptionStep2LastCheckboxFocused.fireEvent('change', this.addSubscriptionStep2LastCheckboxFocused);
                },
                scope: this,
                defaultEventAction: 'stopPropagation'
            }, {
                key: "s",
                fn: function() {
                    // Check standard collection of enabled descendant FieldInclusionCheckbox components
                    this.checkStandardFieldInclusionCheckboxes(parentContainer);

                    // Ensures the appropriate validateStep2x method is triggered
                    this.addSubscriptionStep2LastCheckboxFocused.fireEvent('change', this.addSubscriptionStep2LastCheckboxFocused);
                },
                scope: this,
                defaultEventAction: 'stopPropagation'
            }, {
                key: "n",
                fn: function() {
                    // Uncheck all of the descendant FieldInclusionCheckbox components
                    this.uncheckAllFieldInclusionCheckboxes(parentContainer);

                    // Ensures the appropriate validateStep2x method is triggered
                    this.addSubscriptionStep2LastCheckboxFocused.fireEvent('change', this.addSubscriptionStep2LastCheckboxFocused);
                },
                scope: this,
                defaultEventAction: 'stopPropagation'
            }]
        });

    },

    /**
     * # Stub.
     *
     * @param container
     * @param eOpts
     */
    onAddSubscriptionStep2ContainerBeforeActivate: function(container, eOpts) {
        this.optDebug('onAddSubscriptionStep2ContainerBeforeActivate.arguments: ', arguments);

        var descendantFormPanelId = container.down('form').id;

        function checkDefaultFieldInclusions() {
            if (this.editSubscription) {
                this.checkFieldInclusionCheckboxes(container, this.editSubscription.get('content_restrictions').fields);
            } else {
                this.checkStandardFieldInclusionCheckboxes(container);
            }
        }

        switch(descendantFormPanelId) {
            case 'addSubscriptionStep2aFormPanel':
            // Enable GenericBackButton component
            this.getAddSubscriptionBackButton().setDisabled(false);

            // Default to standard field inclusion collection
            if (!this.addSubscriptionStep2aFields) {
                checkDefaultFieldInclusions.call(this);
            }

            // Proceed to step 2a validation
            this.validateStep2a();
            break;

            case 'addSubscriptionStep2bFormPanel':
            // Default to standard field inclusion collection
            if (!this.addSubscriptionStep2bFields) {
                checkDefaultFieldInclusions.call(this);
            }

            // Proceed to step 2b validation
            this.validateStep2b();
            break;

            case 'addSubscriptionStep2cFormPanel':
            // Default to standard field inclusion collection
            if (!this.addSubscriptionStep2cFields) {
                checkDefaultFieldInclusions.call(this);
            }

            // Proceed to step 2c validation
            this.validateStep2c();
            break;

            case 'addSubscriptionStep2dFormPanel':
            // Default to standard field inclusion collection
            if (!this.addSubscriptionStep2dFields) {
                checkDefaultFieldInclusions.call(this);
            }

            // Proceed to step 2d validation
            this.validateStep2d();
            break;
        }


    },

    /**
     * # Stub.
     *
     * @param container
     * @param eOpts
     */
    onAddSubscriptionStep2ContainerActivate: function(container, eOpts) {
        this.optDebug('onAddSubscriptionStep2ContainerActivate.arguments: ', arguments);

        // Focus first input field in the step 2 view
        this.addSubscriptionStep2LastCheckboxFocused = container.down('fieldinclusioncheckbox[disabled=false]:first');
        this.addSubscriptionStep2LastCheckboxFocused.focus(false, 200);

        // Resize window to content
        this.resizeWindowHeight(container, 0, true);

    },

    /**
     * # Stub.
     *
     * @param fieldInclusionCheckbox
     * @param newValue
     * @param oldValue
     * @param eOpts
     */
    onAddSubscriptionStep2FieldInclusionCheckboxChange: function(fieldInclusionCheckbox, newValue, oldValue, eOpts) {
        this.optDebug('onAddSubscriptionStep2FieldInclusionCheckboxChange.arguments: ', arguments);

        if (this.addSubscriptionStep2IgnoreCheckboxChange) {
            return;
        }

        // Parent form panel for the step 2 view's FieldInclusionCheckbox components
        var parentFormPanelId = fieldInclusionCheckbox.up('form').id;

        // Perform step 2 view validation
        switch(parentFormPanelId) {
            case 'addSubscriptionStep2aFormPanel':
            this.validateStep2a();
            break;
            case 'addSubscriptionStep2bFormPanel':
            this.validateStep2b();
            break;
            case 'addSubscriptionStep2cFormPanel':
            this.validateStep2c();
            break;
            case 'addSubscriptionStep2dFormPanel':
            this.validateStep2d();
            break;
        }

        // Focus changed step 2 view FieldInclusionCheckbox components
        this.addSubscriptionStep2LastCheckboxFocused = fieldInclusionCheckbox;
        fieldInclusionCheckbox.focus();
    },

    /**
     * # Stub.
     *
     * @param button
     * @param clickEvent
     * @param eOpts
     */
    onAddSubscriptionStep2FieldCheckerButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onAddSubscriptionStep2FieldCheckerButtonClick.arguments: ', arguments);

        // Parent form panel for the step 2 view's FieldInclusionCheckbox components
        var parentContainer = button.up('form').up('container');

        if (button.getText() == 'Standard') {
            // Check standard collection of enabled descendant FieldInclusionCheckbox components
            this.checkStandardFieldInclusionCheckboxes(parentContainer);
        } else {
            // Uncheck all of the descendant FieldInclusionCheckbox components
            if (button.getText() == 'None') {
                this.uncheckAllFieldInclusionCheckboxes(parentContainer);
            } else {
                // Check all enabled descendant FieldInclusionCheckbox components
                this.checkAllFieldInclusionCheckboxes(parentContainer);
            }
        }

        // Ensures the appropriate validateStep2x method is triggered
        this.addSubscriptionStep2LastCheckboxFocused.fireEvent('change', this.addSubscriptionStep2LastCheckboxFocused);
    },

    /**
     * # Stub.
     *
     * @param container
     * @param eOpts
     */
    onAddSubscriptionStep3ContainerBeforeActivate: function(container, eOpts) {
        this.optDebug('onAddSubscriptionStep3ContainerBeforeActivate.arguments: ', arguments);

        var step1Fields = this.addSubscriptionStep1Fields;
        var submitActionText = "Add Subscription";

        if (this.editSubscription) {
            submitActionText = "Submit Changes";
            //step1Fields.subscriber = this.editSubscription.get('parent_datastore_key_urlsafe');
        }

        this.getAddSubscriptionConfirmParentSubscriberDisplayField().setValue(this.getFormattedParentSubscriber(step1Fields.subscriber));
        this.getAddSubscriptionConfirmFriendlyNameDisplayField().setValue(step1Fields.friendly_name);
        this.getAddSubscriptionConfirmDescriptionDisplayField().setValue(step1Fields.description);

        // set subscription type and date interval
        if (step1Fields.subscription_type === 'data_api_subscription') {
            this.getAddSubscriptionConfirmSubscriptionTypeDisplayField().setValue('Mäklarstatistik Data API');
        } else {
            this.getAddSubscriptionConfirmSubscriptionTypeDisplayField().setValue('Mäklarstatistik Widget Service');
        }

        var subscriptionDateInterval = step1Fields.subscription_start_date;
        if (step1Fields.subscription_end_date) {
            subscriptionDateInterval += ' – ' + step1Fields.subscription_end_date;
        } else {
            subscriptionDateInterval += ' – <i>indefinite</i>';
        }

        this.getAddSubscriptionConfirmSubscriptionDateIntervalDisplayField().setValue(subscriptionDateInterval);


        var dataDateInterval = step1Fields.start_date;
        if (step1Fields.end_date) {
            dataDateInterval += ' – ' + step1Fields.end_date;
        } else {
            dataDateInterval += ' – <i>indefinite</i>';
        }

        this.getAddSubscriptionConfirmDateIntervalDisplayField().setValue(dataDateInterval);

        /*
        var quota = step1Fields.quota + ' full dataset downloads per update';
        this.getAddSubscriptionConfirmQuotaDisplayField().setValue(quota);
        */

        var updateFrequency = 'Monthly'; // 'subscription_monthly_update_frequency'
        switch (step1Fields.update_frequency) {
            case 'subscription_weekly_update_frequency':
            updateFrequency = 'Weekly';
            break;
            case 'subscription_daily_update_frequency':
            updateFrequency = 'Daily';
            break;
        }

        this.getAddSubscriptionConfirmUpdateFrequencyDisplayField().setValue(updateFrequency);

        var fieldInclusion = this.addSubscriptionStep2aFields.field_inclusion;

        if (this.addSubscriptionStep2bFields.field_inclusion) {
            fieldInclusion = fieldInclusion.concat(this.addSubscriptionStep2bFields.field_inclusion);
        }

        if (this.addSubscriptionStep2cFields.field_inclusion) {
            fieldInclusion = fieldInclusion.concat(this.addSubscriptionStep2cFields.field_inclusion);
        }

        if (this.addSubscriptionStep2dFields.field_inclusion) {
            fieldInclusion = fieldInclusion.concat(this.addSubscriptionStep2dFields.field_inclusion);
        }

        this.getAddSubscriptionConfirmFieldInclusionDisplayField().setValue(fieldInclusion.join(', '));

        this.getAddSubscriptionNextButton().setText(submitActionText);

    },

    /**
     * # Stub.
     *
     * @param container
     * @param eOpts
     */
    onAddSubscriptionStep3ContainerActivate: function(container, eOpts) {
        this.optDebug('onAddSubscriptionStep3ContainerActivate.arguments:', arguments);

        // Focus first input field in the step 3 view
        this.getAddSubscriptionNextButton().focus(true, 200);

        // Resize window to content + 40px
        this.resizeWindowHeight(container, 40, true);

    },

    /**
     * # Stub.
     *
     * @param container
     * @param eOpts
     */
    onAddSubscriptionStep3ContainerBeforeDeactivate: function(container, eOpts) {
        this.optDebug('onAddSubscriptionStep3ContainerBeforeDeactivate.arguments: ', arguments);

        // Restore GenericNextButton component label
        this.getAddSubscriptionNextButton().setText("Next");

    },

    /**
     * # Stub.
     *
     * @param button
     * @param clickEvent
     * @param eOpts
     */
    onAddSubscriptionCancelButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onAddSubscriptionCancelButtonClick.arguments: ', arguments);

        var addSubscriberWindow = button.up('window');

        this.hideWindow(addSubscriberWindow);

    },

    /**
     * # Stub.
     *
     * @param button
     * @param clickEvent
     * @param eOpts
     */
    onAddSubscriptionBackButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onAddSubscriptionBackButtonClick.arguments: ', arguments);

        var backConfigCallback = this.stepConfig[this.activeStep].backCallback;

        if (backConfigCallback) {
            backConfigCallback.call(this, button, clickEvent, eOpts);
        }

        if (this.activeStep > 0) {
            this.setActiveStep(this.activeStep - 1);
        }

    },

    /**
     * # Stub.
     *
     * @param button
     * @param clickEvent
     * @param eOpts
     */
    onAddSubscriptionNextButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onAddSubscriptionNextButtonClick.arguments: ', arguments);

        var nextConfigCallback = this.stepConfig[this.activeStep].nextCallback;

        if (nextConfigCallback) {
            nextConfigCallback.call(this, button, clickEvent, eOpts);
        }

        if (this.activeStep < (this.stepConfig.length - 1)) {
            this.setActiveStep(this.activeStep + 1);
        }

    },

    /**
     * # Stub.
     *
     * @param controllerId
     * @param parentSubscriber
     * @param subscription
     * @param eOpts
     */
    onExDialogOpen: function(controllerId, parentSubscriber, subscription, eOpts) {
        this.optDebug('onExDialogOpen.arguments: ', arguments);

        if (controllerId === this.id) {
            this.showWindow(parentSubscriber, subscription);
        }
    },

    /**
     * # Stub.
     *
     * @param controllerId
     * @param eOpts
     */
    onExDialogClose: function(controllerId, eOpts) {
        this.optDebug('onExDialogClose.arguments: ', arguments);
    }

});
