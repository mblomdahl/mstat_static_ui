/*
 * File: app/controller/ViewSubscriberCtrl.js
 * Author: Mats Blomdahl
 * Version: 1.06
 */

Ext.define('AdminInterface.controller.ViewSubscriberCtrl', {
    extend: 'Ext.app.Controller',
    alias: 'controller.viewsubscriberctrl',

    models: [
        'OrganizationAccount'
    ],
    stores: [
        'AdminInterface.store.SubscriberAccounts'
    ],
    views: [
        'AdminInterface.view.window.ViewSubscriberWindow'
    ],

    refs: [
        {
            ref: 'viewSubscriberWindow',
            selector: 'viewsubscriberwindow',
            xtype: 'viewsubscriberwindow'
        },
        {
            ref: 'viewSubscriberDetailsFormPanel',
            selector: '#viewSubscriberDetailsFormPanel',
            xtype: 'form'
        },
        {
            ref: 'viewSubscriberCancelButton',
            selector: 'viewsubscriberwindow genericcancelbutton',
            xtype: 'genericcancelbutton'
        },
        {
            ref: 'viewSubscriberEditButton',
            selector: 'viewsubscriberwindow genericactionbutton[text="Edit Account"]',
            xtype: 'genericactionbutton'
        },
        {
            ref: 'viewSubscriberSuspendButton',
            selector: 'viewsubscriberwindow genericactionbutton[text="Suspend Account"]',
            xtype: 'genericactionbutton'
        },
        {
            ref: 'viewSubscriberUnsuspendButton',
            selector: 'viewsubscriberwindow genericactionbutton[text="Un-suspend Account"]',
            xtype: 'genericactionbutton'
        },
        {
            ref: 'viewSubscriberDeleteButton',
            selector: 'viewsubscriberwindow genericactionbutton[text="Delete Account"]',
            xtype: 'genericactionbutton'
        }
    ],

    init: function(application) {
        this.optDebug = application.optDebug;
        this.debug = false || application.debug;
        this.optDebug('ViewSubscriberCtrl.arguments', arguments);

        this.control({
            '#viewSubscriberWindow': {
                hide: this.onViewSubscriberWindowHide,
                show: this.onViewSubscriberWindowShow
            },
            '#viewSubscriberStep1Container': {
                activate: this.onViewSubscriberStep1ContainerActivate,
                //beforeshow: this.onViewUserStep1ContainerBeforeShow
                beforerender: this.onViewSubscriberStep1ContainerBeforeRender
            },
            'viewsubscriberwindow genericcancelbutton': {
                click: this.onViewSubscriberCancelButtonClick
            },
            'viewsubscriberwindow genericactionbutton[text="Edit Account"]': {
                click: this.onViewSubscriberEditButtonClick
            },
            'viewsubscriberwindow genericactionbutton[text="Suspend Account"]': {
                click: this.onViewSubscriberSuspendButtonClick
            },
            'viewsubscriberwindow genericactionbutton[text="Un-suspend Account"]': {
                click: this.onViewSubscriberUnsuspendButtonClick
            },
            'viewsubscriberwindow genericactionbutton[text="Delete Account"]': {
                click: this.onViewSubscriberDeleteButtonClick
            }
        });

        this.activeStep = 0;

        application.on({
            exDialogOpen: {
                fn: this.onExDialogOpen,
                scope: this
            },
            exDialogClose: {
                fn: this.onExDialogClose,
                scope: this
            }
        });
    },

    resetFields: function() {
        this.optDebug('resetFields.arguments: ', arguments);

        this.subscriberId = null;
    },

    showWindow: function(subscriber) {
        this.optDebug('showWindow.arguments: ', arguments);

        this.resetFields();

        Ext.ClassManager.get('AdminInterface.view.window.ViewSubscriberWindow').create();

        this.subscriberId = subscriber;

        this.getViewSubscriberWindow().show();

    },

    hideWindow: function(dialogWindow) {
        this.optDebug('hideWindow.arguments: ', arguments);

        dialogWindow = dialogWindow || this.getViewSubscriberWindow();

        dialogWindow.close();
    },

    resizeWindowHeight: function(container, heightExtra, reposition) {
        this.optDebug('resizeWindowHeight.arguments: ', arguments);

        reposition = reposition || false;

        var parentWindow = container.up('window');
        if (!parentWindow.rendered) {
            this.optDebug('parentWindow.rendered:' + parentWindow.rendered);
            return;
        } else {
            this.optDebug('parentWindow:', parentWindow);
        }

        var activeContainerWrapper = container.up('container');
        if (!activeContainerWrapper.rendered) {
            this.optDebug('activeContainerWrapper.rendered:' + activeContainerWrapper.rendered);
            return;
        } else {
            this.optDebug('activeContainerWrapper:', activeContainerWrapper);
        }

        var wrapperHeightDelta = activeContainerWrapper.getHeight();
        this.optDebug('wrapperHeightDelta:', wrapperHeightDelta);

        var activeViewContent = activeContainerWrapper.query('#' + container.id + ' > container');
        this.optDebug('activeViewContent:', activeViewContent);

        for (var i = 0, j = activeViewContent.length; i < j; i++) {

            if (!activeViewContent[i].rendered) {
                this.optDebug('activeViewContent[i].rendered:' + activeViewContent[i].rendered);
                return;
            }

            wrapperHeightDelta -= activeViewContent[i].getHeight();
        }

        if (heightExtra) {
            wrapperHeightDelta -= heightExtra;
        }

        parentWindow.setHeight(parentWindow.getHeight() - wrapperHeightDelta);

        if (reposition) {
            return parentWindow.center();
            /*
            var windowPosition = parentWindow.getPosition();

            windowPosition[1] += wrapperHeightDelta / 2;
            if (windowPosition[1] < 0) {
            windowPosition[1] = 0;
            }

            parentWindow.setPosition(windowPosition[0], windowPosition[1]);
            */
        }
    },

    updateSubscriberDetails: function() {
        this.optDebug('updateSubscriberDetails.arguments: ', arguments);

        // User details form
        var viewSubscriberForm = this.getViewSubscriberDetailsFormPanel().getForm();

        this.optDebug('viewSubscriberForm: ', viewSubscriberForm);

        // User account record
        var subscriberAccountRecord = Ext.getStore('AdminInterface.store.SubscriberAccounts').getById(this.subscriberId);

        this.optDebug('subscriberAccountRecord: ', subscriberAccountRecord);

        viewSubscriberForm.loadRecord(subscriberAccountRecord);

        var label = Ext.ComponentQuery.query('#viewSubscriberStep1Container > container > label')[0];
        label.html = '<h3>Subscriber Details for ' + subscriberAccountRecord.get('ex_organization_account') + '</h3>';

        var suspendUnsuspendActionButton = this.getViewSubscriberSuspendButton() || this.getViewSubscriberUnsuspendButton();

        if (suspendUnsuspendActionButton) {
            if (subscriberAccountRecord.get('suspended')) {
                suspendUnsuspendActionButton.setText('Un-suspend Account');
            } else {
                suspendUnsuspendActionButton.setText('Suspend Account');
            }
        }
    },

    onViewSubscriberWindowShow: function(dialogWindow, eOpts) {
        this.optDebug('onViewSubscriberWindowShow.arguments: ', arguments);

    },

    onViewSubscriberWindowHide: function(dialogWindow, eOpts) {
        this.optDebug('onViewSubscriberWindowHide.arguments: ', arguments);

    },

    onViewSubscriberStep1ContainerBeforeRender: function(container, eOpts) {
        this.optDebug('onViewSubscriberStep1ContainerBeforeRender.arguments: ', arguments);

        this.updateSubscriberDetails();
    },

    onViewSubscriberStep1ContainerActivate: function(container, eOpts) {
        this.optDebug('onViewSubscriberStep1ContainerActivate.arguments: ', arguments);

        // Focus GenericCancelButton in the step 1 view
        this.getViewSubscriberCancelButton().focus(true, 250);

        // Resize window to content + 30px
        this.resizeWindowHeight(container, 30, true);
    },

    onViewSubscriberCancelButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onViewSubscriberCancelButtonClick.arguments: ', arguments);

        var viewSubscriberWindow = button.up('window');

        this.hideWindow(viewSubscriberWindow);

    },

    onViewSubscriberEditButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onViewSubscriberEditButtonClick.arguments: ', arguments);

        this.hideWindow();

        this.application.fireEvent('exDialogOpen', 'EditSubscriberCtrl', this.subscriberId);
    },

    onViewSubscriberSuspendButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onViewSubscriberSuspendButtonClick.arguments: ', arguments);
    },

    onViewSubscriberUnsuspendButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onViewSubscriberUnsuspendButtonClick.arguments: ', arguments);
    },

    onViewSubscriberDeleteButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onViewSubscriberDeleteButtonClick.arguments: ', arguments);
    },

    onExDialogOpen: function(controllerId, subscriber, eOpts) {
        this.optDebug('onExDialogOpen.arguments: ', arguments);

        if (controllerId === this.id) {
            this.showWindow(subscriber);
        }
    },

    onExDialogClose: function(controllerId, eOpts) {
        this.optDebug('onExDialogClose.arguments: ', arguments);

    }

});
