/*
 * File: app/controller/OrganizationDetailsCtrl.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.controller.OrganizationDetailsCtrl', {
    extend: 'Ext.app.Controller',

    views: [
        'AdminInterface.view.window.AddSubscriptionWindow',
        'AdminInterface.view.window.AddSubscriberWindow'
    ],

    refs: [
        {
            ref: 'organizationDetailsOrganizationNameTextField',
            selector: 'organizationnametextfield[name="organization_name"]',
            xtype: 'organizationnametextfield'
        },
        {
            ref: 'organizationDetailsDepartmentNameTextField',
            selector: 'organizationnametextfield[name="department_name"]',
            xtype: 'organizationnametextfield'
        },
        {
            ref: 'organizationDetailsRegistrationNumberTextField',
            selector: 'registrationnumbertextfield',
            xtype: 'registrationnumbertextfield'
        },
        {
            ref: 'organizationDetailsSubscriberAccountCombobox',
            selector: 'subscriberaccountcombobox',
            xtype: 'subscriberaccountcombobox'
        }
    ],

    init: function(application) {
        this.optDebug = application.optDebug;
        this.debug = false || application.debug;
        this.optDebug('OrganizationDetailsCtrl.arguments', arguments);

        this.control({
            'organizationnametextfield': {
                blur: this.onOrganizationDetailsOrganizationNameTextFieldBlur
            },
            'registrationnumbertextfield': {
                blur: this.onOrganizationDetailsRegistrationNumberTextFieldBlur
            }
        });

        this.vTypeSetup();

    },

    vTypeSetup: function() {
        this.optDebug('vTypeSetup.arguments:', arguments);

        // Validator setup: "organizationname"
        (function() {
            Ext.apply(Ext.form.field.VTypes, {
                organizationname: function(val, field) { // vType validation function
                    var organizationname = /^[A-Za-z\d éüÜåäøöÅÄØÖ\-\.:,]+$/;
                    return organizationname.test(val);
                },
                organizationnameText: 'Namnet innehåller ogiltiga tecken. Namnfält kan enbart innehålla en begränsad teckenuppsättning, närmare bestämt a-ö, bindestreck och blanksteg.', // vType text property
                organizationnameMask: /[A-Za-z\d éüÜåäøöÅÄØÖ\-\.:,]/ // vType mask property
            });
        })();

        // Validator setup: "registrationnumber"
        (function() {
            Ext.apply(Ext.form.field.VTypes, {
                registrationnumber: function(val, field) { // vType validation function
                    var registrationnumber = /^\d{6}\-\d{4}$/;
                    return registrationnumber.test(val);
                },
                registrationnumberText: 'Ogiltigt organisationsnr. Organisationsnummer måste anges på formen NNNNNN-NNNN.', // vType text property
                registrationnumberMask: /[\d\-]/ // vType mask property
            });
        })();


    },

    onOrganizationDetailsOrganizationNameTextFieldBlur: function(organizationNameTextField, blurEvent, eOpts) {
        this.optDebug('onOrganizationDetailsOrganizationNameTextFieldBlur.arguments:', arguments);

        var fieldValue = organizationNameTextField.getValue();

        if (fieldValue) {
            var newValue = fieldValue.trim();
            var regExpS = /\s{2}/;
            while (newValue.search(regExpS) !== -1) {
                newValue = newValue.replace(regExpS, ' ');
            }

            if (newValue !== fieldValue) {
                organizationNameTextField.setValue(newValue);
            }
        }

    },

    onOrganizationDetailsRegistrationNumberTextFieldBlur: function(registrationNumberTextField, blurEvent, eOpts) {
        this.optDebug('onOrganizationDetailsRegistrationNumberTextFieldBlur.arguments:', arguments);

        if (registrationNumberTextField.validate()) {
            return;
        }

        var fieldValue = registrationNumberTextField.getValue();

        if (fieldValue && fieldValue.length) {
            var newValue = [];
            var regExpD = /\d/;
            for (var i = 0, j = fieldValue.length; i < j; i++) {
                if (regExpD.test(fieldValue[i])) {
                    newValue.push(fieldValue[i]);
                }
            }
            newValue = newValue.join('');
            if (newValue.length >= 6) {
                newValue = newValue.substr(0, 6) + '-' + newValue.substr(6);
            }
            if (newValue !== fieldValue) {
                registrationNumberTextField.setValue(newValue);
            }
        }

    }

});
