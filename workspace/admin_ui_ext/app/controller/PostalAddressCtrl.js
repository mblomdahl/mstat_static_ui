/*
 * File: app/controller/PostalAddressCtrl.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.controller.PostalAddressCtrl', {
    extend: 'Ext.app.Controller',

    views: [
        'AdminInterface.view.container.PostalAddressContainer'
    ],

    stores: [
        'AdminInterface.store.ApartmentFloors'
    ],

    refs: [
        {
            ref: 'postalAddressRecipientLines1TextField',
            selector: '[name="recipient_lines_1"]',
            xtype: 'generictextfield'
        },
        {
            ref: 'postalAddressRecipientLines2TextField',
            selector: '[name="recipient_lines_2"]',
            xtype: 'generictextfield'
        },
        {
            ref: 'postalAddressTypeRadioGroup',
            selector: 'radiogroup',
            xtype: 'radiogroup'
        },
        {
            ref: 'postalAddressTypeContainer',
            selector: 'postaladdresstypecontainer',
            xtype: 'postaladdresstypecontainer'
        },
        {
            ref: 'postalAddressRouteNameTextField',
            selector: 'routenametextfield',
            xtype: 'routenametextfield'
        },
        {
            ref: 'postalAddressStreetNumberTextField',
            selector: 'streetnumbertextfield',
            xtype: 'streetnumbertextfield'
        },
        {
            ref: 'postalAddressStreetEntranceTextField',
            selector: 'streetentrancetextfield',
            xtype: 'streetentrancetextfield'
        },
        {
            ref: 'postalAddressApartmentFloorCombobox',
            selector: 'apartmentfloorcombobox',
            xtype: 'apartmentfloorcombobox'
        },
        {
            ref: 'postalAddressBoxAddressTextField',
            selector: 'boxaddresstextfield',
            xtype: 'boxaddresstextfield'
        },
        {
            ref: 'postalAddressPostalCodeTextField',
            selector: 'postalcodetextfield',
            xtype: 'postalcodetextfield'
        },
        {
            ref: 'postalAddressPostalTownTextField',
            selector: 'postaltowntextfield',
            xtype: 'postaltowntextfield'
        },
        {
            ref: 'postalAddressCountryTextField',
            selector: 'countrytextfield',
            xtype: 'countrytextfield'
        }
    ],

    init: function(application) {
        this.optDebug = application.optDebug;
        this.debug = false || application.debug;
        this.optDebug('PostalAddressCtrl.arguments', arguments);

        this.control({
            'postaladdresscontainer radiogroup': {
                change: this.onPostalAddressTypeRadioGroupChange
            },
            'postaladdresscontainer field:focusable:not(radiofield)': {
                change: this.onPostalAddressFieldChange
            },
            'postaladdresscontainer textfield': {
                blur: this.onPostalAddressTextFieldBlur
            },
            'postaladdresscontainer streetentrancetextfield': {
                blur: this.onPostalAddressStreetEntranceTextFieldBlur
            },
            'postaladdresscontainer postalcodetextfield': {
                blur: this.onPostalAddressPostalCodeTextFieldBlur
            },
            'postaladdresstypecontainer': {
                beforerender: this.onPostalAddressTypeBeforeRender
            },
            'postaladdresstypecontainer container': {
                beforeactivate: this.onPostalAddressTypeBeforeActivate,
                beforedeactivate: this.onPostalAddressTypeBeforeDeactivate
            }/*,
            'postaladdresscontainer': {
                fieldsDisable: this.onPostalAddressFieldsDisable,
                fieldsEnable: this.onPostalAddressFieldsEnable,
                fieldsMirror: this.onPostalAddressFieldsMirror
            }*/
        });

        this.vTypeSetup();

        window.formFields = {};

        /*
        // Postal address fields to query for
        this.addressFieldXTypes = [
        'routenametextfield',
        'boxaddresstextfield',
        'countrytextfield',
        'apartmentfloorcombobox',
        'postalcodetextfield',
        'postaltowntextfield',
        'streetnumbertextfield',
        'streetentrancetextfield',
        'generictextfield',
        'radiogroup'
        ];
        */

    },

    vTypeSetup: function() {
        this.optDebug('vTypeSetup.arguments:', arguments);

        // Validator setup: "genericname"
        (function() {
            Ext.apply(Ext.form.field.VTypes, {
                genericname: function(val, field) { // vType validation function
                    var genericname = /^[A-Za-z éüÜåäøöÅÄØÖ\-]+$/;
                    return genericname.test(val);
                },
                genericnameText: 'Namnet innehåller ogiltiga tecken. Namnfält kan enbart innehålla en begränsad teckenuppsättning, närmare bestämt a-ö, bindestreck och blanksteg.', // vType text property
                genericnameMask: /[A-Za-z éüÜåäøöÅÄØÖ\-]/ // vType mask property
            });
        })();

        // Validator setup: "routename"
        (function() {
            Ext.apply(Ext.form.field.VTypes, {
                routename: function(val, field) { // vType validation function
                    var routename = /^[A-Za-z \.:éüÜåäøöÅÄØÖ\-]+$/;
                    return routename.test(val);
                },
                routenameText: 'Gatuadressen innehåller ogiltiga tecken. Fältet "gatuadress" kan enbart innehålla en begränsad teckenuppsättning, närmare besämt a-ö, bindestreck och blanksteg. För gatunummer och portuppgång, se fälten "gatunummer" och "portnr."', // vType text property
                routenameMask: /[A-Za-z \.:éüÜåäøöÅÄØÖ\-]/ // vType mask property
            });
        })();

        // Validator setup: "streetnumber"
        (function() {
            Ext.apply(Ext.form.field.VTypes, {
                streetnumber: function(val, field) { // vType validation function
                    var streetnumber = /^\d{1,3}$|^\d{1,3}\-\d{1,3}$/;
                    return streetnumber.test(val);
                },
                streetnumberText: 'Gatunumret innehåller ogiltiga tecken eller har en ogiltig längd. Ett gatunummer kan enbart innehålla siffror från 0-9. För portuppgång (exempelvis "B" i "2B") resp. våningsplan, se fälten "portnr." och "trappor".', // vType text property
                streetnumberMask: /[\d\-]/ // vType mask property
            });
        })();


        // Validator setup: "boxaddress"
        (function() {
            Ext.apply(Ext.form.field.VTypes, {
                boxaddress: function(val, field) { // vType validation function
                    var streetnumber = /^\d{1,5}$/;
                    return streetnumber.test(val);
                },
                boxaddressText: 'Boxadressen innehåller ogiltiga tecken eller har en ogiltig längd. Ett boxadress anges som boxnummer och utan prefixet "Box ".', // vType text property
                boxaddressMask: /[\d]/ // vType mask property
            });
        })();

        // Validator setup: "streetentrance"
        (function() {
            Ext.apply(Ext.form.field.VTypes, {
                streetentrance: function(val, field) { // vType validation function
                    var streetentrance = /^[a-z]$/i;
                    return streetentrance.test(val);
                },
                streetentranceText: 'Portuppgången innehåller ogiltiga tecken eller har en ogiltig längd. För våningsplan (exempelvis "N.B." i "Hemgatan 4 A, N.B.") se fältet "trappor".', // vType text property
                streetentranceMask: /[a-z]/i // vType mask property
            });
        })();


        // Validator setup: "streetapartment"
        (function() {
            Ext.apply(Ext.form.field.VTypes, {
                streetapartment: function(val, field) { // vType validation function
                    var streetapartment = /^\d+$/;
                    return streetapartment.test(val);
                },
                streetapartmentText: 'Lägenhetsnumret innehåller ogiltiga tecken eller har en ogiltig längd.', // vType text property
                streetapartmentMask: /\d/ // vType mask property
            });
        })();


        // Validator setup: "postalcode"
        (function() {
            Ext.apply(Ext.form.field.VTypes, {
                postalcode: function(val, field) { // vType validation function
                    var postalcode = /^[\d]{5}$/;
                    return postalcode.test(val);
                },
                postalcodeText: 'Postnumret innehåller ogiltiga tecken eller har en ogiltig längd. Korrekt format är "NNNNN" (exempelvis "11421", utan blanksteg).', // vType text property
                postalcodeMask: /[\d]/ // vType mask property
            });
        })();

        // Validator setup: "postaltown"
        (function() {
            Ext.apply(Ext.form.field.VTypes, {
                postaltown: function(val, field) { // vType validation function
                    var postaltown = /^[A-Za-z åäöÅÄÖ\-]+$/;
                    return postaltown.test(val);
                },
                postaltownText: 'Postorten innehåller ogiltiga tecken. Fältet "postort" kan enbart innehålla en begränsad teckenuppsättning, närmare bestämt a-ö, bindestreck och blanksteg.', // vType text property
                postaltownMask: /[A-Za-z åäöÅÄÖ\-]/ // vType mask property
            });
        })();

        // Validator setup: "country"
        (function() {
            Ext.apply(Ext.form.field.VTypes, {
                country: function(val, field) { // vType validation function
                    var country = /^Sverige$|^Sweden$/;
                    return country.test(val);
                },
                countryText: 'Som land stöds för närvarande enbart svenska adresser. Det enda giltiga värdet för fältet "land" är därmed "Sverige" eller "Sweden".', // vType text property
                countryMask: /[Svwdrigen]/i // vType mask property
            });
        })();

    },

    setActiveAddressType: function(addressContainer, addressType) {
        this.optDebug('setActiveAddressType.arguments:', arguments);

        var activeItem = 0;

        if (addressType == "box_address") {
            activeItem = 1;
        }

        this.optDebug('setActiveItem('+activeItem+')', addressContainer.getLayout().setActiveItem(activeItem));

    },

    onPostalAddressTypeRadioGroupChange: function(radioGroup, newValue, oldValue, eOpts) {
        this.optDebug('onPostalAddressTypeRadioGroupChange.arguments:', arguments);

        var container = radioGroup.up('postaladdresscontainer');
        var linkedContainers = container.getLinkedContainers();
        var targetAddressContainer = container.down('postaladdresstypecontainer');
        var parentFormPanel = container.up('form');

        this.optDebug('container, targetAddressContainer, newValue:', container, targetAddressContainer, newValue);

        this.setActiveAddressType(targetAddressContainer, newValue['address_type']);

        if (linkedContainers) {
            this.optDebug('here we updates ' + radioGroup.name + ' = ' + radioGroup.getValue());

            for (var i = linkedContainers.length; i--; ) {
                var addressTypeRadioGroup = linkedContainers[i].down('radiogroup');
                this.optDebug('update: ' + radioGroup.name + ' = ', newValue);
                addressTypeRadioGroup.setValue(newValue);
            }
        }

        if (parentFormPanel) {
            parentFormPanel.fireEvent('validitychange', {
                valid: false
            });
        }

    },

    onPostalAddressTypeBeforeRender: function(outerAddressContainer) {
        this.optDebug('onPostalAddressTypeBeforeRender.arguments: ', arguments);

        // evaluates which card should be displayed

    },

    onPostalAddressTypeBeforeActivate: function(addressContainer, eOpts) {
        this.optDebug('onPostalAddressTypeBeforeActivate.arguments: ', arguments);

        var routeNameTextField = addressContainer.down('routenametextfield');

        if (routeNameTextField) { // street address activated
            this.optDebug('routeNameTextField found', routeNameTextField);
            if (!routeNameTextField.preserveDisabled) {
                var streetNumberTextField = addressContainer.down('streetnumbertextfield');

                this.optDebug('streetNumberTextField found', streetNumberTextField);
                routeNameTextField.setDisabled(false);
                streetNumberTextField.setDisabled(false);
            }

        } else { // box address activated

            var boxAddressTextField = addressContainer.down('boxaddresstextfield');

            this.optDebug('boxAddressTextField found: ', boxAddressTextField);

            if (!boxAddressTextField.preserveDisabled) {
                boxAddressTextField.setDisabled(false);
            }
        }

    },

    onPostalAddressTypeBeforeDeactivate: function(addressContainer, eOpts) {
        this.optDebug('onPostalAddressTypeBeforeDeactivate.arguments: ', arguments);

        var routeNameTextField = addressContainer.down('routenametextfield');

        if (routeNameTextField) { // street address deactivated
            var streetNumberTextField = addressContainer.down('streetnumbertextfield');
            routeNameTextField.setDisabled(true);
            streetNumberTextField.setDisabled(true);

            this.optDebug('routeNameTextField found', routeNameTextField, streetNumberTextField);

        } else { // box address deactivated
            var boxAddressTextField = addressContainer.down('boxaddresstextfield');
            boxAddressTextField.setDisabled(true);

            this.optDebug('boxAddressTextField found', boxAddressTextField);
        }

    },

    onPostalAddressFieldChange: function(formField, newValue, oldValue, eOpts) {
        this.optDebug('onPostalAddressFieldChange.arguments:', arguments);

        window.formFields[formField.name] = formField;

        var container = formField.up('postaladdresscontainer');
        var linkedContainers = container.getLinkedContainers();

        if (linkedContainers) {
            this.optDebug('here we updates ' + formField.name + ' = ' + formField.getValue());

            for (var i = linkedContainers.length; i--; ) {
                var targetField = linkedContainers[i].down('field[name=' + formField.name + ']');
                targetField.setValue(newValue);
                this.optDebug('update: ' + targetField.name + ' = ' + targetField.getValue());
            }

            /*

            var containerFields = container.query();

            for (var i = containerFields.length; i--; ) {
            console.log('containerFields[i]... iterating');
            if (containerFields[i].isXType('field') || containerFields[i].isXType('checkboxgroup')) {
            containerFields[i].setDisabled(true);
            }
            }

            return false;



            console.error('fieldsMirror event');

            console.log(arguments);

            //var targetAddressFieldsQuery = this.addressFieldXTypes.join(', ');

            var addressTypeCheckbox = this.query('radiogroup')[0];

            if (addressTypeCheckbox.address_type !== values.address_type) {
                var oldValue = addressTypeCheckbox.getValue();
                var newValue = { 'address_type': values.address_type };
                addressTypeCheckbox.setValue(newValue);
                addressTypeCheckbox.fireEvent('change', addressTypeCheckbox, newValue, oldValue);
            }

            var containerFields = this.query('field');

            for (var i = 0, j = containerFields.length; i < j; i++) {
                var field = containerFields[i];
                console.error('field: ' + field.name + ' = ' + field.value);
                if (field.isHidden() === false && field.value !== values[field.name]) {
                    console.error('value setting');
                    field.suspendEvents();
                    field.setValue(values[field.name]);
                    field.resumeEvents();
                }
            }

            */
        }

    },

    onPostalAddressTextFieldBlur: function(textfield, blurEvent, eOpts) {
        this.optDebug('onPostalAddressTextFieldBlur.arguments:', arguments);

        var fieldValue = textfield.getValue();

        if (fieldValue) {
            var newValue = fieldValue.trim();
            var regExpS = /\s{2}/;
            while (newValue.search(regExpS) !== -1) {
                newValue = newValue.replace(regExpS, ' ');
            }

            if (newValue !== fieldValue) {
                textfield.setValue(newValue);
            }
        }

    },

    onPostalAddressStreetEntranceTextFieldBlur: function(streetEntranceTextField, blurEvent, eOpts) {
        this.optDebug('onPostalAddressStreetEntranceTextFieldBlur.arguments:', arguments);

        var fieldValue = streetEntranceTextField.getValue();

        if (fieldValue && fieldValue !== fieldValue.toUpperCase()) {
            streetEntranceTextField.setValue(fieldValue.toUpperCase());
        }

    },

    onPostalAddressPostalCodeTextFieldBlur: function(postalCodeTextField, blurEvent, eOpts) {
        this.optDebug('onPostalAddressPostalCodeTextFieldBlur.arguments:', arguments);

        if (postalCodeTextField.validate()) {
            return;
        }

        var fieldValue = postalCodeTextField.getValue();

        if (fieldValue && fieldValue.length) {
            var newValue = [];
            var regExpD = /\d/;
            for (var i = 0, j = fieldValue.length; i < j; i++) {
                if (regExpD.test(fieldValue[i])) {
                    newValue.push(fieldValue[i]);
                }
            }
            newValue = newValue.join('');
            if (newValue !== fieldValue) {
                postalCodeTextField.setValue(newValue);
            }
        }

    }

});
