/*
 * File: app/controller/ViewUserCtrl.js
 * Author: Mats Blomdahl
 * Version: 1.06
 */

Ext.define('AdminInterface.controller.ViewUserCtrl', {
    extend: 'Ext.app.Controller',
    alias: 'controller.viewuserctrl',

    models: [
        'OrganizationAccount',
        'UserAccount'
    ],
    stores: [
        'AdminInterface.store.SubscriberAccounts',
        'AdminInterface.store.UserAccounts'
    ],
    views: [
        'AdminInterface.view.window.ViewUserWindow'
    ],

    refs: [
        {
            ref: 'viewUserWindow',
            selector: 'viewuserwindow',
            xtype: 'viewuserwindow'
        },
        {
            ref: 'viewUserParentSubscriberDisplayField',
            selector: '#viewUserParentSubscriberDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'viewUserDetailsFormPanel',
            selector: '#viewUserDetailsFormPanel',
            xtype: 'form'
        },
        {
            ref: 'viewUserCancelButton',
            selector: 'viewuserwindow genericcancelbutton',
            xtype: 'genericcancelbutton'
        },
        {
            ref: 'viewUserEditButton',
            selector: 'viewuserwindow genericactionbutton[text="Edit User"]',
            xtype: 'genericactionbutton'
        },
        {
            ref: 'viewUserSuspendButton',
            selector: 'viewuserwindow genericactionbutton[text="Suspend User"]',
            xtype: 'genericactionbutton'
        },
        {
            ref: 'viewUserUnsuspendButton',
            selector: 'viewuserwindow genericactionbutton[text="Un-suspend User"]',
            xtype: 'genericactionbutton'
        },
        {
            ref: 'viewUserDeleteButton',
            selector: 'viewuserwindow genericactionbutton[text="Delete User"]',
            xtype: 'genericactionbutton'
        }
    ],

    init: function(application) {
        this.optDebug = application.optDebug;
        this.debug = false || application.debug;
        this.optDebug('ViewUserCtrl.arguments', arguments);

        this.control({
            '#viewUserWindow': {
                hide: this.onViewUserWindowHide,
                show: this.onViewUserWindowShow
            },
            '#viewUserStep1Container': {
                activate: this.onViewUserStep1ContainerActivate,
                //beforeshow: this.onViewUserStep1ContainerBeforeShow
                beforerender: this.onViewUserStep1ContainerBeforeRender
            },
            'viewuserwindow genericcancelbutton': {
                click: this.onViewUserCancelButtonClick
            },
            'viewuserwindow genericactionbutton[text="Edit User"]': {
                click: this.onViewUserEditButtonClick
            },
            'viewuserwindow genericactionbutton[text="Suspend User"]': {
                click: this.onViewUserSuspendButtonClick
            },
            'viewuserwindow genericactionbutton[text="Un-suspend User"]': {
                click: this.onViewUserUnsuspendButtonClick
            },
            'viewuserwindow genericactionbutton[text="Delete User"]': {
                click: this.onViewUserDeleteButtonClick
            }
        });

        this.activeStep = 0;


        application.on({
            exDialogOpen: {
                fn: this.onExDialogOpen,
                scope: this
            },
            exDialogClose: {
                fn: this.onExDialogClose,
                scope: this
            }
        });
    },

    resetFields: function() {
        this.optDebug('resetFields.arguments: ', arguments);

        this.parentSubscriberId = null;
        this.userId = null;
    },

    showWindow: function(parentSubscriber, user) {
        this.optDebug('showWindow.arguments: ', arguments);

        this.resetFields();

        Ext.ClassManager.get('AdminInterface.view.window.ViewUserWindow').create();

        this.parentSubscriberId = parentSubscriber;
        this.userId = user;

        this.getViewUserWindow().show();

    },

    hideWindow: function(dialogWindow) {
        this.optDebug('hideWindow.arguments: ', arguments);

        dialogWindow = dialogWindow || this.getViewUserWindow();

        dialogWindow.close();
    },

    resizeWindowHeight: function(container, heightExtra, reposition) {
        this.optDebug('resizeWindowHeight.arguments: ', arguments);

        reposition = reposition || false;

        var parentWindow = container.up('window');
        if (!parentWindow.rendered) {
            this.optDebug('parentWindow.rendered:' + parentWindow.rendered);
            return;
        } else {
            this.optDebug('parentWindow:', parentWindow);
        }

        var activeContainerWrapper = container.up('container');
        if (!activeContainerWrapper.rendered) {
            this.optDebug('activeContainerWrapper.rendered:' + activeContainerWrapper.rendered);
            return;
        } else {
            this.optDebug('activeContainerWrapper:', activeContainerWrapper);
        }

        var wrapperHeightDelta = activeContainerWrapper.getHeight();
        this.optDebug('wrapperHeightDelta:', wrapperHeightDelta);

        var activeViewContent = activeContainerWrapper.query('#' + container.id + ' > container');
        this.optDebug('activeViewContent:', activeViewContent);

        for (var i = 0, j = activeViewContent.length; i < j; i++) {

            if (!activeViewContent[i].rendered) {
                this.optDebug('activeViewContent[i].rendered:' + activeViewContent[i].rendered);
                return;
            }

            wrapperHeightDelta -= activeViewContent[i].getHeight();
        }

        if (heightExtra) {
            wrapperHeightDelta -= heightExtra;
        }

        parentWindow.setHeight(parentWindow.getHeight() - wrapperHeightDelta);

        if (reposition) {
            return parentWindow.center();
            /*
            var windowPosition = parentWindow.getPosition();

            windowPosition[1] += wrapperHeightDelta / 2;
            if (windowPosition[1] < 0) {
            windowPosition[1] = 0;
            }

            parentWindow.setPosition(windowPosition[0], windowPosition[1]);
            */
        }

    },

    getFormattedParentSubscriber: function() {
        this.optDebug('getFormattedParentSubscriber.arguments: ', arguments);

        var subscriberAccountsStore = Ext.getStore('AdminInterface.store.SubscriberAccounts');
        var subscriberDetails = subscriberAccountsStore.getById(this.parentSubscriberId);

        if (subscriberDetails) {
            subscriberDetails = subscriberDetails.getFormatted();
        }

        this.optDebug('subscriberDetails: ', subscriberDetails);

        return subscriberDetails;
    },

    updateUserDetails: function() {
        this.optDebug('updateUserDetails.arguments: ', arguments);

        this.getViewUserParentSubscriberDisplayField().setValue(this.getFormattedParentSubscriber());

        // User details form
        var viewUserForm = this.getViewUserDetailsFormPanel().getForm();

        this.optDebug('viewUserForm: ', viewUserForm);

        // User account record
        var userAccountRecord = Ext.getStore('AdminInterface.store.UserAccounts').getById(this.userId);

        this.optDebug('userAccountRecord: ', userAccountRecord);

        viewUserForm.loadRecord(userAccountRecord);

        var label = Ext.ComponentQuery.query('#viewUserStep1Container > container > label')[0];
        label.html = '<h3>User Details for ' + userAccountRecord.get('ex_user_account') + '</h3>';

        var suspendUnsuspendActionButton = this.getViewUserSuspendButton() || this.getViewUserUnsuspendButton();

        if (suspendUnsuspendActionButton) {
            if (userAccountRecord.get('suspended')) {
                suspendUnsuspendActionButton.setText('Un-suspend User');
            } else {
                suspendUnsuspendActionButton.setText('Suspend User');
            }
        }
    },

    onViewUserWindowShow: function(dialogWindow, eOpts) {
        this.optDebug('onViewUserWindowShow.arguments: ', arguments);

        //this.updateUserDetails();

        // Focus GenericCancelButton in the step 1 view
        //this.getViewUserCancelButton().focus(true, 250);

        // Resize window to content + 30px
        //this.resizeWindowHeight(container, 30, true);

    },

    onViewUserWindowHide: function(dialogWindow, eOpts) {
        this.optDebug('onViewUserWindowHide.arguments: ', arguments);
    },

    onViewUserStep1ContainerBeforeRender: function(container, eOpts) {
        this.optDebug('onViewUserStep1ContainerBeforeRender.arguments: ', arguments);

        this.updateUserDetails();
    },

    onViewUserStep1ContainerActivate: function(container, eOpts) {
        this.optDebug('onViewUserStep1ContainerActivate.arguments: ', arguments);

        // Focus GenericCancelButton in the step 1 view
        this.getViewUserCancelButton().focus(true, 250);

        // Resize window to content + 30px
        this.resizeWindowHeight(container, 30, true);

    },

    onViewUserCancelButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onViewUserCancelButtonClick.arguments: ', arguments);

        var viewUserWindow = button.up('window');

        this.hideWindow(viewUserWindow);


    },

    onViewUserEditButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onViewUserEditButtonClick.arguments: ', arguments);

        this.hideWindow();

        this.application.fireEvent('exDialogOpen', 'AddUserCtrl', this.parentSubscriberId, this.userId);
    },

    onViewUserSuspendButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onViewUserSuspendButtonClick.arguments: ', arguments);
    },

    onViewUserUnsuspendButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onViewUserUnsuspendButtonClick.arguments: ', arguments);
    },

    onViewUserDeleteButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onViewUserDeleteButtonClick.arguments: ', arguments);
    },

    onExDialogOpen: function(controllerId, parentSubscriber, user, eOpts) {
        this.optDebug('onExDialogOpen.arguments: ', arguments);

        if (controllerId === this.id) {
            this.showWindow(parentSubscriber, user);
        }
    },

    onExDialogClose: function(controllerId, eOpts) {
        this.optDebug('onExDialogClose.arguments: ', arguments);


    }

});
