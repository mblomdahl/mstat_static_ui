/*
 * File: app/controller/EditSubscriberCtrl.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.controller.EditSubscriberCtrl', {
    extend: 'Ext.app.Controller',
    alias: 'controller.editsubscriberctrl',

    models: [
        'OrganizationAccount'
    ],
    stores: [
        'AdminInterface.store.SubscriberAccounts'
    ],
    views: [
        'AdminInterface.view.window.EditSubscriberWindow'
    ],

    refs: [
        {
            ref: 'editSubscriberWindow',
            selector: '#editSubscriberWindow',
            xtype: 'editsubscriberwindow'
        },
        {
            ref: 'editSubscriberStepContainer',
            selector: '#editSubscriberStepContainer',
            xtype: 'container'
        },
        {
            ref: 'editSubscriberStep1Container',
            selector: '#editSubscriberStep1Container',
            xtype: 'container'
        },
        {
            ref: 'editSubscriberCompanyNameAndRegistrationNumberFormPanel',
            selector: '#editSubscriberCompanyNameAndRegistrationNumberFormPanel',
            xtype: 'form'
        },
        {
            ref: 'editSubscriberCancelButton',
            selector: 'editsubscriberwindow genericcancelbutton',
            xtype: 'genericcancelbutton'
        },
        {
            ref: 'editSubscriberBackButton',
            selector: 'editsubscriberwindow genericbackbutton',
            xtype: 'genericbackbutton'
        },
        {
            ref: 'editSubscriberNextButton',
            selector: 'editsubscriberwindow genericnextbutton',
            xtype: 'genericnextbutton'
        }
    ],

    init: function(application) {
        this.optDebug = application.optDebug;
        this.debug = false || application.debug;
        this.optDebug('EditSubscriberCtrl.arguments', arguments);

        this.control({
            '#editSubscriberWindow': {
                hide: this.onEditSubscriberWindowHide,
                show: this.onEditSubscriberWindowShow
            },
            //'#editSubscriberWindow textfield:focusable': {
            //    specialkey: this.onEditSubscriberWindowTextFieldSpecialKey
            //},
            '#editSubscriberStep1Container': {
                beforeactivate: this.onEditSubscriberStep1ContainerBeforeActivate,
                activate: this.onEditSubscriberStep1ContainerActivate,
                beforeshow: this.onEditSubscriberStep1ContainerActivate
            },
            '#editSubscriberCompanyNameAndRegistrationNumberFormPanel': {
                fieldvaliditychange: this.onEditSubscriberStep1FormPanelFieldValidityChange,
                validitychange: this.onEditSubscriberStep1FormPanelValidityChange
            },
            'editsubscriberwindow genericcancelbutton': {
                click: this.onEditSubscriberCancelButtonClick
            },
            'editsubscriberwindow genericbackbutton': {
                click: this.onEditSubscriberBackButtonClick
            },
            'editsubscriberwindow genericnextbutton': {
                click: this.onEditSubscriberNextButtonClick
            }
        });

        this.stepConfig = [{
            // step 1
            nextCallback: function(button, clickEvent, eOpts) {
                this.optDebug('step1NextCallback.arguments', arguments);
                this.validateStep1();
                this.submitEditSubscriber();
            }
        }];

        this.activeStep = 0;


        application.on({
            exDialogOpen: {
                fn: this.onExDialogOpen,
                scope: this
            },
            exDialogClose: {
                fn: this.onExDialogClose,
                scope: this
            }
        });
    },

    showWindow: function(subscriber) {
        this.optDebug('showWindow.arguments: ', arguments);

        // Reset saved state for the window views
        this.resetFields();

        Ext.ClassManager.get('AdminInterface.view.window.EditSubscriberWindow').create();

        var editSubscriberWindow = this.getEditSubscriberWindow(); // red

        var subscriberAccountsStore = Ext.getStore('AdminInterface.store.SubscriberAccounts');
        this.editSubscriber = subscriberAccountsStore.getById(subscriber);

        this.companyNameAndRegistrationNumberFields = {
            organization_name: this.editSubscriber.get('organization_name'),
            department_name: this.editSubscriber.get('department_name'),
            organization_id: this.editSubscriber.get('organization_id')
        };

        this.getEditSubscriberWindow().show();

        this.setActiveStep(0);

    },

    hideWindow: function(dialogWindow) {
        this.optDebug('hideWindow.arguments: ', arguments);

        dialogWindow = dialogWindow || this.getEditSubscriberWindow();

        dialogWindow.close();
    },

    resizeWindowHeight: function(container, heightExtra, reposition) {
        this.optDebug('resizeWindowHeight.arguments: ', arguments);

        reposition = reposition || false;

        var parentWindow = container.up('window');
        if (!parentWindow.rendered) {
            this.optDebug('parentWindow.rendered:' + parentWindow.rendered);
            return;
        } else {
            this.optDebug('parentWindow:', parentWindow);
        }

        var activeContainerWrapper = container.up('container');
        if (!activeContainerWrapper.rendered) {
            this.optDebug('activeContainerWrapper.rendered:' + activeContainerWrapper.rendered);
            return;
        } else {
            this.optDebug('activeContainerWrapper:', activeContainerWrapper);
        }

        var wrapperHeightDelta = activeContainerWrapper.getHeight();
        this.optDebug('wrapperHeightDelta:', wrapperHeightDelta);

        var activeViewContent = activeContainerWrapper.query('#' + container.id + ' > container');
        this.optDebug('activeViewContent:', activeViewContent);

        for (var i = 0, j = activeViewContent.length; i < j; i++) {

            if (!activeViewContent[i].rendered) {
                this.optDebug('activeViewContent[i].rendered:' + activeViewContent[i].rendered);
                return;
            }

            wrapperHeightDelta -= activeViewContent[i].getHeight();
        }

        if (heightExtra) {
            wrapperHeightDelta -= heightExtra;
        }

        parentWindow.setHeight(parentWindow.getHeight() - wrapperHeightDelta);

        if (reposition) {
            return parentWindow.center();
            /*
            var windowPosition = parentWindow.getPosition();

            windowPosition[1] += wrapperHeightDelta / 2;
            if (windowPosition[1] < 0) {
            windowPosition[1] = 0;
            }

            parentWindow.setPosition(windowPosition[0], windowPosition[1]);
            */
        }

    },

    setActiveStep: function(stepIndex) {
        this.optDebug('setActiveStep.arguments', arguments);

        // Card layout of step view wrapper
        var stepContainerLayout = this.getEditSubscriberStepContainer().getLayout();

        // Save wrapper active step state
        this.activeStep = stepIndex;

        stepContainerLayout.setActiveItem(stepIndex);


    },

    resetFields: function() {
        this.optDebug('resetFields.arguments', arguments);

        // Reset editSubscriber
        this.editSubscriber = null;

        // Reset step 1 saved state
        this.companyNameAndRegistrationNumberFields = null;

    },

    validateStep1: function() {
        this.optDebug('validateStep1.arguments', arguments);

        // Company name and registration number form panel
        var nameAndRegistrationNumberFormPanel = this.getEditSubscriberCompanyNameAndRegistrationNumberFormPanel();

        // Required form panels
        var validateFormPanels = [
        nameAndRegistrationNumberFormPanel
        ];

        // Check for invalid form fields
        var invalidFields = false;
        for (var i = 0, j = validateFormPanels.length; i < j; i++ ) {
            if (validateFormPanels[i].getForm().hasInvalidField()) {
                invalidFields = true;
                break;
            }
        }

        // Save state for step 1 view
        if (!invalidFields) {
            this.companyNameAndRegistrationNumberFields = nameAndRegistrationNumberFormPanel.getForm().getValues();
        }

        // Update `next` button to reflect validation outcome
        this.getEditSubscriberNextButton().setDisabled(invalidFields);

    },

    getApiInput: function() {
        this.optDebug('getApiInput.arguments', arguments);

        /*
        var postalAddresses, contactDetails, webAddresses;

        var adminUser, technicalContact, billingContact;

        if (this.officeAddressAsBillingAddress) {
        var billingAndOfficeAddress = this.billingAddressFields;
        billingAndOfficeAddress.labels = [
        'office',
        'office_address',
        'billing',
        'billing_address'
        ];
        postalAddresses = [billingAndOfficeAddress];

        } else {
        var billingAddress = this.billingAddressFields,
        officeAddress = this.officeAddressFields;

        billingAddress.labels = ['billing', 'billing_address'];
        officeAddress.labels = ['office', 'office_address'];

        postalAddresses = [billingAddress, officeAddress];
        }

        billingContact = this.billingContactFields;
        billingContact.labels = ['billing', 'billing_contact'];

        if (this.technicalContactAsBillingContact) {
        billingContact.labels.push('technical_contact');
        billingContact.labels.push('tech');
        } else {
        technicalContact = this.technicalContactFields;
        technicalContact.labels = ['tech', 'technical_contact'];
        }

        if (this.adminUserAsTechnicalContact && this.technicalContactAsBillingContact) {
        billingContact.labels.push('admin_account');
        billingContact.labels.push('admin');
        } else {
        if (this.adminUserAsTechnicalContact) {
        technicalContact.labels.push('admin_account');
        technicalContact.labels.push('admin');
        } else {
        adminUser = this.adminUserFields;
        adminUser.labels = ['admin', 'admin_account'];
        }
        }

        contactDetails = [billingContact];

        if (technicalContact) {
        contactDetails.push(technicalContact);
        }

        if (adminUser) {
        contactDetails.push(adminUser);
        }

        if (this.companyWebPage) {
        webAddresses = [{
        labels: ['company_main', 'company_main_url', 'web_page', 'url'],
        url: this.companyWebPage
        }];
        }
        */

        return {
            subscriber: this.editSubscriber.get('datastore_key_urlsafe'),
            company_identity_details: this.companyNameAndRegistrationNumberFields //,
            //postal_addresses: postalAddresses,
            //contact_details: contactDetails,
            //web_addresses: webAddresses
        };

    },

    submitEditSubscriber: function() {
        this.optDebug('submitEditSubscriber.arguments', arguments);

        var me = this;
        var editSubscriberWindow = this.getEditSubscriberWindow();
        var loadMask = editSubscriberWindow.setLoading('Submitting Changes ...');

        // Store for adding the new subscriber upon successful submission
        //var subscriberAccountsStore = Ext.getStore('AdminInterface.store.SubscriberAccounts');

        // Store for adding the new subscriber's users
        //var userAccountsStore = Ext.getStore('AdminInterface.store.UserAccounts');

        Remote.AdminCmd.add_or_edit_subscriber_account(this.getApiInput(), function(result, request, success) {
            me.optDebug('AdminCmd.add_or_EDIT_subscriber_account.result', result);
            me.optDebug('AdminCmd.add_or_EDIT_subscriber_account.request', request);
            me.optDebug('AdminCmd.add_or_EDIT_subscriber_account.success', success);

            editSubscriberWindow.setLoading(false);

            if (success) {
                // Close the dialog window and submit an email notification request for each user created
                me.editSubscriber.raw = result.subscriber_account;
                me.editSubscriber.set(result.subscriber_account);
                me.editSubscriber.postProcessExFields(true);
                me.editSubscriber.commit();

                editSubscriberWindow.close();
                //subscriberAccountsStore.add(result.subscriber_account);
                //for (var i = 0, j = result.user_accounts.length; i < j; i++) {
                //    userAccountsStore.add(result.user_accounts[i]);
                //    me.submitEmailNotification(result.user_accounts[i].datastore_key_urlsafe, "subscriber_activation_email");
                //}
            }
        });

    },

    onEditSubscriberWindowShow: function(dialogWindow, eOpts) {
        this.optDebug('onEditSubscriberWindowShow.arguments: ', arguments);

        //return true;
    },

    onEditSubscriberWindowHide: function(dialogWindow, eOpts) {
        this.optDebug('onEditSubscriberWindowHide.arguments: ', arguments);

        //return true;
    },

    onEditSubscriberStep1ContainerBeforeActivate: function(container, eOpts) {
        this.optDebug('onEditSubscriberStep1ContainerBeforeActivate.arguments: ', arguments);

        // Basic form components for step 1 view
        var companyNameAndRegistrationNumberForm = this.getEditSubscriberCompanyNameAndRegistrationNumberFormPanel().getForm();

        if (this.companyNameAndRegistrationNumberFields) {
            // Restore saved state for company details
            companyNameAndRegistrationNumberForm.setValues(this.companyNameAndRegistrationNumberFields);
        } else {
            // Reset form fields for company details
            companyNameAndRegistrationNumberForm.reset();
        }

        this.getEditSubscriberBackButton().setDisabled(true);

        // Proceed to standard validation
        this.validateStep1();

    },

    onEditSubscriberStep1ContainerActivate: function(container, eOpts) {
        this.optDebug('onEditSubscriberStep1ContainerActivate.arguments: ', arguments);

        // Focus first input field in the step 1 view
        container.down('organizationnametextfield[name="organization_name"]').focus(true, 250);

        // Resize window to content + 10px
        this.resizeWindowHeight(container, 10, true);
    },

    onEditSubscriberStep1FormPanelFieldValidityChange: function(fieldAncestor, labelable, isValid, eOpts) {
        this.optDebug('onEditSubscriberStep1FormPanelFieldValidityChange.arguments: ', arguments);

        // Proceed to standard validation
        this.validateStep1();

    },

    onEditSubscriberStep1FormPanelValidityChange: function(formPanel, valid, eOpts) {
        this.optDebug('onEditSubscriberStep1FormPanelValidityChange.arguments: ', arguments);

        // Proceed to standard validation
        this.validateStep1();

    },

    onEditSubscriberCancelButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onEditSubscriberCancelButtonClick.arguments:', arguments);

        var editSubscriberWindow = button.up('window');

        this.hideWindow(editSubscriberWindow);

    },

    onEditSubscriberBackButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onEditSubscriberBackButtonClick.arguments:', arguments);

        var backConfigCallback = this.stepConfig[this.activeStep].backCallback;

        if (backConfigCallback) {
            backConfigCallback.call(this, button, clickEvent, eOpts);
        }

        if (this.activeStep > 0) {
            this.setActiveStep(this.activeStep - 1);
        }

    },

    onEditSubscriberNextButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onEditSubscriberNextButtonClick.arguments:', arguments);

        var nextConfigCallback = this.stepConfig[this.activeStep].nextCallback;

        if (nextConfigCallback) {
            nextConfigCallback.call(this, button, clickEvent, eOpts);
        }

        if (this.activeStep < (this.stepConfig.length - 1)) {
            this.setActiveStep(this.activeStep + 1);
        }

    },

    onExDialogOpen: function(controllerId, subscriber, eOpts) {
        this.optDebug('onExDialogOpen.arguments: ', arguments);

        if (controllerId === this.id) {
            this.showWindow(subscriber);
        }
    },

    onExDialogClose: function(controllerId, eOpts) {
        this.optDebug('onExDialogClose.arguments: ', arguments);

    }

});
