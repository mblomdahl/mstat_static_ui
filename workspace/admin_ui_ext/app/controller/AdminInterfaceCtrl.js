/*
 * File: app/controller/AdminInterfaceCtrl.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.controller.AdminInterfaceCtrl', {
    extend: 'Ext.app.Controller',

    models: [
        'UserAccount',
        'OrganizationAccount'
    ],
    views: [
        'AdminInterface.view.AdminInterfaceViewport'
    ],

    refs: [
    ],

    init: function(application) {
        this.optDebug = application.optDebug;
        this.debug = false || application.debug;
        this.optDebug('AdminInterfaceCtrl.arguments', arguments);

        application.on({
            exLogin: {
                fn: this.onExLogin,
                scope: this
            },
            exLoginComplete: {
                fn: this.onExLoginComplete,
                scope: this
            },
            exLogout: {
                fn: this.onExLogout,
                scope: this
            }
        });
    },

    onExLogin: function(eOpts) {
        this.optDebug('onExLogin.arguments: ', arguments);

        var me = this;
        var UserAccountModel = this.getUserAccountModel();
        var SystemAdminAccountModel = this.getOrganizationAccountModel();

        Remote.AdminCmd.admin_login({}, function(result, request, success) {

        me.optDebug('AdminCmd.admin_login.request: ', request);
        me.optDebug('AdminCmd.admin_login.result: ', result);
        me.optDebug('AdminCmd.admin_login.success: ', success);

        if (success) {
            me.application.loggedIn = true;
            //foo = result.user_account;
            //uam = UserAccountModel;
            me.optDebug('login success: '+success);

            var systemAdminAccountRecord = result.system_admin_account.datastore_key_urlsafe;
            //var systemAdminAccountRecord = new SystemAdminAccountModel(result.system_admin_account);
            //systemAdminAccountRecord.setExOrganizationKindSystemAdminAccount();
            //systemAdminAccountRecord.postProcessExFields();

            var userAccountRecord = result.user_account.datastore_key_urlsafe;
            //var userAccountRecord = new UserAccountModel(result.user_account);
            //userAccountRecord.postProcessExFields();

            me.application.fireEvent('exLoginComplete', userAccountRecord, systemAdminAccountRecord);

        } //else {
            //    me.optDebug('login failed: '+success);
            //}

        });
    },

    onExLoginComplete: function(userAccountRecord, systemAdminAccountRecord, eOpts) {
        this.optDebug('onLoginComplete.arguments: ', arguments);
        this.optDebug('AdminInterfaceCtrl: logincomplete');

        this.application.session = {
            userAccount: userAccountRecord,
            systemAdminAccount: systemAdminAccountRecord
        };


    },

    onExLogout: function(eOpts) {
        this.optDebug('onExLogout.arguments: ', arguments);

        this.application.loggedIn = false;

        this.application.session = {
            systemAdminAccountRecord: null,
            userAccountRecord: null
        };

    }

});
