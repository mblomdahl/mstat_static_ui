/*
 * File: app/controller/ManageSubscribersCtrl.js
 * Author: Mats Blomdahl
 * Version: 1.06
 */

Ext.define('AdminInterface.controller.ManageSubscribersCtrl', {
    extend: 'Ext.app.Controller',

    models: [
        'UserAccount',
        'OrganizationAccount',
        'Subscription',
        'ContactDetails'
    ],
    stores: [
        'AdminInterface.store.WebAddresses',
        'AdminInterface.store.UserAccounts',
        'AdminInterface.store.PostalAddresses',
        'AdminInterface.store.ContactDetails',
        'AdminInterface.store.SubscriberAccounts',
        'AdminInterface.store.EmptySet',
        'AdminInterface.store.Subscriptions'
    ],
    views: [
        'AdminInterface.view.panel.ManageSubscribersTabPanel'
    ],

    refs: [
        {
            ref: 'manageSubscribersTabPanel',
            selector: 'managesubscriberstabpanel',
            xtype: 'managesubscriberstabpanel'
        },
        {
            ref: 'manageSubscribersSubscriberAccountsGridPanel',
            selector: 'managesubscriberstabpanel subscriberaccountsgridpanel',
            xtype: 'subscriberaccountsgridpanel'
        },
        {
            ref: 'manageSubscribersAddSubscriberButton',
            selector: '#manageSubscribersAddSubscriberButton',
            xtype: 'genericactionbutton'
        },
        {
            ref: 'manageSubscribersEditSubscriberButton',
            selector: '#manageSubscribersEditSubscriberButton',
            xtype: 'genericactionbutton'
        },
        {
            ref: 'manageSubscribersSuspendSubscriberButton',
            selector: '#manageSubscribersSuspendSubscriberButton',
            xtype: 'genericactionbutton'
        },
        {
            ref: 'manageSubscribersUnsuspendSubscriberButton',
            selector: '#manageSubscribersUnsuspendSubscriberButton',
            xtype: 'genericactionbutton'
        },
        {
            ref: 'manageSubscribersDeleteSubscriberButton',
            selector: '#manageSubscribersDeleteSubscriberButton',
            xtype: 'genericactionbutton'
        },
        {
            ref: 'manageSubscribersUserAccountsGridPanel',
            selector: 'managesubscriberstabpanel useraccountsgridpanel',
            xtype: 'useraccountsgridpanel'
        },
        {
            ref: 'manageSubscribersAddUserButton',
            selector: '#manageSubscribersAddUserButton',
            xtype: 'genericactionbutton'
        },
        {
            ref: 'manageSubscribersEditUserButton',
            selector: '#manageSubscribersEditUserButton',
            xtype: 'genericactionbutton'
        },
        {
            ref: 'manageSubscribersSuspendUserButton',
            selector: '#manageSubscribersSuspendUserButton',
            xtype: 'genericactionbutton'
        },
        {
            ref: 'manageSubscribersUnsuspendUserButton',
            selector: '#manageSubscribersUnsuspendUserButton',
            xtype: 'genericactionbutton'
        },
        {
            ref: 'manageSubscribersDeleteUserButton',
            selector: '#manageSubscribersDeleteUserButton',
            xtype: 'genericactionbutton'
        },
        {
            ref: 'manageSubscribersSubscriberAccountsGridPanelRefreshTool',
            selector: '#manageSubscribersSelectSubscriberContainer gridpanelrefreshtool',
            xtype: 'gridpanelrefreshtool'
        },
        {
            ref: 'manageSubscribersSubscriptionsGridPanel',
            selector: 'managesubscriberstabpanel subscriptionsgridpanel',
            xtype: 'subscriptionsgridpanel'
        },
        {
            ref: 'manageSubscribersAddSubscriptionButton',
            selector: '#manageSubscribersAddSubscriptionButton',
            xtype: 'genericactionbutton'
        },
        {
            ref: 'manageSubscribersEditSubscriptionButton',
            selector: '#manageSubscribersEditSubscriptionButton',
            xtype: 'genericactionbutton'
        },
        {
            ref: 'manageSubscribersSuspendSubscriptionButton',
            selector: '#manageSubscribersSuspendSubscriptionButton',
            xtype: 'genericactionbutton'
        },
        {
            ref: 'manageSubscribersUnsuspendSubscriptionButton',
            selector: '#manageSubscribersUnsuspendSubscriptionButton',
            xtype: 'genericactionbutton'
        },
        {
            ref: 'manageSubscribersDeleteSubscriptionButton',
            selector: '#manageSubscribersDeleteSubscriptionButton',
            xtype: 'genericactionbutton'
        }
    ],

    init: function(application) {
        this.optDebug = application.optDebug;
        this.debug = false || application.debug;
        this.optDebug('ManageSubscribersCtrl.arguments', arguments);

        this.control({
            'managesubscriberstabpanel': {
                beforeactivate: this.onManageSubscribersTabPanelBeforeActivate,
                beforedeactivate: this.onManageSubscribersTabPanelBeforeDeactivate
            },
            'managesubscriberstabpanel subscriberaccountsgridpanel': {
                beforerender: this.onManageSubscribersSubscriberAccountsGridPanelBeforeRender,
                selectionchange: this.onManageSubscribersSubscriberAccountsGridPanelSelectionChange,
                deselect: this.onManageSubscribersSubscriberAccountsGridPanelDeselect,
                celldblclick: this.onManageSubscribersSubscriberAccountsGridPanelCellDblClick
                //beforehide: this.beforeD1
            },
            'managesubscriberstabpanel useraccountsgridpanel': {
                beforerender: this.onManageSubscribersUserAccountsGridPanelBeforeRender,
                selectionchange: this.onManageSubscribersUserAccountsGridPanelSelectionChange,
                deselect: this.onManageSubscribersUserAccountsGridPanelDeselect,
                celldblclick: this.onManageSubscribersUserAccountsGridPanelCellDblClick
                //beforehide: this.beforeD2
            },
            'managesubscriberstabpanel subscriptionsgridpanel': {
                beforerender: this.onManageSubscribersSubscriptionsGridPanelBeforeRender,
                selectionchange: this.onManageSubscribersSubscriptionsGridPanelSelectionChange,
                deselect: this.onManageSubscribersSubscriptionsGridPanelDeselect,
                celldblclick: this.onManageSubscribersSubscriptionsGridPanelCellDblClick
                //beforehide: this.beforeD2
            },
            '#manageSubscribersAddSubscriberButton': {
                click: this.onManageSubscribersAddSubscriberButtonClick
            },
            '#manageSubscribersEditSubscriberButton': {
                click: this.onManageSubscribersEditSubscriberButtonClick
            },
            '#manageSubscribersSuspendSubscriberButton': {
                click: this.onManageSubscribersSuspendSubscriberButtonClick
            },
            '#manageSubscribersUnsuspendSubscriberButton': {
                click: this.onManageSubscribersUnsuspendSubscriberButtonClick
            },
            '#manageSubscribersDeleteSubscriberButton': {
                click: this.onManageSubscribersDeleteSubscriberButtonClick
            },
            '#manageSubscribersAddUserButton': {
                click: this.onManageSubscribersAddUserButtonClick
            },
            '#manageSubscribersEditUserButton': {
                click: this.onManageSubscribersEditUserButtonClick
            },
            '#manageSubscribersSuspendUserButton': {
                click: this.onManageSubscribersSuspendUserButtonClick
            },
            '#manageSubscribersUnsuspendUserButton': {
                click: this.onManageSubscribersUnsuspendUserButtonClick
            },
            '#manageSubscribersDeleteUserButton': {
                click: this.onManageSubscribersDeleteUserButtonClick
            },
            '#manageSubscribersAddSubscriptionButton': {
                click: this.onManageSubscribersAddSubscriptionButtonClick
            },
            '#manageSubscribersEditSubscriptionButton': {
                click: this.onManageSubscribersEditSubscriptionButtonClick
            },
            '#manageSubscribersSuspendSubscriptionButton': {
                click: this.onManageSubscribersSuspendSubscriptionButtonClick
            },
            '#manageSubscribersUnsuspendSubscriptionButton': {
                click: this.onManageSubscribersUnsuspendSubscriptionButtonClick
            },
            '#manageSubscribersDeleteSubscriptionButton': {
                click: this.onManageSubscribersDeleteSubscriptionButtonClick
            }
        });

        this.currentSubscriberSelected = null;
        this.currentUserSelected = null;
        this.currentSubscriptionSelected = null;

        this.optDebug('initz: done');

        application.on({
            exLoginComplete: {
                fn: this.onExLoginComplete,
                scope: this
            },
            exLogoutInit: {
                fn: this.onExLogoutInit,
                scope: this
            }
        });
    },

    generateSubscriberAccountDescriptorLine: function(subscriberRecordKey) {
        this.optDebug('generateSubscriberAccountDescriptorLine.arguments: ', arguments);

        var subscriberAccountsStore = Ext.getStore('AdminInterface.store.SubscriberAccounts');
        var subscriberRecord = subscriberAccountsStore.getById(subscriberRecordKey);
        var subscriberCompanyName = subscriberRecord.get('organization_name');
        var subscriberDepartmentName = subscriberRecord.get('department_name');
        var subscriberRegistrationNumber = subscriberRecord.get('organization_id');

        var descriptorLine = ''+subscriberCompanyName;

        if (subscriberDepartmentName) {
            descriptorLine += ', ' + subscriberDepartmentName;
        }

        if (subscriberRegistrationNumber) {
            descriptorLine += ' (' + subscriberRegistrationNumber + ')';
        }

        return descriptorLine
    },

    getUserAccountsGridPanelTitle: function(subscriber) {
        this.optDebug('getUserAccountsGridPanelTitle.arguments: ', arguments);

        if (subscriber) {
            return 'Users for ' + this.generateSubscriberAccountDescriptorLine(subscriber);
        } else {
            return 'Users <i>(select an account to list associated users)</i>';
        }

    },

    getSubscriptionsGridPanelTitle: function(subscriber) {
        this.optDebug('getSubscriptionsGridPanelTitle.arguments: ', arguments);

        if (subscriber) {
            return 'Subscriptions for ' + this.generateSubscriberAccountDescriptorLine(subscriber);
        } else {
            return 'Subscriptions <i>(select an account to list associated subscriptions)</i>';
        }


    },

    resetManageSubscribersSubscriberAccountsGridPanel: function() {
        this.optDebug('resetManageSubscribersSubscriberAccountsGridPanel.arguments: ', arguments);

        var subscriberAccountsGridPanel = this.getManageSubscribersSubscriberAccountsGridPanel();
        var subscriberAccountsSelectionModel = subscriberAccountsGridPanel.getSelectionModel();

        subscriberAccountsSelectionModel.deselectAll(false);

        subscriberAccountsGridPanel.setTitle('Accounts');
    },

    resetManageSubscribersUserAccountsGridPanel: function() {
        this.optDebug('resetManageSubscribersUserAccountsGridPanel.arguments: ', arguments);

        var userAccountsGridPanel = this.getManageSubscribersUserAccountsGridPanel();
        var userAccountsSelectionModel = userAccountsGridPanel.getSelectionModel();

        userAccountsSelectionModel.deselectAll(true);

        userAccountsGridPanel.setTitle(this.getUserAccountsGridPanelTitle());
    },

    resetManageSubscribersSubscriptionsGridPanel: function() {
        this.optDebug('resetManageSubscribersSubscriptionsGridPanel.arguments: ', arguments);

        var subscriptionsGridPanel = this.getManageSubscribersSubscriptionsGridPanel();
        var subscriptionsSelectionModel = subscriptionsGridPanel.getSelectionModel();

        subscriptionsSelectionModel.deselectAll(true);

        subscriptionsGridPanel.setTitle(this.getSubscriptionsGridPanelTitle());
    },

    clearManageSubscribersUserAccountsGridPanel: function() {
        this.optDebug('clearManageSubscribersUserAccountsGridPanel.arguments: ', arguments);

        var userAccountsGridPanel = this.getManageSubscribersUserAccountsGridPanel();

        userAccountsGridPanel.setTitle(this.getUserAccountsGridPanelTitle());
        userAccountsGridPanel.setDisabled(true);

        this.getManageSubscribersAddUserButton().setDisabled(true);
        this.getManageSubscribersEditSubscriberButton().setDisabled(true);
        this.getManageSubscribersSuspendSubscriberButton().setDisabled(true);
        this.getManageSubscribersUnsuspendSubscriberButton().setDisabled(true);
        this.getManageSubscribersDeleteSubscriberButton().setDisabled(true);

        this.currentUserSelected = null;

    },

    clearManageSubscribersSubscriptionsGridPanel: function() {
        this.optDebug('clearManageSubscribersSubscriptionsGridPanel.arguments: ', arguments);

        var subscriptionsGridPanel = this.getManageSubscribersSubscriptionsGridPanel();

        subscriptionsGridPanel.setTitle(this.getSubscriptionsGridPanelTitle());
        subscriptionsGridPanel.setDisabled(true);

        this.getManageSubscribersAddSubscriptionButton().setDisabled(true);
        this.getManageSubscribersEditSubscriptionButton().setDisabled(true);
        this.getManageSubscribersSuspendSubscriptionButton().setDisabled(true);
        this.getManageSubscribersUnsuspendSubscriptionButton().setDisabled(true);
        this.getManageSubscribersDeleteSubscriptionButton().setDisabled(true);

        this.currentSubscriptionSelected = null;

    },

    activateManageSubscribersSubscriberAccountsGridPanel: function() {
        this.optDebug('activateManageSubscribersSubscriberAccountsGridPanel.arguments: ', arguments);

        /*
        if (this.application.loggedIn) {
        var subscriberAccountsGridPanel = this.getManageSubscribersSubscriberAccountsGridPanel();
        var subscriberAccountsStore = subscriberAccountsGridPanel.getStore();

        if (!subscriberAccountsStore.isLoading() && !subscriberAccountsStore.getCount()) {
        //subscriberAccountsStore.prefetch({
        //    limit: 10
        //});
        subscriberAccountsStore.load();

        this.optDebug('post-(re)load subscriberAccountsStore:', subscriberAccountsStore);

    } else {
        this.optDebug('load of subscriberAccountsStore omitted');
    }

    return true;

            } else {
    return false;
            }
            */

            var subscriberAccountsGridPanel = this.getManageSubscribersSubscriberAccountsGridPanel();
            subscriberAccountsGridPanel.setDisabled(false);

    },

    reloadManageSubscribersUserAccountsGridPanel: function(subscriberRecordKey) {
        this.optDebug('reloadManageSubscribersUserAccountsGridPanel.arguments: ', arguments);

        var userAccountsGridPanel = this.getManageSubscribersUserAccountsGridPanel();
        var userAccountsGridView = userAccountsGridPanel.getView();
        var userAccountsStore = Ext.getStore('AdminInterface.store.UserAccounts');

        userAccountsGridPanel.setTitle(this.getUserAccountsGridPanelTitle(subscriberRecordKey));

        /*
        if (userAccountsGridView.getStore().$className == "AdminInterface.store.EmptySet") {
        this.optDebug('useraccountsstsore rebound...');
        userAccountsGridPanel.reconfigure(userAccountsStore);
        //userAccountsGridView.bindStore(userAccountsStore);
        //userAccountsGridView.refresh();
    }

    var ancestorFilter = new Ext.util.Filter({
        property: 'ancestor',
        value   : subscriberRecordKey
    });

    var notDeletedFilter = new Ext.util.Filter({
        property: 'deleted',
        value   : false
    });

    userAccountsStore.load({
        filters: [ancestorFilter, notDeletedFilter]
    });
    */

    userAccountsStore.filterByAncestor(subscriberRecordKey);

    this.getManageSubscribersAddUserButton().setDisabled(false);
    userAccountsGridPanel.setDisabled(false);
    this.currentUserSelected = null;

    },

    reloadManageSubscribersSubscriptionsGridPanel: function(subscriberRecordKey) {
        this.optDebug('reloadManageSubscribersSubscriptionsGridPanel.arguments: ', arguments);

        var subscriptionsGridPanel = this.getManageSubscribersSubscriptionsGridPanel();
        var subscriptionsGridView = subscriptionsGridPanel.getView();
        var subscriptionsStore = Ext.getStore('AdminInterface.store.Subscriptions');

        subscriptionsGridPanel.setTitle(this.getSubscriptionsGridPanelTitle(subscriberRecordKey));

        /*
        if (subscriptionsGridView.getStore().$className == "AdminInterface.store.EmptySet") {
        this.optDebug('subscriptionsstsore rebound...');
        subscriptionsGridPanel.reconfigure(subscriptionsStore);
        //subscriptionsGridView.bindStore(subscriptionsStore);
        //subscriptionsGridView.refresh();
    }


    var ancestorFilter = new Ext.util.Filter({
        property: 'ancestor',
        value   : subscriberRecordKey
    });

    subscriptionsStore.load({
        filters: [ancestorFilter]
    });
    */

    subscriptionsStore.filterByAncestor(subscriberRecordKey);

    this.getManageSubscribersAddSubscriptionButton().setDisabled(false);
    subscriptionsGridPanel.setDisabled(false);
    this.currentSubscriptionSelected = null;

    },

    onManageSubscribersTabPanelBeforeActivate: function(tabPanel, eOpts) {
        this.optDebug('onManageSubscribersTabPanelBeforeActivate.arguments: ', arguments);

        this.resetManageSubscribersSubscriberAccountsGridPanel();
        this.resetManageSubscribersUserAccountsGridPanel();
        this.resetManageSubscribersSubscriptionsGridPanel();

        var subscriberAccountsGridPanel = this.getManageSubscribersSubscriberAccountsGridPanel();
        var subscriberAccountsStore = subscriberAccountsGridPanel.getStore();
        var userAccountsGridPanel = this.getManageSubscribersSubscriberAccountsGridPanel();
        var userAccountsStore = userAccountsGridPanel.getStore();

        if (this.debug) {
            if (!window.mstatDebug) {
                window.mstatDebug = {};
            }

            window.mstatDebug.subscriberAccountsGridPanel = subscriberAccountsGridPanel;
            window.mstatDebug.subscriberAccountsStore = subscriberAccountsStore;
            window.mstatDebug.userAccountsGridPanel = userAccountsGridPanel;
            window.mstatDebug.userAccountsStore = userAccountsStore;
        }

        this.optDebug('pre-(re)load subscriberAccountsStore:', subscriberAccountsStore);

        //this.reloadManageSubscribersSubscriberAccountsGridPanel();


    },

    onManageSubscribersTabPanelBeforeDeactivate: function(tabPanel, eOpts) {
        this.optDebug('onManageSubscribersTabPanelBeforeDeactivate.arguments: ', arguments);

        this.resetManageSubscribersSubscriberAccountsGridPanel();
        this.resetManageSubscribersUserAccountsGridPanel();
        this.resetManageSubscribersSubscriptionsGridPanel();

    },

    onManageSubscribersSubscriberAccountsGridPanelBeforeRender: function(gridPanel, eOpts) {
        this.optDebug('onManageSubscribersSubscriberAccountsGridPanelBeforeRender.arguments: ', arguments);

        gridPanel.setTitle('Accounts');

        //gridPanel.getStore().
        //this.getManageSubscribersEditSubscriberButton().setDisabled(true);
        //this.getManageSubscribersSuspendSubscriberButton().setDisabled(true);
        //this.getManageSubscribersDeleteSubscriberButton().setDisabled(true);

        //var refreshTool = this.getManageSubscribersSubscriberAccountsGridPanelRefreshTool();

        //refreshTool.fireEvent('click');
    },

    onManageSubscribersSubscriberAccountsGridPanelDeselect: function(gridPanel, record, index, eOpts) {
        this.optDebug('onManageSubscribersSubscriberAccountsGridPanelDeselect.arguments: ', arguments);

        var userAccountsGridPanel = this.getManageSubscribersUserAccountsGridPanel();
        var userAccountsStoreView = userAccountsGridPanel.getView();
        var subscriptionsGridPanel = this.getManageSubscribersSubscriptionsGridPanel();
        var subscriptionsStoreView = subscriptionsGridPanel.getView();

        //var emptySetStore = Ext.getStore('AdminInterface.store.EmptySetStore');

        userAccountsGridPanel.getStore().filterByAncestor();
        subscriptionsGridPanel.getStore().filterByAncestor();

        //userAccountsGridPanel.reconfigure(emptySetStore);
        //subscriptionsGridPanel.reconfigure(emptySetStore);
        //userAccountsStoreView.bindStore(emptySetStore);
        //subscriptionsStoreView.bindStore(emptySetStore);

        /*
        var userAccountsStore = Ext.getStore('AdminInterface.store.UserAccounts'),
        ancestorFilter = new Ext.util.Filter({
        property: 'ancestor',
        value   : null
        });

        userAccountsStore.load({
        filters: [
        ancestorFilter
        ]
        });
        */

    },

    onManageSubscribersSubscriberAccountsGridPanelSelectionChange: function(gridPanel, selected, eOpts) {
        this.optDebug('onManageSubscribersSubscriberAccountsGridPanelSelectionChange.arguments: ', arguments);

        if (selected.length) {

            var subscriberRecord = selected[0];
            var subscriberRecordKey = subscriberRecord.getId();

            this.currentSubscriberSelected = subscriberRecordKey;

            this.reloadManageSubscribersUserAccountsGridPanel(subscriberRecordKey);
            this.reloadManageSubscribersSubscriptionsGridPanel(subscriberRecordKey);

            this.getManageSubscribersEditSubscriberButton().setDisabled(false);

            if (!subscriberRecord.get('suspended')) {
                this.getManageSubscribersSuspendSubscriberButton().setDisabled(false);
            } else {
                this.getManageSubscribersUnsuspendSubscriberButton().setDisabled(false);
            }

            this.getManageSubscribersDeleteSubscriberButton().setDisabled(false);

        } else {

            this.currentSubscriberSelected = null;

            this.clearManageSubscribersSubscriptionsGridPanel();
            this.clearManageSubscribersUserAccountsGridPanel();

        }
    },

    onManageSubscribersSubscriberAccountsGridPanelCellDblClick: function(gridPanel, td, cellIndex, record, tr, rowIndex, dblClickEvent, eOpts) {
        this.optDebug('onManageSubscribersSubscriberAccountsGridPanelCellDblClick.arguments: ', arguments);

        this.optDebug('this.currentSubscriberSelected:', this.currentSubscriberSelected);

        this.application.fireEvent('exDialogOpen', 'ViewSubscriberCtrl', this.currentSubscriberSelected);

    },

    onManageSubscribersUserAccountsGridPanelBeforeRender: function(gridPanel, eOpts) {
        this.optDebug('onManageSubscribersUserAccountsGridPanelBeforeRender.arguments: ', arguments);


    },

    onManageSubscribersUserAccountsGridPanelDeselect: function(gridPanel, record, index, eOpts) {
        this.optDebug('onManageSubscribersUserAccountsGridPanelDeselect.arguments: ', arguments);


    },

    onManageSubscribersUserAccountsGridPanelSelectionChange: function(gridPanel, selected, eOpts) {
        this.optDebug('onManageSubscribersUserAccountsGridPanelSelectionChange.arguments: ', arguments);

        if (selected.length) {

            var userAccountsGridPanel = this.getManageSubscribersUserAccountsGridPanel();
            var userAccountsStore = Ext.getStore('AdminInterface.store.UserAccounts');
            var userAccountsStoreView = userAccountsGridPanel.getView();
            var userRecord = selected[0];
            var userRecordKey = userRecord.getId();

            this.optDebug('userRecordKey: ', userRecordKey);

            this.getManageSubscribersEditUserButton().setDisabled(false);

            this.getManageSubscribersSuspendUserButton().setDisabled(!!userRecord.get('suspended'));
            this.getManageSubscribersUnsuspendUserButton().setDisabled(!userRecord.get('suspended'));

            this.getManageSubscribersDeleteUserButton().setDisabled(false);

            this.currentUserSelected = userRecordKey;

        } else {
            this.getManageSubscribersEditUserButton().setDisabled(true);
            this.getManageSubscribersSuspendUserButton().setDisabled(true);
            this.getManageSubscribersUnsuspendUserButton().setDisabled(true);
            this.getManageSubscribersDeleteUserButton().setDisabled(true);

            this.currentUserSelected = null;

        }
    },

    onManageSubscribersUserAccountsGridPanelCellDblClick: function(gridPanel, td, cellIndex, record, tr, rowIndex, dblClickEvent, eOpts) {
        this.optDebug('onManageSubscribersUserAccountsGridPanelCellDblClick.arguments: ', arguments);

        this.optDebug('this.currentSubscriberSelected:', this.currentSubscriberSelected);
        this.optDebug('this.currentUserSelected:', this.currentUserSelected);

        this.application.fireEvent('exDialogOpen', 'ViewUserCtrl', this.currentSubscriberSelected, this.currentUserSelected);


    },

    onManageSubscribersSubscriptionsGridPanelBeforeRender: function(gridPanel, eOpts) {
        this.optDebug('onManageSubscribersSubscriptionsGridPanelBeforeRender.arguments: ', arguments);


    },

    onManageSubscribersSubscriptionsGridPanelDeselect: function(gridPanel, record, index, eOpts) {
        this.optDebug('onManageSubscribersSubscriptionsGridPanelDeselect.arguments: ', arguments);


    },

    onManageSubscribersSubscriptionsGridPanelSelectionChange: function(gridPanel, selected, eOpts) {
        this.optDebug('onManageSubscribersSubscriptionsGridPanelSelectionChange.arguments: ', arguments);

        if (selected.length) {

            var subscriptionsGridPanel = this.getManageSubscribersSubscriptionsGridPanel(),
                subscriptionsStore = Ext.getStore('AdminInterface.store.Subscriptions'),
                subscriptionsStoreView = subscriptionsGridPanel.getView(),
                subscriptionRecord = selected[0],
                subscriptionRecordKey = subscriptionRecord.getId();

            this.optDebug('subscriptionRecordKey: ', subscriptionRecordKey);

            this.getManageSubscribersEditSubscriptionButton().setDisabled(false);

            this.getManageSubscribersSuspendSubscriptionButton().setDisabled(!!subscriptionRecord.get('suspended'));
            this.getManageSubscribersUnsuspendSubscriptionButton().setDisabled(!subscriptionRecord.get('suspended'));

            this.getManageSubscribersDeleteSubscriptionButton().setDisabled(false);

            this.currentSubscriptionSelected = subscriptionRecordKey;

        } else {
            this.getManageSubscribersEditSubscriptionButton().setDisabled(true);
            this.getManageSubscribersSuspendSubscriptionButton().setDisabled(true);
            this.getManageSubscribersUnsuspendSubscriptionButton().setDisabled(true);
            this.getManageSubscribersDeleteSubscriptionButton().setDisabled(true);

            this.currentSubscriptionSelected = null;

        }
    },

    onManageSubscribersSubscriptionsGridPanelCellDblClick: function(gridPanel, td, cellIndex, record, tr, rowIndex, dblClickEvent, eOpts) {
        this.optDebug('onManageSubscribersSubscriptionsGridPanelCellDblClick.arguments: ', arguments);

        this.optDebug('this.currentSubscriberSelected:', this.currentSubscriberSelected);
        this.optDebug('this.currentSubscriptionSelected:', this.currentSubscriptionSelected);

        this.application.fireEvent('exDialogOpen', 'ViewSubscriptionCtrl', this.currentSubscriberSelected, this.currentSubscriptionSelected);

    },

    onManageSubscribersAddSubscriberButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onManageSubscribersAddSubscriberButtonClick.arguments: ', arguments);

        this.application.fireEvent('exDialogOpen', 'AddSubscriberCtrl', null);

    },

    onManageSubscribersEditSubscriberButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onManageSubscribersEditSubscriberButtonClick.arguments: ', arguments);

        this.optDebug('this.currentSubscriberSelected:', this.currentSubscriberSelected);

        this.application.fireEvent('exDialogOpen', 'EditSubscriberCtrl', this.currentSubscriberSelected);

    },

    onManageSubscribersSuspendSubscriberButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onManageSubscribersSuspendSubscriberButtonClick.arguments: ', arguments);

        this.optDebug('this.currentSubscriberSelected:', this.currentSubscriberSelected);

        var me = this;

        var subscriberAccountsGridView = this.getManageSubscribersSubscriberAccountsGridPanel().getView();
        var subscriberAccountsStore = subscriberAccountsGridView.getStore();
        var currentSubscriberSelected = this.currentSubscriberSelected;
        var subscriberRecord = subscriberAccountsStore.getById(currentSubscriberSelected);

        var unsuspendSubscriberAccountButton = this.getManageSubscribersUnsuspendSubscriberButton();
        var userAccountsGridView = this.getManageSubscribersUserAccountsGridPanel().getView();
        var subscriptionsGridView = this.getManageSubscribersSubscriptionsGridPanel().getView();

        this.optDebug('subscriberRecord: ', subscriberRecord);

        var subscriberAccountId = subscriberRecord.get('datastore_key_id');

        button.setDisabled(true);

        subscriberAccountsGridView.setLoading('Suspending Account #'+subscriberAccountId+' ...');
        userAccountsGridView.setLoading('Suspending Users for Account #'+subscriberAccountId+' ...');
        subscriptionsGridView.setLoading('Suspending Subscriptions for Account #'+subscriberAccountId+' ...');

        Remote.AdminCmd.suspend_subscriber_account({
            subscriber_account: currentSubscriberSelected
        }, function(result, request, success) {

            if (success) {
                me.optDebug('suspended: ' + result);
                subscriberRecord.set('suspended', 'true');
                subscriberRecord.postProcessExFields(true);
                subscriberRecord.commit();

                subscriberAccountsGridView.refresh();

                me.reloadManageSubscribersUserAccountsGridPanel(currentSubscriberSelected);
                me.reloadManageSubscribersSubscriptionsGridPanel(currentSubscriberSelected);

                unsuspendSubscriberAccountButton.setDisabled(false);
            } else {
                me.optDebug('suspended failed');
                button.setDisabled(false);
            }

            subscriberAccountsGridView.setLoading(false);
            userAccountsGridView.setLoading(false);
            subscriptionsGridView.setLoading(false);
        });

    },

    onManageSubscribersUnsuspendSubscriberButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onManageSubscribersUnsuspendSubscriberButtonClick.arguments: ', arguments);

        var me = this;

        var subscriberAccountsGridView = this.getManageSubscribersSubscriberAccountsGridPanel().getView();
        var subscriberAccountsStore = subscriberAccountsGridView.getStore();
        var currentSubscriberSelected = this.currentSubscriberSelected;
        var subscriberRecord = subscriberAccountsStore.getById(currentSubscriberSelected);

        var suspendSubscriberAccountButton = this.getManageSubscribersSuspendSubscriberButton();
        var userAccountsGridView = this.getManageSubscribersUserAccountsGridPanel().getView();
        var subscriptionsGridView = this.getManageSubscribersSubscriptionsGridPanel().getView();

        this.optDebug('subscriberRecord: ', subscriberRecord);

        var subscriberAccountId = subscriberRecord.get('datastore_key_id');

        button.setDisabled(true);

        subscriberAccountsGridView.setLoading('Unsuspending Account #'+subscriberAccountId+' ...');
        userAccountsGridView.setLoading('Unsuspending Users for Account #'+subscriberAccountId+' ...');
        subscriptionsGridView.setLoading('Unsuspending Subscriptions for Account #'+subscriberAccountId+' ...');

        Remote.AdminCmd.unsuspend_subscriber_account({
            subscriber_account: currentSubscriberSelected
        }, function(result, request, success) {

            if (success) {
                me.optDebug('unsuspended: ' + result);
                subscriberRecord.set('suspended', 'false');
                subscriberRecord.postProcessExFields(true);
                subscriberRecord.commit();
                subscriberAccountsGridView.refresh();

                me.reloadManageSubscribersUserAccountsGridPanel(currentSubscriberSelected);
                me.reloadManageSubscribersSubscriptionsGridPanel(currentSubscriberSelected);

                suspendSubscriberAccountButton.setDisabled(false);
            } else {
                me.optDebug('unsuspended failed');
                button.setDisabled(false);
            }

            subscriberAccountsGridView.setLoading(false);
            userAccountsGridView.setLoading(false);
            subscriptionsGridView.setLoading(false);
        });

    },

    onManageSubscribersDeleteSubscriberButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onManageSubscribersDeleteSubscriberButtonClick.arguments: ', arguments);

        this.optDebug('this.currentSubscriberSelected:', this.currentSubscriberSelected);

        var me = this;
        var subscriberAccountsGridView = this.getManageSubscribersSubscriberAccountsGridPanel().getView();
        var subscriberAccountsStore = subscriberAccountsGridView.getStore();
        var currentSubscriberSelected = this.currentSubscriberSelected;
        var subscriberRecord = subscriberAccountsStore.getById(currentSubscriberSelected);
        var userAccountsGridView = this.getManageSubscribersUserAccountsGridPanel().getView();
        var subscriptionsGridView = this.getManageSubscribersSubscriptionsGridPanel().getView();
        var emptySetStore = Ext.getStore('AdminInterface.store.EmptySetStore');

        this.optDebug('subscriberRecord: ', subscriberRecord);

        var subscriberAccountId = subscriberRecord.get('datastore_key_id');

        button.setDisabled(true);

        function deleteSubscriberCallback(buttonId, text, opt) {

            if (buttonId == 'yes') {
                subscriberAccountsGridView.setLoading('Deleting Account #' + subscriberAccountId + ' ...');
                userAccountsGridView.setLoading('Deleting Users for Account #' + subscriberAccountId + ' ...');
                subscriptionsGridView.setLoading('Deleting Subscriptions for Account #' + subscriberAccountId + ' ...');
                me.optDebug('delete call submitted..');
                Remote.AdminCmd.delete_subscriber_account({
                    subscriber_account: currentSubscriberSelected
                }, function(result, request, success) {
                    if (success) {
                        me.optDebug('delete: ' + result);
                        subscriberAccountsStore.remove(subscriberRecord);
                        userAccountsGridView.bindStore(emptySetStore);
                        subscriptionsGridView.bindStore(emptySetStore);
                    } else {
                        me.optDebug('delete failed');
                        button.setDisabled(false);
                    }
                    subscriberAccountsGridView.setLoading(false);
                    userAccountsGridView.setLoading(false);
                    subscriptionsGridView.setLoading(false);
                });

                return true;
            }

            if (buttonId == 'cancel' || buttonId == 'no') {
                button.setDisabled(false);
            }
        }

        Ext.Msg.show({
            title: 'Confirm Account Deletion',
            msg: 'Are you sure you want to delete account #' + subscriberAccountId + '? This action may be irreversible.',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: deleteSubscriberCallback
        });


    },

    onManageSubscribersAddUserButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onManageSubscribersAddUserButtonClick.arguments: ', arguments);

        this.optDebug('this.currentSubscriberSelected:', this.currentSubscriberSelected);

        this.application.fireEvent('exDialogOpen', 'AddUserCtrl', this.currentSubscriberSelected, null);

    },

    onManageSubscribersEditUserButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onManageSubscribersEditUserButtonClick.arguments: ', arguments);

        this.application.fireEvent('exDialogOpen', 'AddUserCtrl', this.currentSubscriberSelected, this.currentUserSelected);

    },

    onManageSubscribersSuspendUserButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onManageSubscribersSuspendUserButtonClick.arguments: ', arguments);

        this.optDebug('this.currentUserSelected:', this.currentUserSelected);

        var me = this;
        var userAccountsGridPanel = this.getManageSubscribersUserAccountsGridPanel();
        var userAccountsGridView = userAccountsGridPanel.getView();
        var userAccountsStore = userAccountsGridView.getStore(); //Ext.getStore('AdminInterface.store.UserAccounts');
        var unsuspendUserButton = this.getManageSubscribersUnsuspendUserButton();
        var currentUserSelected = this.currentUserSelected;

        userAccountsGridView.setLoading('Suspending User ...');
        button.setDisabled(true);

        Remote.AdminCmd.suspend_user_account({
            user_account: currentUserSelected
        }, function(result, request, success) {
            me.optDebug('suspended: ' + result);
            if (success) {
                var account = userAccountsStore.getById(result);
                account.set('suspended', true);
                account.postProcessExFields(true);
                account.commit();
                //userAccountsGridPanel.reconfigure(userAccountsStore);
                userAccountsGridView.refresh();
                unsuspendUserButton.setDisabled(false);
            } else {
                button.setDisabled(false);
            }
            userAccountsGridView.setLoading(false);
        });
    },

    onManageSubscribersUnsuspendUserButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onManageSubscribersUnsuspendUserButtonClick.arguments: ', arguments);

        this.optDebug('this.currentUserSelected:', this.currentUserSelected);

        var me = this;
        var userAccountsGridView = this.getManageSubscribersUserAccountsGridPanel().getView();
        var userAccountsStore = userAccountsGridView.getStore(); //Ext.getStore('AdminInterface.store.UserAccounts'),
            var suspendUserButton = this.getManageSubscribersSuspendUserButton();
        var currentUserSelected = this.currentUserSelected;

        button.setDisabled(true);
        userAccountsGridView.setLoading('Unsuspending User ...');

        Remote.AdminCmd.unsuspend_user_account({
            user_account: currentUserSelected
        }, function(result, request, success) {
            me.optDebug('un-suspended: ' + result);
            if (success) {
                var account = userAccountsStore.getById(result);
                account.set('suspended', false);
                account.postProcessExFields(true);
                account.commit();
                userAccountsGridView.refresh();
                suspendUserButton.setDisabled(false);
            } else {
                button.setDisabled(false);
            }
            userAccountsGridView.setLoading(false);
        });
    },

    onManageSubscribersDeleteUserButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onManageSubscribersDeleteUserButtonClick.arguments: ', arguments);

        this.optDebug('this.currentUserSelected:', this.currentUserSelected);

        var me = this;

        this.optDebug('this.currentSubscriberSelected:', this.currentSubscriberSelected);

        var userAccountsGridView = this.getManageSubscribersUserAccountsGridPanel().getView();
        var userAccountsStore = userAccountsGridView.getStore();
        var currentUserSelected = this.currentUserSelected;
        var userRecord = userAccountsStore.getById(currentUserSelected);
        var emptySetStore = Ext.getStore('AdminInterface.store.EmptySetStore');

        this.optDebug('userRecord: ', userRecord);

        var userAccountId = userRecord.get('datastore_key_id');

        button.setDisabled(true);

        function deleteUserCallback(buttonId, text, opt) {

            if (buttonId == 'yes') {
                userAccountsGridView.setLoading('Deleting User #'+userAccountId+' ...');
                me.optDebug('delete call submitted..');
                Remote.AdminCmd.delete_user_account({
                    user_account: currentUserSelected
                }, function(result, request, success) {
                    if (success) {
                        me.optDebug('delete: ' + result);
                        userAccountsStore.remove(userRecord);
                    } else {
                        me.optDebug('delete failed');
                        button.setDisabled(false);
                    }
                    userAccountsGridView.setLoading(false);
                });

                return true;
            }

            if (buttonId == 'cancel' || buttonId == 'no') {
                button.setDisabled(false);
            }
        }

        Ext.Msg.show({
            title: 'Confirm Account Deletion',
            msg: 'Are you sure you want to delete user #' + userAccountId + '? This action may be irreversible.',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: deleteUserCallback
        });

    },

    onManageSubscribersAddSubscriptionButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onManageSubscribersAddSubscriptionButtonClick.arguments: ', arguments);

        this.optDebug('this.currentSubscriberSelected:', this.currentSubscriberSelected);

        this.application.fireEvent('exDialogOpen', 'AddSubscriptionCtrl', this.currentSubscriberSelected, null);

    },

    onManageSubscribersEditSubscriptionButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onManageSubscribersEditSubscriptionButtonClick.arguments: ', arguments);

        this.optDebug('this.currentSubscriptionSelected:', this.currentSubscriptionSelected);

        this.application.fireEvent('exDialogOpen', 'AddSubscriptionCtrl', this.currentSubscriberSelected, this.currentSubscriptionSelected);

    },

    onManageSubscribersSuspendSubscriptionButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onManageSubscribersSuspendSubscriptionButtonClick.arguments: ', arguments);

        this.optDebug('this.currentSubscriptionSelected:', this.currentSubscriptionSelected);

        var me = this;
        var subscriptionsGridView = this.getManageSubscribersSubscriptionsGridPanel().getView();
        var subscriptionsStore = subscriptionsGridView.getStore(); //Ext.getStore('AdminInterface.store.UserAccounts');
        var unsuspendSubscriptionButton = this.getManageSubscribersUnsuspendSubscriptionButton();
        var currentSubscriptionSelected = this.currentSubscriptionSelected;

        subscriptionsGridView.setLoading('Suspending Subscription ...');
        button.setDisabled(true);

        Remote.AdminCmd.suspend_subscription({
            subscription: currentSubscriptionSelected
        }, function(result, request, success) {
            me.optDebug('suspended: ' + result);
            if (success) {
                var subscriptionRecord = subscriptionsStore.getById(result);
                subscriptionRecord.set('suspended', true);
                subscriptionRecord.postProcessExFields(true);
                subscriptionRecord.commit();
                subscriptionsGridView.refresh();
                unsuspendSubscriptionButton.setDisabled(false);
            } else {
                button.setDisabled(false);
            }
            subscriptionsGridView.setLoading(false);
        });
    },

    onManageSubscribersUnsuspendSubscriptionButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onManageSubscribersUnsuspendSubscriptionButtonClick.arguments: ', arguments);

        this.optDebug('this.currentSubscriptionSelected:', this.currentSubscriptionSelected);

        var me = this;
        var subscriptionsGridView = this.getManageSubscribersSubscriptionsGridPanel().getView();
        var subscriptionsStore = subscriptionsGridView.getStore();
        var suspendSubscriptionButton = this.getManageSubscribersSuspendSubscriptionButton();
        var currentSubscriptionSelected = this.currentSubscriptionSelected;

        subscriptionsGridView.setLoading('Unsuspending Subscription ...');
        button.setDisabled(true);

        Remote.AdminCmd.unsuspend_subscription({
            subscription: currentSubscriptionSelected
        }, function(result, request, success) {
            me.optDebug('suspended: ' + result);
            if (success) {
                var subscriptionRecord = subscriptionsStore.getById(result);
                subscriptionRecord.set('suspended', false);
                subscriptionRecord.postProcessExFields(true);
                subscriptionRecord.commit();
                subscriptionsGridView.refresh();
                suspendSubscriptionButton.setDisabled(false);
            } else {
                button.setDisabled(false);
            }
            subscriptionsGridView.setLoading(false);
        });
    },

    onManageSubscribersDeleteSubscriptionButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onManageSubscribersDeleteSubscriptionButtonClick.arguments: ', arguments);

        this.optDebug('this.currentSubscriptionSelected:', this.currentSubscriptionSelected);

        var me = this;

        this.optDebug('this.currentSubscriberSelected:', this.currentSubscriberSelected);

        var subscriptionsGridView = this.getManageSubscribersSubscriptionsGridPanel().getView();
        var subscriptionsStore = subscriptionsGridView.getStore();
        var currentSubscriptionSelected = this.currentSubscriptionSelected;
        var subscriptionRecord = subscriptionsStore.getById(currentSubscriptionSelected);
        var emptySetStore = Ext.getStore('AdminInterface.store.EmptySetStore');

        this.optDebug('subscriptionRecord: ', subscriptionRecord);

        var subscriptionId = subscriptionRecord.get('datastore_key_id');

        button.setDisabled(true);

        function deleteSubscriptionCallback(buttonId, text, opt) {

            if (buttonId == 'yes') {
                subscriptionsGridView.setLoading('Deleting Subscription #'+subscriptionId+' ...');
                me.optDebug('Delete call submitted...');
                Remote.AdminCmd.delete_subscription({
                    subscription: currentSubscriptionSelected
                }, function(result, request, success) {
                    if (success) {
                        me.optDebug('Delete: ' + result);
                        subscriptionsStore.remove(subscriptionRecord);
                    } else {
                        me.optDebug('Delete failed.');
                        button.setDisabled(false);
                    }
                    subscriptionsGridView.setLoading(false);
                });

                return true;
            }

            if (buttonId == 'cancel' || buttonId == 'no') {
                button.setDisabled(false);
            }
        }

        Ext.Msg.show({
            title: 'Confirm Subscription Deletion',
            msg: 'Are you sure you want to delete subscription #' + subscriptionId + '? This action may be irreversible.',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: deleteSubscriptionCallback
        });

    },

    onExLoginComplete: function(userAccountRecord, systemAdminAccountRecord, eOpts) {
        this.optDebug('onExLoginComplete.arguments: ', arguments);

        var subscriberAccountsStore = Ext.getStore('AdminInterface.store.SubscriberAccounts');
        var subscriptionsStore = Ext.getStore('AdminInterface.store.Subscriptions');
        var userAccountsStore = Ext.getStore('AdminInterface.store.UserAccounts');
        var storesLoaded = 0;

        subscriberAccountsStore.load({
            scope: this,
            callback: function(records, operation, success) {
                this.optDebug('subscriberAccountsStore.load.arguments: ', arguments);
                if (++storesLoaded == 3) {
                    this.activateManageSubscribersSubscriberAccountsGridPanel();
                }
            }
        });

        subscriptionsStore.load({
            scope: this,
            callback: function(records, operation, success) {
                this.optDebug('subscriberAccountsStore.load.arguments: ', arguments);
                subscriptionsStore.filterByAncestor();
                if (++storesLoaded == 3) {
                    this.activateManageSubscribersSubscriberAccountsGridPanel();
                }
            }
        });

        userAccountsStore.load({
            scope: this,
            callback: function(records, operation, success) {
                this.optDebug('subscriberAccountsStore.load.arguments: ', arguments);
                userAccountsStore.filterByAncestor();
                if (++storesLoaded == 3) {
                    this.activateManageSubscribersSubscriberAccountsGridPanel();
                }
            }
        });

    },

    onExLogoutInit: function(eOpts) {
        this.optDebug('onExLogoutInit.arguments: ', arguments);

        /* clear gridpanel, attached stores and what-not */
    }

});
