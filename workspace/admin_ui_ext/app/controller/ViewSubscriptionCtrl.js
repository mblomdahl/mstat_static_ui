/*
 * File: app/controller/ViewSubscriptionCtrl.js
 * Author: Mats Blomdahl
 * Version: 1.06
 */

Ext.define('AdminInterface.controller.ViewSubscriptionCtrl', {
    extend: 'Ext.app.Controller',
    alias: 'controller.viewsubscriptionctrl',

    models: [
        'Subscription',
        'OrganizationAccount'
    ],
    stores: [
        'AdminInterface.store.Subscriptions',
        'AdminInterface.store.SubscriberAccounts'
    ],
    views: [
        'AdminInterface.view.window.ViewSubscriptionWindow'
    ],

    refs: [
        {
            ref: 'viewSubscriptionWindow',
            selector: 'viewsubscriptionwindow',
            xtype: 'viewsubscriptionwindow'
        },
        {
            ref: 'viewSubscriptionDetailsFormPanel',
            selector: '#viewSubscriptionDetailsFormPanel',
            xtype: 'form'
        },
        {
            ref: 'viewSubscriptionParentSubscriberDisplayField',
            selector: '#viewSubscriptionParentSubscriberDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'viewSubscriptionCancelButton',
            selector: 'viewsubscriptionwindow genericcancelbutton',
            xtype: 'genericcancelbutton'
        },
        {
            ref: 'viewSubscriptionEditButton',
            selector: 'viewsubscriptionwindow genericactionbutton[text="Edit Subscription"]',
            xtype: 'genericactionbutton'
        },
        {
            ref: 'viewSubscriptionSuspendButton',
            selector: 'viewsubscriptionwindow genericactionbutton[text="Suspend Subscription"]',
            xtype: 'genericactionbutton'
        },
        {
            ref: 'viewSubscriptionUnsuspendButton',
            selector: 'viewsubscriptionwindow genericactionbutton[text="Un-suspend Subscription"]',
            xtype: 'genericactionbutton'
        },
        {
            ref: 'viewSubscriptionDeleteButton',
            selector: 'viewsubscriptionwindow genericactionbutton[text="Delete Subscription"]',
            xtype: 'genericactionbutton'
        }
    ],

    init: function(application) {
        this.optDebug = application.optDebug;
        this.debug = false || application.debug;
        this.optDebug('ViewSubscriptionCtrl.arguments', arguments);

        this.control({
            '#viewSubscriptionWindow': {
                hide: this.onViewSubscriptionWindowHide,
                show: this.onViewSubscriptionWindowShow
            },
            '#viewSubscriptionStep1Container': {
                activate: this.onViewSubscriptionStep1ContainerActivate,
                //beforeshow: this.onViewUserStep1ContainerBeforeShow
                beforerender: this.onViewSubscriptionStep1ContainerBeforeRender
            },
            'viewsubscriptionwindow genericcancelbutton': {
                click: this.onViewSubscriptionCancelButtonClick
            },
            'viewsubscriptionwindow genericactionbutton[text="Edit Subscription"]': {
                click: this.onViewSubscriptionEditButtonClick
            },
            'viewsubscriptionwindow genericactionbutton[text="Suspend Subscription"]': {
                click: this.onViewSubscriptionSuspendButtonClick
            },
            'viewsubscriptionwindow genericactionbutton[text="Un-suspend Subscription"]': {
                click: this.onViewSubscriptionUnsuspendButtonClick
            },
            'viewsubscriptionwindow genericactionbutton[text="Delete Subscription"]': {
                click: this.onViewSubscriptionDeleteButtonClick
            }
        });

        this.activeStep = 0;

        application.on({
            exDialogOpen: {
                fn: this.onExDialogOpen,
                scope: this
            },
            exDialogClose: {
                fn: this.onExDialogClose,
                scope: this
            }
        });
    },

    resetFields: function() {
        this.optDebug('resetFields.arguments: ', arguments);

        this.parentSubscriberId = null;
        this.subscriptionId = null;
    },

    showWindow: function(parentSubscriber, subscription) {
        this.optDebug('showWindow.arguments: ', arguments);

        this.resetFields();

        Ext.ClassManager.get('AdminInterface.view.window.ViewSubscriptionWindow').create();

        this.parentSubscriberId = parentSubscriber;
        this.subscriptionId = subscription;

        this.getViewSubscriptionWindow().show();
    },

    hideWindow: function(dialogWindow) {
        this.optDebug('hideWindow.arguments: ', arguments);

        dialogWindow = dialogWindow || this.getViewSubscriptionWindow();

        dialogWindow.close();
    },

    resizeWindowHeight: function(container, heightExtra, reposition) {
        this.optDebug('resizeWindowHeight.arguments: ', arguments);

        reposition = reposition || false;

        var parentWindow = container.up('window');
        if (!parentWindow.rendered) {
            this.optDebug('parentWindow.rendered:' + parentWindow.rendered);
            return;
        } else {
            this.optDebug('parentWindow:', parentWindow);
        }

        var activeContainerWrapper = container.up('container');
        if (!activeContainerWrapper.rendered) {
            this.optDebug('activeContainerWrapper.rendered:' + activeContainerWrapper.rendered);
            return;
        } else {
            this.optDebug('activeContainerWrapper:', activeContainerWrapper);
        }

        var wrapperHeightDelta = activeContainerWrapper.getHeight();
        this.optDebug('wrapperHeightDelta:', wrapperHeightDelta);

        var activeViewContent = activeContainerWrapper.query('#' + container.id + ' > container');
        this.optDebug('activeViewContent:', activeViewContent);

        for (var i = 0, j = activeViewContent.length; i < j; i++) {

            if (!activeViewContent[i].rendered) {
                this.optDebug('activeViewContent[i].rendered:' + activeViewContent[i].rendered);
                return;
            }

            wrapperHeightDelta -= activeViewContent[i].getHeight();
        }

        if (heightExtra) {
            wrapperHeightDelta -= heightExtra;
        }

        parentWindow.setHeight(parentWindow.getHeight() - wrapperHeightDelta);

        if (reposition) {
            return parentWindow.center();
            /*
            var windowPosition = parentWindow.getPosition();

            windowPosition[1] += wrapperHeightDelta / 2;
            if (windowPosition[1] < 0) {
            windowPosition[1] = 0;
            }

            parentWindow.setPosition(windowPosition[0], windowPosition[1]);
            */
        }
    },

    getFormattedParentSubscriber: function() {
        this.optDebug('getFormattedParentSubscriber.arguments: ', arguments);

        var subscriberAccountsStore = Ext.getStore('AdminInterface.store.SubscriberAccounts');
        var subscriberDetails = subscriberAccountsStore.getById(this.parentSubscriberId);

        if (subscriberDetails) {
            subscriberDetails = subscriberDetails.getFormatted();
        }

        this.optDebug('subscriberDetails: ', subscriberDetails);

        return subscriberDetails;
    },

    updateSubscriptionDetails: function() {
        this.optDebug('updateSubscriptionDetails.arguments: ', arguments);

        this.getViewSubscriptionParentSubscriberDisplayField().setValue(this.getFormattedParentSubscriber());

        // User details form
        var viewSubscriptionForm = this.getViewSubscriptionDetailsFormPanel().getForm();

        this.optDebug('viewSubscriptionForm: ', viewSubscriptionForm);

        // User account record
        var subscriptionRecord = Ext.getStore('AdminInterface.store.Subscriptions').getById(this.subscriptionId);

        this.optDebug('subscriptionAccountRecord: ', subscriptionRecord);

        viewSubscriptionForm.loadRecord(subscriptionRecord);

        var label = Ext.ComponentQuery.query('#viewSubscriptionStep1Container > container > label')[0];
        label.html = '<h3>Subscription Details for ' + subscriptionRecord.get('friendly_name') + '</h3>';

        var suspendUnsuspendActionButton = this.getViewSubscriptionSuspendButton() || this.getViewSubscriptionUnsuspendButton();

        if (suspendUnsuspendActionButton) {
            if (subscriptionRecord.get('suspended')) {
                suspendUnsuspendActionButton.setText('Un-suspend Subscription');
            } else {
                suspendUnsuspendActionButton.setText('Suspend Subscription');
            }
        }
    },

    onViewSubscriptionWindowShow: function(dialogWindow, eOpts) {
        this.optDebug('onViewSubscriptionWindowShow.arguments: ', arguments);
    },

    onViewSubscriptionWindowHide: function(dialogWindow, eOpts) {
        this.optDebug('onViewSubscriptionWindowHide.arguments: ', arguments);
    },

    onViewSubscriptionStep1ContainerBeforeRender: function(container, eOpts) {
        this.optDebug('onViewSubscriptionStep1ContainerBeforeRender.arguments: ', arguments);

        this.updateSubscriptionDetails();
    },

    onViewSubscriptionStep1ContainerActivate: function(container, eOpts) {
        this.optDebug('onViewSubscriptionStep1ContainerActivate.arguments: ', arguments);

        // Focus GenericCancelButton in the step 1 view
        this.getViewSubscriptionCancelButton().focus(true, 250);

        // Resize window to content + 30px
        this.resizeWindowHeight(container, 30, true);

    },

    onViewSubscriptionCancelButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onViewSubscriptionCancelButtonClick.arguments: ', arguments);

        var viewSubscriptionWindow = button.up('window');

        this.hideWindow(viewSubscriptionWindow);
    },

    onViewSubscriptionEditButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onViewSubscriptionEditButtonClick.arguments: ', arguments);

        this.hideWindow();

        this.application.fireEvent('exDialogOpen', 'AddSubscriptionCtrl', this.parentSubscriberId, this.subscriptionId);
    },

    onViewSubscriptionSuspendButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onViewSubscriptionSuspendButtonClick.arguments: ', arguments);
    },

    onViewSubscriptionUnsuspendButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onViewSubscriptionUnsuspendButtonClick.arguments: ', arguments);
    },

    onViewSubscriptionDeleteButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onViewSubscriptionDeleteButtonClick.arguments: ', arguments);
    },

    onExDialogOpen: function(controllerId, parentSubscriber, subscription, eOpts) {
        this.optDebug('onExDialogOpen.arguments: ', arguments);

        if (controllerId === this.id) {
            this.showWindow(parentSubscriber, subscription);
        }
    },

    onExDialogClose: function(controllerId, eOpts) {
        this.optDebug('onExDialogClose.arguments: ', arguments);

    }

});
