/*
 * File: app/controller/AddUserCtrl.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.controller.AddUserCtrl', {
    extend: 'Ext.app.Controller',
    alias: 'controller.adduserctrl',

    models: [
        'UserAccount',
        'OrganizationAccount'
    ],
    stores: [
        'AdminInterface.store.UserAccounts',
        'AdminInterface.store.SubscriberAccounts'
    ],
    views: [
        'AdminInterface.view.window.AddUserWindow'
    ],

    refs: [
        {
            ref: 'addUserWindow',
            selector: '#addUserWindow',
            xtype: 'adduserwindow'
        },
        {
            ref: 'addUserStepContainer',
            selector: '#addUserStepContainer',
            xtype: 'container'
        },
        {
            ref: 'addUserStep1Container',
            selector: '#addUserStep1Container',
            xtype: 'container'
        },
        {
            ref: 'addUserStep2Container',
            selector: '#addUserStep2Container',
            xtype: 'container'
        },
        {
            ref: 'addUserFormPanel',
            selector: '#addUserFormPanel',
            xtype: 'form'
        },
        {
            ref: 'addUserFormPanelSubscriberCombobox',
            selector: 'adduserwindow subscriberaccountcombobox',
            xtype: 'subscriberaccountcombobox'
        },
        {
            ref: 'addUserConfirmParentSubscriberFormPanel',
            selector: '#addUserConfirmParentSubscriberFormPanel',
            xtype: 'form'
        },
        {
            ref: 'addUserConfirmAccountPrivilegesDisplayField',
            selector: '#addUserConfirmAccountPrivilegesDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addUserConfirmContactDetailsDisplayField',
            selector: '#addUserConfirmContactDetailsDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addUserCancelButton',
            selector: 'adduserwindow genericcancelbutton',
            xtype: 'genericcancelbutton'
        },
        {
            ref: 'addUserBackButton',
            selector: 'adduserwindow genericbackbutton',
            xtype: 'genericbackbutton'
        },
        {
            ref: 'addUserNextButton',
            selector: 'adduserwindow genericnextbutton',
            xtype: 'genericnextbutton'
        }
    ],

    init: function(application) {
        this.optDebug = application.optDebug;
        this.debug = false || application.debug;
        this.optDebug('AddUserCtrl.arguments', arguments);

        this.control({
            '#addUserWindow': {
                hide: this.onAddUserWindowHide,
                show: this.onAddUserWindowShow
            },
            '#addUserWindow field:focusable': {
                specialkey: this.onAddUserWindowFieldSpecialKey
            },
            '#addUserStep1Container': {
                beforeactivate: this.onAddUserStep1ContainerBeforeActivate,
                beforeshow: this.onAddUserStep1ContainerActivate,
                activate: this.onAddUserStep1ContainerActivate
            },
            '#addUserStep2Container': {
                beforeactivate: this.onAddUserStep2ContainerBeforeActivate,
                activate: this.onAddUserStep2ContainerActivate,
                beforedeactivate: this.onAddUserStep2ContainerBeforeDeactivate
            },
            '#addUserFormPanel': {
                fieldvaliditychange: this.onAddUserStep1FormPanelFieldValidityChange
            },
            'adduserwindow genericcancelbutton': {
                click: this.onAddUserCancelButtonClick
            },
            'adduserwindow genericbackbutton': {
                click: this.onAddUserBackButtonClick
            },
            'adduserwindow genericnextbutton': {
                click: this.onAddUserNextButtonClick
            }
        });

        this.stepConfig = [{
            // step 1
            nextCallback: function(button, clickEvent, eOpts) {
                this.optDebug('step1NextCallback.arguments', arguments);
                this.validateStep1();
            }
        }, {
            // step 2
            nextCallback: function(button, clickEvent, eOpts) {
                this.optDebug('step2NextCallback.arguments', arguments);
                if (this.editUser) {
                    this.submitEditUser();
                } else {
                    this.submitAddUser();
                }
            }
        }];

        this.activeStep = 0;


        application.on({
            exDialogOpen: {
                fn: this.onExDialogOpen,
                scope: this
            },
            exDialogClose: {
                fn: this.onExDialogClose,
                scope: this
            }
        });
    },

    showWindow: function(parentSubscriber, userAccount) {
        this.optDebug('showWindow.arguments', arguments);

        this.resetFields();

        Ext.ClassManager.get('AdminInterface.view.window.AddUserWindow').create();

        this.optDebug('parentSubscriber:', parentSubscriber);

        var addUserWindow = this.getAddUserWindow();

        if (parentSubscriber) {
            //this.getAddUserFormPanelAncestorCombobox().setValue(parentSubscriber);

            if (userAccount) {
                var userAccountsStore = Ext.getStore('AdminInterface.store.UserAccounts');
                this.editUser = userAccountsStore.getById(userAccount);

                var contactDetails = this.editUser.get('contact_details');

                this.addUserFields = {
                    subscriber: parentSubscriber,
                    phone_numbers_office: '',
                    phone_numbers_mobile: '',
                    email_addresses_work: '',
                    first_name: contactDetails['first_name'],
                    last_name: contactDetails['last_name'],
                    job_title: contactDetails['job_title'],
                    administrator: this.editUser.get('administrator')
                };

                var emailAddresses = contactDetails['email_addresses'];
                for (var i = 0, j = emailAddresses.length; i < j; i++) {
                    if (emailAddresses[i].label == 'work') {
                        this.addUserFields.email_addresses_work = emailAddresses[i].email_address;
                    }
                }

                var phoneNumbers = contactDetails['phone_numbers'];
                for (var i = 0, j = phoneNumbers.length; i < j; i++) {
                    if (phoneNumbers[i].label == 'office') {
                        this.addUserFields.phone_numbers_office = phoneNumbers[i].phone_number;
                    }

                    if (phoneNumbers[i].label == 'mobile') {
                        this.addUserFields.phone_numbers_mobile = phoneNumbers[i].phone_number;
                    }
                }

                addUserWindow.setTitle('Edit User Account');

            } else {
                this.addUserFields = {
                    subscriber: parentSubscriber
                };

                addUserWindow.setTitle('Add User Account');
            }

        } else {
            addUserWindow.setTitle('Add User Account');
        }

        this.setActiveStep(0);

        this.getAddUserWindow().show();

    },

    hideWindow: function(dialogWindow) {
        this.optDebug('hideWindow.arguments: ', arguments);

        dialogWindow = dialogWindow || this.getAddUserWindow();

        dialogWindow.close();
    },

    resizeWindowHeight: function(container, heightExtra, reposition) {
        this.optDebug('resizeWindowHeight.arguments: ', arguments);

        reposition = reposition || false;

        var parentWindow = container.up('window');
        if (!parentWindow.rendered) {
            this.optDebug('parentWindow.rendered:' + parentWindow.rendered);
            return;
        } else {
            this.optDebug('parentWindow:', parentWindow);
        }

        var activeContainerWrapper = container.up('container');
        if (!activeContainerWrapper.rendered) {
            this.optDebug('activeContainerWrapper.rendered:' + activeContainerWrapper.rendered);
            return;
        } else {
            this.optDebug('activeContainerWrapper:', activeContainerWrapper);
        }

        var wrapperHeightDelta = activeContainerWrapper.getHeight();
        this.optDebug('wrapperHeightDelta:', wrapperHeightDelta);

        var activeViewContent = activeContainerWrapper.query('#' + container.id + ' > container');
        this.optDebug('activeViewContent:', activeViewContent);

        for (var i = 0, j = activeViewContent.length; i < j; i++) {

            if (!activeViewContent[i].rendered) {
                this.optDebug('activeViewContent[i].rendered:' + activeViewContent[i].rendered);
                return;
            }

            wrapperHeightDelta -= activeViewContent[i].getHeight();
        }

        if (heightExtra) {
            wrapperHeightDelta -= heightExtra;
        }

        parentWindow.setHeight(parentWindow.getHeight() - wrapperHeightDelta);

        if (reposition) {
            return parentWindow.center();
            /*
            var windowPosition = parentWindow.getPosition();

            windowPosition[1] += wrapperHeightDelta / 2;
            if (windowPosition[1] < 0) {
            windowPosition[1] = 0;
            }

            parentWindow.setPosition(windowPosition[0], windowPosition[1]);
            */
        }

    },

    resetFields: function() {
        this.optDebug('resetFields.arguments: ', arguments);

        // Reset step 1 saved state
        this.addUserFields = null;

        this.formattedContactDetails = null;

        this.editUser = null;
    },

    setActiveStep: function(stepIndex) {
        this.optDebug('setActiveStep.arguments', arguments);

        // Card layout of step view wrapper
        var stepContainerLayout = this.getAddUserStepContainer().getLayout();

        // Save wrapper active step state
        this.activeStep = stepIndex;

        stepContainerLayout.setActiveItem(stepIndex);


    },

    validateStep1: function() {
        this.optDebug('validateStep1.arguments: ', arguments);

        // Combobox for selecting ancestor entry
        var subscriberCombobox = this.getAddUserFormPanelSubscriberCombobox();

        // Contact details form panel
        var addUserFormPanel = this.getAddUserFormPanel();

        // ContactDetailsContainer component for details form panel
        var contactDetailsContainer = addUserFormPanel.down('contactdetailscontainer');

        // Check if ancestor is valid
        var validateFormPanel = subscriberCombobox.isValid();
        this.optDebug('subscriberCombobox.isValid: ', validateFormPanel);

        if (validateFormPanel) {
            // Enable ContactDetailsContainer component input fields
            contactDetailsContainer.fireEvent('exFieldsEnable');

            // Save state for step 1 view
            this.addUserFields = addUserFormPanel.getForm().getValues();
            this.addUserFields.subscriber = subscriberCombobox.getValue();

            // Check for invalid form fields in contact details form panel
            if (!addUserFormPanel.getForm().hasInvalidField()) {

                // Save state for step 1 view
                this.formattedContactDetails = contactDetailsContainer.getFormatted(this.addUserFields);
                this.optDebug('formattedContactDetails: ', this.formattedContactDetails);

                // Update `next` button to reflect successful validation
                return this.getAddUserNextButton().setDisabled(false);
            }

        } else {
            // Disable ContactDetailsContainer component input fields
            contactDetailsContainer.fireEvent('exFieldsDisable');
        }

        // Update `next` button to reflect validation failure
        this.getAddUserNextButton().setDisabled(true);

        target = this;
    },

    getApiInput: function() {
        this.optDebug('getApiInput.arguments: ', arguments);

        var contactProperties = ['email_addresses_work', 'first_name', 'labels', 'last_name', 'job_title', 'phone_numbers_mobile', 'phone_numbers_office'];
        var formPanelFields = this.addUserFields;
        var userRoleAdministrator = formPanelFields.administrator;
        var userContactDetails = {};

        if (userRoleAdministrator == "true" || userRoleAdministrator === true) {
            userRoleAdministrator = true;
            formPanelFields.labels = ['admin', 'admin_account'];
        } else {
            userRoleAdministrator = false;
            formPanelFields.labels = ['user', 'user_account'];
        }

        for (var i = contactProperties.length; i--; ) {
            if (formPanelFields[contactProperties[i]]) {
                userContactDetails[contactProperties[i]] = formPanelFields[contactProperties[i]];
            } else {
                userContactDetails[contactProperties[i]] = undefined;
            }
        }

        var apiEnvelope = {
            ancestor: formPanelFields.subscriber,
            administrator: userRoleAdministrator,
            contact_details: userContactDetails
        };

        if (this.editUser) {
            apiEnvelope.user_account = this.editUser.get('datastore_key_urlsafe');
        }

        return apiEnvelope;

    },

    submitAddUser: function() {
        this.optDebug('submitAddUser.arguments: ', arguments);

        var me = this;
        var addUserWindow = this.getAddUserWindow();
        var loadMask = addUserWindow.setLoading('Adding User ...');

        // Store for adding the new user upon successful submission
        var userAccountsStore = Ext.getStore('AdminInterface.store.UserAccounts');

        Remote.AdminCmd.add_or_edit_user_account(this.getApiInput(), function(result, request, success) {
            me.optDebug('AdminCmd.ADD_or_edit_user_account.result', result);
            me.optDebug('AdminCmd.ADD_or_edit_user_account.request', request);
            me.optDebug('AdminCmd.ADD_or_edit_user_account.success', success);

            addUserWindow.setLoading(false);

            if (success) {
                addUserWindow.close();
                userAccountsStore.add(result);
                me.submitEmailNotification(result.datastore_key_urlsafe, "subscriber_user_activation_email");
            }
        });


    },

    submitEditUser: function() {
        this.optDebug('submitEditUser.arguments: ', arguments);

        var me = this;
        var addUserWindow = this.getAddUserWindow();
        var loadMask = addUserWindow.setLoading('Submitting Changes ...');
        var userAccountsStore = Ext.getStore('AdminInterface.store.UserAccounts');

        Remote.AdminCmd.add_or_edit_user_account(this.getApiInput(), function(result, request, success) {
            me.optDebug('AdminCmd.add_or_EDIT_user_account.result', result);
            me.optDebug('AdminCmd.add_or_EDIT_user_account.request', request);
            me.optDebug('AdminCmd.add_or_EDIT_user_account.success', success);

            addUserWindow.setLoading(false);

            if (success) {
                //me.editUser.beginEdit();
                me.editUser.raw = result;
                me.editUser.set(result);
                me.editUser.postProcessExFields(true);
                me.editUser.commit();
                //me.editUser.endEdit();
                addUserWindow.close();
                //me.submitEmailNotification(result.datastore_key_urlsafe, "user_activation_email");
            }
        });

    },

    submitEmailNotification: function(userAccount, templateName) {
        this.optDebug('submitEmailNotification.arguments: ', arguments);

        var me = this;

        Remote.AdminCmd.send_email_notification({
            template: templateName,
            user_account: userAccount
        }, function(result, request, success) {
            me.optDebug('AdminCmd.send_email_notification.result', result);
            me.optDebug('AdminCmd.send_email_notification.request', request);
            me.optDebug('AdminCmd.send_email_notification.success', success);
        });
    },

    onAddUserWindowShow: function(window, eOpts) {
        this.optDebug('onAddUserWindowShow.arguments: ', arguments);
    },

    onAddUserWindowHide: function(window, eOpts) {
        this.optDebug('onAddUserWindowHide.arguments: ', arguments);

        //this.resetFields();


        //this.onSetActivePage(0);
    },

    onAddUserWindowFieldSpecialKey: function(field, keyEvent, eOpts) {
        this.optDebug('onAddUserWindowFieldSpecialKey.arguments:', arguments);

        if (keyEvent.getKey() !== keyEvent.ENTER) {
            return;
        } else if (field.isXType('subscriberaccountcombobox')) {
            return;
        }

        var nextButton = this.getAddUserNextButton();

        if (!nextButton.isDisabled()) {
            return nextButton.fireEvent('click');
        } else {
            switch(this.activeStep) {
                case this.steps[0]:
                this.validateStep1();
                break;
                default:
                return;
            }

            if (!nextButton.isDisabled()) {
                return nextButton.fireEvent('click');
            }
        }
    },

    onAddUserStep1ContainerBeforeActivate: function(container, eOpts) {
        this.optDebug('onAddUserStep1ContainerBeforeActivate.arguments: ', arguments);

        // User details form
        var addUserForm = this.getAddUserFormPanel().getForm();

        if (this.addUserFields) {
            // Restore saved state
            addUserForm.setValues(this.addUserFields);
        } else {
            // Reset form fields
            addUserForm.reset();
        }

        var addUserSubscriberCombobox = this.getAddUserFormPanelSubscriberCombobox();
        addUserSubscriberCombobox.setDisabled(!!(this.editUserAccount));

        // Proceed to standard validation
        this.validateStep1();

    },

    onAddUserStep1ContainerActivate: function(container, eOpts) {
        this.optDebug('onAddUserStep1ContainerActivate.arguments:', arguments);

        // Focus first input field in the step 1 view
        container.down('personnametextfield[name="first_name"]').focus(true, 200);

        // Resize window to content + 10px
        this.resizeWindowHeight(container, 10, true);

    },

    onAddUserStep1FormPanelFieldValidityChange: function(fieldAncestor, labelable, isValid, eOpts) {
        this.optDebug('onAddUserStep1FormPanelFieldValidityChange.arguments:', arguments);

        // Proceed to standard validation
        this.validateStep1();
    },

    onAddUserStep2ContainerBeforeActivate: function(container, eOpts) {
        this.optDebug('onAddUserStep2ContainerBeforeActivate.arguments: ', arguments);

        var subscriberAccountsStore = Ext.getStore('AdminInterface.store.SubscriberAccounts');

        // Parent subscriber form panel
        var confirmParentSubscriberFormPanel = this.getAddUserConfirmParentSubscriberFormPanel();

        this.optDebug('subscriber:', this.addUserFields.subscriber);

        // Parent subscriber record
        var subscriberRecordData = subscriberAccountsStore.getById(this.addUserFields.subscriber).getData(false);

        // Parent subscriber department name and organization id display fields
        var organizationIdDisplayField = confirmParentSubscriberFormPanel.down('displayfield[name="organization_id"]');
        var departmentNameDisplayField = confirmParentSubscriberFormPanel.down('displayfield[name="department_name"]');

        // Hide empty department name and organization id for parent subscriber
        organizationIdDisplayField.setVisible(!!(subscriberRecordData.organization_id));
        departmentNameDisplayField.setVisible(!!(subscriberRecordData.department_name));

        // Set parent subscriber form to store record values
        confirmParentSubscriberFormPanel.getForm().setValues(subscriberRecordData);

        // Set user privileges display field to saved state from step 1 view
        if (this.addUserFields.administrator == 'true' || this.addUserFields.administrator === true) {
            this.getAddUserConfirmAccountPrivilegesDisplayField().setValue("Administrator");
        } else {
            this.getAddUserConfirmAccountPrivilegesDisplayField().setValue("User");
        }

        // Set user contact details display field to saved state from step 1 view
        this.getAddUserConfirmContactDetailsDisplayField().setValue(this.formattedContactDetails);

        if (this.editUser) {
            this.getAddUserNextButton().setText("Submit Changes");
        } else {
            this.getAddUserNextButton().setText("Add User");
        }

    },

    onAddUserStep2ContainerActivate: function(container, eOpts) {
        this.optDebug('onAddUserStep2ContainerActivate.arguments:', arguments);

        // Focus GenericNextButton in the step 2 view
        this.getAddUserNextButton().focus(true, 200);

        // Resize window to content + 30px
        this.resizeWindowHeight(container, 30, true);

    },

    onAddUserStep2ContainerBeforeDeactivate: function(container, eOpts) {
        this.optDebug('onAddUserStep2ContainerBeforeDeactivate.arguments: ', arguments);

        // Reset GenericNextButton component label
        this.getAddUserNextButton().setText("Next");

    },

    onAddUserCancelButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onAddUserCancelButtonClick.arguments: ', arguments);

        var addUserWindow = button.up('window');

        this.hideWindow(addUserWindow);

    },

    onAddUserBackButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onAddUserBackButtonClick.arguments: ', arguments);

        var backConfigCallback = this.stepConfig[this.activeStep].backCallback;

        if (backConfigCallback) {
            backConfigCallback.call(this, button, clickEvent, eOpts);
        }

        if (this.activeStep > 0) {
            this.setActiveStep(this.activeStep - 1);
        }

    },

    onAddUserNextButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onAddUserNextButtonClick.arguments: ', arguments);

        var nextConfigCallback = this.stepConfig[this.activeStep].nextCallback;

        if (nextConfigCallback) {
            nextConfigCallback.call(this, button, clickEvent, eOpts);
        }

        if (this.activeStep < (this.stepConfig.length - 1)) {
            this.setActiveStep(this.activeStep + 1);
        }

    },

    onExDialogOpen: function(controllerId, parentSubscriber, userAccount, eOpts) {
        this.optDebug('onExDialogOpen.arguments: ', arguments);

        if (controllerId === this.id) {
            this.showWindow(parentSubscriber, userAccount);
        }

    },

    onExDialogClose: function(controllerId, eOpts) {
        this.optDebug('onExDialogClose.arguments: ', arguments);
    }

});
