/*
 * File: app/controller/ContactDetailsCtrl.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.controller.ContactDetailsCtrl', {
    extend: 'Ext.app.Controller',

    views: [
        'AdminInterface.view.container.ContactDetailsContainer'
    ],

    refs: [
        {
            ref: 'contactDetailsFirstNameTextField',
            selector: 'contactdetailscontainer personnametextfield[name="first_name"]',
            xtype: 'personnametextfield'
        },
        {
            ref: 'contactDetailsLastNameTextField',
            selector: 'contactdetailscontainer personnametextfield[name="last_name"]',
            xtype: 'personnametextfield'
        },
        {
            ref: 'contactDetailsJobTitleTextField',
            selector: 'contactdetailscontainer generictextfield[name="job_title"]',
            xtype: 'generictextfield'
        },
        {
            ref: 'contactDetailsWorkEmailAddressTextField',
            selector: 'contactdetailscontainer emailaddresstextfield',
            xtype: 'emailaddresstextfield'
        },
        {
            ref: 'contactDetailsMobilePhoneNumberTextField',
            selector: 'contactdetailscontainer phonenumbertextfield[name="phone_numbers_mobile"]',
            xtype: 'phonenumbertextfield'
        },
        {
            ref: 'contactDetailsOfficePhoneNumberTextField',
            selector: 'contactdetailscontainer phonenumbertextfield[name="phone_numbers_office"]',
            xtype: 'phonenumbertextfield'
        },
        {
            ref: 'contactDetailsWebAddressTextField',
            selector: 'webaddresstextfield',
            xtype: 'webaddresstextfield'
        }
    ],

    init: function(application) {
        this.optDebug = application.optDebug;
        this.debug = false || application.debug;
        this.optDebug('ContactDetailsCtrl.arguments', arguments);

        this.control({
            'contactdetailscontainer textfield': {
                change: this.onContactDetailsTextFieldChange,
                blur: this.onContactDetailsTextFieldBlur,
                specialkey: this.onContactDetailsTextFieldBlur
            }
        });

        this.vTypeSetup();

    },

    vTypeSetup: function() {
        this.optDebug('vTypeSetup.arguments:', arguments);

        // Validator Setup: "personname"
        (function() {
            Ext.apply(Ext.form.field.VTypes, {
                personname: function(val, field) { // vType validation function
                    var personname = /^[A-Za-z éüÜåäøöÅÄØÖ\-]+$/;
                    return personname.test(val);
                },
                personnameText: 'Namnet innehåller ogiltiga tecken. Namnfält kan enbart innehålla en begränsad teckenuppsättning, närmare bestämt a-ö, bindestreck och blanksteg.', // vType text property
                personnameMask: /[A-Za-z éüÜåäøöÅÄØÖ\-]/ // vType mask property
            });
        })();

        // Validator Setup: "jobtitle"
        (function() {
            Ext.apply(Ext.form.field.VTypes, {
                jobtitle: function(val, field) { // vType validation function
                    var jobtitle = /^[A-Za-z éüÜåäøöÅÄØÖ\-]+$/;
                    return jobtitle.test(val);
                },
                jobtitleText: 'Namnet innehåller ogiltiga tecken. Namnfält kan enbart innehålla en begränsad teckenuppsättning, närmare bestämt a-ö, bindestreck och blanksteg.', // vType text property
                jobtitleMask: /[A-Za-z åäöÅÄÖ\-]/ // vType mask property
            });
        })();

        // Validator setup: "personalid"
        (function() {
            Ext.apply(Ext.form.field.VTypes, {
                personalid: function(val, field) { // vType validation function
                    var personalid = /^[4-9][0-9](0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-2])\-[A-Z\d][\d]{3}$/;
                    return personalid.test(val);
                },
                personalidText: 'Ogiltigt personnr. Personnummer måste anges på formen YYMMDD-NNNN. Exempel: "860722-0332" – där "86" markerar år 1986, "07" juli månad, "22" dag 22 i juli månad och "0332" de sista fyra siffrorna i personnumret.', // vType text property
                personalidMask: /[\dA-Z\-]/i // vType mask property
            });
        })();

        // Validator Setup: "phonenumber"
        (function() {
            Ext.apply(Ext.form.field.VTypes, {
                phonenumber: function(val, field) { // vType validation function
                    var phonenumber = /^[\+\d][\d \-]{9,18}$/;
                    return phonenumber.test(val);
                },
                phonenumberText: 'Ogiltigt telefonnr. Fältet "telefonnummer" måste innehålla minst 8 tecken och kan enbart innehålla en begränsad teckenuppsättning. Accepterade tecken är + (som första tecken), 0-9, blanksteg och bindestreck. Exempel på giltiga telefonnummer är "+4672-522 32 33" och "08 920 20 25"', // vType text property
                phonenumberMask: /[\d \+\-]/ // vType mask property
            });
        })();

    },

    postProcessTextField: function(textField, fieldValue) {
        this.optDebug('postProcessTextField.arguments:', arguments);

        fieldValue = fieldValue || textField.getValue();

        var newValue = fieldValue;  // .trim();
        var regExpS = /\s{2}/;
        while (newValue.search(regExpS) !== -1) {
            newValue = newValue.replace(regExpS, ' ');
        }

        if (textField.isXType('emailaddresstextfield')) {
            newValue = fieldValue.toLowerCase();
        }

        return newValue;
    },

    onContactDetailsTextFieldChange: function(textfield, newValue, oldValue, eOpts) {
        this.optDebug('onContactDetailsTextFieldChange.arguments:', arguments);

        var container = textfield.up('contactdetailscontainer');
        var linkedContainers = container.getLinkedContainers();

        if (linkedContainers) {
            this.optDebug('onContactDetailsTextFieldChange.' + textfield.name + ' = ' + textfield.getValue());

            for (var i = linkedContainers.length; i--; ) {
                var targetField = linkedContainers[i].down('field[name=' + textfield.name + ']');
                targetField.setValue(newValue);
                this.optDebug('onContactDetailsTextFieldChange.' + targetField.name + ' = ' + targetField.getValue());
            }
        }
    },

    onContactDetailsTextFieldBlur: function(textField, blurEvent, eOpts) {
        this.optDebug('onContactDetailsTextFieldBlur.arguments:', arguments);

        var fieldValue = textField.getValue();

        if (fieldValue) {
            var newValue = this.postProcessTextField(textField, fieldValue);
            if (newValue !== fieldValue) {
                textField.setValue(newValue);
            }
        }

    },

    onContactDetailsTextFieldSpecialKey: function(textField, keyEvent, eOpts) {
        this.optDebug('onContactDetailsTextFieldSpecialKey.arguments:', arguments);

        if (keyEvent.getKey() !== keyEvent.ENTER) {
            return;
        } else {
            var fieldValue = textField.getValue();
            if (fieldValue) {
                var newValue = this.postProcessTextField(textField, fieldValue);
                if (newValue !== fieldValue) {
                    textField.setValue(newValue);
                }
            }
        }

    }

});
