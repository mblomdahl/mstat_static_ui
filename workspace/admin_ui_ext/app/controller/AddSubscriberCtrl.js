/*
 * File: app/controller/AddSubscriberCtrl.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.controller.AddSubscriberCtrl', {
    extend: 'Ext.app.Controller',
    alias: 'controller.addsubscriberctrl',

    models: [
        'UserAccount',
        'OrganizationAccount'
    ],
    stores: [
        'AdminInterface.store.SubscriberAccounts',
        'AdminInterface.store.UserAccounts'
    ],
    views: [
        'AdminInterface.view.window.AddSubscriberWindow'
    ],

    refs: [
        {
            ref: 'addSubscriberWindow',
            selector: '#addSubscriberWindow',
            xtype: 'addsubscriberwindow'
        },
        {
            ref: 'addSubscriberStepContainer',
            selector: '#addSubscriberStepContainer',
            xtype: 'container'
        },
        {
            ref: 'addSubscriberStep1Container',
            selector: '#addSubscriberStep1Container',
            xtype: 'container'
        },
        {
            ref: 'addSubscriberStep2Container',
            selector: '#addSubscriberStep2Container',
            xtype: 'container'
        },
        {
            ref: 'addSubscriberStep3Container',
            selector: '#addSubscriberStep3Container',
            xtype: 'container'
        },
        {
            ref: 'addSubscriberCompanyNameAndRegistrationNumberFormPanel',
            selector: '#addSubscriberCompanyNameAndRegistrationNumberFormPanel',
            xtype: 'form'
        },
        {
            ref: 'addSubscriberBillingAddressFormPanel',
            selector: '#addSubscriberBillingAddressFormPanel',
            xtype: 'form'
        },
        {
            ref: 'addSubscriberOfficeAddressAsBillingAddressCheckbox',
            selector: 'addsubscriberwindow genericcheckbox[name="office_address_as_billing_address"]',
            xtype: 'genericcheckbox'
        },
        {
            ref: 'addSubscriberOfficeAddressFormPanel',
            selector: '#addSubscriberOfficeAddressFormPanel',
            xtype: 'form'
        },
        {
            ref: 'addSubscriberBillingContactFormPanel',
            selector: '#addSubscriberBillingContactFormPanel',
            xtype: 'form'
        },
        {
            ref: 'addSubscriberTechnicalContactAsBillingContactCheckbox',
            selector: 'addsubscriberwindow genericcheckbox[name="technical_contact_as_billing_contact"]',
            xtype: 'genericcheckbox'
        },
        {
            ref: 'addSubscriberTechnicalContactFormPanel',
            selector: '#addSubscriberTechnicalContactFormPanel',
            xtype: 'form'
        },
        {
            ref: 'addSubscriberCompanyWebPageTextField',
            selector: '#addSubscriberCompanyWebPageTextField',
            xtype: 'webaddresstextfield'
        },
        {
            ref: 'addSubscriberAdminUserAsTechnicalContactCheckbox',
            selector: 'addsubscriberwindow genericcheckbox[name="admin_user_as_technical_contact"]',
            xtype: 'genericcheckbox'
        },
        {
            ref: 'addSubscriberAdminUserFormPanel',
            selector: '#addSubscriberAdminUserFormPanel',
            xtype: 'form'
        },
        {
            ref: 'addSubscriberConfirmCompanyNameAndRegistrationNumberFormPanel',
            selector: '#addSubscriberConfirmCompanyNameAndRegistrationNumberFormPanel',
            xtype: 'form'
        },
        {
            ref: 'addSubscriberConfirmBillingAddressDisplayField',
            selector: '#addSubscriberConfirmBillingAddressDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addSubscriberConfirmOfficeAddressDisplayField',
            selector: '#addSubscriberConfirmOfficeAddressDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addSubscriberConfirmBillingContactDisplayField',
            selector: '#addSubscriberConfirmBillingContactDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addSubscriberConfirmTechnicalContactDisplayField',
            selector: '#addSubscriberConfirmTechnicalContactDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addSubscriberConfirmCompanyWebPageDisplayField',
            selector: '#addSubscriberConfirmCompanyWebPageDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addSubscriberConfirmAdminUserDisplayField',
            selector: '#addSubscriberConfirmAdminUserDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addSubscriberCancelButton',
            selector: 'addsubscriberwindow genericcancelbutton',
            xtype: 'genericcancelbutton'
        },
        {
            ref: 'addSubscriberBackButton',
            selector: 'addsubscriberwindow genericbackbutton',
            xtype: 'genericbackbutton'
        },
        {
            ref: 'addSubscriberNextButton',
            selector: 'addsubscriberwindow genericnextbutton',
            xtype: 'genericnextbutton'
        }
    ],

    init: function(application) {
        this.optDebug = application.optDebug;
        this.debug = false || application.debug;
        this.optDebug('AddSubscriberCtrl.arguments', arguments);

        this.control({
            '#addSubscriberWindow': {
                hide: this.onAddSubscriberWindowHide,
                show: this.onAddSubscriberWindowShow
            },
            '#addSubscriberWindow textfield:focusable': {
                specialkey: this.onAddSubscriberWindowTextFieldSpecialKey
            },
            '#addSubscriberStep1Container': {
                beforeactivate: this.onAddSubscriberStep1ContainerBeforeActivate,
                activate: this.onAddSubscriberStep1ContainerActivate,
                beforeshow: this.onAddSubscriberStep1ContainerActivate
            },
            '#addSubscriberStep2Container': {
                beforeactivate: this.onAddSubscriberStep2ContainerBeforeActivate,
                activate: this.onAddSubscriberStep2ContainerActivate
            },
            '#addSubscriberStep3Container': {
                beforeactivate: this.onAddSubscriberStep3ContainerBeforeActivate,
                activate: this.onAddSubscriberStep3ContainerActivate,
                beforedeactivate: this.onAddSubscriberStep3ContainerBeforeDeactivate
            },
            '#addSubscriberCompanyNameAndRegistrationNumberFormPanel organizationnametextfield': {
                change: this.onAddSubscriberCompanyNameAndRegistrationNumberTextFieldChange
            },
            '#addSubscriberCompanyNameAndRegistrationNumberFormPanel, #addSubscriberBillingAddressFormPanel, #addSubscriberOfficeAddressFormPanel': {
                fieldvaliditychange: this.onAddSubscriberStep1FormPanelFieldValidityChange,
                validitychange: this.onAddSubscriberStep1FormPanelValidityChange
            },
            'addsubscriberwindow genericcheckbox[name="office_address_as_billing_address"]': {
                change: this.onAddSubscriberOfficeAddressAsBillingAddressCheckboxChange
            },
            '#addSubscriberBillingContactFormPanel, #addSubscriberTechnicalContactFormPanel, #addSubscriberAdminUserFormPanel': {
                fieldvaliditychange: this.onAddSubscriberStep2FormPanelFieldValidityChange
            },
            'addsubscriberwindow genericcheckbox[name="technical_contact_as_billing_contact"]': {
                change: this.onAddSubscriberTechnicalContactAsBillingContactCheckboxChange
            },
            'addsubscriberwindow genericcheckbox[name="admin_user_as_technical_contact"]': {
                change: this.onAddSubscriberAdminUserAsTechnicalContactCheckboxChange
            },
            'addsubscriberwindow genericcancelbutton': {
                click: this.onAddSubscriberCancelButtonClick
            },
            'addsubscriberwindow genericbackbutton': {
                click: this.onAddSubscriberBackButtonClick
            },
            'addsubscriberwindow genericnextbutton': {
                click: this.onAddSubscriberNextButtonClick
            }
        });

        this.stepConfig = [{
            // step 1
            nextCallback: function(button, clickEvent, eOpts) {
                this.optDebug('step1NextCallback.arguments', arguments);
                this.validateStep1();
            }
        }, {
            // step 2
            backCallback: function(button, clickEvent, eOpts) {
                this.validateStep2();
                //button.setDisabled(true);
            },
            nextCallback: function(button, clickEvent, eOpts) {
                this.optDebug('step2NextCallback.arguments', arguments);
                this.validateStep2();
            }
        }, {
            // step 3
            nextCallback: function(button, clickEvent, eOpts) {
                this.optDebug('step3NextCallback.arguments', arguments);
                this.submitAddSubscriber(button);
            }
        }];

        this.activeStep = 0;


        application.on({
            exDialogOpen: {
                fn: this.onExDialogOpen,
                scope: this
            },
            exDialogClose: {
                fn: this.onExDialogClose,
                scope: this
            }
        });
    },

    showWindow: function() {
        this.optDebug('showWindow.arguments: ', arguments);

        // Reset saved state for the window views
        this.resetFields();

        Ext.ClassManager.get('AdminInterface.view.window.AddSubscriberWindow').create().show();

        this.setActiveStep(0);
    },

    hideWindow: function(dialogWindow) {
        this.optDebug('hideWindow.arguments: ', arguments);

        dialogWindow = dialogWindow || this.getAddSubscriberWindow();

        dialogWindow.close();
    },

    resizeWindowHeight: function(container, heightExtra, reposition) {
        this.optDebug('resizeWindowHeight.arguments: ', arguments);

        reposition = reposition || false;

        var parentWindow = container.up('window');
        if (!parentWindow.rendered) {
            this.optDebug('parentWindow.rendered:' + parentWindow.rendered);
            return;
        } else {
            this.optDebug('parentWindow:', parentWindow);
        }

        var activeContainerWrapper = container.up('container');
        if (!activeContainerWrapper.rendered) {
            this.optDebug('activeContainerWrapper.rendered:' + activeContainerWrapper.rendered);
            return;
        } else {
            this.optDebug('activeContainerWrapper:', activeContainerWrapper);
        }

        var wrapperHeightDelta = activeContainerWrapper.getHeight();
        this.optDebug('wrapperHeightDelta:', wrapperHeightDelta);

        var activeViewContent = activeContainerWrapper.query('#' + container.id + ' > container');
        this.optDebug('activeViewContent:', activeViewContent);

        for (var i = 0, j = activeViewContent.length; i < j; i++) {

            if (!activeViewContent[i].rendered) {
                this.optDebug('activeViewContent[i].rendered:' + activeViewContent[i].rendered);
                return;
            }

            wrapperHeightDelta -= activeViewContent[i].getHeight();
        }

        if (heightExtra) {
            wrapperHeightDelta -= heightExtra;
        }

        parentWindow.setHeight(parentWindow.getHeight() - wrapperHeightDelta);

        if (reposition) {
            return parentWindow.center();
            /*
            var windowPosition = parentWindow.getPosition();

            windowPosition[1] += wrapperHeightDelta / 2;
            if (windowPosition[1] < 0) {
            windowPosition[1] = 0;
            }

            parentWindow.setPosition(windowPosition[0], windowPosition[1]);
            */
        }

    },

    setActiveStep: function(stepIndex) {
        this.optDebug('setActiveStep.arguments', arguments);

        // Card layout of step view wrapper
        var stepContainerLayout = this.getAddSubscriberStepContainer().getLayout();

        // Save wrapper active step state
        this.activeStep = stepIndex;

        stepContainerLayout.setActiveItem(stepIndex);



    },

    resetFields: function() {
        this.optDebug('resetFields.arguments', arguments);

        // Reset step 1 saved state
        this.companyNameAndRegistrationNumberFields = null;

        this.billingAddressFields = null;
        this.formattedBillingAddress = null;

        this.officeAddressAsBillingAddress = true;
        this.officeAddressFields = null;
        this.formattedOfficeAddress = null;

        // Reset step 2 saved state
        this.billingContactFields = null;
        this.formattedBillingContact = null;

        this.technicalContactAsBillingContact = true;
        this.technicalContactFields = null;
        this.formattedTechnicalContact = null;

        this.companyWebPage = null;

        this.adminUserAsTechnicalContact = true;
        this.adminUserFields = null;
        this.formattedAdminUser = null;

    },

    setFormFieldMirroring: function(srcFormPanel, targetFormPanel) {
        this.optDebug('setFormFieldMirroring.arguments:', arguments);

        // Locate a descendant PostalAddressContainer or ContactDetailsContainer
        var targetContainer = targetFormPanel.down('postaladdresscontainer, contactdetailscontainer');
        var srcContainer = srcFormPanel.down('postaladdresscontainer, contactdetailscontainer');
        var srcValues = Ext.Object.merge(srcFormPanel.getForm().getValues(), srcContainer.getValues());

        // Disable descendant form fields of the container
        targetContainer.fireEvent('exFieldsDisable');

        // Set target form initial state to source form values

        targetFormPanel.getForm().setValues(srcValues);

        // Set up mirroring of source inputs to target inputs
        srcContainer.bindLinkedContainer(targetContainer);
    },

    unsetFormFieldMirroring: function(srcFormPanel, targetFormPanel) {
        this.optDebug('unsetFormFieldMirroring.arguments:', arguments);

        // Locate a descendant PostalAddressContainer or ContactDetailsContainer
        var targetContainer = targetFormPanel.down('postaladdresscontainer, contactdetailscontainer');
        var srcContainer = srcFormPanel.down('postaladdresscontainer, contactdetailscontainer');

        // Unset mirroring of source inputs to target inputs
        srcContainer.releaseLinkedContainer();

        // Reset target form fields
        targetFormPanel.getForm().reset();

        // Enable descendant form fields of the container
        targetContainer.fireEvent('exFieldsEnable');

    },

    validateStep1: function() {
        this.optDebug('validateStep1.arguments', arguments);

        // Office address form panel
        var officeAddressFormPanel = this.getAddSubscriberOfficeAddressFormPanel();

        // Billing address form panel
        var billingAddressFormPanel = this.getAddSubscriberBillingAddressFormPanel();

        // Company name and registration number form panel
        var nameAndRegistrationNumberFormPanel = this.getAddSubscriberCompanyNameAndRegistrationNumberFormPanel();

        // Required form panels
        var validateFormPanels = [
        nameAndRegistrationNumberFormPanel,
        billingAddressFormPanel
        ];

        // Check if field values are mirrored from `billing` to `office` address
        if (!this.officeAddressAsBillingAddress) {
            validateFormPanels.push(officeAddressFormPanel);
        }

        // Check for invalid form fields
        var invalidFields = false;
        for (var i = 0, j = validateFormPanels.length; i < j; i++ ) {
            if (validateFormPanels[i].getForm().hasInvalidField()) {
                invalidFields = true;
                break;
            }
        }

        // Save state for step 1 view
        if (!invalidFields) {

            this.companyNameAndRegistrationNumberFields = nameAndRegistrationNumberFormPanel.getForm().getValues();

            var addressContainer = billingAddressFormPanel.down('postaladdresscontainer');
            this.billingAddressFields = billingAddressFormPanel.getForm().getValues();
            this.formattedBillingAddress = addressContainer.getFormatted(this.billingAddressFields);

            if (this.officeAddressAsBillingAddress) {
                this.officeAddressFields = this.billingAddressFields;
                this.formattedOfficeAddress = this.formattedBillingAddress;
            } else {
                this.officeAddressFields = officeAddressFormPanel.getForm().getValues();
                this.formattedOfficeAddress = addressContainer.getFormatted(this.officeAddressFields);
            }
        }

        // Update `next` button to reflect validation outcome
        this.getAddSubscriberNextButton().setDisabled(invalidFields);

    },

    validateStep2: function() {
        this.optDebug('validateStep2.arguments', arguments);

        // Technical contact form panel
        var technicalContactFormPanel = this.getAddSubscriberTechnicalContactFormPanel();

        // Admin user form panel
        var adminUserFormPanel = this.getAddSubscriberAdminUserFormPanel();

        // Billing contact form panel
        var billingContactFormPanel = this.getAddSubscriberBillingContactFormPanel();

        // Check for invalid form fields
        var invalidFields = false;

        if (billingContactFormPanel.getForm().hasInvalidField()) {
            invalidFields = true;
        }

        // Update internal state
        this.billingContactFields = billingContactFormPanel.getForm().getValues();

        if (this.technicalContactAsBillingContact) {
            // Avoid dual validation of mirrored form fields
            this.technicalContactFields = this.billingContactFields;
        } else {
            this.technicalContactFields = technicalContactFormPanel.getForm().getValues();
            if (!invalidFields && technicalContactFormPanel.getForm().hasInvalidField()) {
                invalidFields = true;
            }
        }

        if (this.adminUserAsTechnicalContact) {
            // Avoid dual validation of mirrored form fields
            this.adminUserFields = this.technicalContactFields;
        } else {
            this.adminUserFields = adminUserFormPanel.getForm().getValues();
            if (!invalidFields && adminUserFormPanel.getForm().hasInvalidField()) {
                invalidFields = true;
            }
        }

        // Validate company web page input
        var companyWebPageTextField = this.getAddSubscriberCompanyWebPageTextField();
        if (companyWebPageTextField.getValue() && companyWebPageTextField.validate()) {
            this.companyWebPage = companyWebPageTextField.getValue();
        } else {
            this.companyWebPage = null;
        }

        // Save state for step 2 view
        if (!invalidFields) {
            var contactContainer = billingContactFormPanel.down('contactdetailscontainer');
            this.formattedBillingContact = contactContainer.getFormatted(this.billingContactFields);

            this.optDebug('formattedBillingContact: ', this.formattedBillingContact);

            if (this.technicalContactAsBillingContact) {
                this.formattedTechnicalContact = this.formattedBillingContact;
            } else {
                this.formattedTechnicalContact = contactContainer.getFormatted(this.technicalContactFields);
            }

            this.optDebug('formattedTechnicalContact: ', this.formattedTechnicalContact);

            if (this.adminUserAsTechnicalContact) {
                this.formattedAdminUser = this.formattedTechnicalContact;
            } else {
                this.formattedAdminUser = contactContainer.getFormatted(this.adminUserFields);
            }

            this.optDebug('formattedAdminUser: ', this.formattedAdminUser);
        }

        // Update `next` button to reflect validation outcome
        this.getAddSubscriberNextButton().setDisabled(invalidFields);

    },

    getApiInput: function() {
        this.optDebug('getApiInput.arguments', arguments);

        var postalAddresses, contactDetails, webAddresses;

        var adminUser, technicalContact, billingContact;

        if (this.officeAddressAsBillingAddress) {
            var billingAndOfficeAddress = this.billingAddressFields;
            billingAndOfficeAddress.labels = [
            'office',
            'office_address',
            'billing',
            'billing_address'
            ];
            postalAddresses = [billingAndOfficeAddress];

        } else {
            var billingAddress = this.billingAddressFields,
                officeAddress = this.officeAddressFields;

            billingAddress.labels = ['billing', 'billing_address'];
            officeAddress.labels = ['office', 'office_address'];

            postalAddresses = [billingAddress, officeAddress];
        }

        billingContact = this.billingContactFields;
        billingContact.labels = ['billing', 'billing_contact'];

        if (this.technicalContactAsBillingContact) {
            billingContact.labels.push('technical_contact');
            billingContact.labels.push('tech');
        } else {
            technicalContact = this.technicalContactFields;
            technicalContact.labels = ['tech', 'technical_contact'];
        }

        if (this.adminUserAsTechnicalContact && this.technicalContactAsBillingContact) {
            billingContact.labels.push('admin_account');
            billingContact.labels.push('admin');
        } else {
            if (this.adminUserAsTechnicalContact) {
                technicalContact.labels.push('admin_account');
                technicalContact.labels.push('admin');
            } else {
                adminUser = this.adminUserFields;
                adminUser.labels = ['admin', 'admin_account'];
            }
        }

        contactDetails = [billingContact];

        if (technicalContact) {
            contactDetails.push(technicalContact);
        }

        if (adminUser) {
            contactDetails.push(adminUser);
        }

        if (this.companyWebPage) {
            webAddresses = [{
                labels: ['company_main', 'company_main_url', 'web_page', 'url'],
                url: this.companyWebPage
            }];
        }

        return {
            company_identity_details: this.companyNameAndRegistrationNumberFields,
            postal_addresses: postalAddresses,
            contact_details: contactDetails,
            web_addresses: webAddresses
        };

    },

    submitAddSubscriber: function(button) {
        this.optDebug('submitAddSubscriber.arguments', arguments);

        var me = this;
        var addSubscriberWindow = this.getAddSubscriberWindow();
        var loadMask = addSubscriberWindow.setLoading('Adding Subscriber ...');

        // Store for adding the new subscriber upon successful submission
        var subscriberAccountsStore = Ext.getStore('AdminInterface.store.SubscriberAccounts');

        // Store for adding the new subscriber's users
        var userAccountsStore = Ext.getStore('AdminInterface.store.UserAccounts');

        function addSubscriberCallback(buttonId, text, opt) {

            Remote.AdminCmd.add_or_edit_subscriber_account(me.getApiInput(), function(result, request, success) {
                me.optDebug('AdminCmd.ADD_or_edit_subscriber_account.result', result);
                me.optDebug('AdminCmd.ADD_or_edit_subscriber_account.request', request);
                me.optDebug('AdminCmd.ADD_or_edit_subscriber_account.success', success);

                addSubscriberWindow.setLoading(false);

                if (success) {
                    // Close the dialog window and submit an email notification request for each user created
                    addSubscriberWindow.close();
                    subscriberAccountsStore.add(result.subscriber_account);
                    for (var i = 0, j = result.user_accounts.length; i < j; i++) {
                        userAccountsStore.add(result.user_accounts[i]);
                        me.submitEmailNotification(result.user_accounts[i].datastore_key_urlsafe, "subscriber_user_activation_email");
                    }
                    if (buttonId == 'yes') {
                        me.getController('AddSubscriptionCtrl').submitDemoSubscription(result.subscriber_account.datastore_key_urlsafe, function(success) {
                            me.optDebug('callback: ' + success);
                        });
                    } else if (buttonId == 'cancel' || buttonId == 'no') {
                        me.optDebug('demo subscription creation cancelled');
                    }
                }
            });
        }

        Ext.Msg.show({
            title: 'Demo Subcription',
            msg: 'Include a 3 day demo subscription upon account creation?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: addSubscriberCallback
        });
    },

    submitEmailNotification: function(userAccount, templateName) {
        this.optDebug('submitEmailNotification.arguments', arguments);

        var me = this;

        // Submit an email notification request
        Remote.AdminCmd.send_email_notification({
            template: templateName,
            user_account: userAccount
        }, function(result, request, success) {
            me.optDebug('AdminCmd.send_email_notification.result', result);
            me.optDebug('AdminCmd.send_email_notification.request', request);
            me.optDebug('AdminCmd.send_email_notification.success', success);
        });
    },

    onAddSubscriberWindowShow: function(dialogWindow, eOpts) {
        this.optDebug('onAddSubscriberWindowShow.arguments: ', arguments);

        //return true;
    },

    onAddSubscriberWindowHide: function(dialogWindow, eOpts) {
        this.optDebug('onAddSubscriberWindowHide.arguments: ', arguments);

        //this.resetFields();


        //this.onSetActivePage(0);
    },

    onAddSubscriberWindowTextFieldSpecialKey: function(textField, keyEvent, eOpts) {
        this.optDebug('onAddSubscriberWindowTextFieldSpecialKey.arguments: ', arguments);

        if (keyEvent.getKey() !== keyEvent.ENTER) {
            return;
        }

        var nextButton = this.getAddSubscriberNextButton();

        if (!nextButton.isDisabled()) {
            // Trigger a click event on enabled `Next` button
            return nextButton.fireEvent('click');
        } else {
            // Force form validation, attempting to enable the `Next` button
            switch(this.activeStep) {
                case this.steps[0]: this.validateStep1(); break;
                case this.steps[1]: this.validateStep2(); break;
                default: return;
            }

            if (!nextButton.isDisabled()) {
                return nextButton.fireEvent('click');
            }
        }
    },

    onAddSubscriberStep1ContainerBeforeActivate: function(container, eOpts) {
        this.optDebug('onAddSubscriberStep1ContainerBeforeActivate.arguments: ', arguments);

        // Basic form components for step 1 view
        var companyNameAndRegistrationNumberForm = this.getAddSubscriberCompanyNameAndRegistrationNumberFormPanel().getForm();
        var billingAddressForm = this.getAddSubscriberBillingAddressFormPanel().getForm();
        var officeAddressForm = this.getAddSubscriberOfficeAddressFormPanel().getForm();

        if (this.companyNameAndRegistrationNumberFields) {
            // Restore saved state for company details
            companyNameAndRegistrationNumberForm.setValues(this.companyNameAndRegistrationNumberFields);
        } else {
            // Reset form fields for company details
            companyNameAndRegistrationNumberForm.reset();
        }


        if (this.billingAddressFields) {
            // Restore saved state for billing address
            billingAddressForm.setValues(this.billingAddressFields);
        } else {
            // Reset form fields for billing address
            billingAddressForm.reset();
        }

        if (this.officeAddressFields && !this.officeAddressAsBillingAddress) {
            // Restore saved state for office address
            officeAddressForm.setValues(this.officeAddressFields);
        } else {
            if (this.billingAddressFields && this.officeAddressAsBillingAddress) {
                // Restore saved state for billing address as office address
                officeAddressForm.setValues(this.billingAddressFields);
            } else {
                // Reset form fields for office address
                officeAddressForm.reset();
            }
        }

        // Restore saved state for `office address as billing address` checkbox
        var officeAddressAsBillingAddressCheckbox = this.getAddSubscriberOfficeAddressAsBillingAddressCheckbox();
        officeAddressAsBillingAddressCheckbox.setValue(this.officeAddressAsBillingAddress);

        this.getAddSubscriberNextButton().setText('Next');

        this.getAddSubscriberBackButton().setDisabled(true);

        // Proceed to standard validation
        this.validateStep1();

    },

    onAddSubscriberStep1ContainerActivate: function(container, eOpts) {
        this.optDebug('onAddSubscriberStep1ContainerActivate.arguments: ', arguments);

        // Focus first input field in the step 1 view
        container.down('organizationnametextfield[name="organization_name"]').focus(true, 250);

        // Resize window to content + 10px
        this.resizeWindowHeight(container, 10, true);
    },

    onAddSubscriberStep1FormPanelFieldValidityChange: function(fieldAncestor, labelable, isValid, eOpts) {
        this.optDebug('onAddSubscriberStep1FormPanelFieldValidityChange.arguments: ', arguments);

        // Proceed to standard validation
        this.validateStep1();

    },

    onAddSubscriberStep1FormPanelValidityChange: function(formPanel, valid, eOpts) {
        this.optDebug('onAddSubscriberStep1FormPanelValidityChange.arguments: ', arguments);

        // Proceed to standard validation
        this.validateStep1();
    },

    onAddSubscriberCompanyNameAndRegistrationNumberTextFieldChange: function(textField, newValue, oldValue, eOpts) {
        this.optDebug('onAddSubscriberCompanyNameAndRegistrationNumberTextFieldChange.arguments: ', arguments);

        var targetField;

        if (textField.name === 'organization_name') {
            // Source input field is the company name
            targetField = textField.up('window').query('#addSubscriberBillingAddressFormPanel generictextfield[name="recipient_lines_1"]')[0];
        } else {
            // Source input field is the department name
            targetField = textField.up('window').query('#addSubscriberBillingAddressFormPanel generictextfield[name="recipient_lines_2"]')[0];
        }

        if (!targetField.getValue() || targetField.getValue() == oldValue) {
            targetField.setValue(newValue);
        }

    },

    onAddSubscriberOfficeAddressAsBillingAddressCheckboxChange: function(checkboxField, newValue, oldValue, eOpts) {
        this.optDebug('onAddSubscriberOfficeAddressAsBillingAddressCheckboxChange.arguments: ', arguments);

        // Office address form panel
        var officeAddressFormPanel = this.getAddSubscriberOfficeAddressFormPanel();

        // Billing address form panel
        var billingAddressFormPanel = this.getAddSubscriberBillingAddressFormPanel();

        // Update internal state
        this.officeAddressAsBillingAddress = newValue;

        if (newValue) {
            // Set up mirroring of billing address inputs to office address inputs
            this.setFormFieldMirroring(billingAddressFormPanel, officeAddressFormPanel);
        } else {
            // Unset mirroring of billing address inputs to office address inputs
            this.unsetFormFieldMirroring(billingAddressFormPanel, officeAddressFormPanel);
        }

        // Proceed to standard validation
        return this.validateStep1();
    },

    onAddSubscriberStep2ContainerBeforeActivate: function(container, eOpts) {
        this.optDebug('onAddSubscriberStep2ContainerBeforeActivate.arguments: ', arguments);

        // Basic form components for step 2 view
        var billingContactForm = this.getAddSubscriberBillingContactFormPanel().getForm();
        var technicalContactForm = this.getAddSubscriberTechnicalContactFormPanel().getForm();
        var adminUserForm = this.getAddSubscriberAdminUserFormPanel().getForm();

        // Checkbox components for mirroring contact fields
        var technicalContactAsBillingContactCheckbox = this.getAddSubscriberTechnicalContactAsBillingContactCheckbox();
        var adminUserAsTechnicalContactCheckbox = this.getAddSubscriberAdminUserAsTechnicalContactCheckbox();

        // Company web page input field
        var companyWebPageTextField = this.getAddSubscriberCompanyWebPageTextField();

        if (this.billingContactFields) {
            // Restore saved state for billing contact
            billingContactForm.setValues(this.billingContactFields);
            this.optDebug('state 1a');
        } else {
            // Reset form fields for billing contact
            billingContactForm.reset();
            this.optDebug('state 1b');
        }

        if (this.technicalContactFields && !this.technicalContactAsBillingContact) {
            // Restore saved state for technical contact
            technicalContactForm.setValues(this.technicalContactFields);
            this.optDebug('state 2');
        } else {
            if (this.technicalContactAsBillingContact && this.billingContactFields) {
                // Restore saved state for technical contact as billing contact
                technicalContactForm.setValues(this.billingContactFields);
                this.optDebug('state 2a');
            } else {
                // Reset form fields for technical contact
                technicalContactForm.reset();
                this.optDebug('state 2b');
            }
        }

        if (this.adminUserFields && !this.adminUserAsTechnicalContact) {
            // Restore saved state for billing contact
            adminUserForm.setValues(this.adminUserFields);
            this.optDebug('state 3');
        } else {
            if (this.adminUserAsTechnicalContact && this.technicalContactFields) {
                // Restore saved state for admin user as technical contact
                adminUserForm.setValues(this.technicalContactFields);
                this.optDebug('state 3a');
            } else {
                // Reset form fields for admin user
                adminUserForm.reset();
                this.optDebug('state 3b');
            }
        }


        if (this.companyWebPage) {
            // Restore saved state for company web page
            companyWebPageTextField.setValue(this.companyWebPage);
        } else {
            // Reset form field for company web page
            companyWebPageTextField.reset();
        }

        // Restore saved state for `technical contact as billing contact` checkbox
        technicalContactAsBillingContactCheckbox.setValue(this.technicalContactAsBillingContact);

        // Restore saved state for `admin user as technical contact` checkbox
        adminUserAsTechnicalContactCheckbox.setValue(this.adminUserAsTechnicalContact);

        this.getAddSubscriberBackButton().setDisabled(false);

        // Proceed to standard validation
        this.validateStep2();


    },

    onAddSubscriberStep2ContainerActivate: function(container, eOpts) {
        this.optDebug('onAddSubscriberStep2ContainerActivate.arguments:', arguments);

        // Focus first input field in the step 2 view
        container.down('personnametextfield[name="first_name"]:focusable').focus(true, 200);

        // Resize window to content + 10px
        this.resizeWindowHeight(container, 10, true);
    },

    onAddSubscriberStep2FormPanelFieldValidityChange: function(fieldAncestor, labelable, isValid, eOpts) {
        this.optDebug('onAddSubscriberStep2FormPanelFieldValidityChange.arguments:', arguments);

        // Proceed to standard validation
        this.validateStep2();

    },

    onAddSubscriberTechnicalContactAsBillingContactCheckboxChange: function(checkboxField, newValue, oldValue, eOpts) {
        this.optDebug('onAddSubscriberTechnicalContactAsBillingContactCheckboxChange.arguments:', arguments);

        // Billing contact form panel
        var billingContactFormPanel = this.getAddSubscriberBillingContactFormPanel();

        // Technical contact form panel
        var technicalContactFormPanel = this.getAddSubscriberTechnicalContactFormPanel();

        // Save state for the `technical contact as billing contact` checkbox
        this.technicalContactAsBillingContact = newValue;

        if (newValue) {
            // Set up mirroring of billing contact inputs to technical contact inputs
            this.setFormFieldMirroring(billingContactFormPanel, technicalContactFormPanel);
        } else {
            // Unset mirroring of billing contact inputs to technical contact inputs
            this.unsetFormFieldMirroring(billingContactFormPanel, technicalContactFormPanel);
        }

        // Proceed to standard validation
        return this.validateStep2();

    },

    onAddSubscriberAdminUserAsTechnicalContactCheckboxChange: function(checkboxField, newValue, oldValue, eOpts) {
        this.optDebug('onAddSubscriberAdminUserAsTechnicalContactCheckboxChange.arguments:', arguments);

        // Technical contact form panel
        var technicalContactFormPanel = this.getAddSubscriberTechnicalContactFormPanel();

        // Admin user form panel
        var adminUserFormPanel = this.getAddSubscriberAdminUserFormPanel();

        // Save state for the `admin user as technical contact` checkbox
        this.adminUserAsTechnicalContact = newValue;

        if (newValue) {
            // Set up mirroring of technical contact inputs to admin user inputs
            this.setFormFieldMirroring(technicalContactFormPanel, adminUserFormPanel);
        } else {
            // Unset mirroring of technical contact inputs to admin user inputs
            this.unsetFormFieldMirroring(technicalContactFormPanel, adminUserFormPanel);
        }

        // Proceed to standard validation
        return this.validateStep2();

    },

    onAddSubscriberStep3ContainerBeforeActivate: function(container, eOpts) {
        this.optDebug('onAddSubscriberStep3ContainerBeforeActivate.arguments:', arguments);

        // Subscriber company identity form panel
        var confirmCompanyNameAndRegistrationNumberFormPanel = this.getAddSubscriberConfirmCompanyNameAndRegistrationNumberFormPanel();

        // Department name and organization id display fields
        var organizationIdDisplayField = confirmCompanyNameAndRegistrationNumberFormPanel.down('displayfield[name="organization_id"]');
        var departmentNameDisplayField = confirmCompanyNameAndRegistrationNumberFormPanel.down('displayfield[name="department_name"]');

        // Hide empty department name and organization id
        organizationIdDisplayField.setVisible(!!(this.companyNameAndRegistrationNumberFields.organization_id));
        departmentNameDisplayField.setVisible(!!(this.companyNameAndRegistrationNumberFields.department_name));

        // Set subscriber company identity form to saved state from step 1 view
        confirmCompanyNameAndRegistrationNumberFormPanel.getForm().setValues(this.companyNameAndRegistrationNumberFields);

        // Set company address display fields to saved state from step 1 view
        var billingAddressDisplayField = this.getAddSubscriberConfirmBillingAddressDisplayField();
        var officeAddressDisplayField = this.getAddSubscriberConfirmOfficeAddressDisplayField();

        billingAddressDisplayField.setValue(this.formattedBillingAddress);
        if (this.formattedBillingAddress === this.formattedOfficeAddress) {
            // Display office and billing address under a single label
            billingAddressDisplayField.setFieldLabel('Billing/Office Address');
            officeAddressDisplayField.setVisible(false);
        } else {
            // Display office and billing address as separate labels
            billingAddressDisplayField.setFieldLabel('Billing Address');
            officeAddressDisplayField.setValue(this.formattedOfficeAddress);
            officeAddressDisplayField.setVisible(true);
        }

        // Set billing and technical contact display fields to saved state from step 2 view
        var billingContactDisplayField = this.getAddSubscriberConfirmBillingContactDisplayField();
        var technicalContactDisplayField = this.getAddSubscriberConfirmTechnicalContactDisplayField();

        billingContactDisplayField.setValue(this.formattedBillingContact);
        if (this.formattedBillingContact === this.formattedTechnicalContact) {
            // Display billing and technical contact under a single label
            billingContactDisplayField.setFieldLabel('Billing/Technical Contact');
            technicalContactDisplayField.setVisible(false);
        } else {
            // Display billing and technical contact as separate labels
            billingContactDisplayField.setFieldLabel('Billing Contact');
            technicalContactDisplayField.setValue(this.formattedTechnicalContact);
            technicalContactDisplayField.setVisible(true);
        }

        //this.getAddSubscriberConfirmBillingContactDisplayField().setValue(this.formattedBillingContact);
        //this.getAddSubscriberConfirmTechnicalContactDisplayField().setValue(this.formattedTechnicalContact);

        // Hide empty company web page and set display field to saved state from step 2 view
        this.getAddSubscriberConfirmCompanyWebPageDisplayField().setVisible(!!(this.companyWebPage));
        this.getAddSubscriberConfirmCompanyWebPageDisplayField().setValue(this.companyWebPage);

        // Set admin user display field to saved state from step 2 view
        this.getAddSubscriberConfirmAdminUserDisplayField().setValue(this.formattedAdminUser);

        // Update GenericNextButton component label
        this.getAddSubscriberNextButton().setText("Add Subscriber");
    },

    onAddSubscriberStep3ContainerActivate: function(container, eOpts) {
        this.optDebug('onAddSubscriberStep3ContainerActivate.arguments:', arguments);

        // Focus GenericNextButton in the step 3 view
        this.getAddSubscriberNextButton().focus(true, 200);

        // Resize window to content + 40px
        this.resizeWindowHeight(container, 40, true);

    },

    onAddSubscriberStep3ContainerBeforeDeactivate: function(container, eOpts) {
        this.optDebug('onAddSubscriberStep3ContainerBeforeDeactivate.arguments:', arguments);

        // Reset GenericNextButton component label
        this.getAddSubscriberNextButton().setText("Next");

    },

    onAddSubscriberCancelButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onAddSubscriberCancelButtonClick.arguments:', arguments);

        var addSubscriberWindow = button.up('window');

        this.hideWindow(addSubscriberWindow);

    },

    onAddSubscriberBackButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onAddSubscriberBackButtonClick.arguments:', arguments);

        var backConfigCallback = this.stepConfig[this.activeStep].backCallback;

        if (backConfigCallback) {
            backConfigCallback.call(this, button, clickEvent, eOpts);
        }

        if (this.activeStep > 0) {
            this.setActiveStep(this.activeStep - 1);
        }

    },

    onAddSubscriberNextButtonClick: function(button, clickEvent, eOpts) {
        this.optDebug('onAddSubscriberNextButtonClick.arguments:', arguments);

        var nextConfigCallback = this.stepConfig[this.activeStep].nextCallback;

        if (nextConfigCallback) {
            nextConfigCallback.call(this, button, clickEvent, eOpts);
        }

        if (this.activeStep < (this.stepConfig.length - 1)) {
            this.setActiveStep(this.activeStep + 1);
        }

    },

    onExDialogOpen: function(controllerId, eOpts) {
        this.optDebug('onExDialogOpen.arguments: ', arguments);

        if (controllerId === this.id) {
            this.showWindow();
        }
    },

    onExDialogClose: function(controllerId, eOpts) {
        this.optDebug('onExDialogClose.arguments: ', arguments);

    }

});
