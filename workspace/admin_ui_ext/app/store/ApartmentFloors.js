/*
 * File: app/store/ApartmentFloors.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.store.ApartmentFloors', {
    extend: 'Ext.data.Store',

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoLoad: true,
            storeId: 'apartmentFloors',
            data: [
                {
                    name: '',
                    value: ''
                },
                {
                    name: 'N.B.',
                    value: 'N.B.'
                },
                {
                    name: '1 tr.',
                    value: '1 tr.'
                },
                {
                    name: '2 tr.',
                    value: '2 tr.'
                },
                {
                    name: '3 tr.',
                    value: '3 tr.'
                },
                {
                    name: '4 tr.',
                    value: '4 tr.'
                },
                {
                    name: '5 tr.',
                    value: '5 tr.'
                },
                {
                    name: '6 tr.',
                    value: '6 tr.'
                },
                {
                    name: '7 tr.',
                    value: '7 tr.'
                },
                {
                    name: '8 tr.',
                    value: '8 tr.'
                },
                {
                    name: '9 tr.',
                    value: '9 tr.'
                },
                {
                    name: '10 tr.',
                    value: '10 tr.'
                },
                {
                    name: '11 tr.',
                    value: '11 tr.'
                },
                {
                    name: '12 tr.',
                    value: '12 tr.'
                }
            ],
            fields: [
                {
                    name: 'name',
                    type: 'string'
                },
                {
                    name: 'value',
                    type: 'string'
                }
            ]
        }, cfg)]);
    }
});
