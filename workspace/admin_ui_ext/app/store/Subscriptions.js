/*
 * File: app/store/Subscriptions.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.store.Subscriptions', {
    extend: 'Ext.data.Store',

    requires: [
        'AdminInterface.model.Subscription'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'AdminInterface.model.Subscription',
            storeId: 'subscriptions',
            pageSize: 100,
            proxy: {
                type: 'direct',
                directionParam: 'order_direction',
                extraParams: {
                    kind: 'Subscription'
                },
                filterParam: 'filter_by',
                groupParam: 'group_by',
                limitParam: 'page_size',
                sortParam: 'order_by',
                startParam: 'offset',
                directFn: Remote.AdminCmd.grid_data,
                reader: {
                    type: 'json',
                    root: 'data'
                },
                writer: {
                    type: 'json',
                    nameProperty: 'datastore_key_urlsafe',
                    allowSingle: false,
                    root: 'data'
                }
            },
            listeners: {
                load: {
                    fn: me.onDirectStoreLoad,
                    scope: me
                },
                add: {
                    fn: me.onDirectStoreAdd,
                    scope: me
                }
            },
            filters: {
                property: 'deleted',
                value: false
            }
        }, cfg)]);
    },

    onDirectStoreLoad: function(store, records, successful, eOpts) {
        Ext.Array.each(records, function(record, index, dataset) {
            //console.error(record);
            record.postProcessExFields();
        });
        return;
    },

    onDirectStoreAdd: function(store, records, index, eOpts) {
        /*console.error('onDirectStoreAdd:');
        console.error(arguments);*/

        Ext.Array.each(records, function(record, index, dataset) {
            //console.error(record);
            record.postProcessExFields();
            //console.error(record.postProcessExFields());
        });

    },

    filterByAncestor: function(ancestorId) {

        this.filterBy(function(record, recordIndex, allRecords) {
            return record.get('parent_datastore_key_urlsafe') == ancestorId && record.get('deleted') == false;
        }, this);

    }

});
