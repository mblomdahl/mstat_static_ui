/*
 * File: app/store/UserAccounts.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.store.UserAccounts', {
    extend: 'Ext.data.Store',

    requires: [
        'AdminInterface.model.UserAccount'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'AdminInterface.model.UserAccount',
            remoteFilter: true,
            storeId: 'userAccounts',
            pageSize: 100,
            proxy: {
                type: 'direct',
                directionParam: 'order_direction',
                extraParams: {
                    kind: 'UserAccount'
                },
                filterParam: 'filter_by',
                groupParam: 'group_by',
                limitParam: 'page_size',
                sortParam: 'order_by',
                startParam: 'offset',
                directFn: Remote.AdminCmd.grid_data,
                reader: {
                    type: 'json',
                    root: 'data'
                },
                writer: {
                    type: 'json',
                    nameProperty: 'datastore_key_urlsafe',
                    allowSingle: false,
                    root: 'data'
                }
            },
            listeners: {
                load: {
                    fn: me.onDirectStoreLoad,
                    scope: me
                },
                add: {
                    fn: me.onDirectStoreAdd,
                    scope: me
                }
            },
            filters: {
                property: 'deleted',
                value: false
            }
        }, cfg)]);
    },

    onDirectStoreLoad: function(store, records, successful, eOpts) {
        //return;
        Ext.Array.each(records, function(record, index, dataset) {
            //console.error(record);
            record.postProcessExFields();
        });
        return;

        /*var UserAccountModel = Ext.ModelManager.getModel('AdminInterface.model.UserAccount');
        var SystemAdminAccountModel = Ext.ModelManager.getModel('AdminInterface.model.SystemAdminAccount');
        var SubscriberAccountModel = Ext.ModelManager.getModel('AdminInterface.model.SubscriberAccountModel');
        var ContactDetailsModel = Ext.ModelManager.getModel('AdminInterface.model.ContactDetails');

        Ext.Array.each(records, function(record, index, allItems) {
            var contactDetails = new ContactDetailsModel(record.get('contact_details')).commit();

            record.set({
                sys_origin_user_account: new UserAccountModel(record.get('sys_origin_user_account')).commit(),
                sys_origin_organization_account: new SystemAdminAccountModel(record.get('sys_origin_organization_account')),
                contact_details: contactDetails
            });

            return record;
        });*/
    },

    onDirectStoreAdd: function(store, records, index, eOpts) {
        //console.error('onDirectStoreAdd:');
        //console.error(arguments);

        Ext.Array.each(records, function(record, index, dataset) {
            //console.error(record);
            record.postProcessExFields();
            //console.error(record.postProcessExFields());
        });

    },

    filterByAncestor: function(ancestorId) {

        this.filterBy(function(record, recordIndex, allRecords) {
            return record.get('parent_datastore_key_urlsafe') == ancestorId && record.get('deleted') == false;;
        }, this);

    }

});
