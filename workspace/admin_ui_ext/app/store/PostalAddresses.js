/*
 * File: app/store/PostalAddresses.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.store.PostalAddresses', {
    extend: 'Ext.data.Store',

    requires: [
        'AdminInterface.model.PostalAddress'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'AdminInterface.model.PostalAddress',
            storeId: 'postalAddresses',
            buffered: true,
            proxy: {
                type: 'direct',
                directionParam: 'order_direction',
                extraParams: {
                    kind: 'PostalAddress'
                },
                filterParam: 'filter_by',
                groupParam: 'group_by',
                limitParam: 'page_size',
                sortParam: 'order_by',
                startParam: 'offset',
                directFn: Remote.AdminCmd.grid_data,
                reader: {
                    type: 'json',
                    root: 'data'
                },
                writer: {
                    type: 'json',
                    nameProperty: 'datastore_key_urlsafe',
                    allowSingle: false,
                    root: 'data'
                }
            }
        }, cfg)]);
    }
});
