/*
 * File: app/store/ContactDetails.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.store.ContactDetails', {
    extend: 'Ext.data.Store',

    requires: [
        'AdminInterface.model.ContactDetails'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'AdminInterface.model.ContactDetails',
            storeId: 'contactDetails',
            proxy: {
                type: 'direct',
                directionParam: 'order_direction',
                extraParams: {
                    kind: 'ContactDetails'
                },
                filterParam: 'filter_by',
                groupParam: 'group_by',
                limitParam: 'page_size',
                sortParam: 'order_by',
                startParam: 'offset',
                directFn: Remote.AdminCmd.grid_data,
                reader: {
                    type: 'json',
                    root: 'data'
                },
                writer: {
                    type: 'json',
                    nameProperty: 'datastore_key_urlsafe',
                    allowSingle: false,
                    root: 'data'
                }
            }
        }, cfg)]);
    }
});
