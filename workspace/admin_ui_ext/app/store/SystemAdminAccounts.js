/*
 * File: app/store/SystemAdminAccounts.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.store.SystemAdminAccounts', {
    extend: 'Ext.data.Store',

    requires: [
        'AdminInterface.model.OrganizationAccount'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'AdminInterface.model.OrganizationAccount',
            storeId: 'systemAdminAccounts',
            pageSize: 100,
            proxy: {
                type: 'direct',
                directionParam: 'order_direction',
                extraParams: {
                    kind: 'SystemAdminAccount'
                },
                filterParam: 'filter_by',
                groupParam: 'group_by',
                limitParam: 'page_size',
                sortParam: 'order_by',
                startParam: 'offset',
                directFn: Remote.AdminCmd.grid_data,
                reader: {
                    type: 'json',
                    root: 'data'
                },
                writer: {
                    type: 'json',
                    nameProperty: 'datastore_key_urlsafe',
                    allowSingle: false,
                    root: 'data'
                }
            },
            listeners: {
                load: {
                    fn: me.onDirectStoreLoad,
                    scope: me
                }
            }
        }, cfg)]);
    },

    onDirectStoreLoad: function(store, records, successful, eOpts) {
        Ext.Array.each(records, function(record, index, dataset) {
            //console.error(record);
            record.setExOrganizationKindSystemAdminAccount();
            record.postProcessExFields();
        });

        return;
    }

});
