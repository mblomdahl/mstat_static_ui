/*
 * File: app/store/SubscriberAccounts.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.store.SubscriberAccounts', {
    extend: 'Ext.data.Store',

    requires: [
        'AdminInterface.model.OrganizationAccount'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'AdminInterface.model.OrganizationAccount',
            storeId: 'subscriberAccounts',
            pageSize: 100,
            proxy: {
                type: 'direct',
                directionParam: 'order_direction',
                extraParams: {
                    kind: 'SubscriberAccount'
                },
                filterParam: 'filter_by',
                groupParam: 'group_by',
                limitParam: 'page_size',
                sortParam: 'order_by',
                startParam: 'offset',
                directFn: Remote.AdminCmd.grid_data,
                reader: {
                    type: 'json',
                    root: 'data'
                },
                writer: {
                    type: 'json',
                    nameProperty: 'datastore_key_urlsafe',
                    allowSingle: false,
                    root: 'data'
                }
            },
            filters: {
                property: 'deleted',
                value: false
            },
            listeners: {
                load: {
                    fn: me.onDirectStoreLoad,
                    scope: me
                },
                add: {
                    fn: me.onDirectStoreAdd,
                    scope: me
                }
            }
        }, cfg)]);
    },

    onDirectStoreLoad: function(store, records, successful, eOpts) {
        Ext.Array.each(records, function(record, index, dataset) {
            //console.error(record);
            record.setExOrganizationKindSubscriberAccount();
            record.postProcessExFields();
        });
        return;

        /*
        var UserAccountModel = Ext.ModelManager.getModel('AdminInterface.model.UserAccount'),
        SystemAdminAccountModel = Ext.ModelManager.getModel('AdminInterface.model.SystemAdminAccount');

        Ext.Array.each(records, function(record, index, allItems) {
        record.set({
        sys_origin_user_account: new UserAccountModel(record.get('sys_origin_user_account')),
        sys_origin_organization_account: new SystemAdminAccountModel(record.get('sys_origin_organization_account'))
        });

        return record;
        });
        */
    },

    onDirectStoreAdd: function(store, records, index, eOpts) {
        /*console.error('onDirectStoreAdd:');
        console.error(arguments);*/

        Ext.Array.each(records, function(record, index, dataset) {
            //console.error(record);
            record.setExOrganizationKindSubscriberAccount();
            record.postProcessExFields();
            //console.error(record.postProcessExFields());
        });

    }

});
