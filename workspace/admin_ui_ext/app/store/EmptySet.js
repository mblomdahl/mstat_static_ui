/*
 * File: app/store/EmptySet.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('AdminInterface.store.EmptySet', {
    extend: 'Ext.data.Store',

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'emptySet',
            data: [

            ],
            leadingBufferZone: 0,
            trailingBufferZone: 0,
            fields: [
                {
                    name: 'name',
                    type: 'string'
                },
                {
                    name: 'value',
                    type: 'string'
                }
            ]
        }, cfg)]);
    }
});
