#!/bin/bash

sencha -ti -cw subscriber_ui_ext app refresh
sencha -ti -cw subscriber_ui_ext app build --environment=testing
