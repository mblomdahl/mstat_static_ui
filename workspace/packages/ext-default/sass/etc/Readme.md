# ext-default/sass/etc

This folder contains miscellaneous SASS files. Unlike `"ext-default/sass/etc"`, these files
need to be used explicitly.
