/*
 * File: app/model/SubscriberAccount.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.SubscriberAccount', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: SubscriberAccount / ' + (new Date()).valueOf());})(),
           
                                   
                                      
                                     
                                 
      

    idProperty: 'datastore_key_urlsafe',

    fields: [
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'organization_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'department_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'organization_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'contact_details',
            type: 'auto',
            useNull: true
        },
        {
            name: 'postal_addresses',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'activated_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'registered_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'suspended_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'deleted_datetime',
            type: 'date',
            useNull: true
        },
        {
            name: 'suspended',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'deleted',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'activated',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'billing_balance',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_origin_user_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_organization_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_changelog_version',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_changelog',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ],

    hasOne: [
        {
            associationKey: 'sys_origin_user_account',
            model: 'Common.model.UserAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_origin_organization_account',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        }
    ],

    hasMany: [
        {
            associationKey: 'contact_details',
            model: 'Common.model.ContactDetails',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'postal_addresses',
            model: 'Common.model.PostalAddress',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'web_addresses',
            model: 'Common.model.WebAddress',
            primaryKey: 'datastore_key_id'
        }
    ]
});

/*
 * File: app/store/SubscriberAccounts.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.store.SubscriberAccounts', {
    extend:  Ext.data.Store ,
    //exLoadInitialized: (function() { console.error('storeId: subscriberAccounts / ' + (new Date()).valueOf());})(),
               
                                        
      

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'Common.model.SubscriberAccount',
            storeId: 'subscriberAccounts',
            leadingBufferZone: 0,
            pageSize: 100,
            trailingBufferZone: 0,
            proxy: {
                type: 'direct',
                directionParam: 'order_direction',
                extraParams: {
                    kind: 'SubscriberAccount'
                },
                filterParam: 'filter_by',
                groupParam: 'group_by',
                limitParam: 'page_size',
                sortParam: 'order_by',
                startParam: 'offset',
                directFn: Remote.UserCmd.grid_data,
                reader: {
                    type: 'json',
                    idProperty: 'datastore_key_id',
                    root: 'data'
                },
                writer: {
                    type: 'json',
                    nameProperty: 'datastore_key_id',
                    allowSingle: false,
                    root: 'data'
                }
            }
        }, cfg)]);
    }
});

/*
 * File: app/model/UserAccount.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.UserAccount', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: UserAccount / ' + (new Date()).valueOf());})(),
           
                                         
                                     
      

    idProperty: 'datastore_key_id',

    fields: [
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_ancestor_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_ancestor_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'user_email',
            type: 'string',
            useNull: true
        },
        {
            name: 'user_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'google_user_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'google_user',
            type: 'auto',
            useNull: true
        },
        {
            name: 'password',
            type: 'string',
            useNull: true
        },
        {
            name: 'contact_details',
            type: 'auto',
            useNull: true
        },
        {
            name: 'api_access_key',
            type: 'string',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'registered_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'activated_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'suspended_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'deleted_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'password_changed_datetime',
            type: 'date',
            useNull: true
        },
        {
            name: 'suspended',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'deleted',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'activated',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'administrator',
            type: 'boolean',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'last_visit_datetime',
            type: 'date',
            useNull: true
        },
        {
            name: 'last_client_ip',
            type: 'string',
            useNull: true
        },
        {
            name: 'sys_user_role',
            type: 'string',
            useNull: true
        },
        {
            name: 'sys_session_id',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_pending_email_update_address',
            type: 'string',
            useNull: true
        },
        {
            name: 'sys_pending_email_update_key',
            type: 'string',
            useNull: true
        },
        {
            name: 'sys_origin_user_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_organization_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_changelog_version',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_changelog',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ],

    hasOne: [
        {
            associationKey: 'sys_origin_organization_account',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_origin_user_account',
            model: 'Common.model.UserAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'contact_details',
            model: 'Common.model.ContactDetails',
            primaryKey: 'datastore_key_id'
        }
    ]
});

/*
 * File: app/store/UserAccounts.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.store.UserAccounts', {
    extend:  Ext.data.Store ,
    //exLoadInitialized: (function() { console.error('storeId: userAccounts / ' + (new Date()).valueOf());})(),
               
                                  
      

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'Common.model.UserAccount',
            storeId: 'userAccounts',
            leadingBufferZone: 0,
            pageSize: 100,
            trailingBufferZone: 0,
            proxy: {
                type: 'direct',
                directionParam: 'order_direction',
                extraParams: {
                    kind: 'UserAccount'
                },
                filterParam: 'filter_by',
                groupParam: 'group_by',
                limitParam: 'page_size',
                sortParam: 'order_by',
                startParam: 'offset',
                directFn: Remote.UserCmd.grid_data,
                reader: {
                    type: 'json',
                    idProperty: 'datastore_key_id',
                    root: 'data'
                },
                writer: {
                    type: 'json',
                    nameProperty: 'datastore_key_id',
                    allowSingle: false,
                    root: 'data'
                }
            }
        }, cfg)]);
    }
});

/*
 * File: app/model/Subscription.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.Subscription', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: Subscription / ' + (new Date()).valueOf());})(),
    idProperty: 'datastore_key_urlsafe',

    fields: [
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_ancestor_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_ancestor_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'subscription_type',
            type: 'string',
            useNull: true
        },
        {
            name: 'friendly_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'description',
            type: 'string',
            useNull: true
        },
        {
            name: 'content_restrictions',
            type: 'auto',
            useNull: true
        },
        {
            name: 'api_access_key',
            type: 'string',
            useNull: true
        },
        {
            name: 'quota',
            type: 'int',
            useNull: true
        },
        {
            dateFormat: 'Y-m-d',
            name: 'start_date',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-d',
            name: 'end_date',
            type: 'date',
            useNull: true
        },
        {
            name: 'update_frequency',
            type: 'string',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'registered_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'suspended_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'deleted_datetime',
            type: 'date',
            useNull: true
        },
        {
            name: 'suspended',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'deleted',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'sys_origin_user_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_organization_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_changelog_version',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_changelog',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ]
});

/*
 * File: app/store/Subscriptions.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.store.Subscriptions', {
    extend:  Ext.data.Store ,
    //exLoadInitialized: (function() { console.error('storeId: subscriptions / ' + (new Date()).valueOf());})(),
               
                                   
      

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'Common.model.Subscription',
            storeId: 'subscriptions',
            leadingBufferZone: 0,
            pageSize: 100,
            trailingBufferZone: 0,
            proxy: {
                type: 'direct',
                directionParam: 'order_direction',
                extraParams: {
                    kind: 'Subscription'
                },
                filterParam: 'filter_by',
                groupParam: 'group_by',
                limitParam: 'page_size',
                sortParam: 'order_by',
                startParam: 'offset',
                directFn: Remote.UserCmd.grid_data,
                reader: {
                    type: 'json',
                    idProperty: 'datastore_key_id',
                    root: 'data'
                },
                writer: {
                    type: 'json',
                    nameProperty: 'datastore_key_id',
                    allowSingle: false,
                    root: 'data'
                }
            }
        }, cfg)]);
    }
});

/*
 * File: app/view/SubscriberAccountCombobox.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.SubscriberAccountCombobox', {
    extend:  Ext.form.field.ComboBox ,
    alias: 'widget.subscriberaccountcombobox',
    //exLoadInitialized: (function() { console.error('xtype: subscriberaccountcombobox / ' + (new Date()).valueOf());})(),
    margin: '0 0 10 0',
    fieldLabel: 'Subscriber Account',
    labelWidth: 125,
    name: 'ancestor',
    displayField: 'organization_name',
    forceSelection: true,
    queryMode: 'local',
    store: 'SubscriberAccounts',
    typeAhead: true,
    typeAheadDelay: 50,
    valueField: 'datastore_key_urlsafe',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});

/*
 * File: app/view/GenericDateField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.GenericDateField', {
    extend:  Ext.form.field.Date ,
    alias: 'widget.genericdatefield',
    //exLoadInitialized: (function() { console.error('xtype: genericdatefield / ' + (new Date()).valueOf());})(),
    fieldLabel: 'Generic Date',
    labelWidth: 125,
    altFormats: 'Y-m-d',
    format: 'Y-m-d',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});

/*
 * File: app/view/FieldInclusionCheckbox.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.FieldInclusionCheckbox', {
    extend:  Ext.form.field.Checkbox ,
    alias: 'widget.fieldinclusioncheckbox',
    //exLoadInitialized: (function() { console.error('xtype: fieldinclusioncheckbox / ' + (new Date()).valueOf());})(),
    padding: '0 5 0 0',
    hideLabel: true,
    name: 'field_inclusion',
    boxLabel: 'Field Label',
    inputValue: 'Field Value',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});

/*
 * File: app/view/GenericCancelButton.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.button.GenericCancelButton', {
    extend:  Ext.button.Button ,
    alias: 'widget.genericcancelbutton',
    //exLoadInitialized: (function() { console.error('xtype: genericcancelbutton / ' + (new Date()).valueOf());})(),
    margin: 10,
    minWidth: 75,
    text: 'Cancel',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});

/*
 * File: app/view/GenericBackButton.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.button.GenericBackButton', {
    extend:  Ext.button.Button ,
    alias: 'widget.genericbackbutton',
    //exLoadInitialized: (function() { console.error('xtype: genericbackbutton / ' + (new Date()).valueOf());})(),
    margin: 10,
    minWidth: 75,
    text: 'Back',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});

/*
 * File: app/view/GenericNextButton.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.button.GenericNextButton', {
    extend:  Ext.button.Button ,
    alias: 'widget.genericnextbutton',
    //exLoadInitialized: (function() { console.error('xtype: genericnextbutton / ' + (new Date()).valueOf());})(),
    margin: 10,
    minWidth: 125,
    text: 'Next',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});

/*
 * File: app/view/AddSubscriptionWindow.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.window.AddSubscriptionWindow', {
    extend:  Ext.window.Window ,
    alias: 'widget.addsubscriptionwindow',
    //exLoadInitialized: (function() { console.error('xtype: addsubscriptionwindow / ' + (new Date()).valueOf());})(),
               
                                                           
                                                  
                                                        
                                                 
                                               
                                              
      

    height: 690,
    id: 'addSubscriptionWindow',
    padding: 0,
    width: 560,
    layout: {
        type: 'anchor'
    },
    bodyBorder: false,
    bodyPadding: 10,
    title: 'Add New Subscription',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    id: 'addSubscriptionStepContainer',
                    margin: 0,
                    layout: {
                        type: 'card'
                    },
                    items: [
                        {
                            xtype: 'container',
                            id: 'addSubscriptionStep1Container',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: '0 0 10 0',
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            frame: true,
                                            height: 69,
                                            html: '<h1>Step 1: Select Subscriber, Subscription Type/Period, and API Access Restrictions</h1>',
                                            margin: '',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'form',
                                    id: 'addSubscriptionStep1FormPanel',
                                    margin: 0,
                                    bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                    items: [
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            title: 'Target/"Parent" Subscriber (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'subscriberaccountcombobox',
                                                    fieldLabel: 'Parent Subscriber*',
                                                    allowBlank: false,
                                                    anchor: '100%'
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            title: 'Specify Subscription Type (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'radiogroup',
                                                    margin: '0 0 10 0',
                                                    fieldLabel: 'Subscription Type*',
                                                    labelWidth: 125,
                                                    allowBlank: false,
                                                    items: [
                                                        {
                                                            xtype: 'radiofield',
                                                            name: 'subscription_type',
                                                            boxLabel: 'Mäklarstatistik Data API',
                                                            checked: true,
                                                            inputValue: 'data_api_subscription'
                                                        },
                                                        {
                                                            xtype: 'radiofield',
                                                            disabled: true,
                                                            name: 'subscription_type',
                                                            boxLabel: 'Mäklarstatistik Widget Service',
                                                            inputValue: 'widget_service_subscription'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'container',
                                                    layout: {
                                                        align: 'stretch',
                                                        type: 'hbox'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'genericdatefield',
                                                            fieldLabel: 'Subscription Period Start Date*',
                                                            labelAlign: 'top',
                                                            labelSeparator: ' ',
                                                            name: 'subscription_start_date',
                                                            allowBlank: false,
                                                            flex: 1,
                                                            margins: '0 5 10 0'
                                                        },
                                                        {
                                                            xtype: 'genericdatefield',
                                                            fieldLabel: 'Period End Date (indefinite if omitted)',
                                                            labelAlign: 'top',
                                                            labelSeparator: ' ',
                                                            name: 'subscription_end_date',
                                                            flex: 1,
                                                            margins: '0 0 10 5'
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            layout: {
                                                align: 'stretch',
                                                type: 'vbox'
                                            },
                                            title: 'Specify Access Restrictions (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'container',
                                                    layout: {
                                                        align: 'stretch',
                                                        type: 'hbox'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'genericdatefield',
                                                            fieldLabel: 'Data Access Start Date*',
                                                            labelAlign: 'top',
                                                            labelSeparator: ' ',
                                                            name: 'start_date',
                                                            allowBlank: false,
                                                            flex: 1,
                                                            margins: '0 5 10 0'
                                                        },
                                                        {
                                                            xtype: 'genericdatefield',
                                                            fieldLabel: 'Data Access End Date (indefinite if omitted)',
                                                            labelAlign: 'top',
                                                            labelSeparator: ' ',
                                                            name: 'end_date',
                                                            flex: 1,
                                                            margins: '0 0 10 5'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'container',
                                                    margin: 0,
                                                    layout: {
                                                        align: 'stretch',
                                                        type: 'hbox'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'container',
                                                            flex: 1,
                                                            margin: '-6 5 0 0',
                                                            layout: {
                                                                align: 'stretch',
                                                                type: 'hbox'
                                                            },
                                                            items: [
                                                                {
                                                                    xtype: 'slider',
                                                                    tipText: function(thumb) {
                                                                        return Ext.String.format('**{0} downloads/update**', thumb);
                                                                    },
                                                                    flex: 1,
                                                                    margins: '0 5 0 0',
                                                                    fieldLabel: 'Full Dataset Downloads Quota per Update*',
                                                                    labelAlign: 'top',
                                                                    labelPad: 0,
                                                                    labelSeparator: ' ',
                                                                    name: 'quota',
                                                                    value: 12,
                                                                    increment: 3,
                                                                    keyIncrement: 3,
                                                                    maxValue: 30,
                                                                    minValue: 3,
                                                                    listeners: {
                                                                        change: {
                                                                            fn: me.onSliderChange,
                                                                            scope: me
                                                                        }
                                                                    }
                                                                },
                                                                {
                                                                    xtype: 'displayfield',
                                                                    margins: '24 0 10 0',
                                                                    id: '',
                                                                    fieldLabel: 'Label',
                                                                    hideLabel: true,
                                                                    labelAlign: 'top',
                                                                    labelWidth: 25,
                                                                    value: 12
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            xtype: 'radiogroup',
                                                            flex: 1,
                                                            margin: '0 0 10 5',
                                                            padding: 0,
                                                            layout: {
                                                                type: 'table'
                                                            },
                                                            fieldLabel: 'Source Data Update Frequency*',
                                                            labelAlign: 'top',
                                                            labelSeparator: ' ',
                                                            labelWidth: 125,
                                                            items: [
                                                                {
                                                                    xtype: 'radiofield',
                                                                    padding: '0 10 0 0',
                                                                    boxLabel: 'Daily',
                                                                    inputValue: 'subscriber_daily_update_frequency'
                                                                },
                                                                {
                                                                    xtype: 'radiofield',
                                                                    padding: '0 10 0 0',
                                                                    name: 'update_frequency',
                                                                    boxLabel: 'Weekly',
                                                                    inputValue: 'subscriber_weekly_update_frequency'
                                                                },
                                                                {
                                                                    xtype: 'radiofield',
                                                                    padding: '0 10 0 0',
                                                                    name: 'update_frequency',
                                                                    boxLabel: 'Monthly',
                                                                    checked: true,
                                                                    inputValue: 'subscriber_monthly_update_frequency'
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            id: 'addSubscriptionStep2Container',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: '0 0 10 0',
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            frame: true,
                                            height: 69,
                                            html: '<h1>Step 2: Select the Subset of Fields to Include in the New Subscription</h1>',
                                            margin: '',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'form',
                                    id: 'addSubscriptionStep2FormPanel',
                                    margin: 0,
                                    bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                    frameHeader: false,
                                    items: [
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            layout: {
                                                type: 'column'
                                            },
                                            title: 'Specify Field Access Restrictions (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'container',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionStep2FieldCheckerContainer',
                                                    layout: {
                                                        pack: 'end',
                                                        type: 'hbox'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'container',
                                                            layout: {
                                                                align: 'stretch',
                                                                pack: 'end',
                                                                type: 'vbox'
                                                            },
                                                            items: [
                                                                {
                                                                    xtype: 'label',
                                                                    margins: '0 0 5 0',
                                                                    text: 'Check fields:'
                                                                },
                                                                {
                                                                    xtype: 'container',
                                                                    layout: {
                                                                        type: 'hbox'
                                                                    },
                                                                    items: [
                                                                        {
                                                                            xtype: 'button',
                                                                            margins: '0 5 0 0',
                                                                            text: 'All'
                                                                        },
                                                                        {
                                                                            xtype: 'button',
                                                                            margins: '0 5 5 5',
                                                                            text: 'Standard'
                                                                        },
                                                                        {
                                                                            xtype: 'button',
                                                                            margins: '0 0 5 5',
                                                                            text: 'None'
                                                                        }
                                                                    ]
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'checkboxgroup',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionBasicTransactionCheckboxGroup',
                                                    margin: '0 -10 7 0',
                                                    layout: {
                                                        columns: 3,
                                                        type: 'table'
                                                    },
                                                    fieldLabel: '<u>Include Basic Transaction Details</u>',
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    labelWidth: 125,
                                                    columns: 3,
                                                    vertical: true,
                                                    items: [
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'transaction_id<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "ObjektId"</i>',
                                                            inputValue: 'transaction_id'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'real_estate_ad_publicized<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Annonsdatum"</i>',
                                                            inputValue: 'real_estate_ad_publicized'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'contract_date<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Kontraktsdatum"</i>',
                                                            inputValue: 'contract_date'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'contract_price<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Pris"</i>',
                                                            inputValue: 'contract_price'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'date_of_possession<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Tilltradesdatum"</i>',
                                                            inputValue: 'date_of_possession'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'data_transfer_date<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Overforingsdatum"</i>',
                                                            inputValue: 'data_transfer_date'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'mstat_restricted<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "KansligAffar"</i>',
                                                            inputValue: 'mstat_restricted'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'real_estate_designation<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Fastbet"</i>',
                                                            inputValue: 'real_estate_designation'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'real_estate_taxation_code<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Taxkod"</i>',
                                                            inputValue: 'real_estate_taxation_code'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'checkboxgroup',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionGeolocationCheckboxgroup',
                                                    margin: '0 -10 0 0',
                                                    layout: {
                                                        columns: 4,
                                                        type: 'table'
                                                    },
                                                    fieldLabel: '<u>Include Location Fields</u>',
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    labelWidth: 125,
                                                    columns: 4,
                                                    vertical: true,
                                                    items: [
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'locality',
                                                            inputValue: 'locality'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'sublocality',
                                                            inputValue: 'sublocality'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'county_name',
                                                            inputValue: 'county_name'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'county_lkf',
                                                            inputValue: 'county_lkf'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'municipality_name',
                                                            inputValue: 'municipality_name'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'municipality_lkf',
                                                            inputValue: 'municipality_lkf'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'congregation_name',
                                                            inputValue: 'congregation_name'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'congregation_lkf<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "LKF"</i>',
                                                            inputValue: 'congregation_lkf'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'checkboxgroup',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionGeoPtCheckboxGroup',
                                                    margin: '0 -10 10 0',
                                                    layout: {
                                                        columns: 3,
                                                        type: 'table'
                                                    },
                                                    fieldLabel: '<u>Include Coordinate Fields for Geopositioning</u>',
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    labelWidth: 125,
                                                    columns: 3,
                                                    vertical: true,
                                                    items: [
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'location_rt90_geopt',
                                                            inputValue: 'location_rt90_geopt'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 0 0',
                                                            boxLabel: 'location_rt90_geopt_east<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "East"</i>',
                                                            inputValue: 'location_rt90_geopt_east'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'location_rt90_geopt_north<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "North"</i>',
                                                            inputValue: 'location_rt90_geopt_north'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 0 0',
                                                            boxLabel: 'location_wgs84_geopt',
                                                            inputValue: 'location_wgs84_geopt'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'checkboxgroup',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionAddressCheckboxGroup',
                                                    margin: '0 -10 5 0',
                                                    layout: {
                                                        columns: 4,
                                                        type: 'table'
                                                    },
                                                    fieldLabel: '<u>Include Address Fields</u>',
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    labelWidth: 125,
                                                    columns: 4,
                                                    vertical: true,
                                                    items: [
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'route_name',
                                                            inputValue: 'route_name'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'street_number',
                                                            inputValue: 'street_number'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'street_entrance',
                                                            inputValue: 'street_entrance'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'apartment_number<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "LghNr"</i>',
                                                            inputValue: 'apartment_number'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'postal_code',
                                                            inputValue: 'postal_code'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'postal_town',
                                                            inputValue: 'postal_town'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 0 0 0',
                                                            boxLabel: 'formatted_address<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Gatuadress"</i>',
                                                            inputValue: 'formatted_address'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'checkboxgroup',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionHousingCooperativeCheckboxGroup',
                                                    margin: '0 -10 5 0',
                                                    layout: {
                                                        columns: 3,
                                                        type: 'table'
                                                    },
                                                    fieldLabel: '<u>Include Housing Cooperative Details</u>',
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    labelWidth: 125,
                                                    columns: 3,
                                                    vertical: true,
                                                    items: [
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 0 0',
                                                            boxLabel: 'housing_cooperative_registration_number<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "OrgNr"</i>',
                                                            inputValue: 'housing_cooperative_registration_number',
                                                            colspan: 2
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 0 0',
                                                            boxLabel: 'housing_cooperative_name<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "BRF"</i>',
                                                            inputValue: 'housing_cooperative_name'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 0 0',
                                                            boxLabel: 'housing_cooperative_share<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Andelstal"</i>',
                                                            inputValue: 'housing_cooperative_share'
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            id: 'addSubscriptionStep3Container',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: '0 0 10 0',
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            frame: true,
                                            height: 69,
                                            html: '<h1>Step 3: Select the Subset of Fields to Include in the New Subscription</h1>',
                                            margin: '',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'form',
                                    id: 'addSubscriptionStep3FormPanel',
                                    margin: 0,
                                    bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                    items: [
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            layout: {
                                                type: 'column'
                                            },
                                            title: 'Specify Field Access Restrictions (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'container',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionStep3FieldCheckerContainer',
                                                    layout: {
                                                        pack: 'end',
                                                        type: 'hbox'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'container',
                                                            layout: {
                                                                align: 'stretch',
                                                                pack: 'end',
                                                                type: 'vbox'
                                                            },
                                                            items: [
                                                                {
                                                                    xtype: 'label',
                                                                    margins: '0 0 5 0',
                                                                    text: 'Check fields:'
                                                                },
                                                                {
                                                                    xtype: 'container',
                                                                    layout: {
                                                                        type: 'hbox'
                                                                    },
                                                                    items: [
                                                                        {
                                                                            xtype: 'button',
                                                                            margins: '0 5 0 0',
                                                                            text: 'All'
                                                                        },
                                                                        {
                                                                            xtype: 'button',
                                                                            margins: '0 5 5 5',
                                                                            text: 'Standard'
                                                                        },
                                                                        {
                                                                            xtype: 'button',
                                                                            margins: '0 0 5 5',
                                                                            text: 'None'
                                                                        }
                                                                    ]
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'checkboxgroup',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionEstateFeesCheckboxGroup',
                                                    margin: '0 -10 5 0',
                                                    layout: {
                                                        columns: 4,
                                                        type: 'table'
                                                    },
                                                    fieldLabel: '<u>Include Details on Fees for the Real Estate</u>',
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    labelWidth: 125,
                                                    columns: 4,
                                                    vertical: true,
                                                    items: [
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 0 0',
                                                            boxLabel: 'monthly_fee<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Manavg"</i>',
                                                            inputValue: 'monthly_fee'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'annual_fee',
                                                            inputValue: 'annual_fee'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'operating_costs',
                                                            inputValue: 'operating_costs'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'heating_included',
                                                            inputValue: 'heating_included'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'checkboxgroup',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionMiscPropertiesCheckboxGroup',
                                                    margin: '0 -10 10 0',
                                                    layout: {
                                                        columns: 3,
                                                        type: 'table'
                                                    },
                                                    fieldLabel: '<u>Include Administrative Classifications and Misc. Estate Properties</u>',
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    labelWidth: 125,
                                                    columns: 3,
                                                    vertical: true,
                                                    items: [
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'type_of_housing<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Boendeform"</i>',
                                                            inputValue: 'type_of_housing'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'housing_category<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Typ"</i>',
                                                            inputValue: 'housing_category'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'housing_tenure<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Upplatelseform"</i>',
                                                            inputValue: 'housing_tenure'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'apartment_floor<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Vaning"</i>',
                                                            inputValue: 'apartment_floor'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'building_storeys<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Vaningar"</i>',
                                                            inputValue: 'building_storeys'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'living_area<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Boyta"</i>',
                                                            inputValue: 'living_area'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'plot_area<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Tomtarea"</i>',
                                                            inputValue: 'plot_area'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'number_of_rooms<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Rum"</i>',
                                                            inputValue: 'number_of_rooms'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'elevator<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Hiss"</i>',
                                                            inputValue: 'elevator'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'balcony<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Balkong"</i>',
                                                            inputValue: 'balcony'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'new_production<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "NyProd"</i>',
                                                            inputValue: 'new_production'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'build_year<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "ByggAr"</i>',
                                                            inputValue: 'build_year'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'build_year_lower',
                                                            inputValue: 'build_year_lower'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'build_year_upper',
                                                            inputValue: 'build_year_upper'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'checkboxgroup',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionRateableValueCheckboxGroup',
                                                    margin: '0 -10 5 0',
                                                    layout: {
                                                        columns: 2,
                                                        type: 'table'
                                                    },
                                                    fieldLabel: '<u>Include Rateable Value Details</u>',
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    labelWidth: 125,
                                                    columns: 2,
                                                    vertical: true,
                                                    items: [
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 0 0',
                                                            boxLabel: 'baseline_year_of_assessment<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Taxar"</i>',
                                                            inputValue: 'baseline_year_of_assessment'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'baseline_rateable_value<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Taxeringsvarde"</i>',
                                                            inputValue: 'baseline_rateable_value'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'year_of_assessment',
                                                            inputValue: 'year_of_assessment'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'rateable_value<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "SCB_Taxeringsvarde"</i>',
                                                            inputValue: 'rateable_value'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'price_by_rateable_value<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "SCB_KB"</i>',
                                                            inputValue: 'price_by_rateable_value'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'price_by_rateable_value_per_sq_meter<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "KBoy"</i>',
                                                            inputValue: 'price_by_rateable_value_per_sq_meter'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'checkboxgroup',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionSystemPropertiesCheckboxGroup',
                                                    margin: '0 -10 5 0',
                                                    layout: {
                                                        columns: 3,
                                                        type: 'table'
                                                    },
                                                    fieldLabel: '<u>Include Invalidation Codes and System Metadata</u>',
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    labelWidth: 125,
                                                    columns: 3,
                                                    vertical: true,
                                                    items: [
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'sys_invalidation_codes',
                                                            inputValue: 'sys_invalidation_codes'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 0 0',
                                                            boxLabel: 'sys_scb_invalidation_code<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "SCBBortfall"</i>',
                                                            inputValue: 'sys_scb_invalidation_code'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'sys_changelog_version',
                                                            inputValue: 'sys_changelog_version'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 0 0',
                                                            boxLabel: 'sys_changelog',
                                                            inputValue: 'sys_changelog'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 0 0',
                                                            boxLabel: 'sys_modified',
                                                            inputValue: 'sys_modified'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'sys_created',
                                                            inputValue: 'sys_created'
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            height: 580,
                            id: 'addSubscriptionStep4Container',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Target/"Parent" Subscriber for the New Subscription',
                                    items: [
                                        {
                                            xtype: 'form',
                                            id: 'addSubscriptionConfirmParentSubscriberFormPanel',
                                            margin: 0,
                                            layout: {
                                                type: 'auto'
                                            },
                                            bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                            items: [
                                                {
                                                    xtype: 'displayfield',
                                                    margin: '0 0 10 0',
                                                    fieldLabel: 'Company Name',
                                                    labelWidth: 150,
                                                    name: 'organization_name'
                                                },
                                                {
                                                    xtype: 'displayfield',
                                                    margin: '0 0 10 0',
                                                    fieldLabel: 'Department Name',
                                                    labelWidth: 150,
                                                    name: 'department_name'
                                                },
                                                {
                                                    xtype: 'displayfield',
                                                    margin: '0 0 10 0',
                                                    fieldLabel: 'Registration Number',
                                                    labelWidth: 150,
                                                    name: 'organization_id'
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Subscription Type and Period',
                                    items: [
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addSubscriptionConfirmSubscriptionTypeDisplayField',
                                            margin: '0 0 10 0',
                                            fieldLabel: 'Subscription Type',
                                            labelWidth: 150
                                        },
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addSubscriptionConfirmSubscriptionDateIntervalDisplayField',
                                            margin: '0 0 10 0',
                                            fieldLabel: 'Subsc. Period Date Interval',
                                            labelWidth: 150
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'panel',
                    dock: 'bottom',
                    frame: true,
                    margin: 0,
                    layout: {
                        align: 'stretch',
                        pack: 'end',
                        type: 'hbox'
                    },
                    bodyPadding: 0,
                    items: [
                        {
                            xtype: 'genericcancelbutton'
                        },
                        {
                            xtype: 'genericbackbutton'
                        },
                        {
                            xtype: 'genericnextbutton'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    },

    onSliderChange: function(slider, newValue, thumb, eOpts) {

        console.log('onSliderChange.arguments:');
        console.log(arguments);

        var parentContainer = slider.up('container'),
            siblingDisplayField = parentContainer.down('displayfield');

        siblingDisplayField.setValue(newValue);
    }

});

/*
 * File: app/controller/AddSubscriptionCtrl.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.controller.AddSubscriptionCtrl', {
    extend:  Ext.app.Controller ,
    alias: 'controller.addsubscriptionctrl',
    //exLoadInitialized: (function() { console.error('xtype: addsubscriptionctrl / ' + (new Date()).valueOf());})(),
    stores: [
        'Common.store.SubscriberAccounts',
        'Common.store.UserAccounts',
        'Common.store.Subscriptions'
    ],
    views: [
        'Common.view.window.AddSubscriptionWindow'
    ],

    refs: [
        {
            ref: 'addSubscriptionWindow',
            selector: '#addSubscriptionWindow',
            xtype: 'addsubscriptionwindow'
        },
        {
            ref: 'addSubscriptionStepContainer',
            selector: '#addSubscriptionStepContainer',
            xtype: 'container'
        },
        {
            ref: 'addSubscriptionStep1Container',
            selector: '#addSubscriptionStep1Container',
            xtype: 'container'
        },
        {
            ref: 'addSubscriptionStep2Container',
            selector: '#addSubscriptionStep2Container',
            xtype: 'container'
        },
        {
            ref: 'addSubscriptionStep3Container',
            selector: '#addSubscriptionStep3Container',
            xtype: 'container'
        },
        {
            ref: 'addSubscriptionStep4Container',
            selector: '#addSubscriptionStep4Container',
            xtype: 'container'
        },
        {
            ref: 'addSubscriptionStep1FormPanel',
            selector: '#addSubscriptionStep1FormPanel',
            xtype: 'form'
        },
        {
            ref: 'addSubscriptionStep2FormPanel',
            selector: '#addSubscriptionStep2FormPanel',
            xtype: 'form'
        },
        {
            ref: 'addSubscriptionStep3FormPanel',
            selector: '#addSubscriptionStep3FormPanel',
            xtype: 'form'
        },
        {
            ref: 'addSubscriptionStep1AncestorCombobox',
            selector: 'addsubscriptionwindow subscriberaccountcombobox',
            xtype: 'subscriberaccountcombobox'
        },
        {
            ref: 'addSubscriptionConfirmParentSubscriberFormPanel',
            selector: '#addSubscriptionConfirmParentSubscriberFormPanel',
            xtype: 'form'
        },
        {
            ref: 'addSubscriptionConfirmSubscriptionDateIntervalDisplayField',
            selector: '#addSubscriptionConfirmSubscriptionDateIntervalDisplayField',
            xtype: 'form'
        },
        {
            ref: 'addSubscriptionConfirmSubscriptionTypeDisplayField',
            selector: '#addSubscriptionConfirmSubscriptionTypeDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addSubscriptionConfirmSubscriptionDateIntervalDisplayField',
            selector: '#addSubscriptionConfirmSubscriptionDateIntervalDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addSubscriptionConfirmDateIntervalDisplayField',
            selector: '#addSubscriptionConfirmDateIntervalDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addSubscriptionConfirmQuotaDisplayField',
            selector: '#addSubscriptionConfirmQuotaDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addSubscriptionConfirmUpdateFrequencyDisplayField',
            selector: '#addSubscriptionConfirmUpdateFrequencyDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addSubscriptionConfirmFieldInclusionDisplayField',
            selector: '#addSubscriptionConfirmFieldInclusionDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addSubscriptionCancelButton',
            selector: 'addsubscriptionwindow genericcancelbutton',
            xtype: 'genericcancelbutton'
        },
        {
            ref: 'addSubscriptionBackButton',
            selector: 'addsubscriptionwindow genericbackbutton',
            xtype: 'genericbackbutton'
        },
        {
            ref: 'addSubscriptionNextButton',
            selector: 'addsubscriptionwindow genericnextbutton',
            xtype: 'genericnextbutton'
        }
    ],

    init: function(application) {
        this.debug = false || application.debug;

        this.control({
            '#addSubscriptionWindow': {
                hide: this.onAddSubscriptionWindowHide,
                show: this.onAddSubscriptionWindowShow
            },
            '#addSubscriptionStep1Container': {
                beforeactivate: this.onAddSubscriptionStep1ContainerBeforeActivate
            },
            '#addSubscriptionStep2Container': {
                beforeactivate: this.onAddSubscriptionStep2ContainerBeforeActivate
            },
            '#addSubscriptionStep3Container': {
                beforeactivate: this.onAddSubscriptionStep3ContainerBeforeActivate
            },
            '#addSubscriptionStep4Container': {
                beforeactivate: this.onAddSubscriptionStep4ContainerBeforeActivate
            },
            '#addSubscriptionStep1FormPanel': {
                validitychange: this.validateStep1
            },
            '#addSubscriptionStep2FormPanel': {
                render: this.onAddSubscriptionStep23FormPanelRender
            },
            '#addSubscriptionStep3FormPanel': {
                render: this.onAddSubscriptionStep23FormPanelRender
            },
            '#addSubscriptionStep2FormPanel fieldinclusioncheckbox': {
                change: this.validateStep2
            },
            '#addSubscriptionStep3FormPanel fieldinclusioncheckbox': {
                change: this.validateStep3
            },
            '#addSubscriptionStep2FieldCheckerContainer button': {
                click: this.onAddSubscriptionStep23FieldCheckerButtonClick
            },
            '#addSubscriptionStep3FieldCheckerContainer button': {
                click: this.onAddSubscriptionStep23FieldCheckerButtonClick
            },
            'addsubscriptionwindow genericcancelbutton': {
                click: this.onAddSubscriptionCancelButtonClick
            },
            'addsubscriptionwindow genericbackbutton': {
                click: this.onAddSubscriptionBackButtonClick
            },
            'addsubscriptionwindow genericnextbutton': {
                click: this.onAddSubscriptionNextButtonClick
            }
        });

        this.resetFields();
        this.steps = ['step_1', 'step_2', 'step_3', 'step_4'];
        this.activeStep = this.steps[0];

        this.disabledFields = ['locality', 'sublocality', 'location_wgs84_geopt', 'route_name', 'street_number', 'street_entrance', 'postal_code', 'postal_town', 'build_year_lower', 'build_year_upper', 'operating_costs', 'sys_changelog'];

        this.standardFields = ['transaction_id', 'contract_price', 'contract_date', 'date_of_possession', 'data_transfer_date', 'mstat_restricted', 'real_estate_designation', 'real_estate_taxation_code', 'municipality_name', 'congregation_lkf', 'monthly_fee', 'location_rt90_geopt', 'apartment_number', 'formatted_address', 'housing_cooperative_registration_number', 'housing_cooperative_name', 'housing_cooperative_share', 'heating_included', 'type_of_housing', 'housing_category', 'housing_tenure', 'apartment_floor', 'building_storeys', 'living_area', 'plot_area', 'number_of_rooms', 'elevator', 'balcony', 'new_production', 'build_year', 'baseline_year_of_assessment', 'baseline_rateable_value', 'sys_invalidation_codes', 'sys_changelog_version', 'sys_modified'];

        //console.log('initz: done');

    },

    showWindow: function(parentSubscriber) {

        this.resetFields();

        this.getAddSubscriptionWindowView().create();

        if (parentSubscriber) {
            console.log('parentSubscriber:');
            console.log(parentSubscriber);
            this.addSubscriptionStep1Fields = {
                ancestor: parentSubscriber,
                subscription_start_date: new Date(),
                start_date: new Date()
            };
            //this.getAddUserFormPanelAncestorCombobox().setValue(parentSubscriber);
        } else {
            console.log('parentSubscriber:');
            console.log(parentSubscriber);
        }

        this.setActiveStep(0);

        this.getAddSubscriptionWindow().show();

    },

    setActiveStep: function(stepIndex) {

        var stepContainerLayout = this.getAddSubscriptionStepContainer().getLayout();

        console.log('setActiveStep('+stepIndex+')');
        console.log(stepContainerLayout);

        if (this.activeStep == this.steps[stepIndex]) {
            this.getAddSubscriptionStep1Container().fireEvent('beforeactivate');
        } else {
            this.activeStep = this.steps[stepIndex];
            stepContainerLayout.setActiveItem(stepIndex);
        }


    },

    resetFields: function() {
        //page 1
        this.addSubscriptionStep1Fields = null;

        //page 2
        this.addSubscriptionStep2Fields = null;

        //page 3
        this.addSubscriptionStep3Fields = null;

        if (this.debug) {
            console.log('this.resetFields called');
        }
    },

    setStandardFields: function(container) {

        var fieldInclusionCheckboxes = container.query('fieldinclusioncheckbox');

        for (var i = fieldInclusionCheckboxes.length; i--; ) {
            if (!fieldInclusionCheckboxes[i].isDisabled()) {
                if (this.standardFields.indexOf(fieldInclusionCheckboxes[i].inputValue) !== -1) {
                    fieldInclusionCheckboxes[i].setValue(true);
                    console.log('checked box for field ' + fieldInclusionCheckboxes[i].inputValue);

                } else {
                    fieldInclusionCheckboxes[i].setValue(false);
                    console.log('unchecked box for field ' + fieldInclusionCheckboxes[i].inputValue);
                }
            } else {
                console.log('ignored further evaluation of box for field ' + fieldInclusionCheckboxes[i].inputValue + ' (disabled)');
            }
        }

    },

    validateStep1: function() {

        var addSubscriptionAncestorCombobox = this.getAddSubscriptionStep1AncestorCombobox(),
            addSubscriptionFormPanel = this.getAddSubscriptionStep1FormPanel(),
            targetAddSubscriptionFormPanelFieldTypes = [
            'radiogroup',
            'genericdatefield',
            'slider',
            'displayfield'
            ].join(', '),
            targetAddSubscriptionFormPanelFields = addSubscriptionFormPanel.query(targetAddSubscriptionFormPanelFieldTypes),
            targetFieldsEnabled = false,
            validateFormPanel = false;


        if (addSubscriptionAncestorCombobox.isValid()) {
            console.log('addSubscriptionAncestorCombobox.isValid() = true');
            validateFormPanel = true;
            targetFieldsEnabled = true;
        } else {
            console.log('addSubscriptionAncestorCombobox.isValid() = false');
        }

        for (var i = targetAddSubscriptionFormPanelFields.length; i--; ) {
            targetAddSubscriptionFormPanelFields[i].setDisabled(!targetFieldsEnabled);
        }

        if (validateFormPanel) {
            console.log('fooz (validating page 1)');

            if (!addSubscriptionFormPanel.getForm().hasInvalidField()) {
                this.addSubscriptionStep1Fields = addSubscriptionFormPanel.getForm().getValues();

                if (!window.mstatDebug) {
                    window.mstatDebug = {};
                }

                window.mstatDebug.addSubscriptionStep1Fields = this.addSubscriptionStep1Fields;


                this.getAddSubscriptionNextButton().setDisabled(false);

            } else {
                this.getAddSubscriptionNextButton().setDisabled(true);
            }

        } else {
            this.getAddSubscriptionNextButton().setDisabled(true);
        }

    },

    validateStep2: function() {
        var addSubscriptionFormPanel = this.getAddSubscriptionStep2FormPanel(),
            targetAddSubscriptionFormPanelFields = addSubscriptionFormPanel.query('checkbox'),
            hasChecked = false;

        console.log('targetAddSubscriptionFormPanelFields: ');
        console.log(targetAddSubscriptionFormPanelFields);

        for (var i = targetAddSubscriptionFormPanelFields.length; i--; ) {
            if (targetAddSubscriptionFormPanelFields[i].getValue()) {
                hasChecked = true;
                console.log('checked field found');
                break;
            }
        }

        console.log('fooz (validating page 2)');
        if (hasChecked) {
            this.addSubscriptionStep2Fields = addSubscriptionFormPanel.getForm().getValues();

            if (typeof(this.addSubscriptionStep2Fields.field_inclusion) == 'string') {
                this.addSubscriptionStep2Fields.field_inclusion = [this.addSubscriptionStep2Fields.field_inclusion];
            }

            if (!window.mstatDebug) {
                window.mstatDebug = {};
            }

            window.mstatDebug.addSubscriptionStep2Fields = this.addSubscriptionStep2Fields;
        }

        this.getAddSubscriptionNextButton().setDisabled(!hasChecked);
        console.log('next btn enabled: ' + hasChecked);
    },

    validateStep3: function() {
        var addSubscriptionFormPanel = this.getAddSubscriptionStep3FormPanel();

        console.log('fooz (validating page 3)');

        this.addSubscriptionStep3Fields = addSubscriptionFormPanel.getForm().getValues();

        if (typeof(this.addSubscriptionStep3Fields.field_inclusion) == 'string') {
            this.addSubscriptionStep3Fields.field_inclusion = [this.addSubscriptionStep3Fields.field_inclusion];
        }

        if (!window.mstatDebug) {

            window.mstatDebug = {};

        }


        window.mstatDebug.addSubscriptionStep3Fields = this.addSubscriptionStep3Fields;

    },

    executeAddSubscription: function(button) {

        var addSubscriptionWindow = button.up('window'),
            loadMask = addSubscriptionWindow.setLoading('Adding Subscription ...');

        var step1Fields = this.addSubscriptionStep1Fields,
            subscriptionStartDate = step1Fields.subscription_start_date,
            subscriptionEndDate = step1Fields.subscription_end_date,
            dataStartDate = step1Fields.start_date,
            dataEndDate = step1Fields.start_date,
            fieldInclusion = this.addSubscriptionStep2Fields.field_inclusion;

        if (!subscriptionEndDate) {
            subscriptionEndDate = undefined;
        }

        if (!dataEndDate) {
            dataEndDate = undefined;
        }

        if (this.addSubscriptionStep3Fields.field_inclusion) {
            for (var i = this.addSubscriptionStep3Fields.field_inclusion.length; i--; ) {
                fieldInclusion.push(this.addSubscriptionStep3Fields.field_inclusion[i]);
            }
        }

        Remote.AdminCmd.add_subscription({
            ancestor: step1Fields.ancestor,
            quota: step1Fields.quota,
            subscription_type: step1Fields.subscription_type,
            start_date: subscriptionStartDate,
            end_date: subscriptionEndDate,
            update_frequency: step1Fields.update_frequency,
            content_restrictions: {
                datastore_kind: 'TransactionCompositeEntry',
                fields: fieldInclusion,
                start_date: dataStartDate,
                end_date: dataEndDate
            }
        }, function(result, response, success) {
            console.log('add callback!\nresult:');
            console.log(result);
            console.log('response:');
            console.log(response);
            console.log('success:');
            console.log(success);
            console.log('(debug) arguments:');
            console.log(arguments);
            addSubscriptionWindow.setLoading(false);

            if (success) {
                button.up('window').close();
            }
        });

        return true;
    },

    onAddSubscriptionWindowShow: function(window) {

        return true;
    },

    onAddSubscriptionWindowHide: function(window) {

        //this.resetFields();


        //this.onSetActivePage(0);
    },

    onAddSubscriptionStep1ContainerBeforeActivate: function(container) {
        var addSubscriptionForm = this.getAddSubscriptionStep1FormPanel().getForm();

        if (this.addSubscriptionStep1Fields) {
            addSubscriptionForm.reset();
            addSubscriptionForm.setValues(this.addSubscriptionStep1Fields);
        } else {
            addSubscriptionForm.reset();
        }

        this.getAddSubscriptionBackButton().setDisabled(true);
        this.getAddSubscriptionNextButton().setText("Next");

        this.validateStep1();

    },

    onAddSubscriptionStep2ContainerBeforeActivate: function(container) {
        var addSubscriptionForm = container.down('form').getForm();

        if (!this.addSubscriptionStep2Fields) {
            this.setStandardFields(container);
        }

        this.getAddSubscriptionBackButton().setDisabled(false);


        this.getAddSubscriptionNextButton().setText("Next");
        this.validateStep2();

        /*

        var fieldInclusionCheckboxes = container.query('fieldinclusioncheckbox');

        for (var i = fieldInclusionCheckboxes.length; i--; ) {
        if (!fieldInclusionCheckboxes[i].isDisabled()) {
        if (this.addSubscriptionStep2Fields.field_inclusion.indexOf(fieldInclusionCheckboxes[i].inputValue) !== -1) {
        fieldInclusionCheckboxes[i].setValue(true);
        } else {
        fieldInclusionCheckboxes[i].setValue(false);
        }
        }
        }

        */
    },

    onAddSubscriptionStep3ContainerBeforeActivate: function(container) {

        var addSubscriptionForm = container.down('form').getForm();

        if (!this.addSubscriptionStep3Fields) {
            this.setStandardFields(container);
        }

        this.getAddSubscriptionBackButton().setDisabled(false);


        this.getAddSubscriptionNextButton().setText("Next");
        this.validateStep3();

    },

    onAddSubscriptionStep4ContainerBeforeActivate: function(container) {

        var step1Fields = this.addSubscriptionStep1Fields,
            ancestor = step1Fields.ancestor;

        var confirmParentSubscriberForm = this.getAddSubscriptionConfirmParentSubscriberFormPanel().getForm(),
            subscriberAccountsStore = this.getSubscriberAccountsStore();


        console.log('ancestor:');
        console.log(ancestor);

        var ancestorRecord = subscriberAccountsStore.getById(ancestor);

        confirmParentSubscriberForm.setValues(ancestorRecord.getData(false));

        // set subscription type and date interval
        if (step1Fields.subscription_type === 'data_api_subscription') {
            this.getAddSubscriptionConfirmSubscriptionTypeDisplayField().setValue('Mäklarstatistik Data API');
        } else {
            this.getAddSubscriptionConfirmSubscriptionTypeDisplayField().setValue('Mäklarstatistik Widget Service');
        }

        var subscriptionDateInterval = step1Fields.subscription_start_date;
        if (step1Fields.subscription_end_date) {
            subscriptionDateInterval += ' – ' + step1Fields.subscription_end_date;
        } else {
            subscriptionDateInterval += ' – <i>indefinite</i>';
        }

        this.getAddSubscriptionConfirmSubscriptionDateIntervalDisplayField().setValue(subscriptionDateInterval);


        var dataDateInterval = step1Fields.start_date;
        if (step1Fields.end_date) {
            dataDateInterval += ' – ' + step1Fields.end_date;
        } else {
            dataDateInterval += ' – <i>indefinite</i>';
        }

        this.getAddSubscriptionConfirmDateIntervalDisplayField().setValue(dataDateInterval);

        var quota = step1Fields.quota + ' full dataset downloads per update';
        this.getAddSubscriptionConfirmQuotaDisplayField().setValue(quota);

        var updateFrequency = step1Fields.update_frequency;
        switch (updateFrequency) {
            case 'subscriber_monthly_update_frequency':
            updateFrequency = 'Monthly';
            break;
            case 'subscriber_weekly_update_frequency':
            updateFrequency = 'Weekly';
            break;
            case 'subscriber_daily_update_frequency':
            updateFrequency = 'Daily';
            break;
        }

        this.getAddSubscriptionConfirmUpdateFrequencyDisplayField().setValue(updateFrequency);

        var fieldInclusion = this.addSubscriptionStep2Fields.field_inclusion;
        if (this.addSubscriptionStep3Fields.field_inclusion) {
            for (var i = this.addSubscriptionStep3Fields.field_inclusion.length; i--; ) {
                fieldInclusion.push(this.addSubscriptionStep3Fields.field_inclusion[i]);
            }
        }

        this.getAddSubscriptionConfirmFieldInclusionDisplayField().setValue(fieldInclusion.join(', '));

        this.getAddSubscriptionBackButton().setDisabled(false);
        this.getAddSubscriptionNextButton().setText("Add Subscription");

    },

    onAddSubscriptionStep1FormPanelRender: function(formPanel) {

    },

    onAddSubscriptionStep23FormPanelRender: function(formPanel) {
        console.log('onAddSub...PanelRender called');

        var formPanelCheckboxes = formPanel.query('fieldinclusioncheckbox');
        for (var j = formPanelCheckboxes.length; j--; ) {
            if (this.disabledFields.indexOf(formPanelCheckboxes[j].inputValue) !== -1) {
                formPanelCheckboxes[j].setDisabled(true);
                console.log('disabled checkbox field ' + formPanelCheckboxes[j].inputValue);
            }
        }

        console.log('onAddSub...PanelRender finishing');

    },

    onAddSubscriptionStep23FieldCheckerButtonClick: function(button) {

        var parentFormPanel = button.up('form');

        if (button.getText() == 'Standard') {
            this.setStandardFields(parentFormPanel);
        } else {

            if (button.getText() == 'None') {
                parentFormPanel.getForm().reset();
            } else { // text = 'All'

                var fieldInclusionCheckboxes = parentFormPanel.query('fieldinclusioncheckbox');

                for (var i = fieldInclusionCheckboxes.length; i--; ) {
                    if (!fieldInclusionCheckboxes[i].isDisabled()) {
                        fieldInclusionCheckboxes[i].setValue(true);
                    }
                }
            }
        }
    },

    onAddSubscriptionCancelButtonClick: function(button) {

        var addSubscriberWindow = button.up('window');

        addSubscriberWindow.close();

    },

    onAddSubscriptionBackButtonClick: function(button) {

        if (this.activeStep == this.steps[1]) {
            this.validateStep1();
            this.setActiveStep(0);
        } else {
            if (this.activeStep == this.steps[2]) {
                this.setActiveStep(1);
            } else {
                if (this.activeStep == this.steps[3]) {
                    this.setActiveStep(2);
                }
            }
        }

    },

    onAddSubscriptionNextButtonClick: function(button) {

        if (this.activeStep == this.steps[0]) {
            console.log('setting page 2');
            this.validateStep1();
            this.setActiveStep(1);
        } else {
            if (this.activeStep == this.steps[1]) {
                console.log('setting page 3');
                this.validateStep2();
                this.setActiveStep(2);
            } else {
                if (this.activeStep == this.steps[2]) {
                    console.log('setting page 4');
                    this.validateStep3();
                    this.setActiveStep(3);
                } else {
                    console.log('executingadds');
                    this.executeAddSubscription(button);
                }
            }
        }

    }

});

/*
 * File: app/view/PersonNameTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.PersonNameTextField', {
    extend:  Ext.form.field.Text ,
    alias: 'widget.personnametextfield',
    //exLoadInitialized: (function() { console.error('xtype: personnametextfield / ' + (new Date()).valueOf());})(),
    width: 780,
    fieldLabel: 'Person Name*',
    labelSeparator: ' ',
    allowBlank: false,

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});

/*
 * File: app/view/GenericTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.GenericTextField', {
    extend:  Ext.form.field.Text ,
    alias: 'widget.generictextfield',
    //exLoadInitialized: (function() { console.error('xtype: generictextfield / ' + (new Date()).valueOf());})(),
    fieldLabel: 'Generic Text',
    labelSeparator: ' ',
    labelWidth: 125,

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});

/*
 * File: app/view/EmailAddressTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.EmailAddressTextField', {
    extend:  Ext.form.field.Text ,
    alias: 'widget.emailaddresstextfield',
    //exLoadInitialized: (function() { console.error('xtype: emailaddresstextfield / ' + (new Date()).valueOf());})(),
    fieldLabel: 'Email Address',
    labelSeparator: ' ',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});

/*
 * File: app/view/PhoneNumberTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.PhoneNumberTextField', {
    extend:  Ext.form.field.Text ,
    alias: 'widget.phonenumbertextfield',
    //exLoadInitialized: (function() { console.error('xtype: phonenumbertextfield / ' + (new Date()).valueOf());})(),
    fieldLabel: 'Phone Number',
    labelSeparator: ' ',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});

/*
 * File: app/view/ContactDetailsContainer.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.container.ContactDetailsContainer', {
    extend:  Ext.container.Container ,
    alias: 'widget.contactdetailscontainer',
    //exLoadInitialized: (function() { console.error('xtype: contactdetailscontainer / ' + (new Date()).valueOf());})(),
               
                                                     
                                                  
                                                       
                                                     
      

    margin: 0,

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    layout: {
                        align: 'stretch',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'personnametextfield',
                            fieldLabel: 'First Name*',
                            labelAlign: 'top',
                            name: 'first_name',
                            flex: 2,
                            margins: '0 5 10 0'
                        },
                        {
                            xtype: 'personnametextfield',
                            fieldLabel: 'Last Name*',
                            labelAlign: 'top',
                            name: 'last_name',
                            flex: 2,
                            margins: '0 5 10 5'
                        },
                        {
                            xtype: 'generictextfield',
                            fieldLabel: 'Job Title',
                            labelAlign: 'top',
                            name: 'job_title',
                            flex: 1.5,
                            margins: '0 0 10 5'
                        }
                    ]
                },
                {
                    xtype: 'container',
                    margin: '0 0 0 0',
                    layout: {
                        align: 'stretch',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'emailaddresstextfield',
                            fieldLabel: 'Email Address*',
                            labelAlign: 'top',
                            name: 'email_addresses_work',
                            allowBlank: false,
                            flex: 3,
                            margins: '0 5 10 0'
                        },
                        {
                            xtype: 'phonenumbertextfield',
                            fieldLabel: 'Office Phone*',
                            labelAlign: 'top',
                            name: 'phone_numbers_office',
                            allowBlank: false,
                            flex: 1.5,
                            margins: '0 5 10 5'
                        },
                        {
                            xtype: 'phonenumbertextfield',
                            fieldLabel: 'Mobile Phone',
                            labelAlign: 'top',
                            name: 'phone_numbers_mobile',
                            flex: 1.5,
                            margins: '0 0 10 5'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});

/*
 * File: app/controller/ContactDetailsCtrl.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.controller.ContactDetailsCtrl', {
    extend:  Ext.app.Controller ,
    alias: 'controller.contactdetailsctrl',
    //exLoadInitialized: (function() { console.error('xtype: contactdetailsctrl / ' + (new Date()).valueOf());})(),
    views: [
        'Common.view.container.ContactDetailsContainer'
    ],

    refs: [
        {
            ref: 'contactDetailsFirstNameTextField',
            selector: 'personnametextfield[name="first_name"]',
            xtype: 'personnametextfield'
        },
        {
            ref: 'contactDetailsLastNameTextField',
            selector: 'personnametextfield[name="last_name"]',
            xtype: 'personnametextfield'
        },
        {
            ref: 'contactDetailsJobTitleTextField',
            selector: 'generictextfield[name="job_title"]',
            xtype: 'generictextfield'
        },
        {
            ref: 'contactDetailsWorkEmailAddressTextField',
            selector: 'emailaddresstextfield',
            xtype: 'emailaddresstextfield'
        },
        {
            ref: 'contactDetailsMobilePhoneNumberTextField',
            selector: 'phonenumbertextfield[name="phone_numbers_mobile"]',
            xtype: 'phonenumbertextfield'
        },
        {
            ref: 'contactDetailsOfficePhoneNumberTextField',
            selector: 'phonenumbertextfield[name="phone_numbers_office"]',
            xtype: 'phonenumbertextfield'
        }
    ],

    init: function(application) {
        /*
        this.control({
        'radiogroup': {
        change: this.onPostalAddressTypeRadioGroupChange
        },
        'postaladdresstypecontainer': {
        beforerender: this.onPostalAddressTypeBeforeRender
        },
        'postaladdresstypecontainer container': {
        beforeactivate: this.onPostalAddressTypeBeforeActivate,
        beforedeactivate: this.onPostalAddressTypeBeforeDeactivate
        }
        });
        */
    },

    formatContactDetails: function(formFields) {
        var formattedDetails = [];

        if (formFields.xtype) {
            if (formFields.xtype !== 'form' && !formFields.up('form')) {
                console.log("formFields.xtype !== 'form' && !formFields.up('form')");
                auxFormFields = {
                    first_name: formFields.down('personnametextfield[name="first_name"]'),
                    last_name: formFields.down('personnametextfield[name="last_name"]'),
                    job_title: formFields.down('generictextfield[name="job_title"]'),
                    email_addresses_work: formFields.down('emailaddresstextfield'),
                    phone_numbers_office: formFields.down('phonenumbertextfield[name="phone_numbers_office"]'),
                    phone_numbers_mobile: formFields.down('phonenumbertextfield[name="phone_numbers_mobile"]')
                };

                console.log('auxFormFields:');
                console.log(auxFormFields);


                var formFieldKeys = [
                'first_name',
                'last_name',
                'job_title',
                'email_addresses_work',
                'phone_numbers_office',
                'phone_numbers_mobile'
                ];

                for (var i = formFieldKeys.length; i--; ) {
                    if (auxFormFields[formFieldKeys[i]]) {
                        auxFormFields[formFieldKeys[i]] = auxFormFields[formFieldKeys[i]].getValue();
                    }
                }
            } else { // xtype is (or can become) "form"
                if (formFields.xtype !== 'form') {
                    console.log("formFields.xtype !== 'form'");

                    formFields = formFields.up('form').getForm().getValues();
                } else {

                    console.log("formFields.xtype == 'form'");
                    formFields = formfields.getForm().getValues();
                }
            }
        }

        console.log('formFields:');
        console.log(formFields);

        var name = formFields.first_name + ' ' + formFields.last_name,
            nameLine = formFields.job_title ? name + ', ' + formFields.job_title : name;

        formattedDetails.push(nameLine);

        formattedDetails.push('Email Address: ' + formFields.email_addresses_work + ' (work)');

        var phoneNumber = 'Phone Number: ' + formFields.phone_numbers_office + ' (office)',
            phoneNumbersLine = formFields.phone_numbers_mobile ?
            phoneNumber + ', ' + formFields.phone_numbers_mobile + ' (mobile)' : phoneNumber;

        formattedDetails.push(phoneNumbersLine);

        return formattedDetails.join('<br>\n');
    }

});

/*
 * File: app/view/RouteNameTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.RouteNameTextField', {
    extend:  Ext.form.field.Text ,
    alias: 'widget.routenametextfield',
    //exLoadInitialized: (function() { console.error('xtype: routenametextfield / ' + (new Date()).valueOf());})(),
    width: 780,
    fieldLabel: 'Route Name*',
    labelAlign: 'top',
    labelSeparator: ' ',
    name: 'route_name',
    allowBlank: false,

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});

/*
 * File: app/view/StreetNumberTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.StreetNumberTextField', {
    extend:  Ext.form.field.Text ,
    alias: 'widget.streetnumbertextfield',
    //exLoadInitialized: (function() { console.error('xtype: streetnumbertextfield / ' + (new Date()).valueOf());})(),
    fieldLabel: 'Street Number*',
    labelAlign: 'top',
    labelSeparator: ' ',
    name: 'street_number',
    allowBlank: false,

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});

/*
 * File: app/view/StreetEntranceTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.StreetEntranceTextField', {
    extend:  Ext.form.field.Text ,
    alias: 'widget.streetentrancetextfield',
    //exLoadInitialized: (function() { console.error('xtype: streetentrancetextfield / ' + (new Date()).valueOf());})(),
    fieldLabel: 'Entrance',
    labelAlign: 'top',
    labelSeparator: ' ',
    name: 'street_entrance',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});

/*
 * File: app/view/ApartmentFloorCombobox.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.ApartmentFloorCombobox', {
    extend:  Ext.form.field.ComboBox ,
    alias: 'widget.apartmentfloorcombobox',
    //exLoadInitialized: (function() { console.error('xtype: apartmentfloorcombobox / ' + (new Date()).valueOf());})(),
    fieldLabel: 'Office Floor',
    labelAlign: 'top',
    labelSeparator: ' ',
    name: 'apartment_floor',
    displayField: 'name',
    forceSelection: true,
    queryMode: 'local',
    store: 'ApartmentFloors',
    typeAhead: true,
    typeAheadDelay: 50,
    valueField: 'value',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});

/*
 * File: app/view/BoxAddressTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.BoxAddressTextField', {
    extend:  Ext.form.field.Text ,
    alias: 'widget.boxaddresstextfield',
    //exLoadInitialized: (function() { console.error('xtype: boxaddresstextfield / ' + (new Date()).valueOf());})(),
    margin: '0 0 10 0',
    fieldLabel: 'Box Address (Note: Do not prefix input with "Box ")*',
    labelAlign: 'top',
    labelSeparator: ' ',
    name: 'box_address',
    allowBlank: false,

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});

/*
 * File: app/view/PostalAddressTypeContainer.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.container.PostalAddressTypeContainer', {
    extend:  Ext.container.Container ,
    alias: 'widget.postaladdresstypecontainer',
    //exLoadInitialized: (function() { console.error('xtype: postaladdresstypecontainer / ' + (new Date()).valueOf());})(),
               
                                                    
                                                       
                                                         
                                                        
                                                    
      

    layout: {
        type: 'card'
    },

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    layout: {
                        align: 'stretch',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'routenametextfield',
                            flex: 2.5,
                            margins: '0 5 10 0'
                        },
                        {
                            xtype: 'streetnumbertextfield',
                            flex: 1.2,
                            margins: '0 5 10 5'
                        },
                        {
                            xtype: 'streetentrancetextfield',
                            flex: 1,
                            margins: '0 5 10 5'
                        },
                        {
                            xtype: 'apartmentfloorcombobox',
                            flex: 1,
                            margins: '0 0 10 5'
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'anchor'
                    },
                    items: [
                        {
                            xtype: 'boxaddresstextfield',
                            disabled: true,
                            hidden: true,
                            anchor: '65%'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});

/*
 * File: app/view/PostalCodeTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.PostalCodeTextField', {
    extend:  Ext.form.field.Text ,
    alias: 'widget.postalcodetextfield',
    //exLoadInitialized: (function() { console.error('xtype: postalcodetextfield / ' + (new Date()).valueOf());})(),
    fieldLabel: 'Postal Code*',
    labelAlign: 'top',
    labelSeparator: ' ',
    name: 'postal_code',
    allowBlank: false,

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});

/*
 * File: app/view/PostalTownTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.PostalTownTextField', {
    extend:  Ext.form.field.Text ,
    alias: 'widget.postaltowntextfield',
    //exLoadInitialized: (function() { console.error('xtype: postaltowntextfield / ' + (new Date()).valueOf());})(),
    fieldLabel: 'Postal Town*',
    labelAlign: 'top',
    labelSeparator: ' ',
    name: 'postal_town',
    allowBlank: false,

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});

/*
 * File: app/view/CountryTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.CountryTextField', {
    extend:  Ext.form.field.Text ,
    alias: 'widget.countrytextfield',
    //exLoadInitialized: (function() { console.error('xtype: countrytextfield / ' + (new Date()).valueOf());})(),
    fieldLabel: 'Country*',
    labelAlign: 'top',
    labelSeparator: ' ',
    name: 'country',
    value: 'Sweden',
    readOnly: true,
    allowBlank: false,

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});

/*
 * File: app/view/PostalAddressContainer.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.container.PostalAddressContainer', {
    extend:  Ext.container.Container ,
    alias: 'widget.postaladdresscontainer',
    //exLoadInitialized: (function() { console.error('xtype: postaladdresscontainer / ' + (new Date()).valueOf());})(),
               
                                                  
                                                           
                                                     
                                                     
                                                 
      

    margin: '0 0 0 0',
    layout: {
        type: 'anchor'
    },

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    layout: {
                        type: 'anchor'
                    },
                    items: [
                        {
                            xtype: 'generictextfield',
                            margin: '0 0 10 0',
                            fieldLabel: 'Address Line 1*',
                            name: 'recipient_lines_1',
                            allowBlank: false,
                            anchor: '100%'
                        },
                        {
                            xtype: 'generictextfield',
                            margin: '0 0 10 0',
                            fieldLabel: 'Address Line 2',
                            name: 'recipient_lines_2',
                            anchor: '100%'
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'anchor'
                    },
                    items: [
                        {
                            xtype: 'radiogroup',
                            margin: '0 0 10 0',
                            fieldLabel: 'Address Type',
                            labelWidth: 125,
                            items: [
                                {
                                    xtype: 'radiofield',
                                    name: 'address_type',
                                    boxLabel: 'Street Address',
                                    checked: true,
                                    inputValue: 'street_address'
                                },
                                {
                                    xtype: 'radiofield',
                                    name: 'address_type',
                                    boxLabel: 'Box Address',
                                    inputValue: 'box_address'
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'postaladdresstypecontainer'
                },
                {
                    xtype: 'container',
                    margin: '0 0 0 0',
                    layout: {
                        align: 'stretch',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'postalcodetextfield',
                            flex: 0.75,
                            margins: '0 5 10 0'
                        },
                        {
                            xtype: 'postaltowntextfield',
                            flex: 2.5,
                            margins: '0 5 10 5'
                        },
                        {
                            xtype: 'countrytextfield',
                            flex: 1,
                            margins: '0 0 10 5'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});

/*
 * File: app/controller/PostalAddressCtrl.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.controller.PostalAddressCtrl', {
    extend:  Ext.app.Controller ,
    alias: 'controller.postaladdressctrl',
    //exLoadInitialized: (function() { console.error('xtype: postaladdressctrl / ' + (new Date()).valueOf());})(),
    views: [
        'Common.view.container.PostalAddressContainer'
    ],

    refs: [
        {
            ref: 'postalAddressRecipientLines1TextField',
            selector: '[name="recipient_lines_1"]',
            xtype: 'generictextfield'
        },
        {
            ref: 'postalAddressRecipientLines2TextField',
            selector: '[name="recipient_lines_2"]',
            xtype: 'generictextfield'
        },
        {
            ref: 'postalAddressTypeRadioGroup',
            selector: 'radiogroup',
            xtype: 'radiogroup'
        },
        {
            ref: 'postalAddressTypeContainer',
            selector: 'postaladdresstypecontainer',
            xtype: 'postaladdresstypecontainer'
        },
        {
            ref: 'postalAddressRouteNameTextField',
            selector: 'routenametextfield',
            xtype: 'routenametextfield'
        },
        {
            ref: 'postalAddressStreetNumberTextField',
            selector: 'streetnumbertextfield',
            xtype: 'streetnumbertextfield'
        },
        {
            ref: 'postalAddressStreetEntranceTextField',
            selector: 'streetentrancetextfield',
            xtype: 'streetentrancetextfield'
        },
        {
            ref: 'postalAddressApartmentFloorCombobox',
            selector: 'apartmentfloorcombobox',
            xtype: 'apartmentfloorcombobox'
        },
        {
            ref: 'postalAddressBoxAddressTextField',
            selector: 'boxaddresstextfield',
            xtype: 'boxaddresstextfield'
        },
        {
            ref: 'postalAddressPostalCodeTextField',
            selector: 'postalcodetextfield',
            xtype: 'postalcodetextfield'
        },
        {
            ref: 'postalAddressPostalTownTextField',
            selector: 'postaltowntextfield',
            xtype: 'postaltowntextfield'
        },
        {
            ref: 'postalAddressCountryTextField',
            selector: 'countrytextfield',
            xtype: 'countrytextfield'
        }
    ],

    init: function(application) {
        this.debug = false || application.debug;

        this.control({
            'radiogroup': {
                change: this.onPostalAddressTypeRadioGroupChange
            },
            'postaladdresstypecontainer': {
                beforerender: this.onPostalAddressTypeBeforeRender
            },
            'postaladdresstypecontainer container': {
                beforeactivate: this.onPostalAddressTypeBeforeActivate,
                beforedeactivate: this.onPostalAddressTypeBeforeDeactivate
            }
        });

    },

    formatPostalAddress: function(formFields) {
        var formattedAddress = [];

        if (formFields.xtype) {
            if (formFields.xtype !== 'form' && !formFields.up('form')) {
                console.log("formFields.xtype !== 'form' && !formFields.up('form')");
                auxFormFields = {
                    recipient_line_1: formFields.down('generictextfield[name="recipient_lines_1"]'),
                    recipient_line_2: formFields.down('generictextfield[name="recipient_lines_2"]'),
                    box_address: formFields.down('boxaddresstextfield'),
                    route_name: formFields.down('routenametextfield'),
                    street_number: formFields.down('streetnumbertextfield'),
                    street_entrance: formFields.down('streetentrancetextfield'),
                    apartment_floor: formFields.down('apartmentfloorcombobox'),
                    postal_code: formFields.down('postalcodetextfield'),
                    postal_town: formFields.down('postaltowntextfield'),
                    country: formFields.down('countrytextfield')
                };

                console.log('auxFormFields:');
                console.log(auxFormFields);


                var formFieldKeys = ['recipient_lines_1', 'recipient_lines_2', 'box_address', 'route_name', 'street_number', 'street_entrance', 'apartment_floor', 'postal_code', 'postal_town', 'country'];

                for (var i = formFieldKeys.length; i--; ) {
                    if (auxFormFields[formFieldKeys[i]]) {
                        auxFormFields[formFieldKeys[i]] = auxFormFields[formFieldKeys[i]].getValue();
                    }
                }
            } else { // xtype is (or can become) "form"
                if (formFields.xtype !== 'form') {
                    console.log("formFields.xtype !== 'form'");

                    formFields = formFields.up('form').getForm().getValues();
                } else {

                    console.log("formFields.xtype == 'form'");
                    formFields = formfields.getForm().getValues();
                }
            }
        }

        console.log('formFields:');
        console.log(formFields);

        formattedAddress.push(formFields.recipient_lines_1);

        formFields.recipient_lines_2 ? formattedAddress.push(formFields.recipient_lines_2) : null;

        if (formFields.box_address) {
            formattedAddress.push('Box '+formFields.box_address);
        } else {
            var streetAddress = [];

            formFields.route_name && formFields.street_number ? streetAddress.push([
            formFields.route_name,
            formFields.street_number,
            formFields.street_entrance
            ].join(' ').trim()) : null;

            formFields.apartment_floor ? streetAddress.push(formFields.apartment_floor) : null;

            formattedAddress.push(streetAddress.join(', '));
        }

        formFields.postal_code.length == 5 && formFields.postal_town ? formattedAddress.push(
        formFields.postal_code.substr(0,3) + ' ' + formFields.postal_code.substr(3) + '&nbsp;&nbsp;'
        + formFields.postal_town) : null;

        return formattedAddress.join('<br>\n');
    },

    onSetActiveAddressType: function(addressContainer, addressType) {

        var activeItem = 0;

        console.log(addressContainer);
        console.log(addressType);

        if (addressType == "box_address") {
            activeItem = 1;
        }

        addressContainer.getLayout().setActiveItem(activeItem);

    },

    onPostalAddressTypeRadioGroupChange: function(radiobox, newValue, oldValue) {

        console.log('radiobox-change');

        var topParentContainer = radiobox.up('postaladdresscontainer'),
            targetAddressContainer = topParentContainer.down('postaladdresstypecontainer'),
            parentFormPanel = radiobox.up('form');

        console.log(topParentContainer);
        console.log(targetAddressContainer);
        console.log(newValue);

        this.onSetActiveAddressType(targetAddressContainer, newValue['address_type']);

        if (parentFormPanel) {
            parentFormPanel.fireEvent('validitychange', {
                valid: false
            });
        }
    },

    onPostalAddressTypeBeforeRender: function(outerAddressContainer) {
        // evaluates which card should be displayed

        console.log('outerAddressContainer-beforeRender');
    },

    onPostalAddressTypeBeforeActivate: function(addressContainer) {

        console.log('addressContainer-beforeActivate');

        var routeNameTextField = addressContainer.down('routenametextfield');

        if (routeNameTextField) { // street address activated
            console.log('routeNameTextField found');

            var streetNumberTextField = addressContainer.down('streetnumbertextfield');

            console.log(routeNameTextField);
            console.log(streetNumberTextField);
            routeNameTextField.setDisabled(false);
            streetNumberTextField.setDisabled(false);

            routeNameTextField.setVisible(true);
            streetNumberTextField.setVisible(true);

        } else { // box address activated
            console.log('boxAddressTextField found');

            var boxAddressTextField = addressContainer.down('boxaddresstextfield');

            console.log(boxAddressTextField);

            boxAddressTextField.setDisabled(false);
            boxAddressTextField.setVisible(true);
        }


    },

    onPostalAddressTypeBeforeDeactivate: function(addressContainer) {

        console.log('addressContainer-beforeDeactivate');

        var routeNameTextField = addressContainer.down('routenametextfield');

        if (routeNameTextField) { // street address deactivated
            console.log('routeNameTextField found');
            var streetNumberTextField = addressContainer.down('streetnumbertextfield');

            routeNameTextField.setDisabled(true);
            routeNameTextField.setVisible(false);

            streetNumberTextField.setDisabled(true);
            streetNumberTextField.setVisible(false);

            console.log(routeNameTextField);
            console.log(streetNumberTextField);

        } else { // box address deactivated
            console.log('boxAddressTextField found');

            var boxAddressTextField = addressContainer.down('boxaddresstextfield');

            boxAddressTextField.setDisabled(true);
            boxAddressTextField.setVisible(false);

            console.log(boxAddressTextField);
        }

    }

});

/*
 * File: app/model/ContactDetails.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.ContactDetails', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: ContactDetails / ' + (new Date()).valueOf());})(),
           
                                     
                                  
                                   
                                        
      

    idProperty: 'datastore_key_id',

    fields: [
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'labels',
            type: 'auto',
            useNull: true
        },
        {
            name: 'personal_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'first_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'last_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'job_title',
            type: 'string',
            useNull: true
        },
        {
            name: 'job_organization_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'job_department_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'job_organization_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'email_addresses',
            type: 'auto',
            useNull: true
        },
        {
            name: 'phone_numbers',
            type: 'auto',
            useNull: true
        },
        {
            name: 'postal_addresses',
            type: 'auto',
            useNull: true
        },
        {
            name: 'web_addresses',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'deleted_datetime',
            type: 'date',
            useNull: true
        },
        {
            name: 'deleted',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'sys_origin_user_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_organization_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_changelog_version',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_changelog',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ],

    hasMany: [
        {
            associationKey: 'postal_addresses',
            model: 'Common.model.PostalAddress',
            primaryKey: 'datastore_key_id',
            autoLoad: true
        },
        {
            associationKey: 'web_addresses',
            model: 'Common.model.WebAddress',
            primaryKey: 'datastore_key_id',
            autoLoad: true
        }
    ],

    belongsTo: [
        {
            associationKey: 'contact_details',
            model: 'Common.model.UserAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'contacts',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        }
    ],

    hasOne: [
        {
            associationKey: 'sys_origin_user_account',
            model: 'Common.model.UserAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_origin_organization_account',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        }
    ]
});

/*
 * File: app/model/DatasetResource.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.DatasetResource', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: DatasetResource / ' + (new Date()).valueOf());})(),
           
                                                
                                                        
                                        
      

    idProperty: 'datastore_key_id',

    fields: [
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'kind',
            type: 'string',
            useNull: true
        },
        {
            name: 'etag',
            type: 'string',
            useNull: true
        },
        {
            name: 'id',
            type: 'string',
            useNull: true
        },
        {
            name: 'self_link',
            type: 'string',
            useNull: true
        },
        {
            name: 'access',
            type: 'auto',
            useNull: true
        },
        {
            name: 'dataset_reference',
            type: 'auto',
            useNull: true
        },
        {
            name: 'friendly_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'description',
            type: 'string',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'last_modified_time',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'creation_time',
            type: 'date',
            useNull: true
        },
        {
            name: 'sys_owners',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_writers',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_readers',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_user_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_organization_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_changelog_version',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_changelog',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ],

    hasOne: [
        {
            associationKey: 'dataset_reference',
            model: 'Common.model.DatasetResourceReference',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'access',
            model: 'Common.model.DatasetResourceAccessControlList'
        }
    ],

    hasMany: [
        {
            associationKey: 'sys_owners',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_writers',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_readers',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        }
    ]
});

/*
 * File: app/model/DatasetResourceAccessControlList.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.DatasetResourceAccessControlList', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: DatasetResourceAccessControlList / ' + (new Date()).valueOf());})(),
           
                                       
                                                           
      

    fields: [
        {
            name: 'access',
            type: 'string',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'Common.model.DatasetResource',
        primaryKey: 'datastore_key_id',
        foreignKey: 'access'
    },

    hasMany: {
        associationKey: 'access',
        model: 'Common.model.DatasetResourceAccessControlListItem'
    }
});

/*
 * File: app/model/DatasetResourceAccessControlListItem.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.DatasetResourceAccessControlListItem', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: DatasetResourceAccessControlListItem / ' + (new Date()).valueOf());})(),
           
                                                       
      

    fields: [
        {
            name: 'role',
            type: 'string',
            useNull: true
        },
        {
            name: 'user_by_email',
            type: 'string',
            useNull: true
        },
        {
            name: 'group_by_email',
            type: 'string',
            useNull: true
        },
        {
            name: 'domain',
            type: 'string',
            useNull: true
        },
        {
            name: 'special_group',
            type: 'string',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'Common.model.DatasetResourceAccessControlList',
        primaryKey: 'datastore_key_id',
        foreignKey: 'access'
    }
});

/*
 * File: app/model/DatasetResourceReference.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.DatasetResourceReference', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: DatasetResourceReference / ' + (new Date()).valueOf());})(),
           
                                      
      

    idProperty: 'dataset_id',

    fields: [
        {
            name: 'project_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'dataset_id',
            type: 'string',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'Common.model.DatasetResource',
        primaryKey: 'datastore_key_id',
        foreignKey: 'dataset_reference'
    }
});

/*
 * File: app/model/GenericError.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.GenericError', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: GenericError / ' + (new Date()).valueOf());})(),
           
                                         
                                              
      

    idProperty: 'datastore_key_id',

    fields: [
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'errors',
            type: 'auto',
            useNull: true
        },
        {
            name: 'code',
            type: 'int',
            useNull: true
        },
        {
            name: 'message',
            type: 'string',
            useNull: true
        },
        {
            name: 'sys_owners',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_writers',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_readers',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_user_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_organization_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_changelog_version',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_changelog',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ],

    hasMany: [
        {
            associationKey: 'sys_owners',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_writers',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_readers',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'errors',
            model: 'Common.model.GenericErrorDescription',
            primaryKey: 'datastore_key_id'
        }
    ]
});

/*
 * File: app/model/GenericErrorDescription.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.GenericErrorDescription', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: GenericErrorDescription / ' + (new Date()).valueOf());})(),
           
                                   
      

    idProperty: 'datastore_key_id',

    fields: [
        {
            name: 'domain',
            type: 'string',
            useNull: true
        },
        {
            name: 'reason',
            type: 'string',
            useNull: true
        },
        {
            name: 'message',
            type: 'string',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'Common.model.GenericError',
        primaryKey: 'datastore_key_id',
        foreignKey: 'errors'
    }
});

/*
 * File: app/model/JobConfiguration.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.JobConfiguration', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: JobConfiguration / ' + (new Date()).valueOf());})(),
           
                                   
                                            
                                               
                                            
                                            
      

    fields: [
        {
            name: 'load',
            type: 'auto',
            useNull: true
        },
        {
            name: 'extract',
            type: 'auto',
            useNull: true
        },
        {
            name: 'copy',
            type: 'auto',
            useNull: true
        },
        {
            name: 'query',
            type: 'auto',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'Common.model.JobResource',
        primaryKey: 'datastore_key_id',
        foreignKey: 'configuration'
    },

    hasOne: [
        {
            associationKey: 'load',
            model: 'Common.model.JobLoadConfiguration'
        },
        {
            associationKey: 'extract',
            model: 'Common.model.JobExtractConfiguration',
            foreignKey: ''
        },
        {
            associationKey: 'copy',
            model: 'Common.model.JobCopyConfiguration'
        },
        {
            associationKey: 'query',
            model: 'Common.model.JobQueryConfiguration'
        }
    ]
});

/*
 * File: app/model/JobCopyConfiguration.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.JobCopyConfiguration', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: JobCopyConfiguration / ' + (new Date()).valueOf());})(),
           
                                       
      

    fields: [
        {
            name: 'source_table',
            type: 'auto',
            useNull: true
        },
        {
            name: 'destination_table',
            type: 'auto',
            useNull: true
        },
        {
            name: 'create_disposition',
            type: 'string',
            useNull: true
        },
        {
            name: 'write_disposition',
            type: 'string',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'Common.model.JobConfiguration',
        foreignKey: 'copy'
    }
});

/*
 * File: app/model/JobErrorDescription.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.JobErrorDescription', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: JobErrorDescription / ' + (new Date()).valueOf());})(),
           
                                
      

    fields: [
        {
            name: 'reason',
            type: 'string',
            useNull: true
        },
        {
            name: 'location',
            type: 'string',
            useNull: true
        },
        {
            name: 'debug_info',
            type: 'string',
            useNull: true
        },
        {
            name: 'message',
            type: 'string',
            useNull: true
        }
    ],

    belongsTo: [
        {
            model: 'Common.model.JobStatus',
            foreignKey: 'error_result'
        },
        {
            associationKey: '',
            model: 'Common.model.JobStatus',
            foreignKey: 'errors'
        }
    ]
});

/*
 * File: app/model/JobExtractConfiguration.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.JobExtractConfiguration', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: JobExtractConfiguration / ' + (new Date()).valueOf());})(),
           
                                       
      

    fields: [
        {
            name: 'source_table',
            type: 'auto',
            useNull: true
        },
        {
            name: 'destination_uris',
            type: 'auto',
            useNull: true
        },
        {
            name: 'field_delimiter',
            type: 'string',
            useNull: true
        },
        {
            name: 'print_header',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'destination_format',
            type: 'string',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'Common.model.JobConfiguration',
        foreignKey: 'extract'
    }
});

/*
 * File: app/model/JobLoadConfiguration.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.JobLoadConfiguration', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: JobLoadConfiguration / ' + (new Date()).valueOf());})(),
           
                                       
      

    fields: [
        {
            name: 'source_uris',
            type: 'auto',
            useNull: true
        },
        {
            name: 'schema',
            type: 'auto',
            useNull: true
        },
        {
            name: 'destination_table',
            type: 'auto',
            useNull: true
        },
        {
            name: 'create_disposition',
            type: 'string',
            useNull: true
        },
        {
            name: 'write_disposition',
            type: 'string',
            useNull: true
        },
        {
            name: 'field_delimiter',
            type: 'string',
            useNull: true
        },
        {
            name: 'skip_leading_rows',
            type: 'int',
            useNull: true
        },
        {
            name: 'encoding',
            type: 'string',
            useNull: true
        },
        {
            name: 'quote',
            type: 'string',
            useNull: true
        },
        {
            name: 'allow_quoted_newlines',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'source_format',
            type: 'string',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'Common.model.JobConfiguration',
        foreignKey: 'load'
    }
});

/*
 * File: app/model/JobLoadStatistics.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.JobLoadStatistics', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: JobLoadStatistics / ' + (new Date()).valueOf());})(),
           
                                    
      

    fields: [
        {
            name: 'input_files',
            type: 'int',
            useNull: true
        },
        {
            name: 'input_file_bytes',
            type: 'int',
            useNull: true
        },
        {
            name: 'output_rows',
            type: 'int',
            useNull: true
        },
        {
            name: 'output_bytes',
            type: 'int',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'Common.model.JobStatistics',
        foreignKey: 'load'
    }
});

/*
 * File: app/model/JobQueryConfiguration.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.JobQueryConfiguration', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: JobQueryConfiguration / ' + (new Date()).valueOf());})(),
           
                                       
      

    fields: [
        {
            name: 'query',
            type: 'string',
            useNull: true
        },
        {
            name: 'destination_table',
            type: 'auto',
            useNull: true
        },
        {
            name: 'create_disposition',
            type: 'string',
            useNull: true
        },
        {
            name: 'write_disposition',
            type: 'string',
            useNull: true
        },
        {
            name: 'default_dataset',
            type: 'auto',
            useNull: true
        },
        {
            name: 'priority',
            type: 'string',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'Common.model.JobConfiguration',
        foreignKey: 'query'
    }
});

/*
 * File: app/model/JobQueryStatistics.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.JobQueryStatistics', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: JobQueryStatistics / ' + (new Date()).valueOf());})(),
           
                                    
      

    fields: [
        {
            name: 'total_bytes_processed',
            type: 'int',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'Common.model.JobStatistics',
        foreignKey: 'query'
    }
});

/*
 * File: app/model/JobResource.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.JobResource', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: JobResource / ' + (new Date()).valueOf());})(),
           
                                            
                                        
                                 
                                     
                                        
      

    idProperty: 'datastore_key_id',

    fields: [
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'kind',
            type: 'string',
            useNull: true
        },
        {
            name: 'etag',
            type: 'string',
            useNull: true
        },
        {
            name: 'id',
            type: 'string',
            useNull: true
        },
        {
            name: 'self_link',
            type: 'string',
            useNull: true
        },
        {
            name: 'job_reference',
            type: 'auto',
            useNull: true
        },
        {
            name: 'configuration',
            type: 'auto',
            useNull: true
        },
        {
            name: 'status',
            type: 'auto',
            useNull: true
        },
        {
            name: 'statistics',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_owners',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_writers',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_readers',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_user_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_organization_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_changelog_version',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_changelog',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ],

    hasOne: [
        {
            associationKey: 'job_reference',
            model: 'Common.model.JobResourceReference',
            primaryKey: 'job_id'
        },
        {
            associationKey: 'configuration',
            model: 'Common.model.JobConfiguration'
        },
        {
            associationKey: 'status',
            model: 'Common.model.JobStatus'
        },
        {
            associationKey: 'statistics',
            model: 'Common.model.JobStatistics'
        }
    ],

    hasMany: [
        {
            associationKey: 'sys_owners',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_writers',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_readers',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        }
    ]
});

/*
 * File: app/model/JobResourceReference.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.JobResourceReference', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: JobResourceReference / ' + (new Date()).valueOf());})(),
           
                                  
      

    idProperty: 'job_id',

    fields: [
        {
            name: 'project_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'job_id',
            type: 'string',
            useNull: false
        }
    ],

    belongsTo: {
        model: 'Common.model.JobResource',
        primaryKey: 'datastore_key_id',
        foreignKey: 'job_reference'
    }
});

/*
 * File: app/model/JobStatistics.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.JobStatistics', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: JobStatistics / ' + (new Date()).valueOf());})(),
           
                                   
                                         
                                         
      

    fields: [
        {
            name: 'query',
            type: 'auto',
            useNull: true
        },
        {
            name: 'load',
            type: 'auto',
            useNull: true
        },
        {
            name: 'total_bytes_processed',
            type: 'int',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'start_time',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'end_time',
            type: 'date',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'Common.model.JobResource',
        primaryKey: 'datastore_key_id',
        foreignKey: 'statistics'
    },

    hasOne: [
        {
            associationKey: 'load',
            model: 'Common.model.JobLoadStatistics'
        },
        {
            associationKey: 'query',
            model: 'Common.model.JobQueryStatistics'
        }
    ]
});

/*
 * File: app/model/JobStatus.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.JobStatus', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: JobStatus / ' + (new Date()).valueOf());})(),
           
                                   
                                          
      

    fields: [
        {
            name: 'state',
            type: 'string',
            useNull: true
        },
        {
            name: 'error_result',
            type: 'auto',
            useNull: true
        },
        {
            name: 'errors',
            type: 'auto',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'Common.model.JobResource',
        primaryKey: 'datastore_key_id',
        foreignKey: 'status'
    },

    hasOne: {
        associationKey: 'error_result',
        model: 'Common.model.JobErrorDescription'
    },

    hasMany: {
        associationKey: 'errors',
        model: 'Common.model.JobErrorDescription'
    }
});

/*
 * File: app/model/PostalAddress.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.PostalAddress', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: PostalAddress / ' + (new Date()).valueOf());})(),
           
                                         
                                  
      

    idProperty: 'datastore_key_id',

    fields: [
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'labels',
            type: 'auto',
            useNull: true
        },
        {
            name: 'recipient_lines',
            type: 'auto',
            useNull: true
        },
        {
            name: 'box_address',
            type: 'string',
            useNull: true
        },
        {
            name: 'route_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'street_number',
            type: 'string',
            useNull: true
        },
        {
            name: 'street_entrance',
            type: 'string',
            useNull: true
        },
        {
            name: 'apartment_number',
            type: 'string',
            useNull: true
        },
        {
            name: 'apartment_floor',
            type: 'float',
            useNull: true
        },
        {
            name: 'postal_code',
            type: 'string',
            useNull: true
        },
        {
            name: 'postal_town',
            type: 'string',
            useNull: true
        },
        {
            name: 'country',
            type: 'string',
            useNull: true
        },
        {
            name: 'formatted_address',
            type: 'string',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'deleted_datetime',
            type: 'date',
            useNull: true
        },
        {
            name: 'deleted',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'sys_origin_user_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_organization_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_changelog_version',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_changelog',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ],

    belongsTo: [
        {
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            model: 'Common.model.PostalAddress',
            primaryKey: 'datastore_key_id'
        }
    ],

    hasOne: [
        {
            associationKey: 'sys_origin_user_account',
            model: 'Common.model.UserAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_origin_organization_account',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        }
    ]
});

/*
 * File: app/model/SystemAdminAccount.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.SystemAdminAccount', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: SystemAdminAccount / ' + (new Date()).valueOf());})(),
           
                                   
                                         
                                      
                                     
                                 
      

    idProperty: 'datastore_key_id',

    fields: [
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'company_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'department_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'registration_number',
            type: 'string',
            useNull: true
        },
        {
            name: 'contact_details',
            type: 'auto',
            useNull: true
        },
        {
            name: 'postal_addresses',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'activated_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'registered_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'suspended_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'deleted_datetime',
            type: 'date',
            useNull: true
        },
        {
            name: 'suspended',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'deleted',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'activated',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'sys_origin_user_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_organization_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_changelog_version',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_changelog',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ],

    hasOne: [
        {
            associationKey: 'sys_origin_user_account',
            model: 'Common.model.UserAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_origin_organization_account',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        }
    ],

    hasMany: [
        {
            associationKey: 'contact_details',
            model: 'Common.model.ContactDetails',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'postal_addresses',
            model: 'Common.model.PostalAddress',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'web_addresses',
            model: 'Common.model.WebAddress',
            primaryKey: 'datastore_key_id'
        }
    ]
});

/*
 * File: app/model/TableDataResource.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.TableDataResource', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: TableDataResource / ' + (new Date()).valueOf());})(),
           
                                        
      

    idProperty: 'datastore_key_id',

    fields: [
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'index',
            type: 'int',
            useNull: true
        },
        {
            name: 'fields',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_owners',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_writers',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_readers',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_user_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_organization_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_changelog_version',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_changelog',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ],

    hasMany: [
        {
            associationKey: 'sys_owners',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_writers',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_readers',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        }
    ]
});

/*
 * File: app/model/TableResource.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.TableResource', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: TableResource / ' + (new Date()).valueOf());})(),
           
                                              
                                   
                                        
      

    idProperty: 'datastore_key_id',

    fields: [
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'kind',
            type: 'string',
            useNull: true
        },
        {
            name: 'etag',
            type: 'string',
            useNull: true
        },
        {
            name: 'id',
            type: 'string',
            useNull: true
        },
        {
            name: 'self_link',
            type: 'string',
            useNull: true
        },
        {
            name: 'schema',
            type: 'auto',
            useNull: true
        },
        {
            name: 'friendly_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'description',
            type: 'string',
            useNull: true
        },
        {
            name: 'num_rows',
            type: 'int',
            useNull: true
        },
        {
            name: 'num_bytes',
            type: 'int',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'last_modified_time',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'creation_time',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'expiration_time',
            type: 'date',
            useNull: true
        },
        {
            name: 'sys_owners',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_writers',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_readers',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_user_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_organization_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_changelog_version',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_changelog',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ],

    hasOne: [
        {
            associationKey: 'table_reference',
            model: 'Common.model.TableResourceReference',
            primaryKey: 'table_id'
        },
        {
            associationKey: 'schema',
            model: 'Common.model.TableSchema'
        }
    ],

    hasMany: [
        {
            associationKey: 'sys_owners',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_writers',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_readers',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        }
    ]
});

/*
 * File: app/model/TableResourceReference.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.TableResourceReference', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: TableResourceReference / ' + (new Date()).valueOf());})(),
           
                                    
      

    idProperty: 'table_id',

    fields: [
        {
            name: 'project_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'dataset_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'table_id',
            type: 'string',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'Common.model.TableResource',
        primaryKey: 'datastore_key_id',
        foreignKey: 'table_reference'
    }
});

/*
 * File: app/model/TableSchema.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.TableSchema', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: TableSchema / ' + (new Date()).valueOf());})(),
           
                                     
                                      
      

    fields: [
        {
            name: 'fields',
            type: 'auto',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'Common.model.TableResource',
        primaryKey: 'datastore_key_id',
        foreignKey: 'schema'
    },

    hasMany: {
        associationKey: 'fields',
        model: 'Common.model.TableSchemaItem'
    }
});

/*
 * File: app/model/TableSchemaItem.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.TableSchemaItem', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: TableSchemaItem / ' + (new Date()).valueOf());})(),
           
                                  
      

    fields: [
        {
            name: 'name',
            type: 'string',
            useNull: true
        },
        {
            name: 'type',
            type: 'string',
            useNull: true
        },
        {
            name: 'mode',
            type: 'string',
            useNull: true
        },
        {
            name: 'fields',
            type: 'auto',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'Common.model.TableSchema',
        foreignKey: 'fields'
    }
});

/*
 * File: app/model/WebAddress.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.WebAddress', {
    extend:  Ext.data.Model ,
    //exLoadInitialized: (function() { console.error('model: WebAddress / ' + (new Date()).valueOf());})(),
           
                                         
                                   
                                     
      

    idProperty: 'datastore_key_id',

    fields: [
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'labels',
            type: 'auto',
            useNull: true
        },
        {
            name: 'url',
            type: 'string',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'deleted_datetime',
            type: 'date',
            useNull: true
        },
        {
            name: 'deleted',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'sys_origin_user_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_organization_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_changelog_version',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_changelog',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ],

    hasOne: [
        {
            associationKey: 'sys_origin_user_account',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_origin_user_account',
            model: 'Common.model.UserAccount',
            primaryKey: 'datastore_key_id'
        }
    ],

    belongsTo: [
        {
            model: 'Common.model.ContactDetails',
            primaryKey: 'datastore_key_id'
        },
        {
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        }
    ]
});

/*
 * File: app/store/ApartmentFloors.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.store.ApartmentFloors', {
    extend:  Ext.data.Store ,
    //exLoadInitialized: (function() { console.error('storeId: apartmentFloors / ' + (new Date()).valueOf());})(),
    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoLoad: true,
            storeId: 'apartmentFloors',
            data: [
                {
                    name: '',
                    value: ''
                },
                {
                    name: 'N.B.',
                    value: 'N.B.'
                },
                {
                    name: '1 tr.',
                    value: '1 tr.'
                },
                {
                    name: '2 tr.',
                    value: '2 tr.'
                },
                {
                    name: '3 tr.',
                    value: '3 tr.'
                },
                {
                    name: '4 tr.',
                    value: '4 tr.'
                },
                {
                    name: '5 tr.',
                    value: '5 tr.'
                },
                {
                    name: '6 tr.',
                    value: '6 tr.'
                },
                {
                    name: '7 tr.',
                    value: '7 tr.'
                },
                {
                    name: '8 tr.',
                    value: '8 tr.'
                },
                {
                    name: '9 tr.',
                    value: '9 tr.'
                },
                {
                    name: '10 tr.',
                    value: '10 tr.'
                },
                {
                    name: '11 tr.',
                    value: '11 tr.'
                },
                {
                    name: '12 tr.',
                    value: '12 tr.'
                }
            ],
            fields: [
                {
                    name: 'name',
                    type: 'string'
                },
                {
                    name: 'value',
                    type: 'string'
                }
            ]
        }, cfg)]);
    }
});

/*
 * File: app/store/ContactDetails.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.store.ContactDetails', {
    extend:  Ext.data.Store ,
    //exLoadInitialized: (function() { console.error('storeId: contactDetails / ' + (new Date()).valueOf());})(),
               
                                     
      

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'Common.model.ContactDetails',
            storeId: 'contactDetails',
            buffered: true,
            proxy: {
                type: 'direct',
                directionParam: 'order_direction',
                extraParams: {
                    kind: 'ContactDetails'
                },
                filterParam: 'filter_by',
                groupParam: 'group_by',
                limitParam: 'page_size',
                sortParam: 'order_by',
                startParam: 'offset',
                directFn: Remote.UserCmd.grid_data,
                reader: {
                    type: 'json',
                    idProperty: 'datastore_key_id',
                    root: 'data'
                },
                writer: {
                    type: 'json',
                    nameProperty: 'datastore_key_id',
                    allowSingle: false,
                    root: 'data'
                }
            }
        }, cfg)]);
    }
});

/*
 * File: app/store/DatasetResources.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.store.DatasetResources', {
    extend:  Ext.data.Store ,
    //exLoadInitialized: (function() { console.error('storeId: datasetResources / ' + (new Date()).valueOf());})(),
               
                                      
      

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'Common.model.DatasetResource',
            storeId: 'datasetResources',
            buffered: true,
            proxy: {
                type: 'direct',
                directionParam: 'order_direction',
                extraParams: {
                    kind: 'DatasetResource'
                },
                filterParam: 'filter_by',
                groupParam: 'group_by',
                limitParam: 'page_size',
                sortParam: 'order_by',
                startParam: 'offset',
                directFn: Remote.UserCmd.grid_data,
                reader: {
                    type: 'json',
                    root: 'data',
                    record: 'datastore_key_id'
                },
                writer: {
                    type: 'json',
                    nameProperty: 'datastore_key_id',
                    allowSingle: false,
                    root: 'data'
                }
            }
        }, cfg)]);
    }
});

/*
 * File: app/store/EmptySet.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.store.EmptySet', {
    extend:  Ext.data.Store ,
    //exLoadInitialized: (function() { console.error('storeId: emptySet / ' + (new Date()).valueOf());})(),
    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'emptySet',
            data: [

            ],
            leadingBufferZone: 0,
            trailingBufferZone: 0,
            fields: [
                {
                    name: 'name',
                    type: 'string'
                },
                {
                    name: 'value',
                    type: 'string'
                }
            ]
        }, cfg)]);
    }
});

/*
 * File: app/store/GenericErrors.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.store.GenericErrors', {
    extend:  Ext.data.Store ,
    //exLoadInitialized: (function() { console.error('storeId: genericErrors / ' + (new Date()).valueOf());})(),
               
                                   
      

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'Common.model.GenericError',
            storeId: 'genericErrors',
            buffered: true,
            proxy: {
                type: 'direct',
                directionParam: 'order_direction',
                extraParams: {
                    kind: 'GenericError'
                },
                filterParam: 'filter_by',
                groupParam: 'group_by',
                limitParam: 'page_size',
                sortParam: 'order_by',
                startParam: 'offset',
                directFn: Remote.UserCmd.grid_data,
                reader: {
                    type: 'json',
                    root: 'data',
                    record: 'datastore_key_id'
                },
                writer: {
                    type: 'json',
                    nameProperty: 'datastore_key_id',
                    allowSingle: false,
                    root: 'data'
                }
            }
        }, cfg)]);
    }
});

/*
 * File: app/store/JobResources.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.store.JobResources', {
    extend:  Ext.data.Store ,
    //exLoadInitialized: (function() { console.error('storeId: jobResources / ' + (new Date()).valueOf());})(),

               
                                  
      

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'Common.model.JobResource',
            storeId: 'jobResources',
            buffered: true,
            proxy: {
                type: 'direct',
                directionParam: 'order_direction',
                extraParams: {
                    kind: 'JobResource'
                },
                filterParam: 'filter_by',
                groupParam: 'group_by',
                limitParam: 'page_size',
                sortParam: 'order_by',
                startParam: 'offset',
                directFn: Remote.UserCmd.grid_data,
                reader: {
                    type: 'json',
                    root: 'data',
                    record: 'datastore_key_id'
                },
                writer: {
                    type: 'json',
                    nameProperty: 'datastore_key_id',
                    allowSingle: false,
                    root: 'data'
                }
            }
        }, cfg)]);
    }
});

/*
 * File: app/store/MyArrayStore.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.store.MyArrayStore', {
    extend:  Ext.data.Store ,
    //exLoadInitialized: (function() { console.error('storeId: myArrayStore / ' + (new Date()).valueOf());})(),
    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'MyArrayStore',
            data: [
                {
                    name: 'CapitexRawDataEntry',
                    value: 'CapitexRawDataEntry'
                },
                {
                    name: 'SfdRawDataEntry',
                    value: 'SfdRawDataEntry'
                },
                {
                    name: 'ScbRawDataEntry',
                    value: 'ScbRawDataEntry'
                },
                {
                    name: 'TransactionCompositeEntry',
                    value: 'TransactionCompositeEntry'
                },
                {
                    name: 'SfdRawDataStats',
                    value: 'SfdRawDataStats'
                },
                {
                    name: 'CapitexRawDataStats',
                    value: 'CapitexRawDataStats'
                },
                {
                    name: 'ScbRawDataStats',
                    value: 'ScbRawDataStats'
                },
                {
                    name: 'TransactionCompositeStats',
                    value: 'TransactionCompositeStats'
                }
            ],
            fields: [
                {
                    name: 'name'
                },
                {
                    name: 'value'
                }
            ]
        }, cfg)]);
    }
});

/*
 * File: app/store/PostalAddresses.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.store.PostalAddresses', {
    extend:  Ext.data.Store ,
    //exLoadInitialized: (function() { console.error('storeId: postalAddresses / ' + (new Date()).valueOf());})(),
               
                                    
      

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'Common.model.PostalAddress',
            storeId: 'postalAddresses',
            buffered: true,
            proxy: {
                type: 'direct',
                directionParam: 'order_direction',
                extraParams: {
                    kind: 'PostalAddress'
                },
                filterParam: 'filter_by',
                groupParam: 'group_by',
                limitParam: 'page_size',
                sortParam: 'order_by',
                startParam: 'offset',
                directFn: Remote.UserCmd.grid_data,
                reader: {
                    type: 'json',
                    idProperty: 'datastore_key_id',
                    root: 'data'
                },
                writer: {
                    type: 'json',
                    nameProperty: 'datastore_key_id',
                    allowSingle: false,
                    root: 'data'
                }
            }
        }, cfg)]);
    }
});

/*
 * File: app/store/SystemAdminAccounts.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.store.SystemAdminAccounts', {
    extend:  Ext.data.Store ,
    //exLoadInitialized: (function() { console.error('storeId: systemAdminAccounts / ' + (new Date()).valueOf());})(),
               
                                        
      

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'Common.model.SubscriberAccount',
            storeId: 'systemAdminAccounts',
            buffered: true,
            proxy: {
                type: 'direct',
                directionParam: 'order_direction',
                extraParams: {
                    kind: 'SystemAdminAccount'
                },
                filterParam: 'filter_by',
                groupParam: 'group_by',
                limitParam: 'page_size',
                sortParam: 'order_by',
                startParam: 'offset',
                directFn: Remote.UserCmd.grid_data,
                reader: {
                    type: 'json',
                    idProperty: 'datastore_key_id',
                    root: 'data'
                },
                writer: {
                    type: 'json',
                    nameProperty: 'datastore_key_id',
                    allowSingle: false,
                    root: 'data'
                }
            }
        }, cfg)]);
    }
});

/*
 * File: app/store/TableDataResources.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.store.TableDataResources', {
    extend:  Ext.data.Store ,
    //exLoadInitialized: (function() { console.error('storeId: tableDataResources / ' + (new Date()).valueOf());})(),
               
                                        
      

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'Common.model.TableDataResource',
            storeId: 'tableDataResources',
            buffered: true,
            proxy: {
                type: 'direct',
                directionParam: 'order_direction',
                extraParams: {
                    kind: 'TableDataResource'
                },
                filterParam: 'filter_by',
                groupParam: 'group_by',
                limitParam: 'page_size',
                sortParam: 'order_by',
                startParam: 'offset',
                directFn: Remote.UserCmd.grid_data,
                reader: {
                    type: 'json',
                    root: 'data',
                    record: 'datastore_key_id'
                },
                writer: {
                    type: 'json',
                    nameProperty: 'datastore_key_id',
                    allowSingle: false,
                    root: 'data'
                }
            }
        }, cfg)]);
    }
});

/*
 * File: app/store/TableResources.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.store.TableResources', {
    extend:  Ext.data.Store ,
    //exLoadInitialized: (function() { console.error('storeId: tableResources / ' + (new Date()).valueOf());})(),
               
                                    
      

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'Common.model.TableResource',
            storeId: 'tableResources',
            buffered: true,
            proxy: {
                type: 'direct',
                directionParam: 'order_direction',
                extraParams: {
                    kind: 'TableResource'
                },
                filterParam: 'filter_by',
                groupParam: 'group_by',
                limitParam: 'page_size',
                sortParam: 'order_by',
                startParam: 'offset',
                directFn: Remote.UserCmd.grid_data,
                reader: {
                    type: 'json',
                    root: 'data',
                    record: 'datastore_key_id'
                },
                writer: {
                    type: 'json',
                    nameProperty: 'datastore_key_id',
                    allowSingle: false,
                    root: 'data'
                }
            }
        }, cfg)]);
    }
});

/*
 * File: app/store/WebAddresses.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.store.WebAddresses', {
    extend:  Ext.data.Store ,
    //exLoadInitialized: (function() { console.error('storeId: webAddresses / ' + (new Date()).valueOf());})(),
               
                                 
      

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'Common.model.WebAddress',
            storeId: 'webAddresses',
            buffered: true,
            proxy: {
                type: 'direct',
                directionParam: 'order_direction',
                extraParams: {
                    kind: 'WebAddress'
                },
                filterParam: 'filter_by',
                groupParam: 'group_by',
                limitParam: 'page_size',
                sortParam: 'order_by',
                startParam: 'offset',
                directFn: Remote.UserCmd.grid_data,
                reader: {
                    type: 'json',
                    idProperty: 'datastore_key_id',
                    root: 'data'
                },
                writer: {
                    type: 'json',
                    nameProperty: 'datastore_key_id',
                    allowSingle: false,
                    root: 'data'
                }
            }
        }, cfg)]);
    }
});

/*
 * File: app/view/GenericActionButton.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.button.GenericActionButton', {
    extend:  Ext.button.Button ,
    alias: 'widget.genericactionbutton',
    //exLoadInitialized: (function() { console.error('xtype: genericactionbutton / ' + (new Date()).valueOf());})(),
    height: 30,
    margin: '0 10 10 10',
    minWidth: 150,
    arrowAlign: 'bottom',
    iconAlign: 'bottom',
    text: 'Action',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});

/*
 * File: app/view/GenericToolbarLoginButton.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.button.GenericToolbarLoginButton', {
    extend:  Ext.button.Button ,
    alias: 'widget.generictoolbarloginbutton',
    //exLoadInitialized: (function() { console.error('xtype: generictoolbarloginbutton / ' + (new Date()).valueOf());})(),
    /*height: 30,
    margin: '0 10 10 10',
    minWidth: 150,
    arrowAlign: 'bottom',
    iconAlign: 'bottom',*/
    text: 'Login',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});

/*
 * File: app/view/GenericToolbarLogoutButton.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.button.GenericToolbarLogoutButton', {
    extend:  Ext.button.Button ,
    alias: 'widget.generictoolbarlogoutbutton',
    //exLoadInitialized: (function() { console.error('xtype: generictoolbarlogoutbutton / ' + (new Date()).valueOf());})(),
    /*height: 30,
    margin: '0 10 10 10',
    minWidth: 150,
    arrowAlign: 'bottom',
    iconAlign: 'bottom',*/
    text: 'Logout',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});

/*
 * File: app/view/GenericCheckbox.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.GenericCheckbox', {
    extend:  Ext.form.field.Checkbox ,
    alias: 'widget.genericcheckbox',
    //exLoadInitialized: (function() { console.error('xtype: genericcheckbox / ' + (new Date()).valueOf());})(),
    margin: '0 0 10 0',
    fieldLabel: '',
    boxLabel: 'Generic Checkbox',
    inputValue: 'true',
    uncheckedValue: 'false',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});

/*
 * File: app/view/GenericNumberField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.GenericNumberField', {
    extend:  Ext.form.field.Number ,
    alias: 'widget.genericnumberfield',
    //exLoadInitialized: (function() { console.error('xtype: genericnumberfield / ' + (new Date()).valueOf());})(),
    margin: '0 0 10 0',
    width: 315,
    fieldLabel: 'GenericNumber',
    labelPad: 0,
    labelSeparator: ' ',
    labelWidth: 135,
    name: 'api_version',
    allowBlank: false,
    allowDecimals: false,

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});

/*
 * File: app/view/RegistrationNumberTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.RegistrationNumberTextField', {
    extend:  Ext.form.field.Text ,
    alias: 'widget.registrationnumbertextfield',
    //exLoadInitialized: (function() { console.error('xtype: registrationnumbertextfield / ' + (new Date()).valueOf());})(),
    fieldLabel: 'Registration Number',
    labelSeparator: ' ',
    labelWidth: 125,

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});

/*
 * File: app/view/WebAddressTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.WebAddressTextField', {
    extend:  Ext.form.field.Text ,
    alias: 'widget.webaddresstextfield',
    //exLoadInitialized: (function() { console.error('xtype: webaddresstextfield / ' + (new Date()).valueOf());})(),
    margin: '0 0 10 0',
    fieldLabel: 'Web Address',
    labelWidth: 125,

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});

/*
 * File: app/view/GridPanelRefreshTool.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.panel.GridPanelRefreshTool', {
    extend:  Ext.panel.Tool ,
    alias: 'widget.gridpanelrefreshtool',
    //exLoadInitialized: (function() { console.error('xtype: gridpanelrefreshtool / ' + (new Date()).valueOf());})(),
    type: 'refresh',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            listeners: {
                click: {
                    fn: me.onToolClick,
                    scope: me
                }
            }
        });

        me.callParent(arguments);
    },

    onToolClick: function(tool, e, eOpts) {


        console.log('yah, refresh \'em');

        var gridPanel = tool.up('gridpanel');

        console.log(gridPanel);

        var store = gridPanel.getStore();

        console.log('pre-(re)load store:');
        console.log(store);

        if (store.getCount()) {
            store.reload();
        } else {
            store.prefetch({limit: 100});
            store.load();
        }

        //store.reload();

        console.log('post-(re)load store:');
        console.log(store);

        if (!window.mstatDebug) {
            window.mstatDebug = {};
        }

        window.mstatDebug.refreshGridPanel = gridPanel;

    }

});

/*
 * File: app/view/ContactDetailsGridPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.grid.ContactDetailsGridPanel', {
    extend:  Ext.grid.Panel ,
    alias: 'widget.contactdetailsgridpanel',
    //exLoadInitialized: (function() { console.error('xtype: contactdetailsgridpanel / ' + (new Date()).valueOf());})(),
               
                                                
      

    title: 'ContactDetails',
    columnLines: true,
    store: 'ContactDetails',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'rownumberer'
                },
                {
                    xtype: 'gridcolumn',
                    width: 110,
                    dataIndex: 'datastore_key_id',
                    text: 'datastore_key_id'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 135,
                    dataIndex: 'datastore_key_urlsafe',
                    text: 'datastore_key_urlsafe'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'labels',
                    text: 'labels'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'personal_id',
                    text: 'personal_id'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'first_name',
                    text: 'first_name'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'last_name',
                    text: 'last_name'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'job_title',
                    text: 'job_title'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'job_organization_name',
                    text: 'job_organization_name'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'job_department_name',
                    text: 'job_department_name'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'job_organization_id',
                    text: 'job_organization_id'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'email_addresses',
                    text: 'email_addresses'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'phone_numbers',
                    text: 'phone_numbers'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'postal_addresses',
                    text: 'postal_addresses'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'web_addresses',
                    text: 'web_addresses'
                },
                {
                    xtype: 'datecolumn',
                    width: 125,
                    dataIndex: 'deleted_datetime',
                    text: 'deleted_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'booleancolumn',
                    dataIndex: 'deleted',
                    text: 'deleted'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_origin_user_account',
                    text: 'sys_origin_user_account'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_origin_organization_account',
                    text: 'sys_origin_organization_account'
                },
                {
                    xtype: 'numbercolumn',
                    hidden: true,
                    width: 140,
                    dataIndex: 'sys_changelog_version',
                    text: 'sys_changelog_version',
                    format: '000'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    dataIndex: 'sys_changelog',
                    text: 'sys_changelog'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_modified',
                    text: 'sys_modified',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_created',
                    text: 'sys_created',
                    format: 'Y-m-d H:i:s'
                }
            ],
            tools: [
                {
                    xtype: 'gridpanelrefreshtool'
                }
            ]
        });

        me.callParent(arguments);
    }

});

/*
 * File: app/view/DatasetResourcesGridPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.grid.DatasetResourcesGridPanel', {
    extend:  Ext.grid.Panel ,
    alias: 'widget.datasetresourcesgridpanel',
    //exLoadInitialized: (function() { console.error('xtype: datasetresourcesgridpanel / ' + (new Date()).valueOf());})(),
               
                                                
      

    title: 'DatasetResources',
    columnLines: true,
    store: 'DatasetResources',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'rownumberer'
                },
                {
                    xtype: 'gridcolumn',
                    width: 110,
                    dataIndex: 'datastore_key_id',
                    text: 'datastore_key_id'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 135,
                    dataIndex: 'datastore_key_urlsafe',
                    text: 'datastore_key_urlsafe'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'kind',
                    text: 'kind'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'etag',
                    text: 'etag'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'id',
                    text: 'id'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'self_link',
                    text: 'self_link'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'access',
                    text: 'access'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'dataset_reference',
                    text: 'dataset_reference'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'friendly_name',
                    text: 'friendly_name'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'description',
                    text: 'description'
                },
                {
                    xtype: 'datecolumn',
                    width: 125,
                    dataIndex: 'last_modified_time',
                    text: 'last_modified_time',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    width: 125,
                    dataIndex: 'creation_time',
                    text: 'creation_time',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_owners',
                    text: 'sys_owners'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_writers',
                    text: 'sys_writers'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_readers',
                    text: 'sys_readers'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_origin_user_account',
                    text: 'sys_origin_user_account'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_origin_organization_account',
                    text: 'sys_origin_organization_account'
                },
                {
                    xtype: 'numbercolumn',
                    hidden: true,
                    width: 140,
                    dataIndex: 'sys_changelog_version',
                    text: 'sys_changelog_version',
                    format: '000'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    dataIndex: 'sys_changelog',
                    text: 'sys_changelog'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_modified',
                    text: 'sys_modified',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_created',
                    text: 'sys_created',
                    format: 'Y-m-d H:i:s'
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top'
                }
            ],
            tools: [
                {
                    xtype: 'gridpanelrefreshtool'
                }
            ]
        });

        me.callParent(arguments);
    }

});

/*
 * File: app/view/GenericErrorsGridPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.grid.GenericErrorsGridPanel', {
    extend:  Ext.grid.Panel ,
    alias: 'widget.genericerrorsgridpanel',
    //exLoadInitialized: (function() { console.error('xtype: genericerrorsgridpanel / ' + (new Date()).valueOf());})(),
               
                                                
      

    title: 'GenericErrors',
    columnLines: true,
    store: 'GenericErrors',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'rownumberer'
                },
                {
                    xtype: 'gridcolumn',
                    width: 110,
                    dataIndex: 'datastore_key_id',
                    text: 'datastore_key_id'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 135,
                    dataIndex: 'datastore_key_urlsafe',
                    text: 'datastore_key_urlsafe'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'errors',
                    text: 'errors'
                },
                {
                    xtype: 'numbercolumn',
                    dataIndex: 'code',
                    text: 'code',
                    format: '000'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'message',
                    text: 'message'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_owners',
                    text: 'sys_owners'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_writers',
                    text: 'sys_writers'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_readers',
                    text: 'sys_readers'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_origin_user_account',
                    text: 'sys_origin_user_account'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_origin_organization_account',
                    text: 'sys_origin_organization_account'
                },
                {
                    xtype: 'numbercolumn',
                    hidden: true,
                    width: 140,
                    dataIndex: 'sys_changelog_version',
                    text: 'sys_changelog_version',
                    format: '000'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    dataIndex: 'sys_changelog',
                    text: 'sys_changelog'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_modified',
                    text: 'sys_modified',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_created',
                    text: 'sys_created',
                    format: 'Y-m-d H:i:s'
                }
            ],
            tools: [
                {
                    xtype: 'gridpanelrefreshtool'
                }
            ]
        });

        me.callParent(arguments);
    }

});

/*
 * File: app/view/JobResourcesGridPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.grid.JobResourcesGridPanel', {
    extend:  Ext.grid.Panel ,
    alias: 'widget.jobresourcesgridpanel',
    //exLoadInitialized: (function() { console.error('xtype: jobresourcesgridpanel / ' + (new Date()).valueOf());})(),
               
                                                
      

    title: 'JobResources',
    columnLines: true,
    store: 'JobResources',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'rownumberer'
                },
                {
                    xtype: 'gridcolumn',
                    width: 110,
                    dataIndex: 'datastore_key_id',
                    text: 'datastore_key_id'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 135,
                    dataIndex: 'datastore_key_urlsafe',
                    text: 'datastore_key_urlsafe'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'labels',
                    text: 'labels'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'url',
                    text: 'url'
                },
                {
                    xtype: 'datecolumn',
                    width: 125,
                    dataIndex: 'deleted_datetime',
                    text: 'deleted_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'booleancolumn',
                    dataIndex: 'deleted',
                    text: 'deleted'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_origin_user_account',
                    text: 'sys_origin_user_account'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_origin_organization_account',
                    text: 'sys_origin_organization_account'
                },
                {
                    xtype: 'numbercolumn',
                    hidden: true,
                    width: 140,
                    dataIndex: 'sys_changelog_version',
                    text: 'sys_changelog_version',
                    format: '000'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    dataIndex: 'sys_changelog',
                    text: 'sys_changelog'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_modified',
                    text: 'sys_modified',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_created',
                    text: 'sys_created',
                    format: 'Y-m-d H:i:s'
                }
            ],
            tools: [
                {
                    xtype: 'gridpanelrefreshtool'
                }
            ]
        });

        me.callParent(arguments);
    }

});

/*
 * File: app/view/PostalAddressesGridPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.grid.PostalAddressesGridPanel', {
    extend:  Ext.grid.Panel ,
    alias: 'widget.postaladdressesgridpanel',
    //exLoadInitialized: (function() { console.error('xtype: postaladdressesgridpanel / ' + (new Date()).valueOf());})(),
               
                                                
      

    title: 'PostalAddresses',
    columnLines: true,
    store: 'PostalAddresses',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'rownumberer'
                },
                {
                    xtype: 'gridcolumn',
                    width: 110,
                    dataIndex: 'datastore_key_id',
                    text: 'datastore_key_id'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 135,
                    dataIndex: 'datastore_key_urlsafe',
                    text: 'datastore_key_urlsafe'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'labels',
                    text: 'labels'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'recipient_lines',
                    text: 'recipient_lines'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'box_address',
                    text: 'box_address'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'route_name',
                    text: 'route_name'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'street_number',
                    text: 'street_number'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'street_entrance',
                    text: 'street_entrance'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'apartment_number',
                    text: 'apartment_number'
                },
                {
                    xtype: 'numbercolumn',
                    dataIndex: 'apartment_floor',
                    text: 'apartment_floor',
                    format: '000'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'postal_code',
                    text: 'postal_code'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'country',
                    text: 'postal_town'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'country',
                    text: 'country'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'formatted_address',
                    text: 'formatted_address'
                },
                {
                    xtype: 'datecolumn',
                    width: 125,
                    dataIndex: 'deleted_datetime',
                    text: 'deleted_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'booleancolumn',
                    dataIndex: 'deleted',
                    text: 'deleted'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_origin_user_account',
                    text: 'sys_origin_user_account'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_origin_organization_account',
                    text: 'sys_origin_organization_account'
                },
                {
                    xtype: 'numbercolumn',
                    hidden: true,
                    width: 140,
                    dataIndex: 'sys_changelog_version',
                    text: 'sys_changelog_version',
                    format: '000'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    dataIndex: 'sys_changelog',
                    text: 'sys_changelog'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_modified',
                    text: 'sys_modified',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_created',
                    text: 'sys_created',
                    format: 'Y-m-d H:i:s'
                }
            ],
            tools: [
                {
                    xtype: 'gridpanelrefreshtool'
                }
            ]
        });

        me.callParent(arguments);
    }

});

/*
 * File: app/view/SubscriberAccountsGridPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.grid.SubscriberAccountsGridPanel', {
    extend:  Ext.grid.Panel ,
    alias: 'widget.subscriberaccountsgridpanel',
    //exLoadInitialized: (function() { console.error('xtype: subscriberaccountsgridpanel / ' + (new Date()).valueOf());})(),
               
                                                
      

    title: 'SubscriberAccounts',
    columnLines: true,
    store: 'SubscriberAccounts',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'rownumberer'
                },
                {
                    xtype: 'gridcolumn',
                    width: 110,
                    dataIndex: 'datastore_key_id',
                    text: 'datastore_key_id'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 135,
                    dataIndex: 'datastore_key_urlsafe',
                    text: 'datastore_key_urlsafe'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'organization_name',
                    text: 'company_name'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'department_name',
                    text: 'department_name'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'organization_id',
                    text: 'registration_number'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'contact_details',
                    text: 'contact_details'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'postal_addresses',
                    text: 'postal_addresses'
                },
                {
                    xtype: 'datecolumn',
                    width: 125,
                    dataIndex: 'activated_datetime',
                    text: 'activated_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    width: 125,
                    dataIndex: 'registered_datetime',
                    text: 'registered_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'suspended_datetime',
                    text: 'suspended_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'deleted_datetime',
                    text: 'deleted_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'booleancolumn',
                    width: 80,
                    dataIndex: 'suspended',
                    text: 'suspended'
                },
                {
                    xtype: 'booleancolumn',
                    hidden: true,
                    width: 65,
                    dataIndex: 'deleted',
                    text: 'deleted'
                },
                {
                    xtype: 'booleancolumn',
                    hidden: true,
                    width: 70,
                    dataIndex: 'activated',
                    text: 'activated'
                },
                {
                    xtype: 'numbercolumn',
                    dataIndex: 'billing_balance',
                    text: 'billing_balance'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 150,
                    dataIndex: 'sys_origin_user_account',
                    text: 'sys_origin_user_account'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 190,
                    dataIndex: 'sys_origin_organization_account',
                    text: 'sys_origin_organization_account'
                },
                {
                    xtype: 'numbercolumn',
                    hidden: true,
                    width: 140,
                    dataIndex: 'sys_changelog_version',
                    text: 'sys_changelog_version',
                    format: '000'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    dataIndex: 'sys_changelog',
                    text: 'sys_changelog'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_modified',
                    text: 'sys_modified',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_created',
                    text: 'sys_created',
                    format: 'Y-m-d H:i:s'
                }
            ],
            tools: [
                {
                    xtype: 'gridpanelrefreshtool'
                }
            ]
        });

        me.callParent(arguments);
    }

});

/*
 * File: app/view/SystemAdminAccountsGridPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.grid.SystemAdminAccountsGridPanel', {
    extend:  Ext.grid.Panel ,
    alias: 'widget.systemadminaccountsgridpanel',
    //exLoadInitialized: (function() { console.error('xtype: systemadminaccountsgridpanel / ' + (new Date()).valueOf());})(),
               
                                                
      

    title: 'SystemAdminAccounts',
    columnLines: true,
    store: 'SystemAdminAccounts',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'rownumberer'
                },
                {
                    xtype: 'gridcolumn',
                    width: 110,
                    dataIndex: 'datastore_key_id',
                    text: 'datastore_key_id'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 135,
                    dataIndex: 'datastore_key_urlsafe',
                    text: 'datastore_key_urlsafe'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'organization_name',
                    text: 'company_name'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'department_name',
                    text: 'department_name'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'organization_id',
                    text: 'registration_number'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'contact_details',
                    text: 'contact_details'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'postal_addresses',
                    text: 'postal_addresses'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'activated_datetime',
                    text: 'activated_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    width: 125,
                    dataIndex: 'registered_datetime',
                    text: 'registered_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'suspended_datetime',
                    text: 'suspended_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'deleted_datetime',
                    text: 'deleted_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'booleancolumn',
                    width: 80,
                    dataIndex: 'suspended',
                    text: 'suspended'
                },
                {
                    xtype: 'booleancolumn',
                    hidden: true,
                    width: 65,
                    dataIndex: 'deleted',
                    text: 'deleted'
                },
                {
                    xtype: 'booleancolumn',
                    hidden: true,
                    width: 70,
                    dataIndex: 'activated',
                    text: 'activated'
                },
                {
                    xtype: 'gridcolumn',
                    width: 150,
                    dataIndex: 'sys_origin_user_account',
                    text: 'sys_origin_user_account'
                },
                {
                    xtype: 'gridcolumn',
                    width: 190,
                    dataIndex: 'sys_origin_organization_account',
                    text: 'sys_origin_organization_account'
                },
                {
                    xtype: 'numbercolumn',
                    hidden: true,
                    width: 140,
                    dataIndex: 'sys_changelog_version',
                    text: 'sys_changelog_version',
                    format: '000'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    dataIndex: 'sys_changelog',
                    text: 'sys_changelog'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_modified',
                    text: 'sys_modified',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_created',
                    text: 'sys_created',
                    format: 'Y-m-d H:i:s'
                }
            ],
            tools: [
                {
                    xtype: 'gridpanelrefreshtool'
                }
            ]
        });

        me.callParent(arguments);
    }

});

/*
 * File: app/view/TableResourcesGridPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.grid.TableResourcesGridPanel', {
    extend:  Ext.grid.Panel ,
    alias: 'widget.tableresourcesgridpanel',
    //exLoadInitialized: (function() { console.error('xtype: tableresourcesgridpanel / ' + (new Date()).valueOf());})(),
               
                                                
      

    title: 'TableResources',
    columnLines: true,
    store: 'TableResources',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'rownumberer'
                },
                {
                    xtype: 'gridcolumn',
                    width: 110,
                    dataIndex: 'datastore_key_id',
                    text: 'datastore_key_id'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 135,
                    dataIndex: 'datastore_key_urlsafe',
                    text: 'datastore_key_urlsafe'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'kind',
                    text: 'kind'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'etag',
                    text: 'etag'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'id',
                    text: 'id'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'self_link',
                    text: 'self_link'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'schema',
                    text: 'schema'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'friendly_name',
                    text: 'friendly_name'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'description',
                    text: 'description'
                },
                {
                    xtype: 'numbercolumn',
                    dataIndex: 'num_rows',
                    text: 'num_rows',
                    format: '0,000'
                },
                {
                    xtype: 'numbercolumn',
                    dataIndex: 'num_bytes',
                    text: 'num_bytes',
                    format: '0,000'
                },
                {
                    xtype: 'datecolumn',
                    width: 125,
                    dataIndex: 'last_modified_time',
                    text: 'last_modified_time',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    width: 125,
                    dataIndex: 'creation_time',
                    text: 'creation_time',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    width: 125,
                    dataIndex: 'expiration_time',
                    text: 'expiration_time',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_owners',
                    text: 'sys_owners'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_writers',
                    text: 'sys_writers'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_readers',
                    text: 'sys_readers'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_origin_user_account',
                    text: 'sys_origin_user_account'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_origin_organization_account',
                    text: 'sys_origin_organization_account'
                },
                {
                    xtype: 'numbercolumn',
                    hidden: true,
                    width: 140,
                    dataIndex: 'sys_changelog_version',
                    text: 'sys_changelog_version',
                    format: '000'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    dataIndex: 'sys_changelog',
                    text: 'sys_changelog'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_modified',
                    text: 'sys_modified',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_created',
                    text: 'sys_created',
                    format: 'Y-m-d H:i:s'
                }
            ],
            tools: [
                {
                    xtype: 'gridpanelrefreshtool'
                }
            ]
        });

        me.callParent(arguments);
    }

});

/*
 * File: app/view/UserAccountsGridPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.grid.UserAccountsGridPanel', {
    extend:  Ext.grid.Panel ,
    alias: 'widget.useraccountsgridpanel',
    //exLoadInitialized: (function() { console.error('xtype: useraccountsgridpanel / ' + (new Date()).valueOf());})(),
               
                                                
      

    title: 'UserAccounts',
    columnLines: true,
    store: 'UserAccounts',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'rownumberer'
                },
                {
                    xtype: 'gridcolumn',
                    width: 110,
                    dataIndex: 'datastore_key_id',
                    text: 'datastore_key_id'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 135,
                    dataIndex: 'datastore_key_urlsafe',
                    text: 'datastore_key_urlsafe'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 160,
                    dataIndex: 'datastore_ancestor_key_id',
                    text: 'datastore_ancestor_key_id'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 185,
                    dataIndex: 'datastore_ancestor_key_urlsafe',
                    text: 'datastore_ancestor_key_urlsafe'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'user_email',
                    text: 'user_email'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 60,
                    dataIndex: 'user_id',
                    text: 'user_id'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    dataIndex: 'google_user_id',
                    text: 'google_user_id'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 85,
                    dataIndex: 'google_user',
                    text: 'google_user'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 75,
                    dataIndex: 'password',
                    text: 'password'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'contact_details',
                    text: 'contact_details'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'api_access_key',
                    text: 'api_access_key'
                },
                {
                    xtype: 'datecolumn',
                    width: 125,
                    dataIndex: 'registered_datetime',
                    text: 'registered_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    width: 125,
                    dataIndex: 'activated_datetime',
                    text: 'activated_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'suspended_datetime',
                    text: 'suspended_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'deleted_datetime',
                    text: 'deleted_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 170,
                    dataIndex: 'password_changed_datetime',
                    text: 'password_changed_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'booleancolumn',
                    width: 80,
                    dataIndex: 'suspended',
                    text: 'suspended'
                },
                {
                    xtype: 'booleancolumn',
                    hidden: true,
                    width: 65,
                    dataIndex: 'deleted',
                    text: 'deleted'
                },
                {
                    xtype: 'booleancolumn',
                    hidden: true,
                    width: 70,
                    dataIndex: 'activated',
                    text: 'activated'
                },
                {
                    xtype: 'booleancolumn',
                    width: 95,
                    dataIndex: 'administrator',
                    text: 'administrator'
                },
                {
                    xtype: 'datecolumn',
                    width: 125,
                    dataIndex: 'last_visit_datetime',
                    text: 'last_visit_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 90,
                    dataIndex: 'last_client_ip',
                    text: 'last_client_ip'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 90,
                    dataIndex: 'sys_user_role',
                    text: 'sys_user_role'
                },
                {
                    xtype: 'numbercolumn',
                    hidden: true,
                    width: 95,
                    dataIndex: 'sys_session_id',
                    text: 'sys_session_id'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 210,
                    dataIndex: 'sys_pending_email_update_address',
                    text: 'sys_pending_email_update_address'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 185,
                    dataIndex: 'sys_pending_email_update_key',
                    text: 'sys_pending_email_update_key'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 150,
                    dataIndex: 'sys_origin_user_account',
                    text: 'sys_origin_user_account'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 190,
                    dataIndex: 'sys_origin_organization_account',
                    text: 'sys_origin_organization_account'
                },
                {
                    xtype: 'numbercolumn',
                    hidden: true,
                    width: 140,
                    dataIndex: 'sys_changelog_version',
                    text: 'sys_changelog_version',
                    format: '000'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    dataIndex: 'sys_changelog',
                    text: 'sys_changelog'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_modified',
                    text: 'sys_modified',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_created',
                    text: 'sys_created',
                    format: 'Y-m-d H:i:s'
                }
            ],
            tools: [
                {
                    xtype: 'gridpanelrefreshtool'
                }
            ]
        });

        me.callParent(arguments);
    }

});

/*
 * File: app/view/WebAddressesGridPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.grid.WebAddressesGridPanel', {
    extend:  Ext.grid.Panel ,
    alias: 'widget.webaddressesgridpanel',

               
                                                
      
    //exLoadInitialized: (function() { console.error('xtype: webaddressesgridpanel / ' + (new Date()).valueOf());})(),
    title: 'WebAddresses',
    columnLines: true,
    store: 'WebAddresses',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'rownumberer'
                },
                {
                    xtype: 'gridcolumn',
                    width: 110,
                    dataIndex: 'datastore_key_id',
                    text: 'datastore_key_id'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 135,
                    dataIndex: 'datastore_key_urlsafe',
                    text: 'datastore_key_urlsafe'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'labels',
                    text: 'labels'
                },
                {
                    xtype: 'gridcolumn',
                    width: 463,
                    dataIndex: 'url',
                    text: 'url'
                },
                {
                    xtype: 'datecolumn',
                    width: 125,
                    dataIndex: 'deleted_datetime',
                    text: 'deleted_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'booleancolumn',
                    dataIndex: 'deleted',
                    text: 'deleted'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_origin_user_account',
                    text: 'sys_origin_user_account'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_origin_organization_account',
                    text: 'sys_origin_organization_account'
                },
                {
                    xtype: 'numbercolumn',
                    hidden: true,
                    width: 140,
                    dataIndex: 'sys_changelog_version',
                    text: 'sys_changelog_version',
                    format: '000'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    dataIndex: 'sys_changelog',
                    text: 'sys_changelog'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_modified',
                    text: 'sys_modified',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_created',
                    text: 'sys_created',
                    format: 'Y-m-d H:i:s'
                }
            ],
            tools: [
                {
                    xtype: 'gridpanelrefreshtool'
                }
            ]
        });

        me.callParent(arguments);
    }

});

/*
 * File: app/view/InteractiveDataExplorerTabPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.panel.InteractiveDataExplorerTabPanel', {
    extend:  Ext.panel.Panel ,
    alias: 'widget.interactivedataexplorertabpanel',
    //exLoadInitialized: (function() { console.error('xtype: interactivedataexplorertabpanel / ' + (new Date()).valueOf());})(),
               
                                                       
                                                        
                                                 
                                                   
                                                    
                                                 
                                                     
                                                   
                                                 
                                                 
      

    layout: {
        align: 'stretch',
        type: 'vbox'
    },
    title: 'Interactive Data Explorer',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'panel',
                    items: [
                        {
                            xtype: 'button',
                            margin: '20 0 0 20',
                            padding: '',
                            width: 187,
                            text: 'TransactionCompositeEntries',
                            listeners: {
                                click: {
                                    fn: me.onButtonClick,
                                    scope: me
                                }
                            }
                        },
                        {
                            xtype: 'button',
                            margin: '20 0 0 20',
                            width: 141,
                            text: 'SfdRawDataEntries',
                            listeners: {
                                click: {
                                    fn: me.onButtonClick1,
                                    scope: me
                                }
                            }
                        },
                        {
                            xtype: 'button',
                            margin: '20 0 0 20',
                            width: 126,
                            text: 'ScbRawDataEntries',
                            listeners: {
                                click: {
                                    fn: me.onButtonClick2,
                                    scope: me
                                }
                            }
                        },
                        {
                            xtype: 'button',
                            margin: '20 0 0 20',
                            width: 156,
                            text: 'CapitexRawDataEntries',
                            listeners: {
                                click: {
                                    fn: me.onButtonClick3,
                                    scope: me
                                }
                            }
                        },
                        {
                            xtype: 'form',
                            padding: '20 0 20 20',
                            items: [
                                {
                                    xtype: 'combobox',
                                    fieldLabel: 'Table',
                                    name: 'kind',
                                    displayField: 'name',
                                    forceSelection: true,
                                    queryMode: 'local',
                                    store: 'MyArrayStore',
                                    typeAhead: true,
                                    typeAheadDelay: 50,
                                    valueField: 'value'
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Start date',
                                    name: 'start_date',
                                    allowBlank: false,
                                    emptyText: 'YYYY-MM-DD'
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'End date',
                                    name: 'end_date',
                                    allowBlank: false,
                                    emptyText: 'YYYY-MM-DD'
                                },
                                {
                                    xtype: 'button',
                                    margin: '20 0 0 20',
                                    width: 110,
                                    text: 'Download',
                                    listeners: {
                                        click: {
                                            fn: me.onButtonClick4,
                                            scope: me
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'tabpanel',
                    flex: 1,
                    activeTab: 0,
                    items: [
                        {
                            xtype: 'panel',
                            layout: {
                                type: 'card'
                            },
                            title: 'SubscriberAccounts',
                            items: [
                                {
                                    xtype: 'subscriberaccountsgridpanel'
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            layout: {
                                type: 'card'
                            },
                            title: 'SystemAdminAccounts',
                            items: [
                                {
                                    xtype: 'systemadminaccountsgridpanel'
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            layout: {
                                type: 'card'
                            },
                            title: 'UserAccounts',
                            items: [
                                {
                                    xtype: 'useraccountsgridpanel'
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            layout: {
                                type: 'card'
                            },
                            title: 'ContactDetails',
                            items: [
                                {
                                    xtype: 'contactdetailsgridpanel'
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            layout: {
                                type: 'card'
                            },
                            title: 'PostalAddresses',
                            items: [
                                {
                                    xtype: 'postaladdressesgridpanel'
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            layout: {
                                type: 'card'
                            },
                            title: 'WebAddresses',
                            items: [
                                {
                                    xtype: 'webaddressesgridpanel'
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            layout: {
                                type: 'card'
                            },
                            title: 'DatasetResources',
                            items: [
                                {
                                    xtype: 'datasetresourcesgridpanel'
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            layout: {
                                type: 'card'
                            },
                            title: 'TableResources',
                            items: [
                                {
                                    xtype: 'tableresourcesgridpanel'
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            layout: {
                                type: 'card'
                            },
                            title: 'JobResources',
                            items: [
                                {
                                    xtype: 'jobresourcesgridpanel'
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            layout: {
                                type: 'card'
                            },
                            title: 'GenericErrors',
                            items: [
                                {
                                    xtype: 'genericerrorsgridpanel'
                                }
                            ]
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    },

    onButtonClick: function(button, e, eOpts) {
        window.location.href = 'http://maklarstatistik.appspot.com/playground/transaction_composite_entries/app.html';
    },

    onButtonClick1: function(button, e, eOpts) {

        window.location.href = 'http://maklarstatistik.appspot.com/playground/sfd_raw_data_entries/app.html';
    },

    onButtonClick2: function(button, e, eOpts) {

        window.location.href = 'http://maklarstatistik.appspot.com/playground/scb_raw_data_entries/app.html';
    },

    onButtonClick3: function(button, e, eOpts) {

        window.location.href = 'http://maklarstatistik.appspot.com/playground/capitex_raw_data_entries/app.html';
    },

    onButtonClick4: function(button, e, eOpts) {

        values = button.up('form').getValues();

        window.location.href = 'http://hps.maklarstatistik.appspot.com/backend/hps/process_transaction_data_to_csv?kind='+values.kind+'&start_date='+values.start_date+'&end_date='+values.end_date+'';
    }

});

/*
 * File: app/view/ManageContactsTabPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.panel.ManageContactsTabPanel', {
    extend:  Ext.panel.Panel ,
    alias: 'widget.managecontactstabpanel',

               
                                                       
                                                
      
    //exLoadInitialized: (function() { console.error('xtype: managecontactstabpanel / ' + (new Date()).valueOf());})(),
    layout: {
        align: 'stretch',
        type: 'vbox'
    },
    title: 'Manage Subscribers',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    flex: 2,
                    id: 'manageSubscribersSelectSubscriberContainer',
                    minHeight: 220,
                    layout: {
                        align: 'stretch',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'subscriberaccountsgridpanel',
                            flex: 1,
                            margins: '10 0 10 10'
                        },
                        {
                            xtype: 'container',
                            margins: '10 0 0 0',
                            layout: {
                                align: 'stretch',
                                type: 'vbox'
                            },
                            items: [
                                {
                                    xtype: 'genericactionbutton',
                                    id: 'manageSubscribersAddSubscriberButton',
                                    text: 'Add Subscriber'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    id: 'manageSubscribersEditSubscriberButton',
                                    text: 'Edit Subscriber'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    id: 'manageSubscribersSuspendSubscriberButton',
                                    text: 'Suspend Subscriber'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    id: 'manageSubscribersUnsuspendSubscriberButton',
                                    text: '&nbsp;Unsuspend Subscriber'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    id: 'manageSubscribersDeleteSubscriberButton',
                                    text: 'Delete Subscriber'
                                }
                            ]
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});

/*
 * File: app/view/ManageUsersTabPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.panel.ManageUsersTabPanel', {
    extend:  Ext.panel.Panel ,
    alias: 'widget.manageuserstabpanel',
    //exLoadInitialized: (function() { console.error('xtype: manageuserstabpanel / ' + (new Date()).valueOf());})(),
               
                                                 
                                                
      

    layout: {
        align: 'stretch',
        type: 'vbox'
    },
    title: 'Manage Subscribers',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    flex: 1,
                    id: 'manageSubscribersSelectUserContainer',
                    minHeight: 220,
                    layout: {
                        align: 'stretch',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'useraccountsgridpanel',
                            flex: 1,
                            margins: '10 0 10 10'
                        },
                        {
                            xtype: 'container',
                            margins: '10 0 0 0',
                            layout: {
                                align: 'stretch',
                                type: 'vbox'
                            },
                            items: [
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    id: 'manageSubscribersAddUserButton',
                                    text: 'Add User'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    id: 'manageSubscribersEditUserButton',
                                    text: 'Edit User'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    id: 'manageSubscribersSuspendUserButton',
                                    text: 'Suspend User'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    id: 'manageSubscribersUnsuspendUserButton',
                                    text: 'Unsuspend User'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    id: 'manageSubscribersDeleteUserButton',
                                    text: 'Delete User'
                                }
                            ]
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});

/*
 * File: app/view/SystemLogsAndPerformanceTabPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.panel.SystemLogsAndPerformanceTabPanel', {
    extend:  Ext.panel.Panel ,
    alias: 'widget.systemlogsandperformancetabpanel',
    //exLoadInitialized: (function() { console.error('xtype: subscriptionsgridpanel / ' + (new Date()).valueOf());})(),
    title: 'System Logs and Performance',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});

/*
 * File: app/view/AddSubscriberWindow.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.window.AddSubscriberWindow', {
    extend:  Ext.window.Window ,
    alias: 'widget.addsubscriberwindow',
    //exLoadInitialized: (function() { console.error('xtype: addsubscriberwindow / ' + (new Date()).valueOf());})(),
               
                                                  
                                                             
                                                       
                                                 
                                                        
                                                     
                                                 
                                               
                                              
      

    height: 690,
    id: 'addSubscriberWindow',
    padding: '',
    width: 560,
    layout: {
        type: 'anchor'
    },
    bodyBorder: false,
    bodyPadding: 10,
    title: 'Add New Subscriber',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    id: 'addSubscriberStepContainer',
                    margin: 0,
                    layout: {
                        type: 'card'
                    },
                    items: [
                        {
                            xtype: 'container',
                            id: 'addSubscriberStep1Container',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: '0 0 10 0',
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            frame: true,
                                            height: 69,
                                            html: '<h1>Step 1: Supply Company Name and Address Details for the New Subscriber</h1>',
                                            margin: '',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Subscriber Company Name and Address ( * marks required fields)',
                                    items: [
                                        {
                                            xtype: 'form',
                                            id: 'addSubscriberCompanyNameAndRegistrationNumberFormPanel',
                                            margin: 0,
                                            layout: {
                                                align: 'stretch',
                                                type: 'hbox'
                                            },
                                            bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                            items: [
                                                {
                                                    xtype: 'generictextfield',
                                                    fieldLabel: 'Company Name*',
                                                    labelAlign: 'top',
                                                    name: 'organization_name',
                                                    allowBlank: false,
                                                    flex: 2,
                                                    margins: '0 5 10 0'
                                                },
                                                {
                                                    xtype: 'generictextfield',
                                                    fieldLabel: 'Company Department',
                                                    labelAlign: 'top',
                                                    name: 'department_name',
                                                    flex: 2,
                                                    margins: '0 5 10 5'
                                                },
                                                {
                                                    xtype: 'registrationnumbertextfield',
                                                    labelAlign: 'top',
                                                    name: 'organization_id',
                                                    flex: 1.3,
                                                    margins: '0 0 10 5'
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'fieldset',
                                            margin: '0 0 10 0',
                                            padding: '4 10 2 10',
                                            title: 'Subscriber Billing Address ( * marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'form',
                                                    id: 'addSubscriberBillingAddressFormPanel',
                                                    margin: 0,
                                                    layout: {
                                                        align: 'stretch',
                                                        type: 'hbox'
                                                    },
                                                    bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                                    items: [
                                                        {
                                                            xtype: 'postaladdresscontainer',
                                                            flex: 1
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'fieldset',
                                            margin: '0 0 10 0',
                                            padding: '4 10 2 10',
                                            title: 'Subscriber Company Office Address ( * marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'container',
                                                    margin: '0 0 10 0',
                                                    layout: {
                                                        type: 'anchor'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'genericcheckbox',
                                                            name: 'office_address_as_billing_address',
                                                            boxLabel: 'Same as Billing Address',
                                                            anchor: '100%'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'form',
                                                    id: 'addSubscriberOfficeAddressFormPanel',
                                                    margin: 0,
                                                    bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                                    items: [
                                                        {
                                                            xtype: 'postaladdresscontainer'
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            id: 'addSubscriberStep2Container',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: '0 0 10 0',
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            frame: true,
                                            height: 69,
                                            html: '<h1>Step 2: Supply Subscriber Contact Details and Create Admin User</h1>',
                                            margin: '',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Subscriber Contact Details',
                                    items: [
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            title: 'Subscriber Billing Contact (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'form',
                                                    id: 'addSubscriberBillingContactFormPanel',
                                                    margin: '0 0 0 0',
                                                    bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                                    items: [
                                                        {
                                                            xtype: 'contactdetailscontainer'
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            title: 'Subscriber Technical Contact (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'container',
                                                    margin: '0 0 10 0',
                                                    layout: {
                                                        type: 'anchor'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'genericcheckbox',
                                                            name: 'technical_contact_as_billing_contact',
                                                            boxLabel: 'Same as Billing Contact',
                                                            anchor: '100%'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'form',
                                                    id: 'addSubscriberTechnicalContactFormPanel',
                                                    margin: 0,
                                                    bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                                    items: [
                                                        {
                                                            xtype: 'contactdetailscontainer'
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            title: 'Subscriber Company Web Page',
                                            items: [
                                                {
                                                    xtype: 'webaddresstextfield',
                                                    id: 'addSubscriberCompanyWebPageTextField',
                                                    fieldLabel: 'Company Web Page',
                                                    anchor: '100%'
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Subscriber Admin User (* marks required fields)',
                                    items: [
                                        {
                                            xtype: 'container',
                                            margin: '0 0 10 0',
                                            layout: {
                                                type: 'anchor'
                                            },
                                            items: [
                                                {
                                                    xtype: 'genericcheckbox',
                                                    name: 'admin_user_as_technical_contact',
                                                    boxLabel: 'Same as Technical Contact',
                                                    anchor: '100%'
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'form',
                                            id: 'addSubscriberAdminUserFormPanel',
                                            margin: 0,
                                            bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                            items: [
                                                {
                                                    xtype: 'contactdetailscontainer'
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            height: 580,
                            id: 'addSubscriberStep3Container',
                            margin: 0,
                            autoScroll: true,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: '0 0 10 0',
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            frame: true,
                                            height: 69,
                                            html: '<h1>Step 3: Review and Confirm Subscriber Details</h1>',
                                            margin: '',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Subscriber Company and Address Details',
                                    items: [
                                        {
                                            xtype: 'form',
                                            id: 'addSubscriberConfirmCompanyNameAndRegistrationNumberFormPanel',
                                            margin: 0,
                                            layout: {
                                                type: 'auto'
                                            },
                                            bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                            items: [
                                                {
                                                    xtype: 'displayfield',
                                                    margin: '0 0 10 0',
                                                    fieldLabel: 'Company Name',
                                                    labelWidth: 125,
                                                    name: 'organization_name'
                                                },
                                                {
                                                    xtype: 'displayfield',
                                                    margin: '0 0 10 0',
                                                    fieldLabel: 'Department Name',
                                                    labelWidth: 125,
                                                    name: 'department_name'
                                                },
                                                {
                                                    xtype: 'displayfield',
                                                    margin: '0 0 10 0',
                                                    fieldLabel: 'Registration Number',
                                                    labelWidth: 125,
                                                    name: 'organization_id'
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addSubscriberConfirmBillingAddressDisplayField',
                                            margin: '0 0 10 0',
                                            fieldLabel: 'Billing Address',
                                            labelWidth: 125
                                        },
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addSubscriberConfirmOfficeAddressDisplayField',
                                            margin: '0 0 10 0',
                                            fieldLabel: 'Office Address',
                                            labelWidth: 125
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Subscriber Contact Details',
                                    items: [
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addSubscriberConfirmBillingContactDisplayField',
                                            margin: '0 0 10 0',
                                            fieldLabel: 'Billing Contact',
                                            labelWidth: 125
                                        },
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addSubscriberConfirmTechnicalContactDisplayField',
                                            margin: '0 0 10 0',
                                            fieldLabel: 'Technical Contact',
                                            labelWidth: 125
                                        },
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addSubscriberConfirmCompanyWebPageDisplayField',
                                            margin: '0 0 10 0',
                                            fieldLabel: 'Company Web Page',
                                            labelWidth: 125
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Subscriber Admin User',
                                    items: [
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addSubscriberConfirmAdminUserDisplayField',
                                            margin: '0 0 10 0',
                                            fieldLabel: 'Admin User',
                                            labelWidth: 125
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'panel',
                    dock: 'bottom',
                    frame: true,
                    margin: 0,
                    layout: {
                        align: 'stretch',
                        pack: 'end',
                        type: 'hbox'
                    },
                    bodyPadding: 0,
                    items: [
                        {
                            xtype: 'genericcancelbutton'
                        },
                        {
                            xtype: 'genericbackbutton'
                        },
                        {
                            xtype: 'genericnextbutton'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});

/*
 * File: app/view/AddUserWindow.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.window.AddUserWindow', {
    extend:  Ext.window.Window ,
    alias: 'widget.adduserwindow',
    //exLoadInitialized: (function() { console.error('xtype: adduserwindow / ' + (new Date()).valueOf());})(),
               
                                                           
                                                        
                                                 
                                               
                                              
      

    height: 690,
    id: 'addUserWindow',
    padding: '',
    width: 560,
    layout: {
        type: 'anchor'
    },
    bodyBorder: false,
    bodyPadding: 10,
    title: 'Add New User',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    id: 'addUserStepContainer',
                    margin: 0,
                    layout: {
                        type: 'card'
                    },
                    items: [
                        {
                            xtype: 'container',
                            id: 'addUserStep1Container',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: '0 0 10 0',
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            frame: true,
                                            height: 69,
                                            html: '<h1>Step 1: Select Subscriber and Submit User Details</h1>',
                                            margin: '',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'form',
                                    id: 'addUserFormPanel',
                                    margin: 0,
                                    bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                    items: [
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            title: 'Target/"Parent" Subscriber (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'subscriberaccountcombobox',
                                                    fieldLabel: 'Parent Subscriber*',
                                                    allowBlank: false,
                                                    anchor: '100%'
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            title: 'User Contact Details and Account Privileges (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'radiogroup',
                                                    margin: '0 0 10 0',
                                                    fieldLabel: 'Account Privileges*',
                                                    labelWidth: 125,
                                                    allowBlank: false,
                                                    items: [
                                                        {
                                                            xtype: 'radiofield',
                                                            name: 'administrator',
                                                            boxLabel: 'Subscriber\'s Administrator',
                                                            checked: true,
                                                            inputValue: 'true'
                                                        },
                                                        {
                                                            xtype: 'radiofield',
                                                            name: 'administrator',
                                                            boxLabel: 'Subscriber\'s User',
                                                            inputValue: 'false'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'contactdetailscontainer'
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            height: 580,
                            id: 'addUserStep2Container',
                            margin: 0,
                            autoScroll: true,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: '0 0 10 0',
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            frame: true,
                                            height: 69,
                                            html: '<h1>Step 2: Review and Confirm New User Details</h1>',
                                            margin: '',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Target/"Parent" Subscriber for the New User',
                                    items: [
                                        {
                                            xtype: 'form',
                                            id: 'addUserConfirmParentSubscriberFormPanel',
                                            margin: 0,
                                            layout: {
                                                type: 'auto'
                                            },
                                            bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                            items: [
                                                {
                                                    xtype: 'displayfield',
                                                    margin: '0 0 10 0',
                                                    fieldLabel: 'Company Name',
                                                    labelWidth: 125,
                                                    name: 'organization_name'
                                                },
                                                {
                                                    xtype: 'displayfield',
                                                    margin: '0 0 10 0',
                                                    fieldLabel: 'Department Name',
                                                    labelWidth: 125,
                                                    name: 'department_name'
                                                },
                                                {
                                                    xtype: 'displayfield',
                                                    margin: '0 0 10 0',
                                                    fieldLabel: 'Registration Number',
                                                    labelWidth: 125,
                                                    name: 'organization_id'
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Contact Details and Account Privileges for the New User',
                                    items: [
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addUserConfirmAccountPrivilegesDisplayField',
                                            margin: '0 0 10 0',
                                            fieldLabel: 'Account Privileges',
                                            labelWidth: 125
                                        },
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addUserConfirmContactDetailsDisplayField',
                                            margin: '0 0 10 0',
                                            fieldLabel: 'User Contact Details',
                                            labelWidth: 125
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'panel',
                    dock: 'bottom',
                    frame: true,
                    margin: 0,
                    layout: {
                        align: 'stretch',
                        pack: 'end',
                        type: 'hbox'
                    },
                    bodyPadding: 0,
                    items: [
                        {
                            xtype: 'genericcancelbutton'
                        },
                        {
                            xtype: 'genericbackbutton'
                        },
                        {
                            xtype: 'genericnextbutton'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});

