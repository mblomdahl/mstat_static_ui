/*
 * File: app/view/StreetNumberTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.StreetNumberTextField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.streetnumbertextfield',
    //exLoadInitialized: (function() { console.error('xtype: streetnumbertextfield / ' + (new Date()).valueOf());})(),
    fieldLabel: 'Street Number*',
    labelAlign: 'top',
    labelSeparator: ' ',
    name: 'street_number',
    allowBlank: false,

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
