/*
 * File: app/view/GenericDateField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.GenericDateField', {
    extend: 'Ext.form.field.Date',
    alias: 'widget.genericdatefield',
    //exLoadInitialized: (function() { console.error('xtype: genericdatefield / ' + (new Date()).valueOf());})(),
    fieldLabel: 'Generic Date',
    labelWidth: 125,
    altFormats: 'Y-m-d',
    format: 'Y-m-d',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
