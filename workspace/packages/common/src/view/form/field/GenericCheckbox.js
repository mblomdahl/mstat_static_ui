/*
 * File: app/view/GenericCheckbox.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.GenericCheckbox', {
    extend: 'Ext.form.field.Checkbox',
    alias: 'widget.genericcheckbox',
    //exLoadInitialized: (function() { console.error('xtype: genericcheckbox / ' + (new Date()).valueOf());})(),
    margin: '0 0 10 0',
    fieldLabel: '',
    boxLabel: 'Generic Checkbox',
    inputValue: 'true',
    uncheckedValue: 'false',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
