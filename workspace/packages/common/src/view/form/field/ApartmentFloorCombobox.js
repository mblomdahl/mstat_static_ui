/*
 * File: app/view/ApartmentFloorCombobox.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.ApartmentFloorCombobox', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.apartmentfloorcombobox',
    //exLoadInitialized: (function() { console.error('xtype: apartmentfloorcombobox / ' + (new Date()).valueOf());})(),
    fieldLabel: 'Office Floor',
    labelAlign: 'top',
    labelSeparator: ' ',
    name: 'apartment_floor',
    displayField: 'name',
    forceSelection: true,
    queryMode: 'local',
    store: 'ApartmentFloors',
    typeAhead: true,
    typeAheadDelay: 50,
    valueField: 'value',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
