/*
 * File: app/view/PostalCodeTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.PostalCodeTextField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.postalcodetextfield',
    //exLoadInitialized: (function() { console.error('xtype: postalcodetextfield / ' + (new Date()).valueOf());})(),
    fieldLabel: 'Postal Code*',
    labelAlign: 'top',
    labelSeparator: ' ',
    name: 'postal_code',
    allowBlank: false,

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
