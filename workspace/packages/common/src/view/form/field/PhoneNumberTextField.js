/*
 * File: app/view/PhoneNumberTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.PhoneNumberTextField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.phonenumbertextfield',
    //exLoadInitialized: (function() { console.error('xtype: phonenumbertextfield / ' + (new Date()).valueOf());})(),
    fieldLabel: 'Phone Number',
    labelSeparator: ' ',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
