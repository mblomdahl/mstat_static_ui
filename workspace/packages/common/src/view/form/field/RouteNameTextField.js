/*
 * File: app/view/RouteNameTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.RouteNameTextField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.routenametextfield',
    //exLoadInitialized: (function() { console.error('xtype: routenametextfield / ' + (new Date()).valueOf());})(),
    width: 780,
    fieldLabel: 'Route Name*',
    labelAlign: 'top',
    labelSeparator: ' ',
    name: 'route_name',
    allowBlank: false,

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
