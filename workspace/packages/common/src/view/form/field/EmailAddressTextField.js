/*
 * File: app/view/EmailAddressTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.EmailAddressTextField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.emailaddresstextfield',
    //exLoadInitialized: (function() { console.error('xtype: emailaddresstextfield / ' + (new Date()).valueOf());})(),
    fieldLabel: 'Email Address',
    labelSeparator: ' ',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
