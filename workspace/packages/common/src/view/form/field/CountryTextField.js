/*
 * File: app/view/CountryTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.CountryTextField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.countrytextfield',
    //exLoadInitialized: (function() { console.error('xtype: countrytextfield / ' + (new Date()).valueOf());})(),
    fieldLabel: 'Country*',
    labelAlign: 'top',
    labelSeparator: ' ',
    name: 'country',
    value: 'Sweden',
    readOnly: true,
    allowBlank: false,

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
