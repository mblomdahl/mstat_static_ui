/*
 * File: app/view/PersonNameTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.PersonNameTextField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.personnametextfield',
    //exLoadInitialized: (function() { console.error('xtype: personnametextfield / ' + (new Date()).valueOf());})(),
    width: 780,
    fieldLabel: 'Person Name*',
    labelSeparator: ' ',
    allowBlank: false,

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
