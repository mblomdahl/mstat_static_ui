/*
 * File: app/view/BoxAddressTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.BoxAddressTextField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.boxaddresstextfield',
    //exLoadInitialized: (function() { console.error('xtype: boxaddresstextfield / ' + (new Date()).valueOf());})(),
    margin: '0 0 10 0',
    fieldLabel: 'Box Address (Note: Do not prefix input with "Box ")*',
    labelAlign: 'top',
    labelSeparator: ' ',
    name: 'box_address',
    allowBlank: false,

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
