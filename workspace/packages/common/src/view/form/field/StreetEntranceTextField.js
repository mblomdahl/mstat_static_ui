/*
 * File: app/view/StreetEntranceTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.StreetEntranceTextField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.streetentrancetextfield',
    //exLoadInitialized: (function() { console.error('xtype: streetentrancetextfield / ' + (new Date()).valueOf());})(),
    fieldLabel: 'Entrance',
    labelAlign: 'top',
    labelSeparator: ' ',
    name: 'street_entrance',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
