/*
 * File: app/view/WebAddressTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.WebAddressTextField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.webaddresstextfield',
    //exLoadInitialized: (function() { console.error('xtype: webaddresstextfield / ' + (new Date()).valueOf());})(),
    margin: '0 0 10 0',
    fieldLabel: 'Web Address',
    labelWidth: 125,

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
