/*
 * File: app/view/RegistrationNumberTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.RegistrationNumberTextField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.registrationnumbertextfield',
    //exLoadInitialized: (function() { console.error('xtype: registrationnumbertextfield / ' + (new Date()).valueOf());})(),
    fieldLabel: 'Registration Number',
    labelSeparator: ' ',
    labelWidth: 125,

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
