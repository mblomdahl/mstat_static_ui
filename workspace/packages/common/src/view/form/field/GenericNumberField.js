/*
 * File: app/view/GenericNumberField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.GenericNumberField', {
    extend: 'Ext.form.field.Number',
    alias: 'widget.genericnumberfield',
    //exLoadInitialized: (function() { console.error('xtype: genericnumberfield / ' + (new Date()).valueOf());})(),
    margin: '0 0 10 0',
    width: 315,
    fieldLabel: 'GenericNumber',
    labelPad: 0,
    labelSeparator: ' ',
    labelWidth: 135,
    name: 'api_version',
    allowBlank: false,
    allowDecimals: false,

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
