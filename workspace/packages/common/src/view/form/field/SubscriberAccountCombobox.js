/*
 * File: app/view/SubscriberAccountCombobox.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.SubscriberAccountCombobox', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.subscriberaccountcombobox',
    //exLoadInitialized: (function() { console.error('xtype: subscriberaccountcombobox / ' + (new Date()).valueOf());})(),
    margin: '0 0 10 0',
    fieldLabel: 'Subscriber Account',
    labelWidth: 125,
    name: 'ancestor',
    displayField: 'organization_name',
    forceSelection: true,
    queryMode: 'local',
    store: 'SubscriberAccounts',
    typeAhead: true,
    typeAheadDelay: 50,
    valueField: 'datastore_key_urlsafe',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
