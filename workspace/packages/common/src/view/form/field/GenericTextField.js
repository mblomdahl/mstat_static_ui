/*
 * File: app/view/GenericTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.GenericTextField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.generictextfield',
    //exLoadInitialized: (function() { console.error('xtype: generictextfield / ' + (new Date()).valueOf());})(),
    fieldLabel: 'Generic Text',
    labelSeparator: ' ',
    labelWidth: 125,

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
