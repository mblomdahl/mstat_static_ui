/*
 * File: app/view/PostalTownTextField.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.PostalTownTextField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.postaltowntextfield',
    //exLoadInitialized: (function() { console.error('xtype: postaltowntextfield / ' + (new Date()).valueOf());})(),
    fieldLabel: 'Postal Town*',
    labelAlign: 'top',
    labelSeparator: ' ',
    name: 'postal_town',
    allowBlank: false,

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
