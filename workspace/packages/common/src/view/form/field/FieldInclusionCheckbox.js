/*
 * File: app/view/FieldInclusionCheckbox.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.form.field.FieldInclusionCheckbox', {
    extend: 'Ext.form.field.Checkbox',
    alias: 'widget.fieldinclusioncheckbox',
    //exLoadInitialized: (function() { console.error('xtype: fieldinclusioncheckbox / ' + (new Date()).valueOf());})(),
    padding: '0 5 0 0',
    hideLabel: true,
    name: 'field_inclusion',
    boxLabel: 'Field Label',
    inputValue: 'Field Value',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
