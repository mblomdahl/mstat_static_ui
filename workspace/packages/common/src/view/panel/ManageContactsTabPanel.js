/*
 * File: app/view/ManageContactsTabPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.panel.ManageContactsTabPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.managecontactstabpanel',

    requires: [
        'Common.view.grid.SubscriberAccountsGridPanel',
        'Common.view.button.GenericActionButton'
    ],
    //exLoadInitialized: (function() { console.error('xtype: managecontactstabpanel / ' + (new Date()).valueOf());})(),
    layout: {
        align: 'stretch',
        type: 'vbox'
    },
    title: 'Manage Subscribers',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    flex: 2,
                    id: 'manageSubscribersSelectSubscriberContainer',
                    minHeight: 220,
                    layout: {
                        align: 'stretch',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'subscriberaccountsgridpanel',
                            flex: 1,
                            margins: '10 0 10 10'
                        },
                        {
                            xtype: 'container',
                            margins: '10 0 0 0',
                            layout: {
                                align: 'stretch',
                                type: 'vbox'
                            },
                            items: [
                                {
                                    xtype: 'genericactionbutton',
                                    id: 'manageSubscribersAddSubscriberButton',
                                    text: 'Add Subscriber'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    id: 'manageSubscribersEditSubscriberButton',
                                    text: 'Edit Subscriber'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    id: 'manageSubscribersSuspendSubscriberButton',
                                    text: 'Suspend Subscriber'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    id: 'manageSubscribersUnsuspendSubscriberButton',
                                    text: '&nbsp;Unsuspend Subscriber'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    id: 'manageSubscribersDeleteSubscriberButton',
                                    text: 'Delete Subscriber'
                                }
                            ]
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
