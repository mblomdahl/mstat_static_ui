/*
 * File: app/view/ManageUsersTabPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.panel.ManageUsersTabPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.manageuserstabpanel',
    //exLoadInitialized: (function() { console.error('xtype: manageuserstabpanel / ' + (new Date()).valueOf());})(),
    requires: [
        'Common.view.grid.UserAccountsGridPanel',
        'Common.view.button.GenericActionButton'
    ],

    layout: {
        align: 'stretch',
        type: 'vbox'
    },
    title: 'Manage Subscribers',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    flex: 1,
                    id: 'manageSubscribersSelectUserContainer',
                    minHeight: 220,
                    layout: {
                        align: 'stretch',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'useraccountsgridpanel',
                            flex: 1,
                            margins: '10 0 10 10'
                        },
                        {
                            xtype: 'container',
                            margins: '10 0 0 0',
                            layout: {
                                align: 'stretch',
                                type: 'vbox'
                            },
                            items: [
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    id: 'manageSubscribersAddUserButton',
                                    text: 'Add User'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    id: 'manageSubscribersEditUserButton',
                                    text: 'Edit User'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    id: 'manageSubscribersSuspendUserButton',
                                    text: 'Suspend User'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    id: 'manageSubscribersUnsuspendUserButton',
                                    text: 'Unsuspend User'
                                },
                                {
                                    xtype: 'genericactionbutton',
                                    disabled: true,
                                    id: 'manageSubscribersDeleteUserButton',
                                    text: 'Delete User'
                                }
                            ]
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
