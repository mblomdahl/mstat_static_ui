/*
 * File: app/view/SystemLogsAndPerformanceTabPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.panel.SystemLogsAndPerformanceTabPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.systemlogsandperformancetabpanel',
    //exLoadInitialized: (function() { console.error('xtype: subscriptionsgridpanel / ' + (new Date()).valueOf());})(),
    title: 'System Logs and Performance',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
