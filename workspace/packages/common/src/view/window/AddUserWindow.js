/*
 * File: app/view/AddUserWindow.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.window.AddUserWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.adduserwindow',
    //exLoadInitialized: (function() { console.error('xtype: adduserwindow / ' + (new Date()).valueOf());})(),
    requires: [
        'Common.view.form.field.SubscriberAccountCombobox',
        'Common.view.container.ContactDetailsContainer',
        'Common.view.button.GenericCancelButton',
        'Common.view.button.GenericBackButton',
        'Common.view.button.GenericNextButton'
    ],

    height: 690,
    id: 'addUserWindow',
    padding: '',
    width: 560,
    layout: {
        type: 'anchor'
    },
    bodyBorder: false,
    bodyPadding: 10,
    title: 'Add New User',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    id: 'addUserStepContainer',
                    margin: 0,
                    layout: {
                        type: 'card'
                    },
                    items: [
                        {
                            xtype: 'container',
                            id: 'addUserStep1Container',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: '0 0 10 0',
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            frame: true,
                                            height: 69,
                                            html: '<h1>Step 1: Select Subscriber and Submit User Details</h1>',
                                            margin: '',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'form',
                                    id: 'addUserFormPanel',
                                    margin: 0,
                                    bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                    items: [
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            title: 'Target/"Parent" Subscriber (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'subscriberaccountcombobox',
                                                    fieldLabel: 'Parent Subscriber*',
                                                    allowBlank: false,
                                                    anchor: '100%'
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            title: 'User Contact Details and Account Privileges (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'radiogroup',
                                                    margin: '0 0 10 0',
                                                    fieldLabel: 'Account Privileges*',
                                                    labelWidth: 125,
                                                    allowBlank: false,
                                                    items: [
                                                        {
                                                            xtype: 'radiofield',
                                                            name: 'administrator',
                                                            boxLabel: 'Subscriber\'s Administrator',
                                                            checked: true,
                                                            inputValue: 'true'
                                                        },
                                                        {
                                                            xtype: 'radiofield',
                                                            name: 'administrator',
                                                            boxLabel: 'Subscriber\'s User',
                                                            inputValue: 'false'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'contactdetailscontainer'
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            height: 580,
                            id: 'addUserStep2Container',
                            margin: 0,
                            autoScroll: true,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: '0 0 10 0',
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            frame: true,
                                            height: 69,
                                            html: '<h1>Step 2: Review and Confirm New User Details</h1>',
                                            margin: '',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Target/"Parent" Subscriber for the New User',
                                    items: [
                                        {
                                            xtype: 'form',
                                            id: 'addUserConfirmParentSubscriberFormPanel',
                                            margin: 0,
                                            layout: {
                                                type: 'auto'
                                            },
                                            bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                            items: [
                                                {
                                                    xtype: 'displayfield',
                                                    margin: '0 0 10 0',
                                                    fieldLabel: 'Company Name',
                                                    labelWidth: 125,
                                                    name: 'organization_name'
                                                },
                                                {
                                                    xtype: 'displayfield',
                                                    margin: '0 0 10 0',
                                                    fieldLabel: 'Department Name',
                                                    labelWidth: 125,
                                                    name: 'department_name'
                                                },
                                                {
                                                    xtype: 'displayfield',
                                                    margin: '0 0 10 0',
                                                    fieldLabel: 'Registration Number',
                                                    labelWidth: 125,
                                                    name: 'organization_id'
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Contact Details and Account Privileges for the New User',
                                    items: [
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addUserConfirmAccountPrivilegesDisplayField',
                                            margin: '0 0 10 0',
                                            fieldLabel: 'Account Privileges',
                                            labelWidth: 125
                                        },
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addUserConfirmContactDetailsDisplayField',
                                            margin: '0 0 10 0',
                                            fieldLabel: 'User Contact Details',
                                            labelWidth: 125
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'panel',
                    dock: 'bottom',
                    frame: true,
                    margin: 0,
                    layout: {
                        align: 'stretch',
                        pack: 'end',
                        type: 'hbox'
                    },
                    bodyPadding: 0,
                    items: [
                        {
                            xtype: 'genericcancelbutton'
                        },
                        {
                            xtype: 'genericbackbutton'
                        },
                        {
                            xtype: 'genericnextbutton'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
