/*
 * File: app/view/AddSubscriberWindow.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.window.AddSubscriberWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.addsubscriberwindow',
    //exLoadInitialized: (function() { console.error('xtype: addsubscriberwindow / ' + (new Date()).valueOf());})(),
    requires: [
        'Common.view.form.field.GenericTextField',
        'Common.view.form.field.RegistrationNumberTextField',
        'Common.view.container.PostalAddressContainer',
        'Common.view.form.field.GenericCheckbox',
        'Common.view.container.ContactDetailsContainer',
        'Common.view.form.field.WebAddressTextField',
        'Common.view.button.GenericCancelButton',
        'Common.view.button.GenericBackButton',
        'Common.view.button.GenericNextButton'
    ],

    height: 690,
    id: 'addSubscriberWindow',
    padding: '',
    width: 560,
    layout: {
        type: 'anchor'
    },
    bodyBorder: false,
    bodyPadding: 10,
    title: 'Add New Subscriber',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    id: 'addSubscriberStepContainer',
                    margin: 0,
                    layout: {
                        type: 'card'
                    },
                    items: [
                        {
                            xtype: 'container',
                            id: 'addSubscriberStep1Container',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: '0 0 10 0',
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            frame: true,
                                            height: 69,
                                            html: '<h1>Step 1: Supply Company Name and Address Details for the New Subscriber</h1>',
                                            margin: '',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Subscriber Company Name and Address ( * marks required fields)',
                                    items: [
                                        {
                                            xtype: 'form',
                                            id: 'addSubscriberCompanyNameAndRegistrationNumberFormPanel',
                                            margin: 0,
                                            layout: {
                                                align: 'stretch',
                                                type: 'hbox'
                                            },
                                            bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                            items: [
                                                {
                                                    xtype: 'generictextfield',
                                                    fieldLabel: 'Company Name*',
                                                    labelAlign: 'top',
                                                    name: 'organization_name',
                                                    allowBlank: false,
                                                    flex: 2,
                                                    margins: '0 5 10 0'
                                                },
                                                {
                                                    xtype: 'generictextfield',
                                                    fieldLabel: 'Company Department',
                                                    labelAlign: 'top',
                                                    name: 'department_name',
                                                    flex: 2,
                                                    margins: '0 5 10 5'
                                                },
                                                {
                                                    xtype: 'registrationnumbertextfield',
                                                    labelAlign: 'top',
                                                    name: 'organization_id',
                                                    flex: 1.3,
                                                    margins: '0 0 10 5'
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'fieldset',
                                            margin: '0 0 10 0',
                                            padding: '4 10 2 10',
                                            title: 'Subscriber Billing Address ( * marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'form',
                                                    id: 'addSubscriberBillingAddressFormPanel',
                                                    margin: 0,
                                                    layout: {
                                                        align: 'stretch',
                                                        type: 'hbox'
                                                    },
                                                    bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                                    items: [
                                                        {
                                                            xtype: 'postaladdresscontainer',
                                                            flex: 1
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'fieldset',
                                            margin: '0 0 10 0',
                                            padding: '4 10 2 10',
                                            title: 'Subscriber Company Office Address ( * marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'container',
                                                    margin: '0 0 10 0',
                                                    layout: {
                                                        type: 'anchor'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'genericcheckbox',
                                                            name: 'office_address_as_billing_address',
                                                            boxLabel: 'Same as Billing Address',
                                                            anchor: '100%'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'form',
                                                    id: 'addSubscriberOfficeAddressFormPanel',
                                                    margin: 0,
                                                    bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                                    items: [
                                                        {
                                                            xtype: 'postaladdresscontainer'
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            id: 'addSubscriberStep2Container',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: '0 0 10 0',
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            frame: true,
                                            height: 69,
                                            html: '<h1>Step 2: Supply Subscriber Contact Details and Create Admin User</h1>',
                                            margin: '',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Subscriber Contact Details',
                                    items: [
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            title: 'Subscriber Billing Contact (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'form',
                                                    id: 'addSubscriberBillingContactFormPanel',
                                                    margin: '0 0 0 0',
                                                    bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                                    items: [
                                                        {
                                                            xtype: 'contactdetailscontainer'
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            title: 'Subscriber Technical Contact (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'container',
                                                    margin: '0 0 10 0',
                                                    layout: {
                                                        type: 'anchor'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'genericcheckbox',
                                                            name: 'technical_contact_as_billing_contact',
                                                            boxLabel: 'Same as Billing Contact',
                                                            anchor: '100%'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'form',
                                                    id: 'addSubscriberTechnicalContactFormPanel',
                                                    margin: 0,
                                                    bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                                    items: [
                                                        {
                                                            xtype: 'contactdetailscontainer'
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            title: 'Subscriber Company Web Page',
                                            items: [
                                                {
                                                    xtype: 'webaddresstextfield',
                                                    id: 'addSubscriberCompanyWebPageTextField',
                                                    fieldLabel: 'Company Web Page',
                                                    anchor: '100%'
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Subscriber Admin User (* marks required fields)',
                                    items: [
                                        {
                                            xtype: 'container',
                                            margin: '0 0 10 0',
                                            layout: {
                                                type: 'anchor'
                                            },
                                            items: [
                                                {
                                                    xtype: 'genericcheckbox',
                                                    name: 'admin_user_as_technical_contact',
                                                    boxLabel: 'Same as Technical Contact',
                                                    anchor: '100%'
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'form',
                                            id: 'addSubscriberAdminUserFormPanel',
                                            margin: 0,
                                            bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                            items: [
                                                {
                                                    xtype: 'contactdetailscontainer'
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            height: 580,
                            id: 'addSubscriberStep3Container',
                            margin: 0,
                            autoScroll: true,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: '0 0 10 0',
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            frame: true,
                                            height: 69,
                                            html: '<h1>Step 3: Review and Confirm Subscriber Details</h1>',
                                            margin: '',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Subscriber Company and Address Details',
                                    items: [
                                        {
                                            xtype: 'form',
                                            id: 'addSubscriberConfirmCompanyNameAndRegistrationNumberFormPanel',
                                            margin: 0,
                                            layout: {
                                                type: 'auto'
                                            },
                                            bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                            items: [
                                                {
                                                    xtype: 'displayfield',
                                                    margin: '0 0 10 0',
                                                    fieldLabel: 'Company Name',
                                                    labelWidth: 125,
                                                    name: 'organization_name'
                                                },
                                                {
                                                    xtype: 'displayfield',
                                                    margin: '0 0 10 0',
                                                    fieldLabel: 'Department Name',
                                                    labelWidth: 125,
                                                    name: 'department_name'
                                                },
                                                {
                                                    xtype: 'displayfield',
                                                    margin: '0 0 10 0',
                                                    fieldLabel: 'Registration Number',
                                                    labelWidth: 125,
                                                    name: 'organization_id'
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addSubscriberConfirmBillingAddressDisplayField',
                                            margin: '0 0 10 0',
                                            fieldLabel: 'Billing Address',
                                            labelWidth: 125
                                        },
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addSubscriberConfirmOfficeAddressDisplayField',
                                            margin: '0 0 10 0',
                                            fieldLabel: 'Office Address',
                                            labelWidth: 125
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Subscriber Contact Details',
                                    items: [
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addSubscriberConfirmBillingContactDisplayField',
                                            margin: '0 0 10 0',
                                            fieldLabel: 'Billing Contact',
                                            labelWidth: 125
                                        },
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addSubscriberConfirmTechnicalContactDisplayField',
                                            margin: '0 0 10 0',
                                            fieldLabel: 'Technical Contact',
                                            labelWidth: 125
                                        },
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addSubscriberConfirmCompanyWebPageDisplayField',
                                            margin: '0 0 10 0',
                                            fieldLabel: 'Company Web Page',
                                            labelWidth: 125
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Subscriber Admin User',
                                    items: [
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addSubscriberConfirmAdminUserDisplayField',
                                            margin: '0 0 10 0',
                                            fieldLabel: 'Admin User',
                                            labelWidth: 125
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'panel',
                    dock: 'bottom',
                    frame: true,
                    margin: 0,
                    layout: {
                        align: 'stretch',
                        pack: 'end',
                        type: 'hbox'
                    },
                    bodyPadding: 0,
                    items: [
                        {
                            xtype: 'genericcancelbutton'
                        },
                        {
                            xtype: 'genericbackbutton'
                        },
                        {
                            xtype: 'genericnextbutton'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
