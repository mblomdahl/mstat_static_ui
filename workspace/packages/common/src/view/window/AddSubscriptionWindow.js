/*
 * File: app/view/AddSubscriptionWindow.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.window.AddSubscriptionWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.addsubscriptionwindow',
    //exLoadInitialized: (function() { console.error('xtype: addsubscriptionwindow / ' + (new Date()).valueOf());})(),
    requires: [
        'Common.view.form.field.SubscriberAccountCombobox',
        'Common.view.form.field.GenericDateField',
        'Common.view.form.field.FieldInclusionCheckbox',
        'Common.view.button.GenericCancelButton',
        'Common.view.button.GenericBackButton',
        'Common.view.button.GenericNextButton'
    ],

    height: 690,
    id: 'addSubscriptionWindow',
    padding: 0,
    width: 560,
    layout: {
        type: 'anchor'
    },
    bodyBorder: false,
    bodyPadding: 10,
    title: 'Add New Subscription',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    id: 'addSubscriptionStepContainer',
                    margin: 0,
                    layout: {
                        type: 'card'
                    },
                    items: [
                        {
                            xtype: 'container',
                            id: 'addSubscriptionStep1Container',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: '0 0 10 0',
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            frame: true,
                                            height: 69,
                                            html: '<h1>Step 1: Select Subscriber, Subscription Type/Period, and API Access Restrictions</h1>',
                                            margin: '',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'form',
                                    id: 'addSubscriptionStep1FormPanel',
                                    margin: 0,
                                    bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                    items: [
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            title: 'Target/"Parent" Subscriber (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'subscriberaccountcombobox',
                                                    fieldLabel: 'Parent Subscriber*',
                                                    allowBlank: false,
                                                    anchor: '100%'
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            title: 'Specify Subscription Type (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'radiogroup',
                                                    margin: '0 0 10 0',
                                                    fieldLabel: 'Subscription Type*',
                                                    labelWidth: 125,
                                                    allowBlank: false,
                                                    items: [
                                                        {
                                                            xtype: 'radiofield',
                                                            name: 'subscription_type',
                                                            boxLabel: 'Mäklarstatistik Data API',
                                                            checked: true,
                                                            inputValue: 'data_api_subscription'
                                                        },
                                                        {
                                                            xtype: 'radiofield',
                                                            disabled: true,
                                                            name: 'subscription_type',
                                                            boxLabel: 'Mäklarstatistik Widget Service',
                                                            inputValue: 'widget_service_subscription'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'container',
                                                    layout: {
                                                        align: 'stretch',
                                                        type: 'hbox'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'genericdatefield',
                                                            fieldLabel: 'Subscription Period Start Date*',
                                                            labelAlign: 'top',
                                                            labelSeparator: ' ',
                                                            name: 'subscription_start_date',
                                                            allowBlank: false,
                                                            flex: 1,
                                                            margins: '0 5 10 0'
                                                        },
                                                        {
                                                            xtype: 'genericdatefield',
                                                            fieldLabel: 'Period End Date (indefinite if omitted)',
                                                            labelAlign: 'top',
                                                            labelSeparator: ' ',
                                                            name: 'subscription_end_date',
                                                            flex: 1,
                                                            margins: '0 0 10 5'
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            layout: {
                                                align: 'stretch',
                                                type: 'vbox'
                                            },
                                            title: 'Specify Access Restrictions (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'container',
                                                    layout: {
                                                        align: 'stretch',
                                                        type: 'hbox'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'genericdatefield',
                                                            fieldLabel: 'Data Access Start Date*',
                                                            labelAlign: 'top',
                                                            labelSeparator: ' ',
                                                            name: 'start_date',
                                                            allowBlank: false,
                                                            flex: 1,
                                                            margins: '0 5 10 0'
                                                        },
                                                        {
                                                            xtype: 'genericdatefield',
                                                            fieldLabel: 'Data Access End Date (indefinite if omitted)',
                                                            labelAlign: 'top',
                                                            labelSeparator: ' ',
                                                            name: 'end_date',
                                                            flex: 1,
                                                            margins: '0 0 10 5'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'container',
                                                    margin: 0,
                                                    layout: {
                                                        align: 'stretch',
                                                        type: 'hbox'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'container',
                                                            flex: 1,
                                                            margin: '-6 5 0 0',
                                                            layout: {
                                                                align: 'stretch',
                                                                type: 'hbox'
                                                            },
                                                            items: [
                                                                {
                                                                    xtype: 'slider',
                                                                    tipText: function(thumb) {
                                                                        return Ext.String.format('**{0} downloads/update**', thumb);
                                                                    },
                                                                    flex: 1,
                                                                    margins: '0 5 0 0',
                                                                    fieldLabel: 'Full Dataset Downloads Quota per Update*',
                                                                    labelAlign: 'top',
                                                                    labelPad: 0,
                                                                    labelSeparator: ' ',
                                                                    name: 'quota',
                                                                    value: 12,
                                                                    increment: 3,
                                                                    keyIncrement: 3,
                                                                    maxValue: 30,
                                                                    minValue: 3,
                                                                    listeners: {
                                                                        change: {
                                                                            fn: me.onSliderChange,
                                                                            scope: me
                                                                        }
                                                                    }
                                                                },
                                                                {
                                                                    xtype: 'displayfield',
                                                                    margins: '24 0 10 0',
                                                                    id: '',
                                                                    fieldLabel: 'Label',
                                                                    hideLabel: true,
                                                                    labelAlign: 'top',
                                                                    labelWidth: 25,
                                                                    value: 12
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            xtype: 'radiogroup',
                                                            flex: 1,
                                                            margin: '0 0 10 5',
                                                            padding: 0,
                                                            layout: {
                                                                type: 'table'
                                                            },
                                                            fieldLabel: 'Source Data Update Frequency*',
                                                            labelAlign: 'top',
                                                            labelSeparator: ' ',
                                                            labelWidth: 125,
                                                            items: [
                                                                {
                                                                    xtype: 'radiofield',
                                                                    padding: '0 10 0 0',
                                                                    boxLabel: 'Daily',
                                                                    inputValue: 'subscriber_daily_update_frequency'
                                                                },
                                                                {
                                                                    xtype: 'radiofield',
                                                                    padding: '0 10 0 0',
                                                                    name: 'update_frequency',
                                                                    boxLabel: 'Weekly',
                                                                    inputValue: 'subscriber_weekly_update_frequency'
                                                                },
                                                                {
                                                                    xtype: 'radiofield',
                                                                    padding: '0 10 0 0',
                                                                    name: 'update_frequency',
                                                                    boxLabel: 'Monthly',
                                                                    checked: true,
                                                                    inputValue: 'subscriber_monthly_update_frequency'
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            id: 'addSubscriptionStep2Container',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: '0 0 10 0',
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            frame: true,
                                            height: 69,
                                            html: '<h1>Step 2: Select the Subset of Fields to Include in the New Subscription</h1>',
                                            margin: '',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'form',
                                    id: 'addSubscriptionStep2FormPanel',
                                    margin: 0,
                                    bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                    frameHeader: false,
                                    items: [
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            layout: {
                                                type: 'column'
                                            },
                                            title: 'Specify Field Access Restrictions (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'container',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionStep2FieldCheckerContainer',
                                                    layout: {
                                                        pack: 'end',
                                                        type: 'hbox'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'container',
                                                            layout: {
                                                                align: 'stretch',
                                                                pack: 'end',
                                                                type: 'vbox'
                                                            },
                                                            items: [
                                                                {
                                                                    xtype: 'label',
                                                                    margins: '0 0 5 0',
                                                                    text: 'Check fields:'
                                                                },
                                                                {
                                                                    xtype: 'container',
                                                                    layout: {
                                                                        type: 'hbox'
                                                                    },
                                                                    items: [
                                                                        {
                                                                            xtype: 'button',
                                                                            margins: '0 5 0 0',
                                                                            text: 'All'
                                                                        },
                                                                        {
                                                                            xtype: 'button',
                                                                            margins: '0 5 5 5',
                                                                            text: 'Standard'
                                                                        },
                                                                        {
                                                                            xtype: 'button',
                                                                            margins: '0 0 5 5',
                                                                            text: 'None'
                                                                        }
                                                                    ]
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'checkboxgroup',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionBasicTransactionCheckboxGroup',
                                                    margin: '0 -10 7 0',
                                                    layout: {
                                                        columns: 3,
                                                        type: 'table'
                                                    },
                                                    fieldLabel: '<u>Include Basic Transaction Details</u>',
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    labelWidth: 125,
                                                    columns: 3,
                                                    vertical: true,
                                                    items: [
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'transaction_id<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "ObjektId"</i>',
                                                            inputValue: 'transaction_id'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'real_estate_ad_publicized<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Annonsdatum"</i>',
                                                            inputValue: 'real_estate_ad_publicized'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'contract_date<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Kontraktsdatum"</i>',
                                                            inputValue: 'contract_date'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'contract_price<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Pris"</i>',
                                                            inputValue: 'contract_price'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'date_of_possession<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Tilltradesdatum"</i>',
                                                            inputValue: 'date_of_possession'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'data_transfer_date<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Overforingsdatum"</i>',
                                                            inputValue: 'data_transfer_date'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'mstat_restricted<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "KansligAffar"</i>',
                                                            inputValue: 'mstat_restricted'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'real_estate_designation<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Fastbet"</i>',
                                                            inputValue: 'real_estate_designation'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'real_estate_taxation_code<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Taxkod"</i>',
                                                            inputValue: 'real_estate_taxation_code'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'checkboxgroup',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionGeolocationCheckboxgroup',
                                                    margin: '0 -10 0 0',
                                                    layout: {
                                                        columns: 4,
                                                        type: 'table'
                                                    },
                                                    fieldLabel: '<u>Include Location Fields</u>',
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    labelWidth: 125,
                                                    columns: 4,
                                                    vertical: true,
                                                    items: [
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'locality',
                                                            inputValue: 'locality'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'sublocality',
                                                            inputValue: 'sublocality'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'county_name',
                                                            inputValue: 'county_name'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'county_lkf',
                                                            inputValue: 'county_lkf'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'municipality_name',
                                                            inputValue: 'municipality_name'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'municipality_lkf',
                                                            inputValue: 'municipality_lkf'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'congregation_name',
                                                            inputValue: 'congregation_name'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'congregation_lkf<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "LKF"</i>',
                                                            inputValue: 'congregation_lkf'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'checkboxgroup',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionGeoPtCheckboxGroup',
                                                    margin: '0 -10 10 0',
                                                    layout: {
                                                        columns: 3,
                                                        type: 'table'
                                                    },
                                                    fieldLabel: '<u>Include Coordinate Fields for Geopositioning</u>',
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    labelWidth: 125,
                                                    columns: 3,
                                                    vertical: true,
                                                    items: [
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'location_rt90_geopt',
                                                            inputValue: 'location_rt90_geopt'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 0 0',
                                                            boxLabel: 'location_rt90_geopt_east<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "East"</i>',
                                                            inputValue: 'location_rt90_geopt_east'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'location_rt90_geopt_north<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "North"</i>',
                                                            inputValue: 'location_rt90_geopt_north'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 0 0',
                                                            boxLabel: 'location_wgs84_geopt',
                                                            inputValue: 'location_wgs84_geopt'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'checkboxgroup',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionAddressCheckboxGroup',
                                                    margin: '0 -10 5 0',
                                                    layout: {
                                                        columns: 4,
                                                        type: 'table'
                                                    },
                                                    fieldLabel: '<u>Include Address Fields</u>',
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    labelWidth: 125,
                                                    columns: 4,
                                                    vertical: true,
                                                    items: [
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'route_name',
                                                            inputValue: 'route_name'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'street_number',
                                                            inputValue: 'street_number'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'street_entrance',
                                                            inputValue: 'street_entrance'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'apartment_number<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "LghNr"</i>',
                                                            inputValue: 'apartment_number'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'postal_code',
                                                            inputValue: 'postal_code'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'postal_town',
                                                            inputValue: 'postal_town'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 0 0 0',
                                                            boxLabel: 'formatted_address<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Gatuadress"</i>',
                                                            inputValue: 'formatted_address'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'checkboxgroup',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionHousingCooperativeCheckboxGroup',
                                                    margin: '0 -10 5 0',
                                                    layout: {
                                                        columns: 3,
                                                        type: 'table'
                                                    },
                                                    fieldLabel: '<u>Include Housing Cooperative Details</u>',
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    labelWidth: 125,
                                                    columns: 3,
                                                    vertical: true,
                                                    items: [
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 0 0',
                                                            boxLabel: 'housing_cooperative_registration_number<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "OrgNr"</i>',
                                                            inputValue: 'housing_cooperative_registration_number',
                                                            colspan: 2
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 0 0',
                                                            boxLabel: 'housing_cooperative_name<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "BRF"</i>',
                                                            inputValue: 'housing_cooperative_name'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 0 0',
                                                            boxLabel: 'housing_cooperative_share<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Andelstal"</i>',
                                                            inputValue: 'housing_cooperative_share'
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            id: 'addSubscriptionStep3Container',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'container',
                                    margin: '0 0 10 0',
                                    items: [
                                        {
                                            xtype: 'label',
                                            border: '',
                                            frame: true,
                                            height: 69,
                                            html: '<h1>Step 3: Select the Subset of Fields to Include in the New Subscription</h1>',
                                            margin: '',
                                            text: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'form',
                                    id: 'addSubscriptionStep3FormPanel',
                                    margin: 0,
                                    bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                    items: [
                                        {
                                            xtype: 'fieldset',
                                            padding: '4 10 2 10',
                                            layout: {
                                                type: 'column'
                                            },
                                            title: 'Specify Field Access Restrictions (* marks required fields)',
                                            items: [
                                                {
                                                    xtype: 'container',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionStep3FieldCheckerContainer',
                                                    layout: {
                                                        pack: 'end',
                                                        type: 'hbox'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'container',
                                                            layout: {
                                                                align: 'stretch',
                                                                pack: 'end',
                                                                type: 'vbox'
                                                            },
                                                            items: [
                                                                {
                                                                    xtype: 'label',
                                                                    margins: '0 0 5 0',
                                                                    text: 'Check fields:'
                                                                },
                                                                {
                                                                    xtype: 'container',
                                                                    layout: {
                                                                        type: 'hbox'
                                                                    },
                                                                    items: [
                                                                        {
                                                                            xtype: 'button',
                                                                            margins: '0 5 0 0',
                                                                            text: 'All'
                                                                        },
                                                                        {
                                                                            xtype: 'button',
                                                                            margins: '0 5 5 5',
                                                                            text: 'Standard'
                                                                        },
                                                                        {
                                                                            xtype: 'button',
                                                                            margins: '0 0 5 5',
                                                                            text: 'None'
                                                                        }
                                                                    ]
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'checkboxgroup',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionEstateFeesCheckboxGroup',
                                                    margin: '0 -10 5 0',
                                                    layout: {
                                                        columns: 4,
                                                        type: 'table'
                                                    },
                                                    fieldLabel: '<u>Include Details on Fees for the Real Estate</u>',
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    labelWidth: 125,
                                                    columns: 4,
                                                    vertical: true,
                                                    items: [
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 0 0',
                                                            boxLabel: 'monthly_fee<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Manavg"</i>',
                                                            inputValue: 'monthly_fee'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'annual_fee',
                                                            inputValue: 'annual_fee'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'operating_costs',
                                                            inputValue: 'operating_costs'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'heating_included',
                                                            inputValue: 'heating_included'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'checkboxgroup',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionMiscPropertiesCheckboxGroup',
                                                    margin: '0 -10 10 0',
                                                    layout: {
                                                        columns: 3,
                                                        type: 'table'
                                                    },
                                                    fieldLabel: '<u>Include Administrative Classifications and Misc. Estate Properties</u>',
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    labelWidth: 125,
                                                    columns: 3,
                                                    vertical: true,
                                                    items: [
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'type_of_housing<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Boendeform"</i>',
                                                            inputValue: 'type_of_housing'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'housing_category<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Typ"</i>',
                                                            inputValue: 'housing_category'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'housing_tenure<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Upplatelseform"</i>',
                                                            inputValue: 'housing_tenure'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'apartment_floor<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Vaning"</i>',
                                                            inputValue: 'apartment_floor'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'building_storeys<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Vaningar"</i>',
                                                            inputValue: 'building_storeys'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'living_area<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Boyta"</i>',
                                                            inputValue: 'living_area'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'plot_area<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Tomtarea"</i>',
                                                            inputValue: 'plot_area'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'number_of_rooms<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Rum"</i>',
                                                            inputValue: 'number_of_rooms'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'elevator<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Hiss"</i>',
                                                            inputValue: 'elevator'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'balcony<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Balkong"</i>',
                                                            inputValue: 'balcony'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'new_production<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "NyProd"</i>',
                                                            inputValue: 'new_production'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'build_year<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "ByggAr"</i>',
                                                            inputValue: 'build_year'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'build_year_lower',
                                                            inputValue: 'build_year_lower'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'build_year_upper',
                                                            inputValue: 'build_year_upper'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'checkboxgroup',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionRateableValueCheckboxGroup',
                                                    margin: '0 -10 5 0',
                                                    layout: {
                                                        columns: 2,
                                                        type: 'table'
                                                    },
                                                    fieldLabel: '<u>Include Rateable Value Details</u>',
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    labelWidth: 125,
                                                    columns: 2,
                                                    vertical: true,
                                                    items: [
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 0 0',
                                                            boxLabel: 'baseline_year_of_assessment<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Taxar"</i>',
                                                            inputValue: 'baseline_year_of_assessment'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'baseline_rateable_value<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "Taxeringsvarde"</i>',
                                                            inputValue: 'baseline_rateable_value'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'year_of_assessment',
                                                            inputValue: 'year_of_assessment'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'rateable_value<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "SCB_Taxeringsvarde"</i>',
                                                            inputValue: 'rateable_value'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'price_by_rateable_value<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "SCB_KB"</i>',
                                                            inputValue: 'price_by_rateable_value'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'price_by_rateable_value_per_sq_meter<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "KBoy"</i>',
                                                            inputValue: 'price_by_rateable_value_per_sq_meter'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'checkboxgroup',
                                                    columnWidth: 1,
                                                    id: 'addSubscriptionSystemPropertiesCheckboxGroup',
                                                    margin: '0 -10 5 0',
                                                    layout: {
                                                        columns: 3,
                                                        type: 'table'
                                                    },
                                                    fieldLabel: '<u>Include Invalidation Codes and System Metadata</u>',
                                                    labelAlign: 'top',
                                                    labelSeparator: ' ',
                                                    labelWidth: 125,
                                                    columns: 3,
                                                    vertical: true,
                                                    items: [
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'sys_invalidation_codes',
                                                            inputValue: 'sys_invalidation_codes'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 0 0',
                                                            boxLabel: 'sys_scb_invalidation_code<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>legacy "SCBBortfall"</i>',
                                                            inputValue: 'sys_scb_invalidation_code'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 13 0',
                                                            boxLabel: 'sys_changelog_version',
                                                            inputValue: 'sys_changelog_version'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 0 0',
                                                            boxLabel: 'sys_changelog',
                                                            inputValue: 'sys_changelog'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            padding: '0 10 0 0',
                                                            boxLabel: 'sys_modified',
                                                            inputValue: 'sys_modified'
                                                        },
                                                        {
                                                            xtype: 'fieldinclusioncheckbox',
                                                            boxLabel: 'sys_created',
                                                            inputValue: 'sys_created'
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            height: 580,
                            id: 'addSubscriptionStep4Container',
                            margin: 0,
                            items: [
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Target/"Parent" Subscriber for the New Subscription',
                                    items: [
                                        {
                                            xtype: 'form',
                                            id: 'addSubscriptionConfirmParentSubscriberFormPanel',
                                            margin: 0,
                                            layout: {
                                                type: 'auto'
                                            },
                                            bodyStyle: 'background-color: rgba(0,0,0,0); border: none;',
                                            items: [
                                                {
                                                    xtype: 'displayfield',
                                                    margin: '0 0 10 0',
                                                    fieldLabel: 'Company Name',
                                                    labelWidth: 150,
                                                    name: 'organization_name'
                                                },
                                                {
                                                    xtype: 'displayfield',
                                                    margin: '0 0 10 0',
                                                    fieldLabel: 'Department Name',
                                                    labelWidth: 150,
                                                    name: 'department_name'
                                                },
                                                {
                                                    xtype: 'displayfield',
                                                    margin: '0 0 10 0',
                                                    fieldLabel: 'Registration Number',
                                                    labelWidth: 150,
                                                    name: 'organization_id'
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: '4 10 2 10',
                                    title: 'Subscription Type and Period',
                                    items: [
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addSubscriptionConfirmSubscriptionTypeDisplayField',
                                            margin: '0 0 10 0',
                                            fieldLabel: 'Subscription Type',
                                            labelWidth: 150
                                        },
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            id: 'addSubscriptionConfirmSubscriptionDateIntervalDisplayField',
                                            margin: '0 0 10 0',
                                            fieldLabel: 'Subsc. Period Date Interval',
                                            labelWidth: 150
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'panel',
                    dock: 'bottom',
                    frame: true,
                    margin: 0,
                    layout: {
                        align: 'stretch',
                        pack: 'end',
                        type: 'hbox'
                    },
                    bodyPadding: 0,
                    items: [
                        {
                            xtype: 'genericcancelbutton'
                        },
                        {
                            xtype: 'genericbackbutton'
                        },
                        {
                            xtype: 'genericnextbutton'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    },

    onSliderChange: function(slider, newValue, thumb, eOpts) {

        console.log('onSliderChange.arguments:');
        console.log(arguments);

        var parentContainer = slider.up('container'),
            siblingDisplayField = parentContainer.down('displayfield');

        siblingDisplayField.setValue(newValue);
    }

});
