/*
 * File: app/view/GenericActionButton.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.button.GenericActionButton', {
    extend: 'Ext.button.Button',
    alias: 'widget.genericactionbutton',
    //exLoadInitialized: (function() { console.error('xtype: genericactionbutton / ' + (new Date()).valueOf());})(),
    height: 30,
    margin: '0 10 10 10',
    minWidth: 150,
    arrowAlign: 'bottom',
    iconAlign: 'bottom',
    text: 'Action',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
