/*
 * File: app/view/GenericToolbarLogoutButton.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('Common.view.button.GenericToolbarLogoutButton', {
    extend: 'Ext.button.Button',
    alias: 'widget.generictoolbarlogoutbutton',
    //exLoadInitialized: (function() { console.error('xtype: generictoolbarlogoutbutton / ' + (new Date()).valueOf());})(),
    /*height: 30,
    margin: '0 10 10 10',
    minWidth: 150,
    arrowAlign: 'bottom',
    iconAlign: 'bottom',*/
    ui: 'default-toolbar-small',
    text: 'Logout',
    margin: 0,
    height: 20,

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
