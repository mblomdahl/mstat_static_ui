/*
 * File: app/view/GenericBackButton.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.button.GenericBackButton', {
    extend: 'Ext.button.Button',
    alias: 'widget.genericbackbutton',
    //exLoadInitialized: (function() { console.error('xtype: genericbackbutton / ' + (new Date()).valueOf());})(),
    margin: 10,
    minWidth: 75,
    text: 'Back',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
