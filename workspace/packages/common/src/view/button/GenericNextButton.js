/*
 * File: app/view/GenericNextButton.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.button.GenericNextButton', {
    extend: 'Ext.button.Button',
    alias: 'widget.genericnextbutton',
    //exLoadInitialized: (function() { console.error('xtype: genericnextbutton / ' + (new Date()).valueOf());})(),
    margin: 10,
    minWidth: 125,
    text: 'Next',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
