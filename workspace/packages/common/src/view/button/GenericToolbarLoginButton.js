/*
 * File: app/view/GenericToolbarLoginButton.js
 * Author: Mats Blomdahl
 * Version: 1.03
 */

Ext.define('Common.view.button.GenericToolbarLoginButton', {
    extend: 'Ext.button.Button',
    alias: 'widget.generictoolbarloginbutton',
    //exLoadInitialized: (function() { console.error('xtype: generictoolbarloginbutton / ' + (new Date()).valueOf());})(),
    /*height: 30,
    margin: '0 10 10 10',
    minWidth: 150,
    arrowAlign: 'bottom',
    iconAlign: 'bottom',*/
    ui: 'default-toolbar-small',
    text: 'Login',
    margin: '0 9px 0 0',
    height: 20,
    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
