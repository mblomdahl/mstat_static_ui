/*
 * File: app/view/GenericCancelButton.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.button.GenericCancelButton', {
    extend: 'Ext.button.Button',
    alias: 'widget.genericcancelbutton',
    //exLoadInitialized: (function() { console.error('xtype: genericcancelbutton / ' + (new Date()).valueOf());})(),
    margin: 10,
    minWidth: 75,
    text: 'Cancel',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});
