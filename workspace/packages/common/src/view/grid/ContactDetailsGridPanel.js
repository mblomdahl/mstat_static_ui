/*
 * File: app/view/ContactDetailsGridPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.grid.ContactDetailsGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.contactdetailsgridpanel',
    //exLoadInitialized: (function() { console.error('xtype: contactdetailsgridpanel / ' + (new Date()).valueOf());})(),
    requires: [
        'Common.view.panel.GridPanelRefreshTool'
    ],

    title: 'ContactDetails',
    columnLines: true,
    store: 'ContactDetails',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'rownumberer'
                },
                {
                    xtype: 'gridcolumn',
                    width: 110,
                    dataIndex: 'datastore_key_id',
                    text: 'datastore_key_id'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 135,
                    dataIndex: 'datastore_key_urlsafe',
                    text: 'datastore_key_urlsafe'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'labels',
                    text: 'labels'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'personal_id',
                    text: 'personal_id'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'first_name',
                    text: 'first_name'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'last_name',
                    text: 'last_name'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'job_title',
                    text: 'job_title'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'job_organization_name',
                    text: 'job_organization_name'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'job_department_name',
                    text: 'job_department_name'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'job_organization_id',
                    text: 'job_organization_id'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'email_addresses',
                    text: 'email_addresses'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'phone_numbers',
                    text: 'phone_numbers'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'postal_addresses',
                    text: 'postal_addresses'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'web_addresses',
                    text: 'web_addresses'
                },
                {
                    xtype: 'datecolumn',
                    width: 125,
                    dataIndex: 'deleted_datetime',
                    text: 'deleted_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'booleancolumn',
                    dataIndex: 'deleted',
                    text: 'deleted'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_origin_user_account',
                    text: 'sys_origin_user_account'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_origin_organization_account',
                    text: 'sys_origin_organization_account'
                },
                {
                    xtype: 'numbercolumn',
                    hidden: true,
                    width: 140,
                    dataIndex: 'sys_changelog_version',
                    text: 'sys_changelog_version',
                    format: '000'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    dataIndex: 'sys_changelog',
                    text: 'sys_changelog'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_modified',
                    text: 'sys_modified',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_created',
                    text: 'sys_created',
                    format: 'Y-m-d H:i:s'
                }
            ],
            tools: [
                {
                    xtype: 'gridpanelrefreshtool'
                }
            ]
        });

        me.callParent(arguments);
    }

});
