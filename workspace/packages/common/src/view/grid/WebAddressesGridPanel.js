/*
 * File: app/view/WebAddressesGridPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.grid.WebAddressesGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.webaddressesgridpanel',

    requires: [
        'Common.view.panel.GridPanelRefreshTool'
    ],
    //exLoadInitialized: (function() { console.error('xtype: webaddressesgridpanel / ' + (new Date()).valueOf());})(),
    title: 'WebAddresses',
    columnLines: true,
    store: 'WebAddresses',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'rownumberer'
                },
                {
                    xtype: 'gridcolumn',
                    width: 110,
                    dataIndex: 'datastore_key_id',
                    text: 'datastore_key_id'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 135,
                    dataIndex: 'datastore_key_urlsafe',
                    text: 'datastore_key_urlsafe'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'labels',
                    text: 'labels'
                },
                {
                    xtype: 'gridcolumn',
                    width: 463,
                    dataIndex: 'url',
                    text: 'url'
                },
                {
                    xtype: 'datecolumn',
                    width: 125,
                    dataIndex: 'deleted_datetime',
                    text: 'deleted_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'booleancolumn',
                    dataIndex: 'deleted',
                    text: 'deleted'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_origin_user_account',
                    text: 'sys_origin_user_account'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_origin_organization_account',
                    text: 'sys_origin_organization_account'
                },
                {
                    xtype: 'numbercolumn',
                    hidden: true,
                    width: 140,
                    dataIndex: 'sys_changelog_version',
                    text: 'sys_changelog_version',
                    format: '000'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    dataIndex: 'sys_changelog',
                    text: 'sys_changelog'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_modified',
                    text: 'sys_modified',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_created',
                    text: 'sys_created',
                    format: 'Y-m-d H:i:s'
                }
            ],
            tools: [
                {
                    xtype: 'gridpanelrefreshtool'
                }
            ]
        });

        me.callParent(arguments);
    }

});
