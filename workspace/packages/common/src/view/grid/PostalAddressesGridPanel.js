/*
 * File: app/view/PostalAddressesGridPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.grid.PostalAddressesGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.postaladdressesgridpanel',
    //exLoadInitialized: (function() { console.error('xtype: postaladdressesgridpanel / ' + (new Date()).valueOf());})(),
    requires: [
        'Common.view.panel.GridPanelRefreshTool'
    ],

    title: 'PostalAddresses',
    columnLines: true,
    store: 'PostalAddresses',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'rownumberer'
                },
                {
                    xtype: 'gridcolumn',
                    width: 110,
                    dataIndex: 'datastore_key_id',
                    text: 'datastore_key_id'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 135,
                    dataIndex: 'datastore_key_urlsafe',
                    text: 'datastore_key_urlsafe'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'labels',
                    text: 'labels'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'recipient_lines',
                    text: 'recipient_lines'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'box_address',
                    text: 'box_address'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'route_name',
                    text: 'route_name'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'street_number',
                    text: 'street_number'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'street_entrance',
                    text: 'street_entrance'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'apartment_number',
                    text: 'apartment_number'
                },
                {
                    xtype: 'numbercolumn',
                    dataIndex: 'apartment_floor',
                    text: 'apartment_floor',
                    format: '000'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'postal_code',
                    text: 'postal_code'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'country',
                    text: 'postal_town'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'country',
                    text: 'country'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'formatted_address',
                    text: 'formatted_address'
                },
                {
                    xtype: 'datecolumn',
                    width: 125,
                    dataIndex: 'deleted_datetime',
                    text: 'deleted_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'booleancolumn',
                    dataIndex: 'deleted',
                    text: 'deleted'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_origin_user_account',
                    text: 'sys_origin_user_account'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_origin_organization_account',
                    text: 'sys_origin_organization_account'
                },
                {
                    xtype: 'numbercolumn',
                    hidden: true,
                    width: 140,
                    dataIndex: 'sys_changelog_version',
                    text: 'sys_changelog_version',
                    format: '000'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    dataIndex: 'sys_changelog',
                    text: 'sys_changelog'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_modified',
                    text: 'sys_modified',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_created',
                    text: 'sys_created',
                    format: 'Y-m-d H:i:s'
                }
            ],
            tools: [
                {
                    xtype: 'gridpanelrefreshtool'
                }
            ]
        });

        me.callParent(arguments);
    }

});
