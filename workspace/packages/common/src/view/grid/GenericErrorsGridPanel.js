/*
 * File: app/view/GenericErrorsGridPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.grid.GenericErrorsGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.genericerrorsgridpanel',
    //exLoadInitialized: (function() { console.error('xtype: genericerrorsgridpanel / ' + (new Date()).valueOf());})(),
    requires: [
        'Common.view.panel.GridPanelRefreshTool'
    ],

    title: 'GenericErrors',
    columnLines: true,
    store: 'GenericErrors',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'rownumberer'
                },
                {
                    xtype: 'gridcolumn',
                    width: 110,
                    dataIndex: 'datastore_key_id',
                    text: 'datastore_key_id'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 135,
                    dataIndex: 'datastore_key_urlsafe',
                    text: 'datastore_key_urlsafe'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'errors',
                    text: 'errors'
                },
                {
                    xtype: 'numbercolumn',
                    dataIndex: 'code',
                    text: 'code',
                    format: '000'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'message',
                    text: 'message'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_owners',
                    text: 'sys_owners'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_writers',
                    text: 'sys_writers'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_readers',
                    text: 'sys_readers'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_origin_user_account',
                    text: 'sys_origin_user_account'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'sys_origin_organization_account',
                    text: 'sys_origin_organization_account'
                },
                {
                    xtype: 'numbercolumn',
                    hidden: true,
                    width: 140,
                    dataIndex: 'sys_changelog_version',
                    text: 'sys_changelog_version',
                    format: '000'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    dataIndex: 'sys_changelog',
                    text: 'sys_changelog'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_modified',
                    text: 'sys_modified',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_created',
                    text: 'sys_created',
                    format: 'Y-m-d H:i:s'
                }
            ],
            tools: [
                {
                    xtype: 'gridpanelrefreshtool'
                }
            ]
        });

        me.callParent(arguments);
    }

});
