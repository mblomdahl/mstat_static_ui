/*
 * File: app/view/UserAccountsGridPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.grid.UserAccountsGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.useraccountsgridpanel',
    //exLoadInitialized: (function() { console.error('xtype: useraccountsgridpanel / ' + (new Date()).valueOf());})(),
    requires: [
        'Common.view.panel.GridPanelRefreshTool'
    ],

    title: 'UserAccounts',
    columnLines: true,
    store: 'UserAccounts',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'rownumberer'
                },
                {
                    xtype: 'gridcolumn',
                    width: 110,
                    dataIndex: 'datastore_key_id',
                    text: 'datastore_key_id'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 135,
                    dataIndex: 'datastore_key_urlsafe',
                    text: 'datastore_key_urlsafe'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 160,
                    dataIndex: 'datastore_ancestor_key_id',
                    text: 'datastore_ancestor_key_id'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 185,
                    dataIndex: 'datastore_ancestor_key_urlsafe',
                    text: 'datastore_ancestor_key_urlsafe'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'user_email',
                    text: 'user_email'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 60,
                    dataIndex: 'user_id',
                    text: 'user_id'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    dataIndex: 'google_user_id',
                    text: 'google_user_id'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 85,
                    dataIndex: 'google_user',
                    text: 'google_user'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 75,
                    dataIndex: 'password',
                    text: 'password'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'contact_details',
                    text: 'contact_details'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'api_access_key',
                    text: 'api_access_key'
                },
                {
                    xtype: 'datecolumn',
                    width: 125,
                    dataIndex: 'registered_datetime',
                    text: 'registered_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    width: 125,
                    dataIndex: 'activated_datetime',
                    text: 'activated_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'suspended_datetime',
                    text: 'suspended_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'deleted_datetime',
                    text: 'deleted_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 170,
                    dataIndex: 'password_changed_datetime',
                    text: 'password_changed_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'booleancolumn',
                    width: 80,
                    dataIndex: 'suspended',
                    text: 'suspended'
                },
                {
                    xtype: 'booleancolumn',
                    hidden: true,
                    width: 65,
                    dataIndex: 'deleted',
                    text: 'deleted'
                },
                {
                    xtype: 'booleancolumn',
                    hidden: true,
                    width: 70,
                    dataIndex: 'activated',
                    text: 'activated'
                },
                {
                    xtype: 'booleancolumn',
                    width: 95,
                    dataIndex: 'administrator',
                    text: 'administrator'
                },
                {
                    xtype: 'datecolumn',
                    width: 125,
                    dataIndex: 'last_visit_datetime',
                    text: 'last_visit_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 90,
                    dataIndex: 'last_client_ip',
                    text: 'last_client_ip'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 90,
                    dataIndex: 'sys_user_role',
                    text: 'sys_user_role'
                },
                {
                    xtype: 'numbercolumn',
                    hidden: true,
                    width: 95,
                    dataIndex: 'sys_session_id',
                    text: 'sys_session_id'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 210,
                    dataIndex: 'sys_pending_email_update_address',
                    text: 'sys_pending_email_update_address'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 185,
                    dataIndex: 'sys_pending_email_update_key',
                    text: 'sys_pending_email_update_key'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 150,
                    dataIndex: 'sys_origin_user_account',
                    text: 'sys_origin_user_account'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 190,
                    dataIndex: 'sys_origin_organization_account',
                    text: 'sys_origin_organization_account'
                },
                {
                    xtype: 'numbercolumn',
                    hidden: true,
                    width: 140,
                    dataIndex: 'sys_changelog_version',
                    text: 'sys_changelog_version',
                    format: '000'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    dataIndex: 'sys_changelog',
                    text: 'sys_changelog'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_modified',
                    text: 'sys_modified',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_created',
                    text: 'sys_created',
                    format: 'Y-m-d H:i:s'
                }
            ],
            tools: [
                {
                    xtype: 'gridpanelrefreshtool'
                }
            ]
        });

        me.callParent(arguments);
    }

});
