/*
 * File: app/view/SystemAdminAccountsGridPanel.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.grid.SystemAdminAccountsGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.systemadminaccountsgridpanel',
    //exLoadInitialized: (function() { console.error('xtype: systemadminaccountsgridpanel / ' + (new Date()).valueOf());})(),
    requires: [
        'Common.view.panel.GridPanelRefreshTool'
    ],

    title: 'SystemAdminAccounts',
    columnLines: true,
    store: 'SystemAdminAccounts',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'rownumberer'
                },
                {
                    xtype: 'gridcolumn',
                    width: 110,
                    dataIndex: 'datastore_key_id',
                    text: 'datastore_key_id'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    width: 135,
                    dataIndex: 'datastore_key_urlsafe',
                    text: 'datastore_key_urlsafe'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'organization_name',
                    text: 'company_name'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'department_name',
                    text: 'department_name'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'organization_id',
                    text: 'registration_number'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'contact_details',
                    text: 'contact_details'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'postal_addresses',
                    text: 'postal_addresses'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'activated_datetime',
                    text: 'activated_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    width: 125,
                    dataIndex: 'registered_datetime',
                    text: 'registered_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'suspended_datetime',
                    text: 'suspended_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'deleted_datetime',
                    text: 'deleted_datetime',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'booleancolumn',
                    width: 80,
                    dataIndex: 'suspended',
                    text: 'suspended'
                },
                {
                    xtype: 'booleancolumn',
                    hidden: true,
                    width: 65,
                    dataIndex: 'deleted',
                    text: 'deleted'
                },
                {
                    xtype: 'booleancolumn',
                    hidden: true,
                    width: 70,
                    dataIndex: 'activated',
                    text: 'activated'
                },
                {
                    xtype: 'gridcolumn',
                    width: 150,
                    dataIndex: 'sys_origin_user_account',
                    text: 'sys_origin_user_account'
                },
                {
                    xtype: 'gridcolumn',
                    width: 190,
                    dataIndex: 'sys_origin_organization_account',
                    text: 'sys_origin_organization_account'
                },
                {
                    xtype: 'numbercolumn',
                    hidden: true,
                    width: 140,
                    dataIndex: 'sys_changelog_version',
                    text: 'sys_changelog_version',
                    format: '000'
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    dataIndex: 'sys_changelog',
                    text: 'sys_changelog'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_modified',
                    text: 'sys_modified',
                    format: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'datecolumn',
                    hidden: true,
                    width: 125,
                    dataIndex: 'sys_created',
                    text: 'sys_created',
                    format: 'Y-m-d H:i:s'
                }
            ],
            tools: [
                {
                    xtype: 'gridpanelrefreshtool'
                }
            ]
        });

        me.callParent(arguments);
    }

});
