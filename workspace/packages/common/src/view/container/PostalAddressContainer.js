/*
 * File: app/view/PostalAddressContainer.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.container.PostalAddressContainer', {
    extend: 'Ext.container.Container',
    alias: 'widget.postaladdresscontainer',
    //exLoadInitialized: (function() { console.error('xtype: postaladdresscontainer / ' + (new Date()).valueOf());})(),
    requires: [
        'Common.view.form.field.GenericTextField',
        'Common.view.container.PostalAddressTypeContainer',
        'Common.view.form.field.PostalCodeTextField',
        'Common.view.form.field.PostalTownTextField',
        'Common.view.form.field.CountryTextField'
    ],

    margin: '0 0 0 0',
    layout: {
        type: 'anchor'
    },

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    layout: {
                        type: 'anchor'
                    },
                    items: [
                        {
                            xtype: 'generictextfield',
                            margin: '0 0 10 0',
                            fieldLabel: 'Address Line 1*',
                            name: 'recipient_lines_1',
                            allowBlank: false,
                            anchor: '100%'
                        },
                        {
                            xtype: 'generictextfield',
                            margin: '0 0 10 0',
                            fieldLabel: 'Address Line 2',
                            name: 'recipient_lines_2',
                            anchor: '100%'
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'anchor'
                    },
                    items: [
                        {
                            xtype: 'radiogroup',
                            margin: '0 0 10 0',
                            fieldLabel: 'Address Type',
                            labelWidth: 125,
                            items: [
                                {
                                    xtype: 'radiofield',
                                    name: 'address_type',
                                    boxLabel: 'Street Address',
                                    checked: true,
                                    inputValue: 'street_address'
                                },
                                {
                                    xtype: 'radiofield',
                                    name: 'address_type',
                                    boxLabel: 'Box Address',
                                    inputValue: 'box_address'
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'postaladdresstypecontainer'
                },
                {
                    xtype: 'container',
                    margin: '0 0 0 0',
                    layout: {
                        align: 'stretch',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'postalcodetextfield',
                            flex: 0.75,
                            margins: '0 5 10 0'
                        },
                        {
                            xtype: 'postaltowntextfield',
                            flex: 2.5,
                            margins: '0 5 10 5'
                        },
                        {
                            xtype: 'countrytextfield',
                            flex: 1,
                            margins: '0 0 10 5'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
