/*
 * File: app/view/PostalAddressTypeContainer.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.container.PostalAddressTypeContainer', {
    extend: 'Ext.container.Container',
    alias: 'widget.postaladdresstypecontainer',
    //exLoadInitialized: (function() { console.error('xtype: postaladdresstypecontainer / ' + (new Date()).valueOf());})(),
    requires: [
        'Common.view.form.field.RouteNameTextField',
        'Common.view.form.field.StreetNumberTextField',
        'Common.view.form.field.StreetEntranceTextField',
        'Common.view.form.field.ApartmentFloorCombobox',
        'Common.view.form.field.BoxAddressTextField'
    ],

    layout: {
        type: 'card'
    },

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    layout: {
                        align: 'stretch',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'routenametextfield',
                            flex: 2.5,
                            margins: '0 5 10 0'
                        },
                        {
                            xtype: 'streetnumbertextfield',
                            flex: 1.2,
                            margins: '0 5 10 5'
                        },
                        {
                            xtype: 'streetentrancetextfield',
                            flex: 1,
                            margins: '0 5 10 5'
                        },
                        {
                            xtype: 'apartmentfloorcombobox',
                            flex: 1,
                            margins: '0 0 10 5'
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'anchor'
                    },
                    items: [
                        {
                            xtype: 'boxaddresstextfield',
                            disabled: true,
                            hidden: true,
                            anchor: '65%'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
