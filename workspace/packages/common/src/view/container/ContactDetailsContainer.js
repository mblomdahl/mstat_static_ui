/*
 * File: app/view/ContactDetailsContainer.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.view.container.ContactDetailsContainer', {
    extend: 'Ext.container.Container',
    alias: 'widget.contactdetailscontainer',
    //exLoadInitialized: (function() { console.error('xtype: contactdetailscontainer / ' + (new Date()).valueOf());})(),
    requires: [
        'Common.view.form.field.PersonNameTextField',
        'Common.view.form.field.GenericTextField',
        'Common.view.form.field.EmailAddressTextField',
        'Common.view.form.field.PhoneNumberTextField'
    ],

    margin: 0,

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    layout: {
                        align: 'stretch',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'personnametextfield',
                            fieldLabel: 'First Name*',
                            labelAlign: 'top',
                            name: 'first_name',
                            flex: 2,
                            margins: '0 5 10 0'
                        },
                        {
                            xtype: 'personnametextfield',
                            fieldLabel: 'Last Name*',
                            labelAlign: 'top',
                            name: 'last_name',
                            flex: 2,
                            margins: '0 5 10 5'
                        },
                        {
                            xtype: 'generictextfield',
                            fieldLabel: 'Job Title',
                            labelAlign: 'top',
                            name: 'job_title',
                            flex: 1.5,
                            margins: '0 0 10 5'
                        }
                    ]
                },
                {
                    xtype: 'container',
                    margin: '0 0 0 0',
                    layout: {
                        align: 'stretch',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'emailaddresstextfield',
                            fieldLabel: 'Email Address*',
                            labelAlign: 'top',
                            name: 'email_addresses_work',
                            allowBlank: false,
                            flex: 3,
                            margins: '0 5 10 0'
                        },
                        {
                            xtype: 'phonenumbertextfield',
                            fieldLabel: 'Office Phone*',
                            labelAlign: 'top',
                            name: 'phone_numbers_office',
                            allowBlank: false,
                            flex: 1.5,
                            margins: '0 5 10 5'
                        },
                        {
                            xtype: 'phonenumbertextfield',
                            fieldLabel: 'Mobile Phone',
                            labelAlign: 'top',
                            name: 'phone_numbers_mobile',
                            flex: 1.5,
                            margins: '0 0 10 5'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
