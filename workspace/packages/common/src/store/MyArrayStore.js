/*
 * File: app/store/MyArrayStore.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.store.MyArrayStore', {
    extend: 'Ext.data.Store',
    //exLoadInitialized: (function() { console.error('storeId: myArrayStore / ' + (new Date()).valueOf());})(),
    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'MyArrayStore',
            data: [
                {
                    name: 'CapitexRawDataEntry',
                    value: 'CapitexRawDataEntry'
                },
                {
                    name: 'SfdRawDataEntry',
                    value: 'SfdRawDataEntry'
                },
                {
                    name: 'ScbRawDataEntry',
                    value: 'ScbRawDataEntry'
                },
                {
                    name: 'TransactionCompositeEntry',
                    value: 'TransactionCompositeEntry'
                },
                {
                    name: 'SfdRawDataStats',
                    value: 'SfdRawDataStats'
                },
                {
                    name: 'CapitexRawDataStats',
                    value: 'CapitexRawDataStats'
                },
                {
                    name: 'ScbRawDataStats',
                    value: 'ScbRawDataStats'
                },
                {
                    name: 'TransactionCompositeStats',
                    value: 'TransactionCompositeStats'
                }
            ],
            fields: [
                {
                    name: 'name'
                },
                {
                    name: 'value'
                }
            ]
        }, cfg)]);
    }
});
