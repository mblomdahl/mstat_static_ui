/*
 * File: app/store/EmptySet.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.store.EmptySet', {
    extend: 'Ext.data.Store',
    //exLoadInitialized: (function() { console.error('storeId: emptySet / ' + (new Date()).valueOf());})(),
    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'emptySet',
            data: [

            ],
            leadingBufferZone: 0,
            trailingBufferZone: 0,
            fields: [
                {
                    name: 'name',
                    type: 'string'
                },
                {
                    name: 'value',
                    type: 'string'
                }
            ]
        }, cfg)]);
    }
});
