/*
 * File: app/store/DatasetResources.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.store.DatasetResources', {
    extend: 'Ext.data.Store',
    //exLoadInitialized: (function() { console.error('storeId: datasetResources / ' + (new Date()).valueOf());})(),
    requires: [
        'Common.model.DatasetResource'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'Common.model.DatasetResource',
            storeId: 'datasetResources',
            buffered: true,
            proxy: {
                type: 'direct',
                directionParam: 'order_direction',
                extraParams: {
                    kind: 'DatasetResource'
                },
                filterParam: 'filter_by',
                groupParam: 'group_by',
                limitParam: 'page_size',
                sortParam: 'order_by',
                startParam: 'offset',
                directFn: Remote.UserCmd.grid_data,
                reader: {
                    type: 'json',
                    root: 'data',
                    record: 'datastore_key_id'
                },
                writer: {
                    type: 'json',
                    nameProperty: 'datastore_key_id',
                    allowSingle: false,
                    root: 'data'
                }
            }
        }, cfg)]);
    }
});
