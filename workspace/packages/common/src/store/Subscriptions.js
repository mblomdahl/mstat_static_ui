/*
 * File: app/store/Subscriptions.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.store.Subscriptions', {
    extend: 'Ext.data.Store',
    //exLoadInitialized: (function() { console.error('storeId: subscriptions / ' + (new Date()).valueOf());})(),
    requires: [
        'Common.model.Subscription'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'Common.model.Subscription',
            storeId: 'subscriptions',
            leadingBufferZone: 0,
            pageSize: 100,
            trailingBufferZone: 0,
            proxy: {
                type: 'direct',
                directionParam: 'order_direction',
                extraParams: {
                    kind: 'Subscription'
                },
                filterParam: 'filter_by',
                groupParam: 'group_by',
                limitParam: 'page_size',
                sortParam: 'order_by',
                startParam: 'offset',
                directFn: Remote.UserCmd.grid_data,
                reader: {
                    type: 'json',
                    idProperty: 'datastore_key_id',
                    root: 'data'
                },
                writer: {
                    type: 'json',
                    nameProperty: 'datastore_key_id',
                    allowSingle: false,
                    root: 'data'
                }
            }
        }, cfg)]);
    }
});
