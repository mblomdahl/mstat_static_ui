/*
 * File: app/store/GenericErrors.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.store.GenericErrors', {
    extend: 'Ext.data.Store',
    //exLoadInitialized: (function() { console.error('storeId: genericErrors / ' + (new Date()).valueOf());})(),
    requires: [
        'Common.model.GenericError'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'Common.model.GenericError',
            storeId: 'genericErrors',
            buffered: true,
            proxy: {
                type: 'direct',
                directionParam: 'order_direction',
                extraParams: {
                    kind: 'GenericError'
                },
                filterParam: 'filter_by',
                groupParam: 'group_by',
                limitParam: 'page_size',
                sortParam: 'order_by',
                startParam: 'offset',
                directFn: Remote.UserCmd.grid_data,
                reader: {
                    type: 'json',
                    root: 'data',
                    record: 'datastore_key_id'
                },
                writer: {
                    type: 'json',
                    nameProperty: 'datastore_key_id',
                    allowSingle: false,
                    root: 'data'
                }
            }
        }, cfg)]);
    }
});
