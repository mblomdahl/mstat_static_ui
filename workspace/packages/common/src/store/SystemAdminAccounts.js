/*
 * File: app/store/SystemAdminAccounts.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.store.SystemAdminAccounts', {
    extend: 'Ext.data.Store',
    //exLoadInitialized: (function() { console.error('storeId: systemAdminAccounts / ' + (new Date()).valueOf());})(),
    requires: [
        'Common.model.SubscriberAccount'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'Common.model.SubscriberAccount',
            storeId: 'systemAdminAccounts',
            buffered: true,
            proxy: {
                type: 'direct',
                directionParam: 'order_direction',
                extraParams: {
                    kind: 'SystemAdminAccount'
                },
                filterParam: 'filter_by',
                groupParam: 'group_by',
                limitParam: 'page_size',
                sortParam: 'order_by',
                startParam: 'offset',
                directFn: Remote.UserCmd.grid_data,
                reader: {
                    type: 'json',
                    idProperty: 'datastore_key_id',
                    root: 'data'
                },
                writer: {
                    type: 'json',
                    nameProperty: 'datastore_key_id',
                    allowSingle: false,
                    root: 'data'
                }
            }
        }, cfg)]);
    }
});
