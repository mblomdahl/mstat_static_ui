/*
 * File: app/store/TableResources.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.store.TableResources', {
    extend: 'Ext.data.Store',
    //exLoadInitialized: (function() { console.error('storeId: tableResources / ' + (new Date()).valueOf());})(),
    requires: [
        'Common.model.TableResource'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'Common.model.TableResource',
            storeId: 'tableResources',
            buffered: true,
            proxy: {
                type: 'direct',
                directionParam: 'order_direction',
                extraParams: {
                    kind: 'TableResource'
                },
                filterParam: 'filter_by',
                groupParam: 'group_by',
                limitParam: 'page_size',
                sortParam: 'order_by',
                startParam: 'offset',
                directFn: Remote.UserCmd.grid_data,
                reader: {
                    type: 'json',
                    root: 'data',
                    record: 'datastore_key_id'
                },
                writer: {
                    type: 'json',
                    nameProperty: 'datastore_key_id',
                    allowSingle: false,
                    root: 'data'
                }
            }
        }, cfg)]);
    }
});
