/*
 * File: app/controller/AddSubscriptionCtrl.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.controller.AddSubscriptionCtrl', {
    extend: 'Ext.app.Controller',
    alias: 'controller.addsubscriptionctrl',
    //exLoadInitialized: (function() { console.error('xtype: addsubscriptionctrl / ' + (new Date()).valueOf());})(),
    stores: [
        'Common.store.SubscriberAccounts',
        'Common.store.UserAccounts',
        'Common.store.Subscriptions'
    ],
    views: [
        'Common.view.window.AddSubscriptionWindow'
    ],

    refs: [
        {
            ref: 'addSubscriptionWindow',
            selector: '#addSubscriptionWindow',
            xtype: 'addsubscriptionwindow'
        },
        {
            ref: 'addSubscriptionStepContainer',
            selector: '#addSubscriptionStepContainer',
            xtype: 'container'
        },
        {
            ref: 'addSubscriptionStep1Container',
            selector: '#addSubscriptionStep1Container',
            xtype: 'container'
        },
        {
            ref: 'addSubscriptionStep2Container',
            selector: '#addSubscriptionStep2Container',
            xtype: 'container'
        },
        {
            ref: 'addSubscriptionStep3Container',
            selector: '#addSubscriptionStep3Container',
            xtype: 'container'
        },
        {
            ref: 'addSubscriptionStep4Container',
            selector: '#addSubscriptionStep4Container',
            xtype: 'container'
        },
        {
            ref: 'addSubscriptionStep1FormPanel',
            selector: '#addSubscriptionStep1FormPanel',
            xtype: 'form'
        },
        {
            ref: 'addSubscriptionStep2FormPanel',
            selector: '#addSubscriptionStep2FormPanel',
            xtype: 'form'
        },
        {
            ref: 'addSubscriptionStep3FormPanel',
            selector: '#addSubscriptionStep3FormPanel',
            xtype: 'form'
        },
        {
            ref: 'addSubscriptionStep1AncestorCombobox',
            selector: 'addsubscriptionwindow subscriberaccountcombobox',
            xtype: 'subscriberaccountcombobox'
        },
        {
            ref: 'addSubscriptionConfirmParentSubscriberFormPanel',
            selector: '#addSubscriptionConfirmParentSubscriberFormPanel',
            xtype: 'form'
        },
        {
            ref: 'addSubscriptionConfirmSubscriptionDateIntervalDisplayField',
            selector: '#addSubscriptionConfirmSubscriptionDateIntervalDisplayField',
            xtype: 'form'
        },
        {
            ref: 'addSubscriptionConfirmSubscriptionTypeDisplayField',
            selector: '#addSubscriptionConfirmSubscriptionTypeDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addSubscriptionConfirmSubscriptionDateIntervalDisplayField',
            selector: '#addSubscriptionConfirmSubscriptionDateIntervalDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addSubscriptionConfirmDateIntervalDisplayField',
            selector: '#addSubscriptionConfirmDateIntervalDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addSubscriptionConfirmQuotaDisplayField',
            selector: '#addSubscriptionConfirmQuotaDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addSubscriptionConfirmUpdateFrequencyDisplayField',
            selector: '#addSubscriptionConfirmUpdateFrequencyDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addSubscriptionConfirmFieldInclusionDisplayField',
            selector: '#addSubscriptionConfirmFieldInclusionDisplayField',
            xtype: 'displayfield'
        },
        {
            ref: 'addSubscriptionCancelButton',
            selector: 'addsubscriptionwindow genericcancelbutton',
            xtype: 'genericcancelbutton'
        },
        {
            ref: 'addSubscriptionBackButton',
            selector: 'addsubscriptionwindow genericbackbutton',
            xtype: 'genericbackbutton'
        },
        {
            ref: 'addSubscriptionNextButton',
            selector: 'addsubscriptionwindow genericnextbutton',
            xtype: 'genericnextbutton'
        }
    ],

    init: function(application) {
        this.debug = false || application.debug;

        this.control({
            '#addSubscriptionWindow': {
                hide: this.onAddSubscriptionWindowHide,
                show: this.onAddSubscriptionWindowShow
            },
            '#addSubscriptionStep1Container': {
                beforeactivate: this.onAddSubscriptionStep1ContainerBeforeActivate
            },
            '#addSubscriptionStep2Container': {
                beforeactivate: this.onAddSubscriptionStep2ContainerBeforeActivate
            },
            '#addSubscriptionStep3Container': {
                beforeactivate: this.onAddSubscriptionStep3ContainerBeforeActivate
            },
            '#addSubscriptionStep4Container': {
                beforeactivate: this.onAddSubscriptionStep4ContainerBeforeActivate
            },
            '#addSubscriptionStep1FormPanel': {
                validitychange: this.validateStep1
            },
            '#addSubscriptionStep2FormPanel': {
                render: this.onAddSubscriptionStep23FormPanelRender
            },
            '#addSubscriptionStep3FormPanel': {
                render: this.onAddSubscriptionStep23FormPanelRender
            },
            '#addSubscriptionStep2FormPanel fieldinclusioncheckbox': {
                change: this.validateStep2
            },
            '#addSubscriptionStep3FormPanel fieldinclusioncheckbox': {
                change: this.validateStep3
            },
            '#addSubscriptionStep2FieldCheckerContainer button': {
                click: this.onAddSubscriptionStep23FieldCheckerButtonClick
            },
            '#addSubscriptionStep3FieldCheckerContainer button': {
                click: this.onAddSubscriptionStep23FieldCheckerButtonClick
            },
            'addsubscriptionwindow genericcancelbutton': {
                click: this.onAddSubscriptionCancelButtonClick
            },
            'addsubscriptionwindow genericbackbutton': {
                click: this.onAddSubscriptionBackButtonClick
            },
            'addsubscriptionwindow genericnextbutton': {
                click: this.onAddSubscriptionNextButtonClick
            }
        });

        this.resetFields();
        this.steps = ['step_1', 'step_2', 'step_3', 'step_4'];
        this.activeStep = this.steps[0];

        this.disabledFields = ['locality', 'sublocality', 'location_wgs84_geopt', 'route_name', 'street_number', 'street_entrance', 'postal_code', 'postal_town', 'build_year_lower', 'build_year_upper', 'operating_costs', 'sys_changelog'];

        this.standardFields = ['transaction_id', 'contract_price', 'contract_date', 'date_of_possession', 'data_transfer_date', 'mstat_restricted', 'real_estate_designation', 'real_estate_taxation_code', 'municipality_name', 'congregation_lkf', 'monthly_fee', 'location_rt90_geopt', 'apartment_number', 'formatted_address', 'housing_cooperative_registration_number', 'housing_cooperative_name', 'housing_cooperative_share', 'heating_included', 'type_of_housing', 'housing_category', 'housing_tenure', 'apartment_floor', 'building_storeys', 'living_area', 'plot_area', 'number_of_rooms', 'elevator', 'balcony', 'new_production', 'build_year', 'baseline_year_of_assessment', 'baseline_rateable_value', 'sys_invalidation_codes', 'sys_changelog_version', 'sys_modified'];

        //console.log('initz: done');

    },

    showWindow: function(parentSubscriber) {

        this.resetFields();

        this.getAddSubscriptionWindowView().create();

        if (parentSubscriber) {
            console.log('parentSubscriber:');
            console.log(parentSubscriber);
            this.addSubscriptionStep1Fields = {
                ancestor: parentSubscriber,
                subscription_start_date: new Date(),
                start_date: new Date()
            };
            //this.getAddUserFormPanelAncestorCombobox().setValue(parentSubscriber);
        } else {
            console.log('parentSubscriber:');
            console.log(parentSubscriber);
        }

        this.setActiveStep(0);

        this.getAddSubscriptionWindow().show();

    },

    setActiveStep: function(stepIndex) {

        var stepContainerLayout = this.getAddSubscriptionStepContainer().getLayout();

        console.log('setActiveStep('+stepIndex+')');
        console.log(stepContainerLayout);

        if (this.activeStep == this.steps[stepIndex]) {
            this.getAddSubscriptionStep1Container().fireEvent('beforeactivate');
        } else {
            this.activeStep = this.steps[stepIndex];
            stepContainerLayout.setActiveItem(stepIndex);
        }


    },

    resetFields: function() {
        //page 1
        this.addSubscriptionStep1Fields = null;

        //page 2
        this.addSubscriptionStep2Fields = null;

        //page 3
        this.addSubscriptionStep3Fields = null;

        if (this.debug) {
            console.log('this.resetFields called');
        }
    },

    setStandardFields: function(container) {

        var fieldInclusionCheckboxes = container.query('fieldinclusioncheckbox');

        for (var i = fieldInclusionCheckboxes.length; i--; ) {
            if (!fieldInclusionCheckboxes[i].isDisabled()) {
                if (this.standardFields.indexOf(fieldInclusionCheckboxes[i].inputValue) !== -1) {
                    fieldInclusionCheckboxes[i].setValue(true);
                    console.log('checked box for field ' + fieldInclusionCheckboxes[i].inputValue);

                } else {
                    fieldInclusionCheckboxes[i].setValue(false);
                    console.log('unchecked box for field ' + fieldInclusionCheckboxes[i].inputValue);
                }
            } else {
                console.log('ignored further evaluation of box for field ' + fieldInclusionCheckboxes[i].inputValue + ' (disabled)');
            }
        }

    },

    validateStep1: function() {

        var addSubscriptionAncestorCombobox = this.getAddSubscriptionStep1AncestorCombobox(),
            addSubscriptionFormPanel = this.getAddSubscriptionStep1FormPanel(),
            targetAddSubscriptionFormPanelFieldTypes = [
            'radiogroup',
            'genericdatefield',
            'slider',
            'displayfield'
            ].join(', '),
            targetAddSubscriptionFormPanelFields = addSubscriptionFormPanel.query(targetAddSubscriptionFormPanelFieldTypes),
            targetFieldsEnabled = false,
            validateFormPanel = false;


        if (addSubscriptionAncestorCombobox.isValid()) {
            console.log('addSubscriptionAncestorCombobox.isValid() = true');
            validateFormPanel = true;
            targetFieldsEnabled = true;
        } else {
            console.log('addSubscriptionAncestorCombobox.isValid() = false');
        }

        for (var i = targetAddSubscriptionFormPanelFields.length; i--; ) {
            targetAddSubscriptionFormPanelFields[i].setDisabled(!targetFieldsEnabled);
        }

        if (validateFormPanel) {
            console.log('fooz (validating page 1)');

            if (!addSubscriptionFormPanel.getForm().hasInvalidField()) {
                this.addSubscriptionStep1Fields = addSubscriptionFormPanel.getForm().getValues();

                if (!window.mstatDebug) {
                    window.mstatDebug = {};
                }

                window.mstatDebug.addSubscriptionStep1Fields = this.addSubscriptionStep1Fields;


                this.getAddSubscriptionNextButton().setDisabled(false);

            } else {
                this.getAddSubscriptionNextButton().setDisabled(true);
            }

        } else {
            this.getAddSubscriptionNextButton().setDisabled(true);
        }

    },

    validateStep2: function() {
        var addSubscriptionFormPanel = this.getAddSubscriptionStep2FormPanel(),
            targetAddSubscriptionFormPanelFields = addSubscriptionFormPanel.query('checkbox'),
            hasChecked = false;

        console.log('targetAddSubscriptionFormPanelFields: ');
        console.log(targetAddSubscriptionFormPanelFields);

        for (var i = targetAddSubscriptionFormPanelFields.length; i--; ) {
            if (targetAddSubscriptionFormPanelFields[i].getValue()) {
                hasChecked = true;
                console.log('checked field found');
                break;
            }
        }

        console.log('fooz (validating page 2)');
        if (hasChecked) {
            this.addSubscriptionStep2Fields = addSubscriptionFormPanel.getForm().getValues();

            if (typeof(this.addSubscriptionStep2Fields.field_inclusion) == 'string') {
                this.addSubscriptionStep2Fields.field_inclusion = [this.addSubscriptionStep2Fields.field_inclusion];
            }

            if (!window.mstatDebug) {
                window.mstatDebug = {};
            }

            window.mstatDebug.addSubscriptionStep2Fields = this.addSubscriptionStep2Fields;
        }

        this.getAddSubscriptionNextButton().setDisabled(!hasChecked);
        console.log('next btn enabled: ' + hasChecked);
    },

    validateStep3: function() {
        var addSubscriptionFormPanel = this.getAddSubscriptionStep3FormPanel();

        console.log('fooz (validating page 3)');

        this.addSubscriptionStep3Fields = addSubscriptionFormPanel.getForm().getValues();

        if (typeof(this.addSubscriptionStep3Fields.field_inclusion) == 'string') {
            this.addSubscriptionStep3Fields.field_inclusion = [this.addSubscriptionStep3Fields.field_inclusion];
        }

        if (!window.mstatDebug) {

            window.mstatDebug = {};

        }


        window.mstatDebug.addSubscriptionStep3Fields = this.addSubscriptionStep3Fields;

    },

    executeAddSubscription: function(button) {

        var addSubscriptionWindow = button.up('window'),
            loadMask = addSubscriptionWindow.setLoading('Adding Subscription ...');

        var step1Fields = this.addSubscriptionStep1Fields,
            subscriptionStartDate = step1Fields.subscription_start_date,
            subscriptionEndDate = step1Fields.subscription_end_date,
            dataStartDate = step1Fields.start_date,
            dataEndDate = step1Fields.start_date,
            fieldInclusion = this.addSubscriptionStep2Fields.field_inclusion;

        if (!subscriptionEndDate) {
            subscriptionEndDate = undefined;
        }

        if (!dataEndDate) {
            dataEndDate = undefined;
        }

        if (this.addSubscriptionStep3Fields.field_inclusion) {
            for (var i = this.addSubscriptionStep3Fields.field_inclusion.length; i--; ) {
                fieldInclusion.push(this.addSubscriptionStep3Fields.field_inclusion[i]);
            }
        }

        Remote.AdminCmd.add_subscription({
            ancestor: step1Fields.ancestor,
            quota: step1Fields.quota,
            subscription_type: step1Fields.subscription_type,
            start_date: subscriptionStartDate,
            end_date: subscriptionEndDate,
            update_frequency: step1Fields.update_frequency,
            content_restrictions: {
                datastore_kind: 'TransactionCompositeEntry',
                fields: fieldInclusion,
                start_date: dataStartDate,
                end_date: dataEndDate
            }
        }, function(result, response, success) {
            console.log('add callback!\nresult:');
            console.log(result);
            console.log('response:');
            console.log(response);
            console.log('success:');
            console.log(success);
            console.log('(debug) arguments:');
            console.log(arguments);
            addSubscriptionWindow.setLoading(false);

            if (success) {
                button.up('window').close();
            }
        });

        return true;
    },

    onAddSubscriptionWindowShow: function(window) {

        return true;
    },

    onAddSubscriptionWindowHide: function(window) {

        //this.resetFields();


        //this.onSetActivePage(0);
    },

    onAddSubscriptionStep1ContainerBeforeActivate: function(container) {
        var addSubscriptionForm = this.getAddSubscriptionStep1FormPanel().getForm();

        if (this.addSubscriptionStep1Fields) {
            addSubscriptionForm.reset();
            addSubscriptionForm.setValues(this.addSubscriptionStep1Fields);
        } else {
            addSubscriptionForm.reset();
        }

        this.getAddSubscriptionBackButton().setDisabled(true);
        this.getAddSubscriptionNextButton().setText("Next");

        this.validateStep1();

    },

    onAddSubscriptionStep2ContainerBeforeActivate: function(container) {
        var addSubscriptionForm = container.down('form').getForm();

        if (!this.addSubscriptionStep2Fields) {
            this.setStandardFields(container);
        }

        this.getAddSubscriptionBackButton().setDisabled(false);


        this.getAddSubscriptionNextButton().setText("Next");
        this.validateStep2();

        /*

        var fieldInclusionCheckboxes = container.query('fieldinclusioncheckbox');

        for (var i = fieldInclusionCheckboxes.length; i--; ) {
        if (!fieldInclusionCheckboxes[i].isDisabled()) {
        if (this.addSubscriptionStep2Fields.field_inclusion.indexOf(fieldInclusionCheckboxes[i].inputValue) !== -1) {
        fieldInclusionCheckboxes[i].setValue(true);
        } else {
        fieldInclusionCheckboxes[i].setValue(false);
        }
        }
        }

        */
    },

    onAddSubscriptionStep3ContainerBeforeActivate: function(container) {

        var addSubscriptionForm = container.down('form').getForm();

        if (!this.addSubscriptionStep3Fields) {
            this.setStandardFields(container);
        }

        this.getAddSubscriptionBackButton().setDisabled(false);


        this.getAddSubscriptionNextButton().setText("Next");
        this.validateStep3();

    },

    onAddSubscriptionStep4ContainerBeforeActivate: function(container) {

        var step1Fields = this.addSubscriptionStep1Fields,
            ancestor = step1Fields.ancestor;

        var confirmParentSubscriberForm = this.getAddSubscriptionConfirmParentSubscriberFormPanel().getForm(),
            subscriberAccountsStore = this.getSubscriberAccountsStore();


        console.log('ancestor:');
        console.log(ancestor);

        var ancestorRecord = subscriberAccountsStore.getById(ancestor);

        confirmParentSubscriberForm.setValues(ancestorRecord.getData(false));

        // set subscription type and date interval
        if (step1Fields.subscription_type === 'data_api_subscription') {
            this.getAddSubscriptionConfirmSubscriptionTypeDisplayField().setValue('Mäklarstatistik Data API');
        } else {
            this.getAddSubscriptionConfirmSubscriptionTypeDisplayField().setValue('Mäklarstatistik Widget Service');
        }

        var subscriptionDateInterval = step1Fields.subscription_start_date;
        if (step1Fields.subscription_end_date) {
            subscriptionDateInterval += ' – ' + step1Fields.subscription_end_date;
        } else {
            subscriptionDateInterval += ' – <i>indefinite</i>';
        }

        this.getAddSubscriptionConfirmSubscriptionDateIntervalDisplayField().setValue(subscriptionDateInterval);


        var dataDateInterval = step1Fields.start_date;
        if (step1Fields.end_date) {
            dataDateInterval += ' – ' + step1Fields.end_date;
        } else {
            dataDateInterval += ' – <i>indefinite</i>';
        }

        this.getAddSubscriptionConfirmDateIntervalDisplayField().setValue(dataDateInterval);

        var quota = step1Fields.quota + ' full dataset downloads per update';
        this.getAddSubscriptionConfirmQuotaDisplayField().setValue(quota);

        var updateFrequency = step1Fields.update_frequency;
        switch (updateFrequency) {
            case 'subscriber_monthly_update_frequency':
            updateFrequency = 'Monthly';
            break;
            case 'subscriber_weekly_update_frequency':
            updateFrequency = 'Weekly';
            break;
            case 'subscriber_daily_update_frequency':
            updateFrequency = 'Daily';
            break;
        }

        this.getAddSubscriptionConfirmUpdateFrequencyDisplayField().setValue(updateFrequency);

        var fieldInclusion = this.addSubscriptionStep2Fields.field_inclusion;
        if (this.addSubscriptionStep3Fields.field_inclusion) {
            for (var i = this.addSubscriptionStep3Fields.field_inclusion.length; i--; ) {
                fieldInclusion.push(this.addSubscriptionStep3Fields.field_inclusion[i]);
            }
        }

        this.getAddSubscriptionConfirmFieldInclusionDisplayField().setValue(fieldInclusion.join(', '));

        this.getAddSubscriptionBackButton().setDisabled(false);
        this.getAddSubscriptionNextButton().setText("Add Subscription");

    },

    onAddSubscriptionStep1FormPanelRender: function(formPanel) {

    },

    onAddSubscriptionStep23FormPanelRender: function(formPanel) {
        console.log('onAddSub...PanelRender called');

        var formPanelCheckboxes = formPanel.query('fieldinclusioncheckbox');
        for (var j = formPanelCheckboxes.length; j--; ) {
            if (this.disabledFields.indexOf(formPanelCheckboxes[j].inputValue) !== -1) {
                formPanelCheckboxes[j].setDisabled(true);
                console.log('disabled checkbox field ' + formPanelCheckboxes[j].inputValue);
            }
        }

        console.log('onAddSub...PanelRender finishing');

    },

    onAddSubscriptionStep23FieldCheckerButtonClick: function(button) {

        var parentFormPanel = button.up('form');

        if (button.getText() == 'Standard') {
            this.setStandardFields(parentFormPanel);
        } else {

            if (button.getText() == 'None') {
                parentFormPanel.getForm().reset();
            } else { // text = 'All'

                var fieldInclusionCheckboxes = parentFormPanel.query('fieldinclusioncheckbox');

                for (var i = fieldInclusionCheckboxes.length; i--; ) {
                    if (!fieldInclusionCheckboxes[i].isDisabled()) {
                        fieldInclusionCheckboxes[i].setValue(true);
                    }
                }
            }
        }
    },

    onAddSubscriptionCancelButtonClick: function(button) {

        var addSubscriberWindow = button.up('window');

        addSubscriberWindow.close();

    },

    onAddSubscriptionBackButtonClick: function(button) {

        if (this.activeStep == this.steps[1]) {
            this.validateStep1();
            this.setActiveStep(0);
        } else {
            if (this.activeStep == this.steps[2]) {
                this.setActiveStep(1);
            } else {
                if (this.activeStep == this.steps[3]) {
                    this.setActiveStep(2);
                }
            }
        }

    },

    onAddSubscriptionNextButtonClick: function(button) {

        if (this.activeStep == this.steps[0]) {
            console.log('setting page 2');
            this.validateStep1();
            this.setActiveStep(1);
        } else {
            if (this.activeStep == this.steps[1]) {
                console.log('setting page 3');
                this.validateStep2();
                this.setActiveStep(2);
            } else {
                if (this.activeStep == this.steps[2]) {
                    console.log('setting page 4');
                    this.validateStep3();
                    this.setActiveStep(3);
                } else {
                    console.log('executingadds');
                    this.executeAddSubscription(button);
                }
            }
        }

    }

});
