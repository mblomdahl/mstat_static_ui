/*
 * File: app/controller/PostalAddressCtrl.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.controller.PostalAddressCtrl', {
    extend: 'Ext.app.Controller',
    alias: 'controller.postaladdressctrl',
    //exLoadInitialized: (function() { console.error('xtype: postaladdressctrl / ' + (new Date()).valueOf());})(),
    views: [
        'Common.view.container.PostalAddressContainer'
    ],

    refs: [
        {
            ref: 'postalAddressRecipientLines1TextField',
            selector: '[name="recipient_lines_1"]',
            xtype: 'generictextfield'
        },
        {
            ref: 'postalAddressRecipientLines2TextField',
            selector: '[name="recipient_lines_2"]',
            xtype: 'generictextfield'
        },
        {
            ref: 'postalAddressTypeRadioGroup',
            selector: 'radiogroup',
            xtype: 'radiogroup'
        },
        {
            ref: 'postalAddressTypeContainer',
            selector: 'postaladdresstypecontainer',
            xtype: 'postaladdresstypecontainer'
        },
        {
            ref: 'postalAddressRouteNameTextField',
            selector: 'routenametextfield',
            xtype: 'routenametextfield'
        },
        {
            ref: 'postalAddressStreetNumberTextField',
            selector: 'streetnumbertextfield',
            xtype: 'streetnumbertextfield'
        },
        {
            ref: 'postalAddressStreetEntranceTextField',
            selector: 'streetentrancetextfield',
            xtype: 'streetentrancetextfield'
        },
        {
            ref: 'postalAddressApartmentFloorCombobox',
            selector: 'apartmentfloorcombobox',
            xtype: 'apartmentfloorcombobox'
        },
        {
            ref: 'postalAddressBoxAddressTextField',
            selector: 'boxaddresstextfield',
            xtype: 'boxaddresstextfield'
        },
        {
            ref: 'postalAddressPostalCodeTextField',
            selector: 'postalcodetextfield',
            xtype: 'postalcodetextfield'
        },
        {
            ref: 'postalAddressPostalTownTextField',
            selector: 'postaltowntextfield',
            xtype: 'postaltowntextfield'
        },
        {
            ref: 'postalAddressCountryTextField',
            selector: 'countrytextfield',
            xtype: 'countrytextfield'
        }
    ],

    init: function(application) {
        this.debug = false || application.debug;

        this.control({
            'radiogroup': {
                change: this.onPostalAddressTypeRadioGroupChange
            },
            'postaladdresstypecontainer': {
                beforerender: this.onPostalAddressTypeBeforeRender
            },
            'postaladdresstypecontainer container': {
                beforeactivate: this.onPostalAddressTypeBeforeActivate,
                beforedeactivate: this.onPostalAddressTypeBeforeDeactivate
            }
        });

    },

    formatPostalAddress: function(formFields) {
        var formattedAddress = [];

        if (formFields.xtype) {
            if (formFields.xtype !== 'form' && !formFields.up('form')) {
                console.log("formFields.xtype !== 'form' && !formFields.up('form')");
                auxFormFields = {
                    recipient_line_1: formFields.down('generictextfield[name="recipient_lines_1"]'),
                    recipient_line_2: formFields.down('generictextfield[name="recipient_lines_2"]'),
                    box_address: formFields.down('boxaddresstextfield'),
                    route_name: formFields.down('routenametextfield'),
                    street_number: formFields.down('streetnumbertextfield'),
                    street_entrance: formFields.down('streetentrancetextfield'),
                    apartment_floor: formFields.down('apartmentfloorcombobox'),
                    postal_code: formFields.down('postalcodetextfield'),
                    postal_town: formFields.down('postaltowntextfield'),
                    country: formFields.down('countrytextfield')
                };

                console.log('auxFormFields:');
                console.log(auxFormFields);


                var formFieldKeys = ['recipient_lines_1', 'recipient_lines_2', 'box_address', 'route_name', 'street_number', 'street_entrance', 'apartment_floor', 'postal_code', 'postal_town', 'country'];

                for (var i = formFieldKeys.length; i--; ) {
                    if (auxFormFields[formFieldKeys[i]]) {
                        auxFormFields[formFieldKeys[i]] = auxFormFields[formFieldKeys[i]].getValue();
                    }
                }
            } else { // xtype is (or can become) "form"
                if (formFields.xtype !== 'form') {
                    console.log("formFields.xtype !== 'form'");

                    formFields = formFields.up('form').getForm().getValues();
                } else {

                    console.log("formFields.xtype == 'form'");
                    formFields = formfields.getForm().getValues();
                }
            }
        }

        console.log('formFields:');
        console.log(formFields);

        formattedAddress.push(formFields.recipient_lines_1);

        formFields.recipient_lines_2 ? formattedAddress.push(formFields.recipient_lines_2) : null;

        if (formFields.box_address) {
            formattedAddress.push('Box '+formFields.box_address);
        } else {
            var streetAddress = [];

            formFields.route_name && formFields.street_number ? streetAddress.push([
            formFields.route_name,
            formFields.street_number,
            formFields.street_entrance
            ].join(' ').trim()) : null;

            formFields.apartment_floor ? streetAddress.push(formFields.apartment_floor) : null;

            formattedAddress.push(streetAddress.join(', '));
        }

        formFields.postal_code.length == 5 && formFields.postal_town ? formattedAddress.push(
        formFields.postal_code.substr(0,3) + ' ' + formFields.postal_code.substr(3) + '&nbsp;&nbsp;'
        + formFields.postal_town) : null;

        return formattedAddress.join('<br>\n');
    },

    onSetActiveAddressType: function(addressContainer, addressType) {

        var activeItem = 0;

        console.log(addressContainer);
        console.log(addressType);

        if (addressType == "box_address") {
            activeItem = 1;
        }

        addressContainer.getLayout().setActiveItem(activeItem);

    },

    onPostalAddressTypeRadioGroupChange: function(radiobox, newValue, oldValue) {

        console.log('radiobox-change');

        var topParentContainer = radiobox.up('postaladdresscontainer'),
            targetAddressContainer = topParentContainer.down('postaladdresstypecontainer'),
            parentFormPanel = radiobox.up('form');

        console.log(topParentContainer);
        console.log(targetAddressContainer);
        console.log(newValue);

        this.onSetActiveAddressType(targetAddressContainer, newValue['address_type']);

        if (parentFormPanel) {
            parentFormPanel.fireEvent('validitychange', {
                valid: false
            });
        }
    },

    onPostalAddressTypeBeforeRender: function(outerAddressContainer) {
        // evaluates which card should be displayed

        console.log('outerAddressContainer-beforeRender');
    },

    onPostalAddressTypeBeforeActivate: function(addressContainer) {

        console.log('addressContainer-beforeActivate');

        var routeNameTextField = addressContainer.down('routenametextfield');

        if (routeNameTextField) { // street address activated
            console.log('routeNameTextField found');

            var streetNumberTextField = addressContainer.down('streetnumbertextfield');

            console.log(routeNameTextField);
            console.log(streetNumberTextField);
            routeNameTextField.setDisabled(false);
            streetNumberTextField.setDisabled(false);

            routeNameTextField.setVisible(true);
            streetNumberTextField.setVisible(true);

        } else { // box address activated
            console.log('boxAddressTextField found');

            var boxAddressTextField = addressContainer.down('boxaddresstextfield');

            console.log(boxAddressTextField);

            boxAddressTextField.setDisabled(false);
            boxAddressTextField.setVisible(true);
        }


    },

    onPostalAddressTypeBeforeDeactivate: function(addressContainer) {

        console.log('addressContainer-beforeDeactivate');

        var routeNameTextField = addressContainer.down('routenametextfield');

        if (routeNameTextField) { // street address deactivated
            console.log('routeNameTextField found');
            var streetNumberTextField = addressContainer.down('streetnumbertextfield');

            routeNameTextField.setDisabled(true);
            routeNameTextField.setVisible(false);

            streetNumberTextField.setDisabled(true);
            streetNumberTextField.setVisible(false);

            console.log(routeNameTextField);
            console.log(streetNumberTextField);

        } else { // box address deactivated
            console.log('boxAddressTextField found');

            var boxAddressTextField = addressContainer.down('boxaddresstextfield');

            boxAddressTextField.setDisabled(true);
            boxAddressTextField.setVisible(false);

            console.log(boxAddressTextField);
        }

    }

});
