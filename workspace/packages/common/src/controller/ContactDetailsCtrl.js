/*
 * File: app/controller/ContactDetailsCtrl.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.controller.ContactDetailsCtrl', {
    extend: 'Ext.app.Controller',
    alias: 'controller.contactdetailsctrl',
    //exLoadInitialized: (function() { console.error('xtype: contactdetailsctrl / ' + (new Date()).valueOf());})(),
    views: [
        'Common.view.container.ContactDetailsContainer'
    ],

    refs: [
        {
            ref: 'contactDetailsFirstNameTextField',
            selector: 'personnametextfield[name="first_name"]',
            xtype: 'personnametextfield'
        },
        {
            ref: 'contactDetailsLastNameTextField',
            selector: 'personnametextfield[name="last_name"]',
            xtype: 'personnametextfield'
        },
        {
            ref: 'contactDetailsJobTitleTextField',
            selector: 'generictextfield[name="job_title"]',
            xtype: 'generictextfield'
        },
        {
            ref: 'contactDetailsWorkEmailAddressTextField',
            selector: 'emailaddresstextfield',
            xtype: 'emailaddresstextfield'
        },
        {
            ref: 'contactDetailsMobilePhoneNumberTextField',
            selector: 'phonenumbertextfield[name="phone_numbers_mobile"]',
            xtype: 'phonenumbertextfield'
        },
        {
            ref: 'contactDetailsOfficePhoneNumberTextField',
            selector: 'phonenumbertextfield[name="phone_numbers_office"]',
            xtype: 'phonenumbertextfield'
        }
    ],

    init: function(application) {
        /*
        this.control({
        'radiogroup': {
        change: this.onPostalAddressTypeRadioGroupChange
        },
        'postaladdresstypecontainer': {
        beforerender: this.onPostalAddressTypeBeforeRender
        },
        'postaladdresstypecontainer container': {
        beforeactivate: this.onPostalAddressTypeBeforeActivate,
        beforedeactivate: this.onPostalAddressTypeBeforeDeactivate
        }
        });
        */
    },

    formatContactDetails: function(formFields) {
        var formattedDetails = [];

        if (formFields.xtype) {
            if (formFields.xtype !== 'form' && !formFields.up('form')) {
                console.log("formFields.xtype !== 'form' && !formFields.up('form')");
                auxFormFields = {
                    first_name: formFields.down('personnametextfield[name="first_name"]'),
                    last_name: formFields.down('personnametextfield[name="last_name"]'),
                    job_title: formFields.down('generictextfield[name="job_title"]'),
                    email_addresses_work: formFields.down('emailaddresstextfield'),
                    phone_numbers_office: formFields.down('phonenumbertextfield[name="phone_numbers_office"]'),
                    phone_numbers_mobile: formFields.down('phonenumbertextfield[name="phone_numbers_mobile"]')
                };

                console.log('auxFormFields:');
                console.log(auxFormFields);


                var formFieldKeys = [
                'first_name',
                'last_name',
                'job_title',
                'email_addresses_work',
                'phone_numbers_office',
                'phone_numbers_mobile'
                ];

                for (var i = formFieldKeys.length; i--; ) {
                    if (auxFormFields[formFieldKeys[i]]) {
                        auxFormFields[formFieldKeys[i]] = auxFormFields[formFieldKeys[i]].getValue();
                    }
                }
            } else { // xtype is (or can become) "form"
                if (formFields.xtype !== 'form') {
                    console.log("formFields.xtype !== 'form'");

                    formFields = formFields.up('form').getForm().getValues();
                } else {

                    console.log("formFields.xtype == 'form'");
                    formFields = formfields.getForm().getValues();
                }
            }
        }

        console.log('formFields:');
        console.log(formFields);

        var name = formFields.first_name + ' ' + formFields.last_name,
            nameLine = formFields.job_title ? name + ', ' + formFields.job_title : name;

        formattedDetails.push(nameLine);

        formattedDetails.push('Email Address: ' + formFields.email_addresses_work + ' (work)');

        var phoneNumber = 'Phone Number: ' + formFields.phone_numbers_office + ' (office)',
            phoneNumbersLine = formFields.phone_numbers_mobile ?
            phoneNumber + ', ' + formFields.phone_numbers_mobile + ' (mobile)' : phoneNumber;

        formattedDetails.push(phoneNumbersLine);

        return formattedDetails.join('<br>\n');
    }

});
