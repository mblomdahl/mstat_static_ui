/*
 * File: app/model/JobQueryStatistics.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.JobQueryStatistics', {
    extend: 'Ext.data.Model',
    //exLoadInitialized: (function() { console.error('model: JobQueryStatistics / ' + (new Date()).valueOf());})(),
    uses: [
        'Common.model.JobStatistics'
    ],

    fields: [
        {
            name: 'total_bytes_processed',
            type: 'int',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'Common.model.JobStatistics',
        foreignKey: 'query'
    }
});
