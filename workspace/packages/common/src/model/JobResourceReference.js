/*
 * File: app/model/JobResourceReference.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.JobResourceReference', {
    extend: 'Ext.data.Model',
    //exLoadInitialized: (function() { console.error('model: JobResourceReference / ' + (new Date()).valueOf());})(),
    uses: [
        'Common.model.JobResource'
    ],

    idProperty: 'job_id',

    fields: [
        {
            name: 'project_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'job_id',
            type: 'string',
            useNull: false
        }
    ],

    belongsTo: {
        model: 'Common.model.JobResource',
        primaryKey: 'datastore_key_id',
        foreignKey: 'job_reference'
    }
});
