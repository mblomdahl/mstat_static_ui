/*
 * File: app/model/JobLoadStatistics.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.JobLoadStatistics', {
    extend: 'Ext.data.Model',
    //exLoadInitialized: (function() { console.error('model: JobLoadStatistics / ' + (new Date()).valueOf());})(),
    uses: [
        'Common.model.JobStatistics'
    ],

    fields: [
        {
            name: 'input_files',
            type: 'int',
            useNull: true
        },
        {
            name: 'input_file_bytes',
            type: 'int',
            useNull: true
        },
        {
            name: 'output_rows',
            type: 'int',
            useNull: true
        },
        {
            name: 'output_bytes',
            type: 'int',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'Common.model.JobStatistics',
        foreignKey: 'load'
    }
});
