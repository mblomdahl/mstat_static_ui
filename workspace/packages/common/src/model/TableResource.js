/*
 * File: app/model/TableResource.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.TableResource', {
    extend: 'Ext.data.Model',
    //exLoadInitialized: (function() { console.error('model: TableResource / ' + (new Date()).valueOf());})(),
    uses: [
        'Common.model.TableResourceReference',
        'Common.model.TableSchema',
        'Common.model.SubscriberAccount'
    ],

    idProperty: 'datastore_key_id',

    fields: [
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'kind',
            type: 'string',
            useNull: true
        },
        {
            name: 'etag',
            type: 'string',
            useNull: true
        },
        {
            name: 'id',
            type: 'string',
            useNull: true
        },
        {
            name: 'self_link',
            type: 'string',
            useNull: true
        },
        {
            name: 'schema',
            type: 'auto',
            useNull: true
        },
        {
            name: 'friendly_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'description',
            type: 'string',
            useNull: true
        },
        {
            name: 'num_rows',
            type: 'int',
            useNull: true
        },
        {
            name: 'num_bytes',
            type: 'int',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'last_modified_time',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'creation_time',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'expiration_time',
            type: 'date',
            useNull: true
        },
        {
            name: 'sys_owners',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_writers',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_readers',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_user_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_organization_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_changelog_version',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_changelog',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ],

    hasOne: [
        {
            associationKey: 'table_reference',
            model: 'Common.model.TableResourceReference',
            primaryKey: 'table_id'
        },
        {
            associationKey: 'schema',
            model: 'Common.model.TableSchema'
        }
    ],

    hasMany: [
        {
            associationKey: 'sys_owners',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_writers',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_readers',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        }
    ]
});
