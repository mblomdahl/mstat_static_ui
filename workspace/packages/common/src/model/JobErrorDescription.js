/*
 * File: app/model/JobErrorDescription.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.JobErrorDescription', {
    extend: 'Ext.data.Model',
    //exLoadInitialized: (function() { console.error('model: JobErrorDescription / ' + (new Date()).valueOf());})(),
    uses: [
        'Common.model.JobStatus'
    ],

    fields: [
        {
            name: 'reason',
            type: 'string',
            useNull: true
        },
        {
            name: 'location',
            type: 'string',
            useNull: true
        },
        {
            name: 'debug_info',
            type: 'string',
            useNull: true
        },
        {
            name: 'message',
            type: 'string',
            useNull: true
        }
    ],

    belongsTo: [
        {
            model: 'Common.model.JobStatus',
            foreignKey: 'error_result'
        },
        {
            associationKey: '',
            model: 'Common.model.JobStatus',
            foreignKey: 'errors'
        }
    ]
});
