/*
 * File: app/model/UserAccount.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.UserAccount', {
    extend: 'Ext.data.Model',
    //exLoadInitialized: (function() { console.error('model: UserAccount / ' + (new Date()).valueOf());})(),
    uses: [
        'Common.model.SubscriberAccount',
        'Common.model.ContactDetails'
    ],

    idProperty: 'datastore_key_id',

    fields: [
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_ancestor_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_ancestor_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'user_email',
            type: 'string',
            useNull: true
        },
        {
            name: 'user_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'google_user_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'google_user',
            type: 'auto',
            useNull: true
        },
        {
            name: 'password',
            type: 'string',
            useNull: true
        },
        {
            name: 'contact_details',
            type: 'auto',
            useNull: true
        },
        {
            name: 'api_access_key',
            type: 'string',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'registered_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'activated_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'suspended_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'deleted_datetime',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'password_changed_datetime',
            type: 'date',
            useNull: true
        },
        {
            name: 'suspended',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'deleted',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'activated',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'administrator',
            type: 'boolean',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'last_visit_datetime',
            type: 'date',
            useNull: true
        },
        {
            name: 'last_client_ip',
            type: 'string',
            useNull: true
        },
        {
            name: 'sys_user_role',
            type: 'string',
            useNull: true
        },
        {
            name: 'sys_session_id',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_pending_email_update_address',
            type: 'string',
            useNull: true
        },
        {
            name: 'sys_pending_email_update_key',
            type: 'string',
            useNull: true
        },
        {
            name: 'sys_origin_user_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_organization_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_changelog_version',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_changelog',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ],

    hasOne: [
        {
            associationKey: 'sys_origin_organization_account',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_origin_user_account',
            model: 'Common.model.UserAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'contact_details',
            model: 'Common.model.ContactDetails',
            primaryKey: 'datastore_key_id'
        }
    ]
});
