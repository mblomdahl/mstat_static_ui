/*
 * File: app/model/JobConfiguration.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.JobConfiguration', {
    extend: 'Ext.data.Model',
    //exLoadInitialized: (function() { console.error('model: JobConfiguration / ' + (new Date()).valueOf());})(),
    uses: [
        'Common.model.JobResource',
        'Common.model.JobLoadConfiguration',
        'Common.model.JobExtractConfiguration',
        'Common.model.JobCopyConfiguration',
        'Common.model.JobQueryConfiguration'
    ],

    fields: [
        {
            name: 'load',
            type: 'auto',
            useNull: true
        },
        {
            name: 'extract',
            type: 'auto',
            useNull: true
        },
        {
            name: 'copy',
            type: 'auto',
            useNull: true
        },
        {
            name: 'query',
            type: 'auto',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'Common.model.JobResource',
        primaryKey: 'datastore_key_id',
        foreignKey: 'configuration'
    },

    hasOne: [
        {
            associationKey: 'load',
            model: 'Common.model.JobLoadConfiguration'
        },
        {
            associationKey: 'extract',
            model: 'Common.model.JobExtractConfiguration',
            foreignKey: ''
        },
        {
            associationKey: 'copy',
            model: 'Common.model.JobCopyConfiguration'
        },
        {
            associationKey: 'query',
            model: 'Common.model.JobQueryConfiguration'
        }
    ]
});
