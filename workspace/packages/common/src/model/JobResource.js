/*
 * File: app/model/JobResource.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.JobResource', {
    extend: 'Ext.data.Model',
    //exLoadInitialized: (function() { console.error('model: JobResource / ' + (new Date()).valueOf());})(),
    uses: [
        'Common.model.JobResourceReference',
        'Common.model.JobConfiguration',
        'Common.model.JobStatus',
        'Common.model.JobStatistics',
        'Common.model.SubscriberAccount'
    ],

    idProperty: 'datastore_key_id',

    fields: [
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'kind',
            type: 'string',
            useNull: true
        },
        {
            name: 'etag',
            type: 'string',
            useNull: true
        },
        {
            name: 'id',
            type: 'string',
            useNull: true
        },
        {
            name: 'self_link',
            type: 'string',
            useNull: true
        },
        {
            name: 'job_reference',
            type: 'auto',
            useNull: true
        },
        {
            name: 'configuration',
            type: 'auto',
            useNull: true
        },
        {
            name: 'status',
            type: 'auto',
            useNull: true
        },
        {
            name: 'statistics',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_owners',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_writers',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_readers',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_user_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_organization_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_changelog_version',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_changelog',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ],

    hasOne: [
        {
            associationKey: 'job_reference',
            model: 'Common.model.JobResourceReference',
            primaryKey: 'job_id'
        },
        {
            associationKey: 'configuration',
            model: 'Common.model.JobConfiguration'
        },
        {
            associationKey: 'status',
            model: 'Common.model.JobStatus'
        },
        {
            associationKey: 'statistics',
            model: 'Common.model.JobStatistics'
        }
    ],

    hasMany: [
        {
            associationKey: 'sys_owners',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_writers',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_readers',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        }
    ]
});
