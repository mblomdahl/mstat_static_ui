/*
 * File: app/model/JobCopyConfiguration.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.JobCopyConfiguration', {
    extend: 'Ext.data.Model',
    //exLoadInitialized: (function() { console.error('model: JobCopyConfiguration / ' + (new Date()).valueOf());})(),
    uses: [
        'Common.model.JobConfiguration'
    ],

    fields: [
        {
            name: 'source_table',
            type: 'auto',
            useNull: true
        },
        {
            name: 'destination_table',
            type: 'auto',
            useNull: true
        },
        {
            name: 'create_disposition',
            type: 'string',
            useNull: true
        },
        {
            name: 'write_disposition',
            type: 'string',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'Common.model.JobConfiguration',
        foreignKey: 'copy'
    }
});
