/*
 * File: app/model/DatasetResourceReference.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.DatasetResourceReference', {
    extend: 'Ext.data.Model',
    //exLoadInitialized: (function() { console.error('model: DatasetResourceReference / ' + (new Date()).valueOf());})(),
    uses: [
        'Common.model.DatasetResource'
    ],

    idProperty: 'dataset_id',

    fields: [
        {
            name: 'project_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'dataset_id',
            type: 'string',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'Common.model.DatasetResource',
        primaryKey: 'datastore_key_id',
        foreignKey: 'dataset_reference'
    }
});
