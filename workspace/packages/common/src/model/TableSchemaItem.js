/*
 * File: app/model/TableSchemaItem.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.TableSchemaItem', {
    extend: 'Ext.data.Model',
    //exLoadInitialized: (function() { console.error('model: TableSchemaItem / ' + (new Date()).valueOf());})(),
    uses: [
        'Common.model.TableSchema'
    ],

    fields: [
        {
            name: 'name',
            type: 'string',
            useNull: true
        },
        {
            name: 'type',
            type: 'string',
            useNull: true
        },
        {
            name: 'mode',
            type: 'string',
            useNull: true
        },
        {
            name: 'fields',
            type: 'auto',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'Common.model.TableSchema',
        foreignKey: 'fields'
    }
});
