/*
 * File: app/model/JobStatus.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.JobStatus', {
    extend: 'Ext.data.Model',
    //exLoadInitialized: (function() { console.error('model: JobStatus / ' + (new Date()).valueOf());})(),
    uses: [
        'Common.model.JobResource',
        'Common.model.JobErrorDescription'
    ],

    fields: [
        {
            name: 'state',
            type: 'string',
            useNull: true
        },
        {
            name: 'error_result',
            type: 'auto',
            useNull: true
        },
        {
            name: 'errors',
            type: 'auto',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'Common.model.JobResource',
        primaryKey: 'datastore_key_id',
        foreignKey: 'status'
    },

    hasOne: {
        associationKey: 'error_result',
        model: 'Common.model.JobErrorDescription'
    },

    hasMany: {
        associationKey: 'errors',
        model: 'Common.model.JobErrorDescription'
    }
});
