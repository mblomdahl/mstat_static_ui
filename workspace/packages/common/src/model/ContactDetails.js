/*
 * File: app/model/ContactDetails.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.ContactDetails', {
    extend: 'Ext.data.Model',
    //exLoadInitialized: (function() { console.error('model: ContactDetails / ' + (new Date()).valueOf());})(),
    uses: [
        'Common.model.PostalAddress',
        'Common.model.WebAddress',
        'Common.model.UserAccount',
        'Common.model.SubscriberAccount'
    ],

    idProperty: 'datastore_key_id',

    fields: [
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'labels',
            type: 'auto',
            useNull: true
        },
        {
            name: 'personal_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'first_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'last_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'job_title',
            type: 'string',
            useNull: true
        },
        {
            name: 'job_organization_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'job_department_name',
            type: 'string',
            useNull: true
        },
        {
            name: 'job_organization_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'email_addresses',
            type: 'auto',
            useNull: true
        },
        {
            name: 'phone_numbers',
            type: 'auto',
            useNull: true
        },
        {
            name: 'postal_addresses',
            type: 'auto',
            useNull: true
        },
        {
            name: 'web_addresses',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'deleted_datetime',
            type: 'date',
            useNull: true
        },
        {
            name: 'deleted',
            type: 'boolean',
            useNull: true
        },
        {
            name: 'sys_origin_user_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_organization_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_changelog_version',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_changelog',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ],

    hasMany: [
        {
            associationKey: 'postal_addresses',
            model: 'Common.model.PostalAddress',
            primaryKey: 'datastore_key_id',
            autoLoad: true
        },
        {
            associationKey: 'web_addresses',
            model: 'Common.model.WebAddress',
            primaryKey: 'datastore_key_id',
            autoLoad: true
        }
    ],

    belongsTo: [
        {
            associationKey: 'contact_details',
            model: 'Common.model.UserAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'contacts',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        }
    ],

    hasOne: [
        {
            associationKey: 'sys_origin_user_account',
            model: 'Common.model.UserAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_origin_organization_account',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        }
    ]
});
