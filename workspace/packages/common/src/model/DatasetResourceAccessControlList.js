/*
 * File: app/model/DatasetResourceAccessControlList.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.DatasetResourceAccessControlList', {
    extend: 'Ext.data.Model',
    //exLoadInitialized: (function() { console.error('model: DatasetResourceAccessControlList / ' + (new Date()).valueOf());})(),
    uses: [
        'Common.model.DatasetResource',
        'Common.model.DatasetResourceAccessControlListItem'
    ],

    fields: [
        {
            name: 'access',
            type: 'string',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'Common.model.DatasetResource',
        primaryKey: 'datastore_key_id',
        foreignKey: 'access'
    },

    hasMany: {
        associationKey: 'access',
        model: 'Common.model.DatasetResourceAccessControlListItem'
    }
});
