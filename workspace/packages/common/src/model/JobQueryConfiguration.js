/*
 * File: app/model/JobQueryConfiguration.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.JobQueryConfiguration', {
    extend: 'Ext.data.Model',
    //exLoadInitialized: (function() { console.error('model: JobQueryConfiguration / ' + (new Date()).valueOf());})(),
    uses: [
        'Common.model.JobConfiguration'
    ],

    fields: [
        {
            name: 'query',
            type: 'string',
            useNull: true
        },
        {
            name: 'destination_table',
            type: 'auto',
            useNull: true
        },
        {
            name: 'create_disposition',
            type: 'string',
            useNull: true
        },
        {
            name: 'write_disposition',
            type: 'string',
            useNull: true
        },
        {
            name: 'default_dataset',
            type: 'auto',
            useNull: true
        },
        {
            name: 'priority',
            type: 'string',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'Common.model.JobConfiguration',
        foreignKey: 'query'
    }
});
