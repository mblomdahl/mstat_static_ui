/*
 * File: app/model/GenericErrorDescription.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.GenericErrorDescription', {
    extend: 'Ext.data.Model',
    //exLoadInitialized: (function() { console.error('model: GenericErrorDescription / ' + (new Date()).valueOf());})(),
    uses: [
        'Common.model.GenericError'
    ],

    idProperty: 'datastore_key_id',

    fields: [
        {
            name: 'domain',
            type: 'string',
            useNull: true
        },
        {
            name: 'reason',
            type: 'string',
            useNull: true
        },
        {
            name: 'message',
            type: 'string',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'Common.model.GenericError',
        primaryKey: 'datastore_key_id',
        foreignKey: 'errors'
    }
});
