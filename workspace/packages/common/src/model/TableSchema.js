/*
 * File: app/model/TableSchema.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.TableSchema', {
    extend: 'Ext.data.Model',
    //exLoadInitialized: (function() { console.error('model: TableSchema / ' + (new Date()).valueOf());})(),
    uses: [
        'Common.model.TableResource',
        'Common.model.TableSchemaItem'
    ],

    fields: [
        {
            name: 'fields',
            type: 'auto',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'Common.model.TableResource',
        primaryKey: 'datastore_key_id',
        foreignKey: 'schema'
    },

    hasMany: {
        associationKey: 'fields',
        model: 'Common.model.TableSchemaItem'
    }
});
