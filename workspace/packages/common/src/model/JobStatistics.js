/*
 * File: app/model/JobStatistics.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.JobStatistics', {
    extend: 'Ext.data.Model',
    //exLoadInitialized: (function() { console.error('model: JobStatistics / ' + (new Date()).valueOf());})(),
    uses: [
        'Common.model.JobResource',
        'Common.model.JobLoadStatistics',
        'Common.model.JobQueryStatistics'
    ],

    fields: [
        {
            name: 'query',
            type: 'auto',
            useNull: true
        },
        {
            name: 'load',
            type: 'auto',
            useNull: true
        },
        {
            name: 'total_bytes_processed',
            type: 'int',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'start_time',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'end_time',
            type: 'date',
            useNull: true
        }
    ],

    belongsTo: {
        model: 'Common.model.JobResource',
        primaryKey: 'datastore_key_id',
        foreignKey: 'statistics'
    },

    hasOne: [
        {
            associationKey: 'load',
            model: 'Common.model.JobLoadStatistics'
        },
        {
            associationKey: 'query',
            model: 'Common.model.JobQueryStatistics'
        }
    ]
});
