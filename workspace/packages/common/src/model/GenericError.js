/*
 * File: app/model/GenericError.js
 * Author: Mats Blomdahl
 * Version: 1.0
 */

Ext.define('Common.model.GenericError', {
    extend: 'Ext.data.Model',
    //exLoadInitialized: (function() { console.error('model: GenericError / ' + (new Date()).valueOf());})(),
    uses: [
        'Common.model.SubscriberAccount',
        'Common.model.GenericErrorDescription'
    ],

    idProperty: 'datastore_key_id',

    fields: [
        {
            name: 'datastore_key_id',
            type: 'string',
            useNull: true
        },
        {
            name: 'datastore_key_urlsafe',
            type: 'string',
            useNull: true
        },
        {
            name: 'errors',
            type: 'auto',
            useNull: true
        },
        {
            name: 'code',
            type: 'int',
            useNull: true
        },
        {
            name: 'message',
            type: 'string',
            useNull: true
        },
        {
            name: 'sys_owners',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_writers',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_readers',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_user_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_origin_organization_account',
            type: 'auto',
            useNull: true
        },
        {
            name: 'sys_changelog_version',
            type: 'int',
            useNull: true
        },
        {
            name: 'sys_changelog',
            type: 'auto',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_modified',
            type: 'date',
            useNull: true
        },
        {
            dateFormat: 'Y-m-dTH:i:s',
            name: 'sys_created',
            type: 'date',
            useNull: true
        }
    ],

    hasMany: [
        {
            associationKey: 'sys_owners',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_writers',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'sys_readers',
            model: 'Common.model.SubscriberAccount',
            primaryKey: 'datastore_key_id'
        },
        {
            associationKey: 'errors',
            model: 'Common.model.GenericErrorDescription',
            primaryKey: 'datastore_key_id'
        }
    ]
});
